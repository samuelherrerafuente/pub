<?php

declare(strict_types=1);

use Slim\Factory\AppFactory;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\ErrorHandler;
use Slim\Routing\RouteCollectorProxy;
use Psr\Http\Server\RequestHandlerInterface;

require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/src/configurations/app-config.php';

\PropelHandler::init();

$app = AppFactory::create();
$app->setBasePath($_ENV["BASE_PATH"]);

// This middleware will append the response header Access-Control-Allow-Methods with all allowed methods
$app->add(function (ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
  $response = $handler->handle($request);
  $response = $response->withHeader('Access-Control-Allow-Credentials', 'true');
  return $response;
});

$app->addRoutingMiddleware();

$app->group(
  '/v1',
  function (RouteCollectorProxy $group) {
    $group->group('/api', \ApiRouteHandler::class . ':routes');
  }
);

$app->get('[/{path:.*}]', function (ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface {
  $path = $args['path'];
  if (empty($path)) {
    $newResponse = $response->withHeader('Content-Type', 'text/html; charset=UTF-8');
    $newResponse->getBody()->write(file_get_contents(__DIR__ . '/public/index.html'));
    return $response;
  }
  $filePath = __DIR__ . '/public/' . $path;
  if (!file_exists($filePath)) {
    $newResponse = $response->withHeader('Content-Type', 'text/html; charset=UTF-8');
    $newResponse->getBody()->write(file_get_contents(__DIR__ . '/public/index.html'));
    return $response;
  }
  switch (pathinfo($filePath, PATHINFO_EXTENSION)) {
    case 'svg':
      $mimeType = 'image/svg+xml';
      break;
    case 'css':
      $mimeType = 'text/css';
      break;
    case 'js':
      $mimeType = 'application/javascript';
      break;
    default:
      $mimeType = 'text/html';
  }
  $newResponse = $response->withHeader('Content-Type', $mimeType . '; charset=UTF-8');
  $newResponse->getBody()->write(file_get_contents($filePath));
  return $newResponse;
});

$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorHandler = $errorMiddleware->getDefaultErrorHandler();
assert($errorHandler === NULL || $errorHandler instanceof ErrorHandler);
$errorHandler->registerErrorRenderer('text/html', \MyCustomErrorRenderer::class);
// $errorHandler->registerErrorRenderer('application/json', \MyCustomErrorRenderer::class);
// $errorHandler->registerErrorRenderer('text/plain', \MyCustomErrorRenderer::class);

$app->run();
