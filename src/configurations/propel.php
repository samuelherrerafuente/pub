<?php

use Propel\Runtime\Propel;
use Propel\Runtime\ServiceContainer;

class PropelHandler
{
    static function init()
    {
        $serviceContainer = Propel::getServiceContainer();
        $serviceContainer->checkVersion('2.0.0-dev');
        $serviceContainer->setAdapterClass('default', 'mysql');
        $manager = new \Propel\Runtime\Connection\ConnectionManagerSingle();
        $manager->setConfiguration(array(
            'dsn' => $_ENV["CFG"]["database"]["dsn"],
            'user' => $_ENV["CFG"]["database"]["user"],
            'password' => $_ENV["CFG"]["database"]["password"],
            'settings' => array('charset' => 'utf8', 'queries' => array()),
            'classname' => '\\Propel\\Runtime\\Connection\\ConnectionWrapper',
            'model_paths' =>
            array(
                0 => 'src',
                1 => 'vendor',
            ),
        ));
        $manager->setName('default');
        $serviceContainer->setConnectionManager('default', $manager);
        $serviceContainer->setDefaultDatasource('default');
    }
}
