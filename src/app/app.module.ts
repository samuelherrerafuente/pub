import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DpDatePickerModule } from 'ng2-date-picker';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { RecaptchaModule, RecaptchaV3Module, RecaptchaSettings, RECAPTCHA_SETTINGS, RECAPTCHA_V3_SITE_KEY } from "ng-recaptcha";
import { MenuComponent } from './components/menu/menu.component';
import { ConfiguracionComponent } from './components/configuracion/configuracion.component';
import { PasswordComponent } from './components/password/password.component';
import { ProgramasComponent } from './components/programas/programas.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';
import { RolesComponent } from './components/roles/roles.component';
import { ModalComponent } from './common/modal/modal.component';
import { HeaderComponent } from './common/header/header.component';
import { ToolbarComponent } from './common/toolbar/toolbar.component';
import { UsuarioModalComponent } from './components/usuario-modal/usuario-modal.component';
import { ConfirmModalComponent } from './common/confirm-modal/confirm-modal.component';
import { TableComponent } from './common/table/table.component';
import { FiltersModalComponent } from './components/filters-modal/filters-modal.component';
import { TabsComponent } from './common/tabs/tabs.component';
import { TabComponent } from './common/tabs/tab.component';
import { RolModalComponent } from './components/rol-modal/rol-modal.component';
import { ProgramaModalComponent } from './components/programa-modal/programa-modal.component';
import { ProgramaConfigModalComponent } from './components/programa-config-modal/programa-config-modal.component';
import { ResetComponent } from './components/reset/reset.component';
import { GrantDirective } from './directives/grant.directive';
import { LogoutComponent } from './components/logout/logout.component';
import { ProgramaRequisitoModalComponent } from './components/programa-requisito-modal/programa-requisito-modal.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    ConfiguracionComponent,
    PasswordComponent,
    ProgramasComponent,
    SolicitudesComponent,
    UsuariosComponent,
    RolesComponent,
    ModalComponent,
    HeaderComponent,
    ToolbarComponent,
    UsuarioModalComponent,
    ConfirmModalComponent,
    TableComponent,
    FiltersModalComponent,
    TabsComponent,
    TabComponent,
    RolModalComponent,
    ProgramaModalComponent,
    ProgramaConfigModalComponent,
    ResetComponent,
    GrantDirective,
    LogoutComponent,
    ProgramaRequisitoModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    RecaptchaModule,
    RecaptchaV3Module,
    CommonModule,
    DpDatePickerModule,
    ToastrModule.forRoot({
      timeOut: 30000,
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
      progressBar: true,
      enableHtml: true
    }),
    NgxDatatableModule.forRoot({
      messages: {
        emptyMessage: 'Registros no encontrados', // Message to show when array is presented, but contains no values
        totalMessage: 'total', // Footer total message
        selectedMessage: 'seleccionado' // Footer selected message
      }
    })
  ],
  providers: [{
    provide: RECAPTCHA_SETTINGS,
    useValue: {
      siteKey: "6Lf-MqMaAAAAAB9omomlMjhrLepO5YftWj5zsmZ2",
      size: 'invisible'
    } as RecaptchaSettings
  }, {
    provide: RECAPTCHA_V3_SITE_KEY,
    useValue: "6Lf-MqMaAAAAAB9omomlMjhrLepO5YftWj5zsmZ2"
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
