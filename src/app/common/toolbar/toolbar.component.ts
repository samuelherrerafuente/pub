import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  @Input()
  buttons: any[] = [];
  @Input()
  enableSearch: boolean = false;
  @Input()
  enableSearchFilters: boolean = false;
  @Input()
  searchFiltersCount: number = 0;
  @Input()
  tabs: any[] = [];
  @Input()
  busquedaLabel: String = "Búsqueda";
  
  @Output() action = new EventEmitter<string>();
  @Output() search = new EventEmitter<string>();
  searchFormControl = new FormControl();
  constructor() { }

  ngOnInit(): void {
    this.searchFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.search.emit(term);
      });
  }

  trigger(action: string) {
    this.action.emit(action);
  }

}


