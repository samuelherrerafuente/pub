import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent implements OnInit {
  modalData!: any;
  constructor(public modalService: ModalService) { }

  private ans = false;

  ngOnInit(): void {
  }

  onClose() {
    return this.ans;
  }

  yes() {
    this.ans = true;
    this.modalService.close(this);
  }

}
