import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';
import { ColumnMode, SelectionType } from '@swimlane/ngx-datatable';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { Page } from './Page';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input()
  rows: any[] = [];
  @Input()
  columns: any[] = [];
  @Input()
  loading = false;
  @Input()
  totalRows: number = 0;
  @Output() paging = new EventEmitter<Page>();
  @Output() selected = new EventEmitter<any>();
  @Output() event = new EventEmitter<any>();
  @ViewChild('activoCell', { static: true, read: TemplateRef }) activoCell!: TemplateRef<any>;
  @ViewChild('selectorCell', { static: true, read: TemplateRef }) selectorCell!: TemplateRef<any>;


  _rows = [];
  _columns = [];

  page = new Page();
  ColumnMode = ColumnMode;
  SelectionType = SelectionType;

  constructor(public catalogService: CatalogsService, public authorizationService: AuthorizationService) {
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.page.totalElements = this.totalRows;
    this.page.offset = 0;
  }

  ngOnInit(): void {
  }

  ngOnChanges(model: any) {
    if (model.columns) {
      this._columns = model.columns.currentValue.map((col: any) => {
        if (col.isBoolean) {
          col.cellTemplate = this.activoCell;
        }
        if (col.isSelector) {
          col.cellTemplate = this.selectorCell;
        }
        return col;
      });
    }
    if (model.rows) {
      this._rows = model.rows.currentValue;
    }
  }

  setPage(pageInfo: Page) {
    this.page.pageNumber = pageInfo.offset || 0;
    this.paging.emit(this.page);
  }

  onSelect(selected: any) {
    this.selected.emit(selected.selected[0]);
  }

  onSort(event: any) {
    this.page.order = event.column.prop;
    this.page.sort = event.newValue;
    this.paging.emit(this.page);
  }

  onActivate(event: any) { } //used for hover

}
