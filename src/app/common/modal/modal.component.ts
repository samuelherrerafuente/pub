import { Component, ComponentFactoryResolver, ComponentRef, ElementRef, Input, OnDestroy, OnInit, Type, ViewChild, ViewContainerRef } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {
  @Input()
  id: string = '';

  @Input()
  dynComponent: Type<any> | undefined = undefined;

  @Input()
  modalData: any;

  @ViewChild('modalHost', { static: true, read: ViewContainerRef }) modalHost!: ViewContainerRef;
  @ViewChild('modalCont', { static: true, read: ElementRef }) modalCont!: ElementRef;
  @ViewChild('modalBack', { static: true, read: ElementRef }) modalBack!: ElementRef;

  private element: ElementRef;
  private childInstance: any;

  constructor(private modalService: ModalService, private el: ElementRef, private componentFactoryResolver: ComponentFactoryResolver) {
    this.element = el;
  }

  ngOnInit(): void {
    if (!this.id) {
      console.error('modal must have an id');
      return;
    }
    document.body.appendChild(this.element.nativeElement);
    this.element.nativeElement.addEventListener('click', (el: any) => {
      if (el.target.className === 'jw-modal') {
        this.modalService.close(this.id);
      }
    });
    this.modalService.add(this);
  }

  ngOnDestroy(): void {
    this.modalService.remove(this.id);
    this.element.nativeElement.remove();
  }

  open(zIndex = 900, extraData?: any): void {
    this.loadComponent(extraData);
    this.element.nativeElement.style.display = 'block';
    this.modalCont.nativeElement.style.zIndex = zIndex + 1;
    this.modalBack.nativeElement.style.zIndex = zIndex;
    document.body.classList.add('jw-modal-open');
  }

  close(): any {
    this.element.nativeElement.style.display = 'none';
    document.body.classList.remove('jw-modal-open');
    if (this.childInstance.onClose) {
      return this.childInstance.onClose();
    }
    return null;
  }

  loadComponent(extraData?: any) {
    if (this.dynComponent) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.dynComponent);
      const viewContainerRef = this.modalHost;
      viewContainerRef.clear();
      const componentRef = viewContainerRef.createComponent(componentFactory);
      componentRef.instance.modalData = this.modalData;
      componentRef.instance.extraData = extraData;
      componentRef.instance.modalId = this.id;
      this.childInstance = componentRef.instance;
    }
  }

}
