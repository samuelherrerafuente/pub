import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthorizationService } from '../services/authorization.service';

@Directive({
  selector: '[grant]'
})
export class GrantDirective {
  @Input('grant') grant!: string | string[] | { grant: string, aux: string } | any[];
  private permission$!: Subscription;
  constructor(private auth: AuthorizationService, private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef) { }

  ngOnInit(): void {
    this.applyPermission();
  }

  private applyPermission(): void {
    this.permission$ = this.auth.whoami().subscribe((res: any) => {
      this.auth.isValid(this.grant).subscribe((res)=>{
        if (res) {
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.permission$.unsubscribe();
  }

}
