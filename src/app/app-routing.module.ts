import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { LogoutComponent } from './components/logout/logout.component';
import { MenuComponent } from './components/menu/menu.component';
import { ProgramasComponent } from './components/programas/programas.component';
import { ResetComponent } from './components/reset/reset.component';
import { RolesComponent } from './components/roles/roles.component';
import { SolicitudesComponent } from './components/solicitudes/solicitudes.component';
import { UsuariosComponent } from './components/usuarios/usuarios.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }, {
    path: 'login',
    component: LoginComponent
  }, {
    path: 'menu',
    component: MenuComponent
  }, {
    path: 'programas',
    component: ProgramasComponent
  },
  {
    path: 'solicitudes',
    component: SolicitudesComponent
  },
  {
    path: 'usuarios',
    component: UsuariosComponent
  }, {
    path: 'roles',
    component: RolesComponent
  }, {
    path: 'reset',
    component: ResetComponent
  }, {
    path: 'logout',
    component: LogoutComponent
  }, {
    path: '**', redirectTo: '/login'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
