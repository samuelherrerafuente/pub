import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { ConfiguracionComponent } from '../configuracion/configuracion.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  configuracionComponent = ConfiguracionComponent;

  constructor(public modalService: ModalService) { }

  ngOnInit(): void {
  }

}
