import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-rol-modal',
  templateUrl: './rol-modal.component.html',
  styleUrls: ['./rol-modal.component.scss']
})
export class RolModalComponent implements OnInit {
  extraData!: any;
  modalData!: string;

  dynamicForm!: FormGroup;
  rolesForm!: FormArray;
  rolesIdxList: any = {};
  permisosForm!: FormArray;
  rolCatalog!: any[];
  permisosCatalog: { [id: string]: any[] } = {};
  permisosSecciones!: string[];
  submitted = false;

  searchRolFormControl = new FormControl();
  searchPermisoFormControl = new FormControl();

  searchRol: string = '';
  searchPermiso: string = '';

  searchRolSub!: Subscription;
  searchPermisoSub!: Subscription;

  constructor(public modalService: ModalService, public catalogService: CatalogsService,
    private formBuilder: FormBuilder, private toastr: ToastrService) {
  }

  ngOnDestroy(): void {
    this.searchRolSub.unsubscribe();
    this.searchPermisoSub.unsubscribe();
  }

  public get c() { return this.dynamicForm.controls; }
  public get rolesFormArr() { return <any>this.rolesForm.controls }
  public get permisosFormArr() { return <any>this.permisosForm.controls }

  private cachedVals: { [id: string]: any[] } = {};
  getSeccionControls(seccion: string) {
    let v = this.cachedVals[seccion];
    if (v) {
      return v;
    } else {
      v = this.permisosForm.controls.filter((i: any) => {
        return i.seccion == seccion;
      });
      return v;
    }
  }

  ngOnInit(): void {
    this.extraData = this.modalData == 'new' ? null : this.extraData;
    this.dynamicForm = this.formBuilder.group({});
    this.rolesForm = this.formBuilder.array([]);
    this.permisosForm = this.formBuilder.array([]);
    this.rolesIdxList = {};
    let activeRols: any = {};
    (<any[]>this.extraData?.RolUsuarios)?.forEach((item: any) => {
      activeRols[item.IdUsuario] = !!item.Activo;
    });
    this.doAsyncUserSearch(activeRols);
    const subs1 = this.catalogService.getCatalog('permisos').subscribe((cat: any[]) => {
      let bySeccion: any = {};
      let activePermisos: any = {};
      (<any[]>this.extraData?.RolPermisos)?.forEach((item: any) => {
        activePermisos[item.IdPermiso] = !!item.Activo;
      });
      cat?.forEach((c) => {
        const grp: any = this.formBuilder.group({});
        grp.addControl('IdPermiso', this.formBuilder.control(c.Clave));
        grp.addControl('Activo', this.formBuilder.control(activePermisos[c.Clave] ? activePermisos[c.Clave] : false));
        grp.item = c;
        grp.seccion = c.Seccion;
        bySeccion[c.Seccion] = '';
        this.permisosForm.push(grp);
      });
      this.permisosSecciones = Object.keys(bySeccion);
      setTimeout(() => { subs1.unsubscribe() }, 1);
    });
    this.dynamicForm.addControl('Clave', this.formBuilder.control(this.extraData?.Clave || -1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Nombre', this.formBuilder.control(this.extraData?.Nombre, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Descripcion', this.formBuilder.control(this.extraData?.Descripcion, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Activo', this.formBuilder.control(this.modalData == 'new' ? true : this.extraData?.Activo));
    this.dynamicForm.addControl('RolUsuarios', this.rolesForm);
    this.dynamicForm.addControl('RolPermisos', this.permisosForm);
    this.searchRolSub = this.searchRolFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.searchRol = term;
        let activeRols: any = {};
        (<any[]>this.extraData?.RolUsuarios)?.forEach((item: any) => {
          activeRols[item.IdUsuario] = !!item.Activo;
        });
        this.doAsyncUserSearch(activeRols, term);
      });

    this.searchPermisoSub = this.searchPermisoFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.searchPermiso = term;
      });
  }

  doAsyncUserSearch(activeRols: any, search: any = '') {
    const subs = this.catalogService.getCatalog(`Usuarios:${search}`).subscribe((cat: any[]) => {
      cat?.forEach((c) => {
        if (!this.rolesIdxList[c.Clave]) {
          const grp: any = this.formBuilder.group({});
          grp.addControl('IdUsuario', this.formBuilder.control(c.Clave));
          grp.addControl('Activo', this.formBuilder.control(activeRols[c.Clave] ? activeRols[c.Clave] : false));
          grp.item = c;
          this.rolesIdxList[c.Clave] = true;
          this.rolesForm.push(grp);
        }
      });
      setTimeout(() => { subs.unsubscribe() }, 1);
    });
  }

  onClose() {
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    let r = (<any[]>this.dynamicForm.value.RolUsuarios)
      .filter((r) => { return r.Activo })
      .map((r) => { return r.IdUsuario });
    let p = (<any[]>this.dynamicForm.value.RolPermisos)
      .filter((p) => { return p.Activo })
      .map((p) => { return p.IdPermiso });
    this.dynamicForm.value.RolUsuarios = r;
    this.dynamicForm.value.RolPermisos = p;
    return this.dynamicForm.value;
  }

  save() {
    if (this.dynamicForm.invalid) {
      this.dynamicForm.markAllAsTouched();
      this.toastr.error('Completar todos los campos para continuar');
      return;
    }
    this.submitted = true;
    this.modalService.close(this);
  }

  shouldDisplayRol(item: any) {
    if (!this.searchRol)
      return true;
    return (<string>item.Usuario).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Nombre).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Apaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Amaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || item.Clave == this.searchRol
  }

  shouldDisplayPermiso(item: any) {
    if (!this.searchPermiso)
      return true;
    return (<string>item.Nombre).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Descripcion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Seccion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Siglas).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || item.Clave == this.searchPermiso
  }
}
