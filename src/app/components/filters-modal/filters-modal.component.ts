import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-filters-modal',
  templateUrl: './filters-modal.component.html',
  styleUrls: ['./filters-modal.component.scss']
})
export class FiltersModalComponent implements OnInit {
  config: any = {
    format: "YYYY-MM-DD",
    locale: 'es'
  };
  extraData!: any;
  dynamicForm!: FormGroup;
  submitted = false;

  constructor(public modalService: ModalService, public catalogService: CatalogsService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.dynamicForm = this.formBuilder.group({});
    for (let i of this.extraData.fields) {
      this.dynamicForm.addControl(i.id,
        this.formBuilder.control(this.extraData.current ? this.extraData.current[i.id] : ''));
    }
  }

  onClose() {
    return this.dynamicForm.value;
  }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      return;
    }
    this.modalService.close(this)
  }

}
