import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ModalService } from 'src/app/services/modal.service';
import { SearchService } from 'src/app/services/search.service';
import { ConfirmModalComponent } from '../../common/confirm-modal/confirm-modal.component';
import { FiltersModalComponent } from '../filters-modal/filters-modal.component';
import { Page } from '../../common/table/Page';
import { UsuarioModalComponent } from '../usuario-modal/usuario-modal.component';
import { UserService } from 'src/app/services/user.service';
import { ToastrService } from 'ngx-toastr';
import { ExportService } from 'src/app/services/export.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit, OnDestroy {
  private get entity() { return 'user' };
  usuarioModalComponent = UsuarioModalComponent;
  confirmModalComponent = ConfirmModalComponent;
  filtersModalComponent = FiltersModalComponent;
  modalSubscription!: Subscription;
  buttons = [
    { action: 'refresh', icon: 'bi-arrow-repeat', grants: ['administracion.usuarios.ver'] },
    { action: 'new-user', icon: 'bi-plus', grants: ['administracion.usuarios.nuevo'] },
    { action: 'edit-user', icon: 'bi-pencil', grants: ['administracion.usuarios.editar'] },
    { action: 'clone-user', icon: 'bi-files', grants: ['administracion.usuarios.nuevo'] },
    { action: 'export-user', icon: 'bi-download', grants: ['administracion.usuarios.ver'] },
    { action: 'delete-user', icon: 'bi-trash', grants: ['administracion.usuarios.desactivar'] }
  ];
  tabs = [{ label: 'Usuarios', link: '/usuarios' }, { label: 'Roles', link: '/roles' }];
  selected: any;
  columns = [{ name: 'Nombre', prop: 'Nombre' }, { name: 'Usuario', prop: 'Usuario' }, { name: 'Correo', prop: 'Correo' }, { name: 'Activo', prop: 'Activo', isBoolean: true }, { name: 'Creado el', prop: 'FechaCreacion' }, { name: 'Modificado el', prop: 'FechaModificacion' }];
  rows = [];
  loading = true;
  totalRows = 0;
  page = new Page();
  search: string | undefined;
  searchFilters: any;
  searchFiltersCount: number = 0;

  constructor(public modalService: ModalService, private searchService: SearchService,
    private userService: UserService, private toastr: ToastrService, private exportService: ExportService) { }

  ngOnDestroy(): void {
    this.modalSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.modalSubscription = this.modalService.modalClose$.subscribe(this.handleCloseEvent.bind(this));
  }

  handleSelected(event: any) {
    this.selected = event;
  }

  handleSearch(event: string) {
    this.search = event;
    this.doSearch();
  }

  handleAction(action: string) {
    switch (action) {
      case 'filters':
        this.modalService.open('filters-user', {
          current: this.searchFilters,
          fields: [
            {
              id: 'Activo', type: 'select', placeholder: '', label: 'Estatus', content: [
                { Nombre: 'Activo', Clave: 1 },
                { Nombre: 'Inactivo', Clave: 0 }
              ]
            },
            { id: 'IdRol', type: 'select', placeholder: '', label: 'Rol', catalog: 'Rol' },
            { id: 'FechaInicio', type: 'date', placeholder: '', label: 'Fecha inicial' },
            { id: 'FechaFin', type: 'date', placeholder: '', label: 'Fecha final' }
          ]
        });
        break;
      case 'refresh':
        this.doSearch();
        break;
      case 'edit-user':
      case 'clone-user':
      case 'delete-user':
        if (this.selected) {
          this.modalService.open(action, this.selected);
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      default:
        this.modalService.open(action, this.selected);
    }
  }

  handleCloseEvent(evt: any) {
    const data = evt.data;
    switch (evt.modalId) {
      case 'filters-user':
        this.searchFilters = evt.data;
        this.searchFiltersCount = Object.entries(this.searchFilters).filter(f => !!f[1]).length;
        this.doSearch();
        break;
      case 'new-user':
        if (data) {
          this.userService.save(data).subscribe((res: any) => {
            this.toastr.success(`Usuario creado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al crear usuario, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'clone-user':
        if (data) {
          this.userService.save(data).subscribe((res: any) => {
            this.toastr.success(`Usuario clonado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al clonar usuario, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'edit-user':
        if (data) {
          this.userService.update(data).subscribe((res: any) => {
            this.toastr.success(`Usuario actualizado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al actualizar usuario, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'delete-user':
        if (data) {
          this.userService.delete(this.selected).subscribe((res: any) => {
            this.toastr.success(`Usuario desactivado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al desactivar usuario, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'export-user':
        if (data) {
          const page = new Page();
          page.pageNumber = 0;
          page.size = 9999999;
          const sub = this.doRequest(page).subscribe((response: any) => {
            this.toastr.success(`Archivo exportado correctamente`);
            this.exportService.exportAsExcelFile(response.rows, 'Usuarios');
            sub.unsubscribe();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al exportar archivo, reintentar o contactar a soporte.`);
          });
        }
        break;
    }
  }

  handlePaging(page: Page) {
    this.page = page;
    this.doSearch();
  }

  doSearch() {
    this.selected = undefined;
    this.loading = true;
    const sub = this.doRequest().subscribe((response: any) => {
      this.rows = response.rows;
      this.page.totalElements = response.totalElements;
      sub.unsubscribe();
      setTimeout(() => this.loading = false, 1);
    });
  }

  private doRequest(page?: Page) {
    let searchObject = this.search ? {
      Nombre: this.search,
      Apaterno: this.search,
      Amaterno: this.search,
      Correo: this.search,
      Usuario: this.search,
    } : {};
    return this.searchService.search(this.entity, { ...searchObject, ...this.searchFilters }, page || this.page);
  }

}
