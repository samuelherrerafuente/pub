import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.scss']
})
export class SolicitudesComponent implements OnInit {
  selected: any;
  buttons = [
    { action: 'refresh', icon: 'bi-arrow-repeat', grants: ['administracion.programas.ver'] },
    { action: 'new-prog', icon: 'bi-plus', grants: ['administracion.programas.nuevo'] },
    { action: 'edit-prog', icon: 'bi-pencil', grants: ['administracion.programas.editar'] },
    { action: 'config-prog', icon: 'bi-sliders', grants: ['administracion.programas.configurar'] },
    { action: 'clone-prog', icon: 'bi-files', grants: ['administracion.programas.nuevo'] },
    { action: 'export-prog', icon: 'bi-download', grants: ['administracion.programas.ver'] },
    { action: 'delete-prog', icon: 'bi-trash', grants: ['administracion.programas.desactivar'] }
  ];
  search: string | undefined;
  searchFilters: any;
  searchFiltersCount: number = 0;
  constructor(private toastr: ToastrService, private modalService: ModalService) { }

  ngOnInit(): void {
  }


  handleSearch(event: string) {
    this.search = event;
    // this.doSearch();
  }

  handleAction(action: string) {
    switch (action) {
      case 'filters':
        this.modalService.open('filters-prog', {
          current: this.searchFilters,
          fields: [
            {
              id: 'Activo', type: 'select', placeholder: '', label: 'Estatus', content: [
                { Nombre: 'Activo', Clave: 1 },
                { Nombre: 'Inactivo', Clave: 0 }
              ]
            },
            { id: 'IdEstatusPrograma', type: 'select', placeholder: '', label: 'Estatus programa', catalog: 'estatus-programa' },
            { id: 'IdEntidadOperativa', type: 'select', placeholder: '', label: 'Dependencia', catalog: 'entidades-operativas' },
            { id: 'FechaInicio', type: 'date', placeholder: '', label: 'Fecha inicial' },
            { id: 'FechaFin', type: 'date', placeholder: '', label: 'Fecha final' }
          ]
        });
        break;
      case 'refresh':
        // this.doSearch();
        break;
      case 'edit-prog':
      case 'clone-prog':
      case 'delete-prog':
        if (this.selected) {
          this.modalService.open(action, this.selected);
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      default:
        this.modalService.open(action, this.selected);
    }
  }

}
