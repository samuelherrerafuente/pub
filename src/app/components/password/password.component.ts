import { Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {
  @Input()
  modalData: string = '';

  pass = new FormControl('', {
    validators: [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/)],
    updateOn: 'blur'
  });
  passConfirm = new FormControl('', {
    validators: [Validators.required, (ctrl: FormControl) => {
      const password = this.pass.value;
      const confirmPassword = ctrl.value;
      return password === confirmPassword ? null : { notSame: true }
    }]
  });

  constructor(public modalService: ModalService, private userServ: UserService) { }

  ngOnInit(): void {
  }

  savePass() {
    const sub = this.userServ.changePass(this.modalData, this.passConfirm.value).subscribe((response)=>{
      sub.unsubscribe();
      this.modalService.close('password');
    });
  }
}
