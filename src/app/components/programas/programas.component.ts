import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { ConfirmModalComponent } from 'src/app/common/confirm-modal/confirm-modal.component';
import { Page } from 'src/app/common/table/Page';
import { ExportService } from 'src/app/services/export.service';
import { ModalService } from 'src/app/services/modal.service';
import { ProgramasService } from 'src/app/services/programas.service';
import { SearchService } from 'src/app/services/search.service';
import { UserService } from 'src/app/services/user.service';
import { FiltersModalComponent } from '../filters-modal/filters-modal.component';
import { ProgramaConfigModalComponent } from '../programa-config-modal/programa-config-modal.component';
import { ProgramaModalComponent } from '../programa-modal/programa-modal.component';
import { UsuarioModalComponent } from '../usuario-modal/usuario-modal.component';

@Component({
  selector: 'app-programas',
  templateUrl: './programas.component.html',
  styleUrls: ['./programas.component.scss']
})
export class ProgramasComponent implements OnInit {

  private get entity() { return 'programa' };
  programaModalComponent = ProgramaModalComponent;
  programaConfigModalComponent = ProgramaConfigModalComponent;
  confirmModalComponent = ConfirmModalComponent;
  filtersModalComponent = FiltersModalComponent;
  modalSubscription!: Subscription;
  selected: any = undefined;
  statusObs!: Function;

  private buildButtons = (selected: any) => {
    return [
      { action: 'refresh', icon: 'bi-arrow-repeat', grants: ['administracion.programas.ver', 'PConsulta', 'PEstatus', 'PPermisos', 'PEdicion'] },
      { action: 'new-prog', icon: 'bi-plus', grants: ['administracion.programas.nuevo'] },
      { action: 'edit-prog', icon: 'bi-pencil', grants: ['administracion.programas.editar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PEdicion', aux: selected?.Clave }] },
      { action: 'config-prog', icon: 'bi-sliders', grants: ['administracion.programas.configurar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PEdicion', aux: selected?.Clave }] },
      { action: 'clone-prog', icon: 'bi-files', grants: ['administracion.programas.nuevo'] },
      { action: 'export-prog', icon: 'bi-download', grants: ['administracion.programas.ver', 'PConsulta', { grant: 'PPermisos', aux: selected?.Clave }] },
      { action: 'delete-prog', icon: 'bi-trash', grants: ['administracion.programas.desactivar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PDesactivar', aux: selected?.Clave }] }
    ];
  }

  buttons = this.buildButtons(undefined);

  columns = [
    { name: 'Nombre', prop: 'Nombre' }, { name: 'Inicio', prop: 'FechaInicio' },
    { name: 'Vigencia', prop: 'FechaFin' }, { name: 'Importe', prop: 'Clave' }, { name: 'Entregado', prop: 'Clave' },
    {
      name: 'Estatus', prop: 'IdEstatusPrograma', isSelector: true, catalog: 'estatus-programa',
      grant: (row: any) => {
        return ['administracion.programas.editar', { grant: 'PEditar', aux: row.Clave }, { grant: 'PEstatus', aux: row.Clave }, { grant: 'PPermisos', aux: row?.Clave }]
      },
      handler: (evt: any, row: any) => {
        if (this.selected) {
          this.modalService.open('change-status-prog', this.selected);
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        this.statusObs = (response: boolean) => {
          if (response) {
            this.progService.status(evt.target.value, row);
            const sub = this.progService.status(evt.target.value, row).subscribe((res) => {
              this.toastr.success(`Estatus actualizado correctamente`);
              sub.unsubscribe();
              this.doSearch();
            }, (err) => {
              this.doSearch();
              this.toastr.error(`Error al actualizar programa, reintentar o contactar a soporte.`);
            });
          } else {
            try {
              evt.target.value = row.IdEstatusPrograma;
              this.doSearch();
            } catch (e) {
              console.error(e);
            }
          }
        }
      }
    }
  ];
  rows = [];
  loading = true;
  totalRows = 0;
  page = new Page();
  search: string | undefined;
  searchFilters: any;
  searchFiltersCount: number = 0;

  constructor(public modalService: ModalService, private searchService: SearchService,
    private progService: ProgramasService, private toastr: ToastrService, private exportService: ExportService) { }

  ngOnDestroy(): void {
    this.modalSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.modalSubscription = this.modalService.modalClose$.subscribe(this.handleCloseEvent.bind(this));
  }

  handleSelected(event: any) {
    this.selected = event;
    this.buttons = this.buildButtons(this.selected);
  }

  handleSearch(event: string) {
    this.search = event;
    this.doSearch();
  }

  handleAction(action: string) {
    switch (action) {
      case 'filters':
        this.modalService.open('filters-prog', {
          current: this.searchFilters,
          fields: [
            {
              id: 'Activo', type: 'select', placeholder: '', label: 'Estatus', content: [
                { Nombre: 'Activo', Clave: 1 },
                { Nombre: 'Inactivo', Clave: 0 }
              ]
            },
            { id: 'IdEstatusPrograma', type: 'select', placeholder: '', label: 'Estatus programa', catalog: 'estatus-programa' },
            { id: 'IdEntidadOperativa', type: 'select', placeholder: '', label: 'Dependencia', catalog: 'entidades-operativas' },
            { id: 'FechaInicio', type: 'date', placeholder: '', label: 'Fecha inicial' },
            { id: 'FechaFin', type: 'date', placeholder: '', label: 'Fecha final' }
          ]
        });
        break;
      case 'refresh':
        this.doSearch();
        break;
      case 'edit-prog':
      case 'clone-prog':
      case 'delete-prog':
      case 'config-prog':
        if (this.selected) {
          this.modalService.open(action, this.selected);
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      default:
        this.modalService.open(action, this.selected);
    }
  }

  handleCloseEvent(evt: any) {
    const data = evt.data;
    switch (evt.modalId) {
      case 'filters-prog':
        this.searchFilters = evt.data;
        this.searchFiltersCount = Object.entries(this.searchFilters).filter(f => !!f[1]).length;
        this.doSearch();
        break;
      case 'new-prog':
        if (data) {
          this.progService.save(data).subscribe((res: any) => {
            this.toastr.success(`Programa creado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al crear programa, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'clone-prog':
        if (data) {
          this.progService.save(data).subscribe((res: any) => {
            this.toastr.success(`Programa clonado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al clonar programa, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'edit-prog':
        if (data) {
          this.progService.update(data).subscribe((res: any) => {
            this.toastr.success(`Programa actualizado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al actualizar programa, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'config-prog':
        if (data) {
          this.progService.configure(data).subscribe((res: any) => {
            this.toastr.success(`Programa actualizado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al actualizar programa, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'delete-prog':
        if (data) {
          this.progService.delete(this.selected).subscribe((res: any) => {
            this.toastr.success(`Programa desactivado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al desactivar programa, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'export-prog':
        if (data) {
          const page = new Page();
          page.pageNumber = 0;
          page.size = 9999999;
          const sub = this.doRequest(page).subscribe((response: any) => {
            this.toastr.success(`Archivo exportado correctamente`);
            this.exportService.exportAsExcelFile(response.rows, 'Programas');
            sub.unsubscribe();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al exportar archivo, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'change-status-prog':
        this.statusObs(data);
        break;
    }
  }

  handlePaging(page: Page) {
    this.page = page;
    this.doSearch();
  }

  doSearch() {
    this.handleSelected(undefined);
    this.loading = true;
    const sub = this.doRequest().subscribe((response: any) => {
      this.rows = response.rows;
      this.page.totalElements = response.totalElements;
      sub.unsubscribe();
      setTimeout(() => this.loading = false, 1);
    });
  }

  private doRequest(page?: Page) {
    let searchObject = this.search ? {
      Nombre: this.search,
      Apaterno: this.search,
      Amaterno: this.search,
      Correo: this.search,
      Usuario: this.search,
    } : {};
    return this.searchService.search(this.entity, { ...searchObject, ...this.searchFilters }, page || this.page);
  }

}
