import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-programa-requisito-modal',
  templateUrl: './programa-requisito-modal.component.html',
  styleUrls: ['./programa-requisito-modal.component.scss']
})
export class ProgramaRequisitoModalComponent implements OnInit {
  modalData!: string;
  extraData!: any;
  dynamicForm!: FormGroup;
  submitted = false;

  public get c() { return this.dynamicForm.controls; }

  constructor(public modalService: ModalService, public catalogService: CatalogsService,
    private formBuilder: FormBuilder, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.dynamicForm = this.formBuilder.group({});
    this.dynamicForm.addControl('Nombre', this.formBuilder.control(this.extraData?.requisito?.Nombre, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('IdTipoRequisito', this.formBuilder.control(this.extraData?.requisito?.IdTipoRequisito || '', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    const isStillActive = (<any[]>this.extraData.etapas).find((i) => i.Nombre == this.extraData?.requisito?.Etapa?.Nombre);
    this.dynamicForm.addControl('IdEtapa', this.formBuilder.control(
      isStillActive ? this.extraData.requisito.Etapa.Nombre : '', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Descripcion', this.formBuilder.control(this.extraData?.requisito?.Descripcion));
    this.dynamicForm.addControl('Obligatorio', this.formBuilder.control(this.extraData?.requisito?.Obligatorio));
  }

  onClose() {
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    this.dynamicForm.value.Activo = true;
    this.dynamicForm.value.Clave = this.extraData?.requisito?.Clave;
    this.dynamicForm.value.Etapa = { Nombre: this.dynamicForm.value.IdEtapa };
    return this.dynamicForm.value;
  }

  save() {
    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      this.dynamicForm.markAllAsTouched();
      this.toastr.error('Completar todos los campos para continuar');
      return;
    }
    this.submitted = true;
    this.modalService.close(this);
  }

}
