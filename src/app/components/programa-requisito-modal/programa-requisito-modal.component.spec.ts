import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaRequisitoModalComponent } from './programa-requisito-modal.component';

describe('ProgramaRequisitoModalComponent', () => {
  let component: ProgramaRequisitoModalComponent;
  let fixture: ComponentFixture<ProgramaRequisitoModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaRequisitoModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaRequisitoModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
