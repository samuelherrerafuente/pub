import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit, OnDestroy {
  private s!: Subscription;
  token: string = '';
  pass = new FormControl('', {
    validators: [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/)],
    updateOn: 'blur'
  });
  passConfirm = new FormControl('', {
    validators: [Validators.required, (ctrl: FormControl) => {
      const password = this.pass.value;
      const confirmPassword = ctrl.value;
      return password === confirmPassword ? null : { notSame: true }
    }]
  });

  constructor(private route: ActivatedRoute, private userServ: UserService, private toastr: ToastrService, private router: Router) { }

  ngOnDestroy(): void {
    this.s.unsubscribe();
  }

  ngOnInit(): void {
    this.s = this.route.queryParams
      .pipe(filter(params => params.token))
      .subscribe(params => {
        this.token = params.token;
      });
    setTimeout(() => {
      if (!this.token) {
        this.router.navigate(['login']);
        this.toastr.warning('Token de reestablecimiento invalido, redirigido a inicio.');
      }
    }, 5000);
  }

  doRecover(captchaToken: any) {
    if (!this.token) {
      this.router.navigate(['login']);
      this.toastr.warning('Token de reestablecimiento invalido, redirigido a inicio.');
    }
    const sub = this.userServ.reset(this.pass.value, this.token, captchaToken)
      .subscribe((response: any) => {
        if (response.message) {
          this.toastr.warning(response.message);
          return;
        }
        this.toastr.success('Contraseña actualizada satisfactoriamente.');
        sub.unsubscribe();
        this.router.navigate(['login']);
      });
  }

}