import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-programa-modal',
  templateUrl: './programa-modal.component.html',
  styleUrls: ['./programa-modal.component.scss']
})
export class ProgramaModalComponent implements OnInit {

  config: any = {
    format: "YYYY-MM-DD"
  };
  extraData!: any;
  modalData!: string;

  dynamicForm!: FormGroup;
  usersForm!: FormArray;
  usersIdxList: any = {};
  permisosForm!: FormArray;
  beneficiosForm!: FormArray;
  rolCatalog!: any[];
  permisosCatalog: { [id: string]: any[] } = {};
  permisosSecciones!: string[];
  submitted = false;

  searchRolFormControl = new FormControl();
  searchPermisoFormControl = new FormControl();

  searchRol: string = '';
  searchPermiso: string = '';

  searchRolSub!: Subscription;

  constructor(public modalService: ModalService, public catalogService: CatalogsService,
    private formBuilder: FormBuilder, private toastr: ToastrService, public authorizationService: AuthorizationService) {
  }

  ngOnDestroy(): void {
    this.searchRolSub.unsubscribe();
  }

  public get c() { return this.dynamicForm.controls; }
  public get usersFormArr() { return <any>this.usersForm.controls }
  public get beneficiosFormArr() { return <any>this.beneficiosForm.controls }

  private cachedVals: { [id: string]: any[] } = {};

  ngOnInit(): void {
    this.extraData = this.modalData == 'new' ? null : this.extraData;
    this.dynamicForm = this.formBuilder.group({});
    this.usersForm = this.formBuilder.array([]);
    this.permisosForm = this.formBuilder.array([]);
    this.beneficiosForm = this.formBuilder.array([]);
    this.usersIdxList = {};
    let activeUsers: any = {};
    (<any[]>this.extraData?.UsuarioConfiguracionProgramas)?.forEach((item: any) => {
      activeUsers[item.IdUsuario] = item;
    });
    (<any[]>this.extraData?.Beneficios)?.forEach((item: any) => {
      const grp: any = this.formBuilder.group({});
      grp.addControl('Clave', this.formBuilder.control(item.Clave));
      grp.addControl('IdTipoBeneficio', this.formBuilder.control(item.IdTipoBeneficio, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('IdTipoDistribucion', this.formBuilder.control(item.IdTipoDistribucion, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('IdTipoRecurso', this.formBuilder.control(item.IdTipoRecurso, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('IdUnidadMedida', this.formBuilder.control(item.IdUnidadMedida, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('Nombre', this.formBuilder.control(item.Nombre, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('Valor', this.formBuilder.control(item.Valor || 1, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      grp.addControl('Cantidad', this.formBuilder.control(item.Cantidad || 1, {
        updateOn: 'blur',
        validators: [Validators.required]
      }));
      this.beneficiosForm.push(grp);
    });
    this.doAsyncUserSearch(activeUsers);
    this.dynamicForm.addControl('Clave', this.formBuilder.control(this.extraData?.Clave || -1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Activo', this.formBuilder.control(this.modalData == 'new' ? true : this.extraData?.Activo));
    this.dynamicForm.addControl('IdEntidadOperativa', this.formBuilder.control(this.extraData?.IdEntidadOperativa || '', {
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Nombre', this.formBuilder.control(this.extraData?.Nombre, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    const fi = (<string>this.extraData?.FechaInicio || '').replace(' 00:00:00', '');
    const ff = (<string>this.extraData?.FechaFin || '').replace(' 00:00:00', '');
    this.dynamicForm.addControl('FechaInicio', this.formBuilder.control(fi, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('FechaFin', this.formBuilder.control(ff, {
      updateOn: 'blur',
      validators: [Validators.required, (fc: FormControl): { [key: string]: any } => {
        let f = this.dynamicForm.controls['FechaInicio'];
        let t = fc;
        if (f.value > t.value) {
          return {
            dates: true
          };
        }
        return {};
      }]
    }));
    this.dynamicForm.addControl('Descripcion', this.formBuilder.control(this.extraData?.Descripcion, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('PersonaMoral', this.formBuilder.control(this.extraData?.PersonaMoral || false));
    this.dynamicForm.addControl('DigramaYucatan', this.formBuilder.control(this.extraData?.DigramaYucatan || false));
    this.dynamicForm.addControl('IdTipoBebeficiario', this.formBuilder.control(this.extraData?.IdTipoBebeficiario || '', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Beneficios', this.beneficiosForm);
    this.dynamicForm.addControl('UsuarioConfiguracionProgramas', this.usersForm);
    this.searchRolSub = this.searchRolFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.searchRol = term;
        let activeUsers: any = {};
        (<any[]>this.extraData?.UsuarioConfiguracionProgramas)?.forEach((item: any) => {
          activeUsers[item.IdUsuario] = item;
        });
        this.doAsyncUserSearch(activeUsers, term);
      });
  }

  doAsyncUserSearch(activeUsers: any, search: any = '') {
    const subs = this.catalogService.getCatalog(`Usuarios:${search}`).subscribe((cat: any[]) => {
      cat?.forEach((c) => {
        if (!this.usersIdxList[c.Clave]) {
          const grp: any = this.formBuilder.group({});
          grp.addControl('IdUsuario', this.formBuilder.control(c.Clave));
          grp.addControl('Activo', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].Activo : false));
          grp.addControl('PPermisos', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].PPermisos : false));
          grp.addControl('PConsulta', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].PConsulta : false));
          grp.addControl('PEdicion', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].PEdicion : false));
          grp.addControl('PEstatus', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].PEstatus : false));
          grp.addControl('PDesactivar', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].PDesactivar : false));
          grp.user = activeUsers[c.Clave];
          grp.item = c;
          this.usersIdxList[c.Clave] = true;
          this.usersForm.push(grp);
        }
      });
      for (let k of Object.keys(activeUsers)) {
        if (!this.usersIdxList[k]) {
          const grp: any = this.formBuilder.group({});
          grp.addControl('IdUsuario', this.formBuilder.control(k));
          grp.addControl('Activo', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].Activo : false));
          grp.addControl('PPermisos', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].PPermisos : false));
          grp.addControl('PConsulta', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].PConsulta : false));
          grp.addControl('PEdicion', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].PEdicion : false));
          grp.addControl('PEstatus', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].PEstatus : false));
          grp.addControl('PDesactivar', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].PDesactivar : false));
          grp.user = activeUsers[k];
          this.catalogService.getCatalogValue('Usuarios:' + k, k).subscribe((res: any) => {
            grp.item = res;
          });
          this.usersIdxList[k] = true;
          this.usersForm.push(grp);
        }
      }
      setTimeout(() => { subs.unsubscribe() }, 1);
    });
  }

  addBeneficio() {
    const grp: any = this.formBuilder.group({});
    grp.addControl('Clave', this.formBuilder.control(undefined));
    grp.addControl('IdTipoBeneficio', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('IdTipoDistribucion', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('IdTipoRecurso', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('IdUnidadMedida', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('Nombre', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('Valor', this.formBuilder.control(1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    grp.addControl('Cantidad', this.formBuilder.control(1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.beneficiosForm.push(grp);
  }

  removeBeneficio(i: number) {
    if (i > -1) {
      this.beneficiosForm.removeAt(i, { emitEvent: false });
    }
  };

  _keyPress(e: any) {
    const regex = new RegExp("^[a-zA-Z0-9]+$");
    const regexNumStart = new RegExp("^[0-9]+$");
    const str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      if (!this.c['Usuario'].value?.length && regexNumStart.test(str)) {
        return false;
      }
      return true;
    }
    return false;
  }

  onClose() {
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    let r = (<any[]>this.dynamicForm.value.UsuarioConfiguracionProgramas)
      .filter((r) => { return r.Activo })
      .map((p) => {
        return {
          IdUsuario: p.IdUsuario,
          PPermisos: p.PPermisos,
          PConsulta: p.PConsulta,
          PEdicion: p.PEdicion,
          PEstatus: p.PEstatus,
          PDesactivar: p.PDesactivar,
        }
      });
    this.dynamicForm.value.UsuarioConfiguracionProgramas = r;
    return this.dynamicForm.value;
  }

  save() {
    // stop here if form is invalid
    if (this.dynamicForm.invalid) {
      this.dynamicForm.markAllAsTouched();
      this.toastr.error('Completar todos los campos para continuar');
      return;
    }
    this.submitted = true;
    this.modalService.close(this);
  }

  shouldDisplayRolUsuario(item: any) {
    if (!this.searchRol)
      return true;
    return (<string>item.Usuario).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Nombre).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Apaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Amaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || item.Clave == this.searchRol
  }

  shouldDisplayPermiso(item: any) {
    if (!this.searchPermiso)
      return true;
    return (<string>item.Nombre).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Descripcion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Seccion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Siglas).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || item.Clave == this.searchPermiso
  }
}
