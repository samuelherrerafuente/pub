import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaModalComponent } from './programa-modal.component';

describe('ProgramaModalComponent', () => {
  let component: ProgramaModalComponent;
  let fixture: ComponentFixture<ProgramaModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
