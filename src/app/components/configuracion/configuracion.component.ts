import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from 'src/app/services/modal.service';
import { UserService } from 'src/app/services/user.service';
import { PasswordComponent } from '../password/password.component';

@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.scss']
})
export class ConfiguracionComponent implements OnInit {
  passwordComponent = PasswordComponent;

  usuario = '';
  nombreUsuario = '';
  emailUsuario = '';
  claveUsuario = '';

  constructor(public modalService: ModalService, private userServ: UserService) { }

  ngOnInit(): void {
    const sub = this.userServ.whoami().subscribe((response: any)=>{
      this.usuario = "@" + response.Usuario;
      this.nombreUsuario = `${response.Nombre} ${response.Apaterno} ${response.Amaterno}`;
      this.emailUsuario = response.Correo;
      this.claveUsuario = response.Clave;
      setTimeout(()=>{
        sub.unsubscribe();
      },0);
    });
  }

}
