import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { Page } from 'src/app/common/table/Page';
import { ExportService } from 'src/app/services/export.service';
import { ModalService } from 'src/app/services/modal.service';
import { RolesPermisosService } from 'src/app/services/roles-permisos.service';
import { SearchService } from 'src/app/services/search.service';
import { UserService } from 'src/app/services/user.service';
import { ConfirmModalComponent } from '../../common/confirm-modal/confirm-modal.component';
import { FiltersModalComponent } from '../filters-modal/filters-modal.component';
import { RolModalComponent } from '../rol-modal/rol-modal.component';
import { UsuarioModalComponent } from '../usuario-modal/usuario-modal.component';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  private get entity() { return 'rol' };
  rolModalComponent = RolModalComponent;
  confirmModalComponent = ConfirmModalComponent;
  filtersModalComponent = FiltersModalComponent;
  modalSubscription!: Subscription;
  buttons = [
    { action: 'refresh', icon: 'bi-arrow-repeat', grants: ['administracion.roles.ver'] },
    { action: 'new-rol', icon: 'bi-plus', grants: ['administracion.roles.nuevo']  },
    { action: 'edit-rol', icon: 'bi-pencil', grants: ['administracion.roles.editar']  },
    { action: 'clone-rol', icon: 'bi-files' , grants: ['administracion.roles.nuevo'] },
    { action: 'export-rol', icon: 'bi-download' , grants: ['administracion.roles.ver'] },
    { action: 'delete-rol', icon: 'bi-trash' , grants: ['administracion.roles.desactivar'] }
  ];
  tabs = [{ label: 'Usuarios', link: '/usuarios' }, { label: 'Roles', link: '/roles' }];
  selected: any;
  columns = [{ name: 'Nombre', prop: 'Nombre' }, { name: 'Activo', prop: 'Activo', isBoolean: true },{ name: 'Descripción', prop: 'Descripcion' } ,{ name: 'Creado el', prop: 'FechaCreacion' }, { name: 'Modificado el', prop: 'FechaModificacion' }];
  rows = [];
  loading = true;
  totalRows = 0;
  page = new Page();
  search: string | undefined;
  searchFilters: any;
  searchFiltersCount: number = 0;

  constructor(public modalService: ModalService, private searchService: SearchService,
    private rolesService: RolesPermisosService, private toastr: ToastrService, private exportService: ExportService) { }

  ngOnDestroy(): void {
    this.modalSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.modalSubscription = this.modalService.modalClose$.subscribe(this.handleCloseEvent.bind(this));
  }

  handleSelected(event: any) {
    this.selected = event;
  }

  handleSearch(event: string) {
    this.search = event;
    this.doSearch();
  }

  handleAction(action: string) {
    switch (action) {
      case 'filters':
        this.modalService.open('filters-rol', {
          current: this.searchFilters,
          fields: [
            {
              id: 'Activo', type: 'select', placeholder: '', label: 'Estatus', content: [
                { Nombre: 'Activo', Clave: 1 },
                { Nombre: 'Inactivo', Clave: 0 }
              ]
            },
            { id: 'FechaInicio', type: 'date', placeholder: '', label: 'Fecha inicial' },
            { id: 'FechaFin', type: 'date', placeholder: '', label: 'Fecha final' }
          ]
        });
        break;
      case 'refresh':
        this.doSearch();
        break;
      case 'edit-rol':
      case 'clone-rol':
      case 'delete-rol':
        if (this.selected) {
          this.modalService.open(action, this.selected);
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      default:
        this.modalService.open(action, this.selected);
    }
  }

  handleCloseEvent(evt: any) {
    const data = evt.data;
    switch (evt.modalId) {
      case 'filters-rol':
        this.searchFilters = evt.data;
        this.searchFiltersCount = Object.entries(this.searchFilters).filter(f => !!f[1]).length;
        this.doSearch();
        break;
      case 'new-rol':
        if (data) {
          this.rolesService.save(data).subscribe((res: any) => {
            this.toastr.success(`Rol creado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al crear rol, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'clone-rol':
        if (data) {
          this.rolesService.save(data).subscribe((res: any) => {
            this.toastr.success(`Rol clonado correctamente, Clave: ${res.clave}`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al clonar rol, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'edit-rol':
        if (data) {
          this.rolesService.update(data).subscribe((res: any) => {
            this.toastr.success(`Rol actualizado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al actualizar rol, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'delete-rol':
        if (data) {
          this.rolesService.delete(this.selected).subscribe((res: any) => {
            this.toastr.success(`UsuRolario desactivado correctamente`);
            this.doSearch();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al desactivar rol, reintentar o contactar a soporte.`);
          });
        }
        break;
      case 'export-rol':
        if (data) {
          const page = new Page();
          page.pageNumber = 0;
          page.size = 9999999;
          const sub = this.doRequest(page).subscribe((response: any) => {
            this.toastr.success(`Archivo exportado correctamente`);
            this.exportService.exportAsExcelFile(response.rows, 'Usuarios');
            sub.unsubscribe();
          }, (err: any) => {
            console.error(err);
            this.toastr.error(`Error al exportar archivo, reintentar o contactar a soporte.`);
          });
        }
        break;
    }
  }

  handlePaging(page: Page) {
    this.page = page;
    this.doSearch();
  }

  doSearch() {
    this.selected = undefined;
    this.loading = true;
    const sub = this.doRequest().subscribe((response: any) => {
      this.rows = response.rows;
      this.page.totalElements = response.totalElements;
      sub.unsubscribe();
      setTimeout(() => this.loading = false, 1);
    });
  }

  private doRequest(page?: Page) {
    let searchObject = this.search ? {
      Nombre: this.search,
      Descripcion: this.search
    } : {};
    return this.searchService.search(this.entity, { ...searchObject, ...this.searchFilters }, page || this.page);
  }


}
