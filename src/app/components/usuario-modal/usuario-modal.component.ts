import { Component, OnDestroy, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-usuario-modal',
  templateUrl: './usuario-modal.component.html',
  styleUrls: ['./usuario-modal.component.scss']
})
export class UsuarioModalComponent implements OnInit, OnDestroy {
  extraData!: any;
  modalData!: string;

  dynamicForm!: FormGroup;
  rolesForm!: FormArray;
  permisosForm!: FormArray;
  rolCatalog!: any[];
  permisosCatalog: { [id: string]: any[] } = {};
  permisosSecciones!: string[];
  submitted = false;

  searchRolFormControl = new FormControl();
  searchPermisoFormControl = new FormControl();

  searchRol: string = '';
  searchPermiso: string = '';

  searchRolSub!: Subscription;
  searchPermisoSub!: Subscription;

  constructor(public modalService: ModalService, public catalogService: CatalogsService,
    private formBuilder: FormBuilder, private toastr: ToastrService) {
  }

  ngOnDestroy(): void {
    this.searchRolSub.unsubscribe();
    this.searchPermisoSub.unsubscribe();
  }

  public get c() { return this.dynamicForm.controls; }
  public get rolesFormArr() { return <any>this.rolesForm.controls }
  public get permisosFormArr() { return <any>this.rolesForm.controls }

  private cachedVals: { [id: string]: any[] } = {};
  getSeccionControls(seccion: string) {
    let v = this.cachedVals[seccion];
    if (v) {
      return v;
    } else {
      v = this.permisosForm.controls.filter((i: any) => {
        return i.seccion == seccion;
      });
      return v;
    }
  }

  ngOnInit(): void {
    this.extraData = this.modalData == 'new' ? null : this.extraData;
    this.dynamicForm = this.formBuilder.group({});
    this.rolesForm = this.formBuilder.array([]);
    this.permisosForm = this.formBuilder.array([]);
    let activeRols: any = {};
    (<any[]>this.extraData?.RolUsuarios)?.forEach((item: any) => {
      activeRols[item.IdRol] = !!item.Activo;
    });
    const subs = this.catalogService.getCatalog('Rol').subscribe((cat: any[]) => {
      cat?.forEach((c) => {
        const grp: any = this.formBuilder.group({});
        grp.addControl('IdRol', this.formBuilder.control(c.Clave));
        grp.addControl('Activo', this.formBuilder.control(activeRols[c.Clave] ? activeRols[c.Clave] : false));
        grp.item = c;
        this.rolesForm.push(grp);
      });
      setTimeout(() => { subs.unsubscribe() }, 1);
    });
    const subs1 = this.catalogService.getCatalog('permisos').subscribe((cat: any[]) => {
      let bySeccion: any = {};
      let activePermisos: any = {};
      (<any[]>this.extraData?.UsuarioPermisos)?.forEach((item: any) => {
        activePermisos[item.IdPermiso] = !!item.Activo;
      });
      cat?.forEach((c) => {
        const grp: any = this.formBuilder.group({});
        grp.addControl('IdPermiso', this.formBuilder.control(c.Clave));
        grp.addControl('Activo', this.formBuilder.control(activePermisos[c.Clave] ? activePermisos[c.Clave] : false));
        grp.item = c;
        grp.seccion = c.Seccion;
        bySeccion[c.Seccion] = '';
        this.permisosForm.push(grp);
      });
      this.permisosSecciones = Object.keys(bySeccion);
      setTimeout(() => { subs1.unsubscribe() }, 1);
    });
    this.dynamicForm.addControl('Clave', this.formBuilder.control(this.extraData?.Clave || -1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Nombre', this.formBuilder.control(this.extraData?.Nombre, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Apaterno', this.formBuilder.control(this.extraData?.Apaterno, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Amaterno', this.formBuilder.control(this.extraData?.Amaterno, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Usuario', this.formBuilder.control(this.extraData?.Usuario, {
      updateOn: 'blur',
      validators: [Validators.required, Validators.minLength(3), Validators.maxLength(10)]
    }));
    this.dynamicForm.addControl('Correo', this.formBuilder.control(this.extraData?.Correo, {
      updateOn: 'blur',
      validators: [Validators.required, Validators.email]
    }));
    this.dynamicForm.addControl('Password', this.formBuilder.control('', {
      updateOn: 'blur',
      validators: this.modalData == 'edit' ? [Validators.nullValidator, (ctrl: FormControl) => {
        const regex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/;
        return !ctrl.value ? null : regex.test(ctrl.value) ? null : { invalidPassword: true };
      }]
        : [Validators.required, Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/)]
    }));
    this.dynamicForm.addControl('Vigencia', this.formBuilder.control(this.extraData?.Vigencia || false));
    // se puede cambiar required por una funcione espeficica
    this.dynamicForm.addControl('DiasVigencia', this.formBuilder.control(this.extraData?.DiasVigencia || 1, {
      updateOn: 'blur',
      validators: [Validators.required, Validators.min(1)]
    }));
    this.dynamicForm.addControl('Activo', this.formBuilder.control(this.modalData == 'new' ? true : this.extraData?.Activo));
    this.dynamicForm.addControl('IdEntidadOperativa', this.formBuilder.control(this.extraData?.IdEntidadOperativa || '', {
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('RolUsuarios', this.rolesForm);
    this.dynamicForm.addControl('UsuarioPermisos', this.permisosForm);
    this.rolesForm.get
    this.searchRolSub = this.searchRolFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.searchRol = term;
      });

    this.searchPermisoSub = this.searchPermisoFormControl.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged()
      )
      .subscribe((term: string) => {
        this.searchPermiso = term;
      });
  }

  _keyPress(e: any) {
    const regex = new RegExp("^[a-zA-Z0-9]+$");
    const regexNumStart = new RegExp("^[0-9]+$");
    const str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      if (!this.c['Usuario'].value?.length && regexNumStart.test(str)) {
        return false;
      }
      return true;
    }
    return false;
  }

  onClose() {
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    let r = (<any[]>this.dynamicForm.value.RolUsuarios)
      .filter((r) => { return r.Activo })
      .map((r) => { return r.IdRol });
    let p = (<any[]>this.dynamicForm.value.UsuarioPermisos)
      .filter((p) => { return p.Activo })
      .map((p) => { return p.IdPermiso });
    this.dynamicForm.value.RolUsuarios = r;
    this.dynamicForm.value.UsuarioPermisos = p;
    return this.dynamicForm.value;
  }

  save() {
    if (this.dynamicForm.invalid) {
      this.dynamicForm.markAllAsTouched();
      this.toastr.error('Completar todos los campos para continuar');
      return;
    }
    this.submitted = true;
    this.modalService.close(this);
  }

  shouldDisplayRol(item: any) {
    if (!this.searchRol)
      return true;
    return (<string>item.Nombre).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Descripcion).toLowerCase().includes(this.searchRol.toLowerCase())
      || item.Clave == this.searchRol
  }

  shouldDisplayPermiso(item: any) {
    if (!this.searchPermiso)
      return true;
    return (<string>item.Nombre).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Descripcion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Seccion).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || (<string>item.Siglas).toLowerCase().includes(this.searchPermiso.toLowerCase())
      || item.Clave == this.searchPermiso
  }
}
