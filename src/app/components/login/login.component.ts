import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  sesionActiva = !!localStorage['saved-u'];
  step = 0;
  showRecover: boolean = false;
  mail = new FormControl('', {
    validators: [Validators.required, Validators.email]
  });
  user = new FormControl(localStorage['saved-u'], {
    validators: [Validators.required, Validators.minLength(3), Validators.maxLength(10)]
  });
  pass = new FormControl(localStorage['saved-p'], {
    validators: [Validators.required, Validators.minLength(8),
    Validators.pattern(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/)
    ]
  });


  constructor(private userServ: UserService, private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void { }

  _keyPress(e: any) {
    const regex = new RegExp("^[a-zA-Z0-9]+$");
    const regexNumStart = new RegExp("^[0-9]+$");
    const str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      if (!this.user.value?.length && regexNumStart.test(str)) {
        return false;
      }
      return true;
    }
    return false;
  }

  mostrarRecuperar() {
    this.showRecover = !this.showRecover;
    this.step = 0;
  }

  sigBtn() {
    this.step++;
  }

  prevBtn() {
    this.step--;
  }

  onChange(checked: any) {
    this.sesionActiva = checked.target.checked;
    if (!this.sesionActiva) {
      localStorage['saved-u'] = '';
      localStorage['saved-p'] = '';
    }
  }

  doLogin(captchaRes: any) {
    if (this.showRecover) {
      this.recuperar(captchaRes);
    } else {
      const sub = this.userServ.login(this.user.value, this.pass.value, captchaRes)
        .subscribe((response: any) => {

          if (response.firstLogin) {
            this.router.navigate(['reset'], { queryParams: { token: response.token } });
            this.toastr.warning('Es necesario actualizar la contraseña por defecto.', 'Primer inicio de sesión');
          } else {
            if (this.sesionActiva) {
              localStorage['saved-u'] = this.user.value;
              localStorage['saved-p'] = this.pass.value;
            }
            localStorage['sessionToken'] = response.tokenExpire;
            sub.unsubscribe();
            this.router.navigate(['menu']);
          }
        }, (error: any) => {
          if (error.status) {
            this.toastr.warning('Inicio de sesión invalido, corregir e intentar de nuevo.');
          }
        });
    }
  }

  recuperar(captchaRes: any) {
    const sub = this.userServ.recover(this.mail.value, captchaRes)
      .subscribe((response: any) => {
        if (response.message) {
          this.toastr.warning(response.message);
          return;
        }
        this.toastr.success('Email de reestablecimiento enviado satisfactoriamente.');
        this.mostrarRecuperar();
        sub.unsubscribe();
      });
  }

}
