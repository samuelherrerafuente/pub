import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaConfigModalComponent } from './programa-config-modal.component';

describe('ProgramaConfigModalComponent', () => {
  let component: ProgramaConfigModalComponent;
  let fixture: ComponentFixture<ProgramaConfigModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgramaConfigModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaConfigModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
