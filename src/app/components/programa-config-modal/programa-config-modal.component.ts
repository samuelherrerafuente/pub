import { Component, OnInit } from '@angular/core';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { CatalogsService } from 'src/app/services/catalogs.service';
import { ModalService } from 'src/app/services/modal.service';
import Drawflow from 'drawflow'
import * as htmlToImage from 'html-to-image';
import { saveAs as fileSaverSave } from 'file-saver';
import { digl } from '@crinkles/digl'
import { AuthorizationService } from 'src/app/services/authorization.service';
import { ProgramaRequisitoModalComponent } from '../programa-requisito-modal/programa-requisito-modal.component';
import { ConfirmModalComponent } from 'src/app/common/confirm-modal/confirm-modal.component';


@Component({
  selector: 'app-programa-config-modal',
  templateUrl: './programa-config-modal.component.html',
  styleUrls: ['./programa-config-modal.component.scss']
})
export class ProgramaConfigModalComponent implements OnInit {
  programaRequisitoModalComponent = ProgramaRequisitoModalComponent;
  confirmModalComponent = ConfirmModalComponent;

  public n!: FormControl;
  public s!: FormControl;

  extraData!: any;
  modalData!: string;

  programaRequisitos: any = { list: [] };

  editor: any;
  selectedNode: any;
  mobile_last_move: any;
  mobile_item_selec: any;
  dynamicForm!: FormGroup;
  usersForm!: FormArray;
  usersIdxList: any = {};
  submitted = false;

  nodesListForSelect: any[] = [];

  searchRolFormControl = new FormControl();
  searchRol: string = '';
  searchRequisito: string = '';
  searchRolSub!: Subscription;

  selectedRequisito: any;
  modalSubscription!: Subscription;

  etapasInValid: any[] = [];

  private buildButtons = (selected: any) => {
    return [
      { action: 'new-req', icon: 'bi-plus', grants: ['administracion.programas.editar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PEdicion', aux: selected?.Clave }] },
      { action: 'edit-req', icon: 'bi-pencil', grants: ['administracion.programas.editar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PEdicion', aux: selected?.Clave }] },
      { action: 'delete-req', icon: 'bi-trash', grants: ['administracion.programas.editar', { grant: 'PPermisos', aux: selected?.Clave }, { grant: 'PEdicion', aux: selected?.Clave }] }
    ];
  }

  buttonsRequisitos!: any;

  constructor(public modalService: ModalService, public catalogService: CatalogsService,
    private formBuilder: FormBuilder, private toastr: ToastrService, public authorizationService: AuthorizationService) {
  }

  ngOnDestroy(): void {
    this.searchRolSub.unsubscribe();
    this.modalSubscription.unsubscribe();
  }

  public get c() { return this.dynamicForm.controls; }
  public get usersFormArr() { return <any>this.usersForm.controls }

  ngOnInit(): void {
    this.modalSubscription = this.modalService.modalClose$.subscribe(this.handleCloseEvent.bind(this));
    this.n = this.formBuilder.control('');
    this.s = this.formBuilder.control('');
    this.extraData = this.modalData == 'new' ? null : this.extraData;
    this.programaRequisitos = { list: this.extraData?.ProgramaRequisitos || [] };
    this.buttonsRequisitos = this.buildButtons(this.extraData);
    this.drawDiagram(this.extraData);
    this.initForm();
    this.searchRolSub =
      this.searchRolFormControl.valueChanges.pipe(
        debounceTime(500),
        distinctUntilChanged()
      ).subscribe((term: string) => {
        this.searchRol = term;
        let activeUsers: any = {};
        (<any[]>this.extraData?.UsuarioConfiguracionProgramas)?.forEach((item: any) => {
          activeUsers[item.IdUsuario] = item;
        });
        this.doAsyncUserSearch(activeUsers, term);
      });
  }

  initForm() {
    this.dynamicForm = this.formBuilder.group({});
    this.usersForm = this.formBuilder.array([]);
    this.usersIdxList = {};
    let activeUsers: any = {};
    (<any[]>this.extraData?.UsuarioConfiguracionProgramas)?.forEach((item: any) => {
      activeUsers[item.IdUsuario] = item;
    });
    this.doAsyncUserSearch(activeUsers);
    this.dynamicForm.addControl('Clave', this.formBuilder.control(this.extraData?.Clave || -1, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Activo', this.formBuilder.control(this.modalData == 'new' ? true : this.extraData?.Activo));
    this.dynamicForm.addControl('IdEntidadOperativa', this.formBuilder.control(this.extraData?.IdEntidadOperativa || '', {
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Nombre', this.formBuilder.control(this.extraData?.Nombre, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('FechaInicio', this.formBuilder.control(this.extraData?.FechaInicio, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('FechaFin', this.formBuilder.control(this.extraData?.FechaFin, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Descripcion', this.formBuilder.control(this.extraData?.Descripcion, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('PersonaMoral', this.formBuilder.control(this.extraData?.PersonaMoral, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('DigramaYucatan', this.formBuilder.control(this.extraData?.DigramaYucatan, {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('IdTipoBebeficiario', this.formBuilder.control(this.extraData?.IdTipoBebeficiario || '', {
      updateOn: 'blur',
      validators: [Validators.required]
    }));
    this.dynamicForm.addControl('Etapas', this.formBuilder.array([]));
    this.dynamicForm.addControl('ProgramaRequisitos', this.formBuilder.array([]));
    this.dynamicForm.addControl('UsuarioConfiguracionProgramas', this.usersForm);
  }

  handleRequisitoSearch(term: any) {
    this.searchRequisito = term;
  }

  handleRequisitoAction(event: any) {
    switch (event) {
      case 'new-req':
        this.modalService.open('new-requisito', { etapas: this.nodesListForSelect });
        break;
      case 'edit-req':
        if (this.selectedRequisito) {
          this.modalService.open('edit-requisito', { requisito: this.selectedRequisito, etapas: this.nodesListForSelect });
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      case 'delete-req':
        if (this.selectedRequisito) {
          this.modalService.open('delete-requisito', { requisito: this.selectedRequisito, etapas: this.nodesListForSelect });
        } else {
          this.toastr.warning('Seleccionar un item primero');
        }
        break;
      default:
    }
  }

  selected(item: any) {
    this.selectedRequisito = item;
  }

  handleCloseEvent(evt: any) {
    const data = evt.data;
    switch (evt.modalId) {
      case 'new-requisito':
        if (data) {
          this.programaRequisitos.list.push(data);
        }
        break;
      case 'edit-requisito':
        if (data) {
          this.programaRequisitos.list =
            this.programaRequisitos.list.map((x: any) => {
              if (x.Clave == data.Clave || x.Nombre == data.Nombre) {
                x.Clave = data.Clave;
                x.Nombre = data.Nombre;
                x.Descripcion = data.Descripcion;
                x.Etapa.Nombre = data.Etapa.Nombre;
                x.Obligatorio = data.Obligatorio;
                x.IdTipoRequisito = data.IdTipoRequisito;
              }
              return x;
            });
        }
        break;
      case 'delete-requisito':
        if (data) {
          this.programaRequisitos.list =
            this.programaRequisitos.list.filter((x: any) => {
              if (this.selectedRequisito.Clave) {
                return this.selectedRequisito.Clave != x.Clave;
              } else {
                return this.selectedRequisito.Nombre != x.Nombre;
              }
            });
        }
        break;
    }
    this.updateNodesList();
  }

  doAsyncUserSearch(activeUsers: any, search: any = '') {
    const subs = this.catalogService.getCatalog(`Usuarios:${search}`).subscribe((cat: any[]) => {
      cat?.forEach((c) => {
        if (!this.usersIdxList[c.Clave]) {
          const grp: FormGroup = this.formBuilder.group({});
          grp.addControl('IdUsuario', this.formBuilder.control(c.Clave));
          grp.addControl('Activo', this.formBuilder.control(activeUsers[c.Clave] ? !!activeUsers[c.Clave].Activo : false));
          grp.addControl('UsuarioConfiguracionEtapas', this.formBuilder.group({}));
          (<any>grp).user = activeUsers[c.Clave];
          if (!activeUsers[c.Clave]) {
          }
          (<any>grp).item = c;
          this.usersIdxList[c.Clave] = true;
          this.usersForm.push(grp);
        }
      });
      for (let k of Object.keys(activeUsers)) {
        if (!this.usersIdxList[k]) {
          const grp: FormGroup = this.formBuilder.group({});
          grp.addControl('IdUsuario', this.formBuilder.control(k));
          grp.addControl('Activo', this.formBuilder.control(activeUsers[k] ? !!activeUsers[k].Activo : false));
          grp.addControl('UsuarioConfiguracionEtapas', this.formBuilder.group({}));
          (<any>grp).user = activeUsers[k];
          this.catalogService.getCatalogValue('Usuarios:' + k, k).subscribe((res: any) => {
            (<any>grp).item = res;
          });
          this.usersIdxList[k] = true;
          this.usersForm.push(grp);
        }
      }
      setTimeout(() => { subs.unsubscribe() }, 1);
    });
  }

  _keyPress(e: any) {
    const regex = new RegExp("^[a-zA-Z0-9]+$");
    const regexNumStart = new RegExp("^[0-9]+$");
    const str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
      if (!this.c['Usuario'].value?.length && regexNumStart.test(str)) {
        return false;
      }
      return true;
    }
    return false;
  }

  updateNodesList() {
    const diagramData = this.editor.export();
    let etapas: any[] = [];
    let etapaMap: any = {};
    for (let i in diagramData.drawflow.Home.data) {
      const elm = diagramData.drawflow.Home.data[i];
      etapas.push({
        Nombre: elm.data.nombre,
        Siglas: elm.data.siglas,
      });
      etapaMap[elm.data.nombre] = true;
    };
    this.nodesListForSelect = etapas;
    this.etapasInValid = [];
    for (const req of this.programaRequisitos.list) {
      if (!etapaMap[req.Etapa.Nombre]) {
        this.etapasInValid.push(req.Nombre);
      }
    }
  }

  processEtapaLabel(nombre: any) {
    return this.nodesListForSelect.find((i: any) => { return i.Nombre == nombre }) ? nombre : `${nombre}** invalido, corregir`;
  }

  onClose() {
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    const diagramData = this.editor.export();
    let etapas: any[] = [];
    let names: any = {};
    for (let i in diagramData.drawflow.Home.data) {
      const elm = diagramData.drawflow.Home.data[i];
      names[elm.id] = elm.data.nombre;
      etapas.push({
        id: elm.id,
        Nombre: elm.data.nombre,
        Siglas: elm.data.siglas,
        EtapasSiguientes: elm.outputs.output_1.connections?.map((c: any) => c.node)
      });
    };
    etapas = etapas.map((etapa: any) => {
      etapa.EtapasSiguientes = etapa.EtapasSiguientes.map((id: any) => {
        return names[id];
      });
      return etapa;
    });
    let new_element: any = document.getElementById("drawflow");
    new_element.innerHTML = "";
    if (this.dynamicForm.invalid || !this.submitted) {
      return null;
    }
    let r = (<any[]>this.dynamicForm.value.UsuarioConfiguracionProgramas)
      .filter((r) => { return r.Activo })
      .map((p) => {
        return {
          IdUsuario: p.IdUsuario,
          UsuarioConfiguracionEtapas: p.UsuarioConfiguracionEtapas
        }
      });
    this.dynamicForm.value.UsuarioConfiguracionProgramas = r;
    this.dynamicForm.value.Etapas = etapas;
    this.dynamicForm.value.ProgramaRequisitos = this.programaRequisitos.list?.filter((x: any) => !!x.Activo);
    return this.dynamicForm.value;
  }

  save() {
    // stop here if form is invalid
    if (this.etapasInValid.length) {
      this.toastr.error(`Requisitos ${this.etapasInValid.join(',')} contienen etapas invalidas, corregir para continuar`);
      return;
    }
    if (this.dynamicForm.invalid) {
      this.dynamicForm.markAllAsTouched();
      this.toastr.error('Completar todos los campos para continuar');
      return;
    }
    this.submitted = true;
    this.modalService.close(this);
  }

  shouldDisplayRolUsuario(item: any) {
    if (!this.searchRol)
      return true;
    return (<string>item.Usuario).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Nombre).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Apaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || (<string>item.Amaterno).toLowerCase().includes(this.searchRol.toLowerCase())
      || item.Clave == this.searchRol
  }

  shouldDisplayRequisitos(item: any) {
    if (!this.searchRequisito)
      return true;
    return (<string>item.Nombre).toLowerCase().includes(this.searchRequisito.toLowerCase())
      || (<string>item.Descripcion).toLowerCase().includes(this.searchRequisito.toLowerCase())
      || (<string>item.Etapa?.Nombre).toLowerCase().includes(this.searchRequisito.toLowerCase())
      || (<string>item.TipoRequisito?.Siglas).toLowerCase().includes(this.searchRequisito.toLowerCase())
      || item.Clave == this.searchRequisito
  }

  drawDiagram(sel: any) {
    setTimeout(() => {
      const new_element = document.getElementById("drawflow");
      if (new_element) {
        this.editor = new Drawflow(new_element);
        this.editor.reroute = true;
        this.editor.start();
        this.editor.on('nodeSelected', (id: any) => { this.updateNodesList(); });
        this.editor.on('nodeRemoved', (id: any) => { this.updateNodesList(); });
        this.editor.on('nodeDataChanged', (id: any) => { this.updateNodesList(); });
        this.editor.on('keydown', (id: any) => { setTimeout(() => { this.updateNodesList(); }, 1); });
        const equiv: any = {};
        const nodes = [];
        const relations = [];
        if (sel && sel.Etapas) {
          for (let etapa of sel.Etapas) {
            if (etapa.Activo) {
              nodes.push(etapa);
              if (etapa.FlujoEtapas) {
                for (let flujo of etapa.FlujoEtapas) {
                  if (flujo.Activo) {
                    relations.push(flujo);
                  }
                }
              }
            }
          }
          const machine = digl({ width: 200, height: 98, orientation: 'horizontal' });
          const _n = nodes.map((n: any) => { return { id: '' + n.Clave } });
          const _r = relations.map((r: any) => { return { source: '' + r.IdEtapa, target: '' + r.IdEtapaSiguiente } })
          const positionedNodes: any[] = machine.positions(_n[0]?.id || '', _n, _r);
          let c = 1;
          for (let etapa of nodes) {
            const n = positionedNodes.find((i) => {
              return i.id == `${etapa.Clave}`
            });
            equiv[etapa.Clave] = this.addNodeToDrawFlow('etapa', (n ? n.x : 0), (n ? n.y + 49 : (c * 98) + (15 * c) + 49), etapa.Clave, etapa.Nombre, etapa.Siglas);
            if (!n) c++;
          }
          for (let rel of relations) {
            this.editor.addConnection(equiv[rel.IdEtapa], equiv[rel.IdEtapaSiguiente], 'output_1', 'input_1');
          }
          this.updateNodesList();
        }
      }
    }, 0);
  }

  addNodeToDrawFlow(name: any, pos_x: any, pos_y: any, id = undefined, nombre = '', siglas = '') {
    if (this.editor.editor_mode === 'fixed') { return; }
    let factorX = (this.editor.precanvas.clientWidth * this.editor.zoom);
    let factorY = (this.editor.precanvas.clientHeight * this.editor.zoom);
    pos_x = (pos_x / this.editor.zoom)
      - (this.editor.precanvas.getBoundingClientRect().x * (this.editor.precanvas.clientWidth / factorX))
      + (73 / this.editor.zoom); // margin correction
    pos_y = (pos_y / this.editor.zoom)
      - (this.editor.precanvas.getBoundingClientRect().y * (this.editor.precanvas.clientHeight / factorY))
      + (226 / this.editor.zoom);// margin correction
    switch (name) {
      case 'etapa':
        const template = `
        <div class="inputs-box">
            <input type="text" placeholder="Siglas" df-siglas>
            <input type="text" placeholder="Etapa" df-nombre>
        </div>`;
        return this.editor.addNode('etapa', 1, 1, pos_x, pos_y, 'etapa', { id, nombre, siglas }, template);
      default:
    }
    this.n.setValue('');
    this.s.setValue('');
  }

  configFormControlControl(fc: any, name: string) {
    let c = (<FormGroup>fc.controls.UsuarioConfiguracionEtapas).get(name);
    if (!c) {
      let val = false;
      if (!fc.user) {
        fc.user = fc.item;
      }
      let etapas: any[] = fc.user.UsuarioConfiguracionEtapas;
      if (etapas) {
        etapas = etapas
          .filter((e: any) => !!e.Activo)
          .map((e: any) => e.Etapa.Nombre);
        val = etapas.includes(name);
      }
      c = this.formBuilder.control(val);
      (<FormGroup>fc.controls.UsuarioConfiguracionEtapas).addControl(name, c);
    }
    return name;
  }

  addNew() {
    this.addNodeToDrawFlow('etapa', 50, 100, undefined, this.n.value, this.s.value);
    this.updateNodesList();
    this.n.setValue('');
    this.s.setValue('');
  }

  downloadImage() {
    var node: any = document.getElementsByClassName('drawflow')[0];
    htmlToImage.toPng(node)
      .then((dataUrl: any) => {
        fileSaverSave(dataUrl, `export.${this.extraData?.Nombre}.png`, { type: 'png' } as any);
      })
      .catch((error: any) => {
        console.error('oops, something went wrong!', error);
      });
  }

}
