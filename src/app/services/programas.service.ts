import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgramasService {

  constructor(private _http: HttpClient) { }
  save(data: any) {
    return this._http.post(`/v1/api/programa/`, data);
  }

  update(data: any) {
    return this._http.put(`/v1/api/programa/${data.Clave}`, data);
  }

  status(newStat: string, data: any) {
    return this._http.put(`/v1/api/programa/${data.Clave}/status`, { IdEstatusPrograma: newStat });
  }

  configure(data: any) {
    return this._http.put(`/v1/api/programa/${data.Clave}/configure`, data);
  }

  delete(data: any) {
    return this._http.delete(`/v1/api/programa/${data.Clave}`);
  }
}
