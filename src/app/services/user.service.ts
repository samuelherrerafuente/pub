import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthorizationService } from './authorization.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private _http: HttpClient, private auth: AuthorizationService, private router: Router) { }

  logout() {
    this.router.navigate(['login']);
    this.auth.logoutEffect();
    return this._http.get(`/v1/api/logout`);
  }

  login(user: string, pass: string, token: string) {
    return this._http.post(`/v1/api/user/${user}/login`,
      { Password: pass, captchaRes: token });
  }

  recover(email: string, token: string) {
    return this._http.post(`/v1/api/user/reestablecer`, {
      captchaRes: token,
      Email: email
    });
  }

  reset(pass: string, token: string, captchaToken: string) {
    return this._http.post(`/v1/api/user/resetPassword/${token}`, {
      captchaRes: captchaToken,
      Password: pass
    });
  }

  whoami() {
    return this.auth.whoami();
  }

  changePass(user: string, pass: string) {
    return this._http.put(`/v1/api/user/${user}/pass`,
      { Password: pass });
  }

  save(data: any) {
    return this._http.post(`/v1/api/user/`, data);
  }

  update(data: any) {
    return this._http.put(`/v1/api/user/${data.Clave}`, data);
  }

  delete(data: any) {
    return this._http.delete(`/v1/api/user/${data.Clave}`);
  }

}
