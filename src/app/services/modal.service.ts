import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { ModalComponent } from '../common/modal/modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private closeSubject$ = new Subject<any>();
  public modalClose$ = this.closeSubject$.asObservable();
  private modals: ModalComponent[] = [];
  private openedModals = 0;
  constructor() { }

  add(modal: any) {
    this.modals.push(modal);
  }

  remove(id: string) {
    this.modals = this.modals.filter(x => x.id !== id);
  }

  open(id: string, extraData?: any) {
    const modal = this.modals.find(x => x.id === id);
    if (modal) {
      modal.open(900 + this.openedModals, extraData);
      this.openedModals++;
    }
  }

  close(id: any) {
    if (typeof id != 'string') {
      id = id.modalId;
    }
    const modal = this.modals.find(x => {
      return x.id === id;
    });
    if (modal) {
      this.openedModals--;
      this.closeSubject$.next({
        modalId: id,
        data: modal.close()
      });
    }
  }
}
