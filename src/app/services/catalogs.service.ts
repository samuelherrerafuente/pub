import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { map, share, tap } from 'rxjs/operators';
import { Page } from '../common/table/Page';
import { SearchService } from './search.service';

@Injectable({
  providedIn: 'root'
})
export class CatalogsService {

  private catalogs: { [id: string]: any } = {};

  constructor(private _http: HttpClient, private searchService: SearchService) { }

  getCatalog(catalog: string): Observable<any> {
    if (!catalog) {
      return of(null);
    }
    const cat = this.catalogs[catalog];
    if (!cat) {
      return this.load(catalog);
    }
    if (typeof cat == 'object' && Array.isArray(cat)) {
      return of(cat);
    }
    return cat;
  }

  getCatalogValue(catalog: string, id: any): Observable<any> {
    if (!catalog) {
      return of(null);
    }
    const cat = this.catalogs[catalog];
    if (!cat) {
      return this.load(catalog, id);
    }
    if (typeof cat == 'object' && Array.isArray(cat)) {
      const x = (<any[]>cat).filter((i) => i.Clave == id);
      return of(!id ? cat : (x && x.length) ? x[0] : undefined);
    }
    return cat;
  }

  reloadCatalog(catalog: string) {
    return this.load(catalog);
  }

  private load(catalog: string, id?: any): Observable<any> {
    if (catalog == 'Rol') {
      this.catalogs[catalog] = this._http.get(`/v1/api/rol/`)
        .pipe(
          tap((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
          }),
          share(),
          map((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
            const x = (<any[]>this.catalogs[catalog]).filter((i) => i.Clave == id);
            return !id ? this.catalogs[catalog] : (x && x.length) ? x[0] : undefined;
          })
        );
    } else if (catalog.startsWith('Usuarios:')) {
      const search = catalog.replace('Usuarios:', '');
      let searchObject = search ? {
        Clave: id,
        Usuario: search,
        Nombre: search,
        Apaterno: search,
        Amaterno: search
      } : {};
      const page = new Page();
      page.pageNumber = 0;
      page.size = 5;
      this.catalogs[catalog] = this.searchService.search('user', searchObject, page)
        .pipe(
          tap((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
          }),
          share(),
          map((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
            const x = (<any[]>this.catalogs[catalog]).filter((i) => i.Clave == id);
            return !id ? this.catalogs[catalog] : (x && x.length) ? x[0] : undefined;
          })
        );
    } else {
      this.catalogs[catalog] = this._http.get(`/v1/api/catalogo/${catalog}`)
        .pipe(
          tap((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
          }),
          share(),
          map((res: any) => {
            this.catalogs[catalog] = res[Object.keys(res)[0]];
            const x = (<any[]>this.catalogs[catalog]).filter((i) => i.Clave == id);
            return !id ? this.catalogs[catalog] : (x && x.length) ? x[0] : undefined;
          })
        );
    }
    return this.catalogs[catalog].pipe(share());;
  }
}
