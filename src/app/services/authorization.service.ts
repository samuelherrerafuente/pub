import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private me: any;
  private grants: any;
  constructor(private _http: HttpClient) { }

  logoutEffect() {
    this.me = null;
  }

  whoami() {
    if (this.me) {
      return of(this.me);
    }
    return this._http.get(`/v1/api/user/whoami`)
      .pipe(tap((res) => {
        this.me = res;
        this.initGrants();
      }));
  }

  isValid(grant: string | string[] | { grant: string, aux: string } | any[]): Observable<boolean> {
    if (this.me) {
      return of(this.validate(grant));
    }
    return this._http.get(`/v1/api/user/whoami`)
      .pipe(tap((res) => {
        this.me = res;
        this.initGrants();
      }), map((o: any): boolean => {
        return this.validate(grant);
      }));
  }

  isInvalid(grant: string | string[] | { grant: string, aux: string } | any[]): Observable<boolean> {
    if (this.me) {
      return of(!this.validate(grant));
    }
    return this._http.get(`/v1/api/user/whoami`)
      .pipe(tap((res) => {
        this.me = res;
        this.initGrants();
      }), map((o: any): boolean => {
        return !this.validate(grant);
      }));
  }

  private initGrants() {
    this.grants = {};
    if (this.me) {
      if (this.me.UsuarioPermisos) {
        (<any[]>this.me.UsuarioPermisos)
          .filter((i: any) => !!i.Activo && !!i.Permisos?.Activo)
          .forEach((i: any) => {
            this.grants[i.Permisos.Siglas] = true;
          });
      }
      if (this.me.RolPermisos) {
        (<any[]>this.me.RolPermisos)
          .filter((i: any) => !!i.Activo && !!i.Permisos?.Activo)
          .forEach((i: any) => {
            this.grants[i.Permisos.Siglas] = true;
          });
      }
      if (this.me.UsuarioConfiguracionProgramas) {
        (<any[]>this.me.UsuarioConfiguracionProgramas)
          .filter((i: any) => !!i.Activo)
          .forEach((i: any) => {
            if (!this.grants['PConsulta']) this.grants['PConsulta'] = {};
            if (!this.grants['PDesactivar']) this.grants['PDesactivar'] = {};
            if (!this.grants['PEdicion']) this.grants['PEdicion'] = {};
            if (!this.grants['PEstatus']) this.grants['PEstatus'] = {};
            if (!this.grants['PPermisos']) this.grants['PPermisos'] = {};
            this.grants['PConsulta'][i.IdPrograma] = !!i.PConsulta;
            this.grants['PDesactivar'][i.IdPrograma] = !!i.PDesactivar;
            this.grants['PEdicion'][i.IdPrograma] = !!i.PEdicion;
            this.grants['PEstatus'][i.IdPrograma] = !!i.PEstatus;
            this.grants['PPermisos'][i.IdPrograma] = !!i.PPermisos;
          });
      }
    }
  }

  private validate(grant: string | string[] | { grant: string, aux: string } | any[]): boolean {
    if (Array.isArray(grant)) {
      for (let _g of grant) {
        if (this._isGranted(_g)) {
          return true;
        }
      }
      return false;
    } else {
      return this._isGranted(grant);
    }
  }

  _isGranted(grant: any) {
    const keys = Object.keys(this.grants);
    let aux: any = 'PU__CONSIDER__ALL__GRANTS';
    if (typeof grant == 'object') {
      aux = grant['aux'];
      grant = grant['grant'];
    }
    const g = this.grants[grant];
    if (g) {
      if (aux != 'PU__CONSIDER__ALL__GRANTS') {
        return !!g[aux];
      } else {
        if (typeof g == 'object') {
          const gKeys = Object.keys(this.grants);
          return gKeys.length > 0;
        } else {
          return !!g;
        }
      }
    } else {
      for (let k of keys) {
        if (k.startsWith(grant)) {
          return true;
        }
      }
      return false;
    }
  }
}
