import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

// import { AuthenticationService } from '@app/_services';

@Injectable()
export class InterceptorService implements HttpInterceptor {
    constructor() { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const tokenS = localStorage['sessionToken'];
        if (tokenS) {
            let token = JSON.parse(tokenS);
            request = request.clone({
                setHeaders: {
                    Authorization: `Basic ${token.token}`
                }
            });
        }

        return next.handle(request);
    }
}
