import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RolesPermisosService {

  constructor(private _http: HttpClient) { }

  save(data: any) {
    return this._http.post(`/v1/api/rol/`,data);
  }

  update(data: any) {
    return this._http.put(`/v1/api/rol/${data.Clave}`,data);
  }

  delete(data: any) {
    return this._http.delete(`/v1/api/rol/${data.Clave}`);
  }
}
