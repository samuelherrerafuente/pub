import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Page } from '../common/table/Page';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private _http: HttpClient) { }

  search(entity: string, search?: any, paging?: Page) {
    let subscription;
    if (search) {
      subscription = this._http.post(
        `/v1/api/${entity}/search?limit=${paging?.size ? paging.size : ''}&offset=${paging?.pageNumber ? paging.pageNumber : ''}&sort=${paging?.order ? paging.order : ''}&order=${paging?.sort ? paging.sort : ''}`, search, {
        observe: 'response'
      });
    } else {
      subscription = this._http.get(`/v1/api/${entity}/?`, {
        observe: 'response'
      });
    }
    return subscription.pipe(map((response: any) => {
      return {
        rows: response.body[Object.keys(response.body)[0]],
        totalElements: response.headers.get('pagination-total')
      }
    }));
  }
}
