<?php

use Base\UsuarioConfiguracionEtapaQuery as BaseUsuarioConfiguracionEtapaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'usuario_configuracion_etapa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UsuarioConfiguracionEtapaQuery extends BaseUsuarioConfiguracionEtapaQuery
{

}
