<?php

use Base\Programa as BasePrograma;

/**
 * Skeleton subclass for representing a row from the 'programa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Programa extends BasePrograma
{

  /**
   * Initializes the collEtapas collection.
   *
   * By default this just sets the collEtapas collection to an empty array (like clearcollEtapas());
   * however, you may wish to override this method in your stub class to provide setting appropriate
   * to your application -- for example, setting the initial array to the values stored in database.
   *
   * @param      boolean $overrideExisting If set to true, the method call initializes
   *                                        the collection even if it is not empty
   *
   * @return void
   */
  public function initEtapas($overrideExisting = true)
  {
    if (null !== $this->collEtapas && !$overrideExisting) {
      return;
    }

    $this->collEtapas = EtapaQuery::create()->findByIdPrograma($this->getClave());
    $this->collEtapas->setModel('\Etapa');
  }

  /**
   * Initializes the collUsuarioConfiguracionProgramas collection.
   *
   * By default this just sets the collUsuarioConfiguracionProgramas collection to an empty array (like clearcollUsuarioConfiguracionProgramas());
   * however, you may wish to override this method in your stub class to provide setting appropriate
   * to your application -- for example, setting the initial array to the values stored in database.
   *
   * @param      boolean $overrideExisting If set to true, the method call initializes
   *                                        the collection even if it is not empty
   *
   * @return void
   */
  public function initUsuarioConfiguracionProgramas($overrideExisting = true)
  {
    if (null !== $this->collUsuarioConfiguracionProgramas && !$overrideExisting) {
      return;
    }

    $this->collUsuarioConfiguracionProgramas = UsuarioConfiguracionProgramaQuery::create()->findByIdPrograma($this->getClave());
    $this->collUsuarioConfiguracionProgramas->setModel('\UsuarioConfiguracionPrograma');
  }

      /**
     * Initializes the collProgramaRequisitos collection.
     *
     * By default this just sets the collProgramaRequisitos collection to an empty array (like clearcollProgramaRequisitos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProgramaRequisitos($overrideExisting = true)
    {
        if (null !== $this->collProgramaRequisitos && !$overrideExisting) {
            return;
        }

        $this->collProgramaRequisitos = ProgramaRequisitoQuery::create()->leftJoinWithEtapa()->findByIdPrograma($this->getClave());
        $this->collProgramaRequisitos->setModel('\ProgramaRequisito');
    }
}
