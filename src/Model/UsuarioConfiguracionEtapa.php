<?php

use Base\UsuarioConfiguracionEtapa as BaseUsuarioConfiguracionEtapa;

/**
 * Skeleton subclass for representing a row from the 'usuario_configuracion_etapa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UsuarioConfiguracionEtapa extends BaseUsuarioConfiguracionEtapa
{

}
