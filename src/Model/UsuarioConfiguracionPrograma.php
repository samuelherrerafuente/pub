<?php

use Base\UsuarioConfiguracionPrograma as BaseUsuarioConfiguracionPrograma;

/**
 * Skeleton subclass for representing a row from the 'usuario_configuracion_programa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UsuarioConfiguracionPrograma extends BaseUsuarioConfiguracionPrograma
{

      /**
     * Initializes the collUsuarioConfiguracionEtapas collection.
     *
     * By default this just sets the collUsuarioConfiguracionEtapas collection to an empty array (like clearcollUsuarioConfiguracionEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionEtapas($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionEtapas && !$overrideExisting) {
            return;
        }
        
        $this->collUsuarioConfiguracionEtapas = UsuarioConfiguracionEtapaQuery::create()->findByIdUsuarioConfiguracionPrograma($this->getClave());
        $this->collUsuarioConfiguracionEtapas->setModel('\UsuarioConfiguracionEtapa');
    }

}
