<?php

namespace Base;

use \FlujoEtapa as ChildFlujoEtapa;
use \FlujoEtapaQuery as ChildFlujoEtapaQuery;
use \Exception;
use \PDO;
use Map\FlujoEtapaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'flujo_etapa' table.
 *
 *
 *
 * @method     ChildFlujoEtapaQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildFlujoEtapaQuery orderByIdEtapa($order = Criteria::ASC) Order by the id_etapa column
 * @method     ChildFlujoEtapaQuery orderByIdEtapaSiguiente($order = Criteria::ASC) Order by the id_etapa_siguiente column
 * @method     ChildFlujoEtapaQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildFlujoEtapaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildFlujoEtapaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildFlujoEtapaQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 *
 * @method     ChildFlujoEtapaQuery groupByClave() Group by the clave column
 * @method     ChildFlujoEtapaQuery groupByIdEtapa() Group by the id_etapa column
 * @method     ChildFlujoEtapaQuery groupByIdEtapaSiguiente() Group by the id_etapa_siguiente column
 * @method     ChildFlujoEtapaQuery groupByActivo() Group by the activo column
 * @method     ChildFlujoEtapaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildFlujoEtapaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildFlujoEtapaQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 *
 * @method     ChildFlujoEtapaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFlujoEtapaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFlujoEtapaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFlujoEtapaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFlujoEtapaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFlujoEtapaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFlujoEtapaQuery leftJoinEtapaRelatedByIdEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the EtapaRelatedByIdEtapa relation
 * @method     ChildFlujoEtapaQuery rightJoinEtapaRelatedByIdEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EtapaRelatedByIdEtapa relation
 * @method     ChildFlujoEtapaQuery innerJoinEtapaRelatedByIdEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the EtapaRelatedByIdEtapa relation
 *
 * @method     ChildFlujoEtapaQuery joinWithEtapaRelatedByIdEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EtapaRelatedByIdEtapa relation
 *
 * @method     ChildFlujoEtapaQuery leftJoinWithEtapaRelatedByIdEtapa() Adds a LEFT JOIN clause and with to the query using the EtapaRelatedByIdEtapa relation
 * @method     ChildFlujoEtapaQuery rightJoinWithEtapaRelatedByIdEtapa() Adds a RIGHT JOIN clause and with to the query using the EtapaRelatedByIdEtapa relation
 * @method     ChildFlujoEtapaQuery innerJoinWithEtapaRelatedByIdEtapa() Adds a INNER JOIN clause and with to the query using the EtapaRelatedByIdEtapa relation
 *
 * @method     ChildFlujoEtapaQuery leftJoinEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a LEFT JOIN clause to the query using the EtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildFlujoEtapaQuery rightJoinEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildFlujoEtapaQuery innerJoinEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a INNER JOIN clause to the query using the EtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildFlujoEtapaQuery joinWithEtapaRelatedByIdEtapaSiguiente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildFlujoEtapaQuery leftJoinWithEtapaRelatedByIdEtapaSiguiente() Adds a LEFT JOIN clause and with to the query using the EtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildFlujoEtapaQuery rightJoinWithEtapaRelatedByIdEtapaSiguiente() Adds a RIGHT JOIN clause and with to the query using the EtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildFlujoEtapaQuery innerJoinWithEtapaRelatedByIdEtapaSiguiente() Adds a INNER JOIN clause and with to the query using the EtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildFlujoEtapaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildFlujoEtapaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildFlujoEtapaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildFlujoEtapaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildFlujoEtapaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFlujoEtapaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildFlujoEtapaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \EtapaQuery|\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFlujoEtapa|null findOne(ConnectionInterface $con = null) Return the first ChildFlujoEtapa matching the query
 * @method     ChildFlujoEtapa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFlujoEtapa matching the query, or a new ChildFlujoEtapa object populated from the query conditions when no match is found
 *
 * @method     ChildFlujoEtapa|null findOneByClave(int $clave) Return the first ChildFlujoEtapa filtered by the clave column
 * @method     ChildFlujoEtapa|null findOneByIdEtapa(int $id_etapa) Return the first ChildFlujoEtapa filtered by the id_etapa column
 * @method     ChildFlujoEtapa|null findOneByIdEtapaSiguiente(int $id_etapa_siguiente) Return the first ChildFlujoEtapa filtered by the id_etapa_siguiente column
 * @method     ChildFlujoEtapa|null findOneByActivo(int $activo) Return the first ChildFlujoEtapa filtered by the activo column
 * @method     ChildFlujoEtapa|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildFlujoEtapa filtered by the fecha_creacion column
 * @method     ChildFlujoEtapa|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildFlujoEtapa filtered by the fecha_modificacion column
 * @method     ChildFlujoEtapa|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildFlujoEtapa filtered by the id_usuario_modificacion column *

 * @method     ChildFlujoEtapa requirePk($key, ConnectionInterface $con = null) Return the ChildFlujoEtapa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOne(ConnectionInterface $con = null) Return the first ChildFlujoEtapa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFlujoEtapa requireOneByClave(int $clave) Return the first ChildFlujoEtapa filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByIdEtapa(int $id_etapa) Return the first ChildFlujoEtapa filtered by the id_etapa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByIdEtapaSiguiente(int $id_etapa_siguiente) Return the first ChildFlujoEtapa filtered by the id_etapa_siguiente column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByActivo(int $activo) Return the first ChildFlujoEtapa filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildFlujoEtapa filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildFlujoEtapa filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFlujoEtapa requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildFlujoEtapa filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFlujoEtapa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFlujoEtapa objects based on current ModelCriteria
 * @method     ChildFlujoEtapa[]|ObjectCollection findByClave(int $clave) Return ChildFlujoEtapa objects filtered by the clave column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByIdEtapa(int $id_etapa) Return ChildFlujoEtapa objects filtered by the id_etapa column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByIdEtapaSiguiente(int $id_etapa_siguiente) Return ChildFlujoEtapa objects filtered by the id_etapa_siguiente column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByActivo(int $activo) Return ChildFlujoEtapa objects filtered by the activo column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildFlujoEtapa objects filtered by the fecha_creacion column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildFlujoEtapa objects filtered by the fecha_modificacion column
 * @method     ChildFlujoEtapa[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildFlujoEtapa objects filtered by the id_usuario_modificacion column
 * @method     ChildFlujoEtapa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FlujoEtapaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\FlujoEtapaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\FlujoEtapa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFlujoEtapaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFlujoEtapaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFlujoEtapaQuery) {
            return $criteria;
        }
        $query = new ChildFlujoEtapaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFlujoEtapa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FlujoEtapaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFlujoEtapa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, id_etapa, id_etapa_siguiente, activo, fecha_creacion, fecha_modificacion, id_usuario_modificacion FROM flujo_etapa WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFlujoEtapa $obj */
            $obj = new ChildFlujoEtapa();
            $obj->hydrate($row);
            FlujoEtapaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFlujoEtapa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the id_etapa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtapa(1234); // WHERE id_etapa = 1234
     * $query->filterByIdEtapa(array(12, 34)); // WHERE id_etapa IN (12, 34)
     * $query->filterByIdEtapa(array('min' => 12)); // WHERE id_etapa > 12
     * </code>
     *
     * @see       filterByEtapaRelatedByIdEtapa()
     *
     * @param     mixed $idEtapa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByIdEtapa($idEtapa = null, $comparison = null)
    {
        if (is_array($idEtapa)) {
            $useMinMax = false;
            if (isset($idEtapa['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA, $idEtapa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtapa['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA, $idEtapa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA, $idEtapa, $comparison);
    }

    /**
     * Filter the query on the id_etapa_siguiente column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtapaSiguiente(1234); // WHERE id_etapa_siguiente = 1234
     * $query->filterByIdEtapaSiguiente(array(12, 34)); // WHERE id_etapa_siguiente IN (12, 34)
     * $query->filterByIdEtapaSiguiente(array('min' => 12)); // WHERE id_etapa_siguiente > 12
     * </code>
     *
     * @see       filterByEtapaRelatedByIdEtapaSiguiente()
     *
     * @param     mixed $idEtapaSiguiente The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByIdEtapaSiguiente($idEtapaSiguiente = null, $comparison = null)
    {
        if (is_array($idEtapaSiguiente)) {
            $useMinMax = false;
            if (isset($idEtapaSiguiente['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $idEtapaSiguiente['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtapaSiguiente['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $idEtapaSiguiente['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $idEtapaSiguiente, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query by a related \Etapa object
     *
     * @param \Etapa|ObjectCollection $etapa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByEtapaRelatedByIdEtapa($etapa, $comparison = null)
    {
        if ($etapa instanceof \Etapa) {
            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA, $etapa->getClave(), $comparison);
        } elseif ($etapa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA, $etapa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEtapaRelatedByIdEtapa() only accepts arguments of type \Etapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EtapaRelatedByIdEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function joinEtapaRelatedByIdEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EtapaRelatedByIdEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EtapaRelatedByIdEtapa');
        }

        return $this;
    }

    /**
     * Use the EtapaRelatedByIdEtapa relation Etapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EtapaQuery A secondary query class using the current class as primary query
     */
    public function useEtapaRelatedByIdEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtapaRelatedByIdEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EtapaRelatedByIdEtapa', '\EtapaQuery');
    }

    /**
     * Use the EtapaRelatedByIdEtapa relation Etapa object
     *
     * @param callable(\EtapaQuery):\EtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtapaRelatedByIdEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtapaRelatedByIdEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Etapa object
     *
     * @param \Etapa|ObjectCollection $etapa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByEtapaRelatedByIdEtapaSiguiente($etapa, $comparison = null)
    {
        if ($etapa instanceof \Etapa) {
            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $etapa->getClave(), $comparison);
        } elseif ($etapa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $etapa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEtapaRelatedByIdEtapaSiguiente() only accepts arguments of type \Etapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EtapaRelatedByIdEtapaSiguiente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function joinEtapaRelatedByIdEtapaSiguiente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EtapaRelatedByIdEtapaSiguiente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EtapaRelatedByIdEtapaSiguiente');
        }

        return $this;
    }

    /**
     * Use the EtapaRelatedByIdEtapaSiguiente relation Etapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EtapaQuery A secondary query class using the current class as primary query
     */
    public function useEtapaRelatedByIdEtapaSiguienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtapaRelatedByIdEtapaSiguiente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EtapaRelatedByIdEtapaSiguiente', '\EtapaQuery');
    }

    /**
     * Use the EtapaRelatedByIdEtapaSiguiente relation Etapa object
     *
     * @param callable(\EtapaQuery):\EtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtapaRelatedByIdEtapaSiguienteQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtapaRelatedByIdEtapaSiguienteQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFlujoEtapa $flujoEtapa Object to remove from the list of results
     *
     * @return $this|ChildFlujoEtapaQuery The current query, for fluid interface
     */
    public function prune($flujoEtapa = null)
    {
        if ($flujoEtapa) {
            $this->addUsingAlias(FlujoEtapaTableMap::COL_CLAVE, $flujoEtapa->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the flujo_etapa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FlujoEtapaTableMap::clearInstancePool();
            FlujoEtapaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FlujoEtapaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FlujoEtapaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FlujoEtapaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // FlujoEtapaQuery
