<?php

namespace Base;

use \TipoDistribucion as ChildTipoDistribucion;
use \TipoDistribucionQuery as ChildTipoDistribucionQuery;
use \Exception;
use \PDO;
use Map\TipoDistribucionTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tipo_distribucion' table.
 *
 *
 *
 * @method     ChildTipoDistribucionQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildTipoDistribucionQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildTipoDistribucionQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 *
 * @method     ChildTipoDistribucionQuery groupByClave() Group by the clave column
 * @method     ChildTipoDistribucionQuery groupByNombre() Group by the nombre column
 * @method     ChildTipoDistribucionQuery groupByDescripcion() Group by the descripcion column
 *
 * @method     ChildTipoDistribucionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTipoDistribucionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTipoDistribucionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTipoDistribucionQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTipoDistribucionQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTipoDistribucionQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTipoDistribucionQuery leftJoinBeneficio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Beneficio relation
 * @method     ChildTipoDistribucionQuery rightJoinBeneficio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Beneficio relation
 * @method     ChildTipoDistribucionQuery innerJoinBeneficio($relationAlias = null) Adds a INNER JOIN clause to the query using the Beneficio relation
 *
 * @method     ChildTipoDistribucionQuery joinWithBeneficio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Beneficio relation
 *
 * @method     ChildTipoDistribucionQuery leftJoinWithBeneficio() Adds a LEFT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildTipoDistribucionQuery rightJoinWithBeneficio() Adds a RIGHT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildTipoDistribucionQuery innerJoinWithBeneficio() Adds a INNER JOIN clause and with to the query using the Beneficio relation
 *
 * @method     \BeneficioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTipoDistribucion|null findOne(ConnectionInterface $con = null) Return the first ChildTipoDistribucion matching the query
 * @method     ChildTipoDistribucion findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTipoDistribucion matching the query, or a new ChildTipoDistribucion object populated from the query conditions when no match is found
 *
 * @method     ChildTipoDistribucion|null findOneByClave(int $clave) Return the first ChildTipoDistribucion filtered by the clave column
 * @method     ChildTipoDistribucion|null findOneByNombre(string $nombre) Return the first ChildTipoDistribucion filtered by the nombre column
 * @method     ChildTipoDistribucion|null findOneByDescripcion(string $descripcion) Return the first ChildTipoDistribucion filtered by the descripcion column *

 * @method     ChildTipoDistribucion requirePk($key, ConnectionInterface $con = null) Return the ChildTipoDistribucion by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoDistribucion requireOne(ConnectionInterface $con = null) Return the first ChildTipoDistribucion matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoDistribucion requireOneByClave(int $clave) Return the first ChildTipoDistribucion filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoDistribucion requireOneByNombre(string $nombre) Return the first ChildTipoDistribucion filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoDistribucion requireOneByDescripcion(string $descripcion) Return the first ChildTipoDistribucion filtered by the descripcion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoDistribucion[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTipoDistribucion objects based on current ModelCriteria
 * @method     ChildTipoDistribucion[]|ObjectCollection findByClave(int $clave) Return ChildTipoDistribucion objects filtered by the clave column
 * @method     ChildTipoDistribucion[]|ObjectCollection findByNombre(string $nombre) Return ChildTipoDistribucion objects filtered by the nombre column
 * @method     ChildTipoDistribucion[]|ObjectCollection findByDescripcion(string $descripcion) Return ChildTipoDistribucion objects filtered by the descripcion column
 * @method     ChildTipoDistribucion[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TipoDistribucionQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TipoDistribucionQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\TipoDistribucion', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTipoDistribucionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTipoDistribucionQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTipoDistribucionQuery) {
            return $criteria;
        }
        $query = new ChildTipoDistribucionQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTipoDistribucion|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TipoDistribucionTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TipoDistribucionTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTipoDistribucion A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, nombre, descripcion FROM tipo_distribucion WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTipoDistribucion $obj */
            $obj = new ChildTipoDistribucion();
            $obj->hydrate($row);
            TipoDistribucionTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTipoDistribucion|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoDistribucionTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%', Criteria::LIKE); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoDistribucionTableMap::COL_DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query by a related \Beneficio object
     *
     * @param \Beneficio|ObjectCollection $beneficio the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function filterByBeneficio($beneficio, $comparison = null)
    {
        if ($beneficio instanceof \Beneficio) {
            return $this
                ->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $beneficio->getIdTipoDistribucion(), $comparison);
        } elseif ($beneficio instanceof ObjectCollection) {
            return $this
                ->useBeneficioQuery()
                ->filterByPrimaryKeys($beneficio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBeneficio() only accepts arguments of type \Beneficio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Beneficio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function joinBeneficio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Beneficio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Beneficio');
        }

        return $this;
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BeneficioQuery A secondary query class using the current class as primary query
     */
    public function useBeneficioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBeneficio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Beneficio', '\BeneficioQuery');
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @param callable(\BeneficioQuery):\BeneficioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withBeneficioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useBeneficioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTipoDistribucion $tipoDistribucion Object to remove from the list of results
     *
     * @return $this|ChildTipoDistribucionQuery The current query, for fluid interface
     */
    public function prune($tipoDistribucion = null)
    {
        if ($tipoDistribucion) {
            $this->addUsingAlias(TipoDistribucionTableMap::COL_CLAVE, $tipoDistribucion->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tipo_distribucion table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoDistribucionTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TipoDistribucionTableMap::clearInstancePool();
            TipoDistribucionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoDistribucionTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TipoDistribucionTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TipoDistribucionTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TipoDistribucionTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TipoDistribucionQuery
