<?php

namespace Base;

use \ProgramaRequisito as ChildProgramaRequisito;
use \ProgramaRequisitoQuery as ChildProgramaRequisitoQuery;
use \Exception;
use \PDO;
use Map\ProgramaRequisitoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'programa_requisito' table.
 *
 *
 *
 * @method     ChildProgramaRequisitoQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildProgramaRequisitoQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildProgramaRequisitoQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method     ChildProgramaRequisitoQuery orderByIdPrograma($order = Criteria::ASC) Order by the id_programa column
 * @method     ChildProgramaRequisitoQuery orderByIdEtapa($order = Criteria::ASC) Order by the id_etapa column
 * @method     ChildProgramaRequisitoQuery orderByIdTipoRequisito($order = Criteria::ASC) Order by the id_tipo_requisito column
 * @method     ChildProgramaRequisitoQuery orderByObligatorio($order = Criteria::ASC) Order by the obligatorio column
 * @method     ChildProgramaRequisitoQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildProgramaRequisitoQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildProgramaRequisitoQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildProgramaRequisitoQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 *
 * @method     ChildProgramaRequisitoQuery groupByClave() Group by the clave column
 * @method     ChildProgramaRequisitoQuery groupByNombre() Group by the nombre column
 * @method     ChildProgramaRequisitoQuery groupByDescripcion() Group by the descripcion column
 * @method     ChildProgramaRequisitoQuery groupByIdPrograma() Group by the id_programa column
 * @method     ChildProgramaRequisitoQuery groupByIdEtapa() Group by the id_etapa column
 * @method     ChildProgramaRequisitoQuery groupByIdTipoRequisito() Group by the id_tipo_requisito column
 * @method     ChildProgramaRequisitoQuery groupByObligatorio() Group by the obligatorio column
 * @method     ChildProgramaRequisitoQuery groupByActivo() Group by the activo column
 * @method     ChildProgramaRequisitoQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildProgramaRequisitoQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildProgramaRequisitoQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 *
 * @method     ChildProgramaRequisitoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProgramaRequisitoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProgramaRequisitoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProgramaRequisitoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProgramaRequisitoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProgramaRequisitoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProgramaRequisitoQuery leftJoinEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Etapa relation
 * @method     ChildProgramaRequisitoQuery rightJoinEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Etapa relation
 * @method     ChildProgramaRequisitoQuery innerJoinEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the Etapa relation
 *
 * @method     ChildProgramaRequisitoQuery joinWithEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Etapa relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinWithEtapa() Adds a LEFT JOIN clause and with to the query using the Etapa relation
 * @method     ChildProgramaRequisitoQuery rightJoinWithEtapa() Adds a RIGHT JOIN clause and with to the query using the Etapa relation
 * @method     ChildProgramaRequisitoQuery innerJoinWithEtapa() Adds a INNER JOIN clause and with to the query using the Etapa relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildProgramaRequisitoQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildProgramaRequisitoQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildProgramaRequisitoQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildProgramaRequisitoQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildProgramaRequisitoQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinTipoRequisito($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoRequisito relation
 * @method     ChildProgramaRequisitoQuery rightJoinTipoRequisito($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoRequisito relation
 * @method     ChildProgramaRequisitoQuery innerJoinTipoRequisito($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoRequisito relation
 *
 * @method     ChildProgramaRequisitoQuery joinWithTipoRequisito($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoRequisito relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinWithTipoRequisito() Adds a LEFT JOIN clause and with to the query using the TipoRequisito relation
 * @method     ChildProgramaRequisitoQuery rightJoinWithTipoRequisito() Adds a RIGHT JOIN clause and with to the query using the TipoRequisito relation
 * @method     ChildProgramaRequisitoQuery innerJoinWithTipoRequisito() Adds a INNER JOIN clause and with to the query using the TipoRequisito relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildProgramaRequisitoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildProgramaRequisitoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildProgramaRequisitoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildProgramaRequisitoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildProgramaRequisitoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildProgramaRequisitoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \EtapaQuery|\ProgramaQuery|\TipoRequisitoQuery|\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProgramaRequisito|null findOne(ConnectionInterface $con = null) Return the first ChildProgramaRequisito matching the query
 * @method     ChildProgramaRequisito findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProgramaRequisito matching the query, or a new ChildProgramaRequisito object populated from the query conditions when no match is found
 *
 * @method     ChildProgramaRequisito|null findOneByClave(int $clave) Return the first ChildProgramaRequisito filtered by the clave column
 * @method     ChildProgramaRequisito|null findOneByNombre(string $nombre) Return the first ChildProgramaRequisito filtered by the nombre column
 * @method     ChildProgramaRequisito|null findOneByDescripcion(string $descripcion) Return the first ChildProgramaRequisito filtered by the descripcion column
 * @method     ChildProgramaRequisito|null findOneByIdPrograma(int $id_programa) Return the first ChildProgramaRequisito filtered by the id_programa column
 * @method     ChildProgramaRequisito|null findOneByIdEtapa(int $id_etapa) Return the first ChildProgramaRequisito filtered by the id_etapa column
 * @method     ChildProgramaRequisito|null findOneByIdTipoRequisito(int $id_tipo_requisito) Return the first ChildProgramaRequisito filtered by the id_tipo_requisito column
 * @method     ChildProgramaRequisito|null findOneByObligatorio(int $obligatorio) Return the first ChildProgramaRequisito filtered by the obligatorio column
 * @method     ChildProgramaRequisito|null findOneByActivo(int $activo) Return the first ChildProgramaRequisito filtered by the activo column
 * @method     ChildProgramaRequisito|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildProgramaRequisito filtered by the fecha_creacion column
 * @method     ChildProgramaRequisito|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildProgramaRequisito filtered by the fecha_modificacion column
 * @method     ChildProgramaRequisito|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildProgramaRequisito filtered by the id_usuario_modificacion column *

 * @method     ChildProgramaRequisito requirePk($key, ConnectionInterface $con = null) Return the ChildProgramaRequisito by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOne(ConnectionInterface $con = null) Return the first ChildProgramaRequisito matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProgramaRequisito requireOneByClave(int $clave) Return the first ChildProgramaRequisito filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByNombre(string $nombre) Return the first ChildProgramaRequisito filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByDescripcion(string $descripcion) Return the first ChildProgramaRequisito filtered by the descripcion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByIdPrograma(int $id_programa) Return the first ChildProgramaRequisito filtered by the id_programa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByIdEtapa(int $id_etapa) Return the first ChildProgramaRequisito filtered by the id_etapa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByIdTipoRequisito(int $id_tipo_requisito) Return the first ChildProgramaRequisito filtered by the id_tipo_requisito column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByObligatorio(int $obligatorio) Return the first ChildProgramaRequisito filtered by the obligatorio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByActivo(int $activo) Return the first ChildProgramaRequisito filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildProgramaRequisito filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildProgramaRequisito filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProgramaRequisito requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildProgramaRequisito filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProgramaRequisito[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProgramaRequisito objects based on current ModelCriteria
 * @method     ChildProgramaRequisito[]|ObjectCollection findByClave(int $clave) Return ChildProgramaRequisito objects filtered by the clave column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByNombre(string $nombre) Return ChildProgramaRequisito objects filtered by the nombre column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByDescripcion(string $descripcion) Return ChildProgramaRequisito objects filtered by the descripcion column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByIdPrograma(int $id_programa) Return ChildProgramaRequisito objects filtered by the id_programa column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByIdEtapa(int $id_etapa) Return ChildProgramaRequisito objects filtered by the id_etapa column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByIdTipoRequisito(int $id_tipo_requisito) Return ChildProgramaRequisito objects filtered by the id_tipo_requisito column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByObligatorio(int $obligatorio) Return ChildProgramaRequisito objects filtered by the obligatorio column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByActivo(int $activo) Return ChildProgramaRequisito objects filtered by the activo column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildProgramaRequisito objects filtered by the fecha_creacion column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildProgramaRequisito objects filtered by the fecha_modificacion column
 * @method     ChildProgramaRequisito[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildProgramaRequisito objects filtered by the id_usuario_modificacion column
 * @method     ChildProgramaRequisito[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProgramaRequisitoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ProgramaRequisitoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\ProgramaRequisito', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProgramaRequisitoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProgramaRequisitoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProgramaRequisitoQuery) {
            return $criteria;
        }
        $query = new ChildProgramaRequisitoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProgramaRequisito|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProgramaRequisitoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProgramaRequisitoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaRequisito A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, nombre, descripcion, id_programa, id_etapa, id_tipo_requisito, obligatorio, activo, fecha_creacion, fecha_modificacion, id_usuario_modificacion FROM programa_requisito WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProgramaRequisito $obj */
            $obj = new ChildProgramaRequisito();
            $obj->hydrate($row);
            ProgramaRequisitoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProgramaRequisito|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%', Criteria::LIKE); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the id_programa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrograma(1234); // WHERE id_programa = 1234
     * $query->filterByIdPrograma(array(12, 34)); // WHERE id_programa IN (12, 34)
     * $query->filterByIdPrograma(array('min' => 12)); // WHERE id_programa > 12
     * </code>
     *
     * @see       filterByPrograma()
     *
     * @param     mixed $idPrograma The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByIdPrograma($idPrograma = null, $comparison = null)
    {
        if (is_array($idPrograma)) {
            $useMinMax = false;
            if (isset($idPrograma['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_PROGRAMA, $idPrograma['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrograma['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_PROGRAMA, $idPrograma['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_PROGRAMA, $idPrograma, $comparison);
    }

    /**
     * Filter the query on the id_etapa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEtapa(1234); // WHERE id_etapa = 1234
     * $query->filterByIdEtapa(array(12, 34)); // WHERE id_etapa IN (12, 34)
     * $query->filterByIdEtapa(array('min' => 12)); // WHERE id_etapa > 12
     * </code>
     *
     * @see       filterByEtapa()
     *
     * @param     mixed $idEtapa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByIdEtapa($idEtapa = null, $comparison = null)
    {
        if (is_array($idEtapa)) {
            $useMinMax = false;
            if (isset($idEtapa['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_ETAPA, $idEtapa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEtapa['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_ETAPA, $idEtapa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_ETAPA, $idEtapa, $comparison);
    }

    /**
     * Filter the query on the id_tipo_requisito column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTipoRequisito(1234); // WHERE id_tipo_requisito = 1234
     * $query->filterByIdTipoRequisito(array(12, 34)); // WHERE id_tipo_requisito IN (12, 34)
     * $query->filterByIdTipoRequisito(array('min' => 12)); // WHERE id_tipo_requisito > 12
     * </code>
     *
     * @see       filterByTipoRequisito()
     *
     * @param     mixed $idTipoRequisito The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByIdTipoRequisito($idTipoRequisito = null, $comparison = null)
    {
        if (is_array($idTipoRequisito)) {
            $useMinMax = false;
            if (isset($idTipoRequisito['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, $idTipoRequisito['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTipoRequisito['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, $idTipoRequisito['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, $idTipoRequisito, $comparison);
    }

    /**
     * Filter the query on the obligatorio column
     *
     * Example usage:
     * <code>
     * $query->filterByObligatorio(1234); // WHERE obligatorio = 1234
     * $query->filterByObligatorio(array(12, 34)); // WHERE obligatorio IN (12, 34)
     * $query->filterByObligatorio(array('min' => 12)); // WHERE obligatorio > 12
     * </code>
     *
     * @param     mixed $obligatorio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByObligatorio($obligatorio = null, $comparison = null)
    {
        if (is_array($obligatorio)) {
            $useMinMax = false;
            if (isset($obligatorio['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_OBLIGATORIO, $obligatorio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($obligatorio['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_OBLIGATORIO, $obligatorio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_OBLIGATORIO, $obligatorio, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query by a related \Etapa object
     *
     * @param \Etapa|ObjectCollection $etapa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByEtapa($etapa, $comparison = null)
    {
        if ($etapa instanceof \Etapa) {
            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_ETAPA, $etapa->getClave(), $comparison);
        } elseif ($etapa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_ETAPA, $etapa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEtapa() only accepts arguments of type \Etapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Etapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function joinEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Etapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Etapa');
        }

        return $this;
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EtapaQuery A secondary query class using the current class as primary query
     */
    public function useEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Etapa', '\EtapaQuery');
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @param callable(\EtapaQuery):\EtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_PROGRAMA, $programa->getClave(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_PROGRAMA, $programa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \TipoRequisito object
     *
     * @param \TipoRequisito|ObjectCollection $tipoRequisito The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByTipoRequisito($tipoRequisito, $comparison = null)
    {
        if ($tipoRequisito instanceof \TipoRequisito) {
            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, $tipoRequisito->getClave(), $comparison);
        } elseif ($tipoRequisito instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, $tipoRequisito->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByTipoRequisito() only accepts arguments of type \TipoRequisito or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoRequisito relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function joinTipoRequisito($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoRequisito');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoRequisito');
        }

        return $this;
    }

    /**
     * Use the TipoRequisito relation TipoRequisito object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TipoRequisitoQuery A secondary query class using the current class as primary query
     */
    public function useTipoRequisitoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoRequisito($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoRequisito', '\TipoRequisitoQuery');
    }

    /**
     * Use the TipoRequisito relation TipoRequisito object
     *
     * @param callable(\TipoRequisitoQuery):\TipoRequisitoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTipoRequisitoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTipoRequisitoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProgramaRequisito $programaRequisito Object to remove from the list of results
     *
     * @return $this|ChildProgramaRequisitoQuery The current query, for fluid interface
     */
    public function prune($programaRequisito = null)
    {
        if ($programaRequisito) {
            $this->addUsingAlias(ProgramaRequisitoTableMap::COL_CLAVE, $programaRequisito->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the programa_requisito table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaRequisitoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProgramaRequisitoTableMap::clearInstancePool();
            ProgramaRequisitoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaRequisitoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProgramaRequisitoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProgramaRequisitoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProgramaRequisitoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProgramaRequisitoQuery
