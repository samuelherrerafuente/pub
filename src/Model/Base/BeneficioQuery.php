<?php

namespace Base;

use \Beneficio as ChildBeneficio;
use \BeneficioQuery as ChildBeneficioQuery;
use \Exception;
use \PDO;
use Map\BeneficioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'beneficio' table.
 *
 *
 *
 * @method     ChildBeneficioQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildBeneficioQuery orderByIdTipoDistribucion($order = Criteria::ASC) Order by the id_tipo_distribucion column
 * @method     ChildBeneficioQuery orderByIdTipoBeneficio($order = Criteria::ASC) Order by the id_tipo_beneficio column
 * @method     ChildBeneficioQuery orderByIdTipoRecurso($order = Criteria::ASC) Order by the id_tipo_recurso column
 * @method     ChildBeneficioQuery orderByIdUnidadMedida($order = Criteria::ASC) Order by the id_unidad_medida column
 * @method     ChildBeneficioQuery orderByValor($order = Criteria::ASC) Order by the valor column
 * @method     ChildBeneficioQuery orderByCantidad($order = Criteria::ASC) Order by the cantidad column
 * @method     ChildBeneficioQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildBeneficioQuery orderByIdPrograma($order = Criteria::ASC) Order by the id_programa column
 * @method     ChildBeneficioQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildBeneficioQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildBeneficioQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 *
 * @method     ChildBeneficioQuery groupByClave() Group by the clave column
 * @method     ChildBeneficioQuery groupByIdTipoDistribucion() Group by the id_tipo_distribucion column
 * @method     ChildBeneficioQuery groupByIdTipoBeneficio() Group by the id_tipo_beneficio column
 * @method     ChildBeneficioQuery groupByIdTipoRecurso() Group by the id_tipo_recurso column
 * @method     ChildBeneficioQuery groupByIdUnidadMedida() Group by the id_unidad_medida column
 * @method     ChildBeneficioQuery groupByValor() Group by the valor column
 * @method     ChildBeneficioQuery groupByCantidad() Group by the cantidad column
 * @method     ChildBeneficioQuery groupByNombre() Group by the nombre column
 * @method     ChildBeneficioQuery groupByIdPrograma() Group by the id_programa column
 * @method     ChildBeneficioQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildBeneficioQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildBeneficioQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 *
 * @method     ChildBeneficioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildBeneficioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildBeneficioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildBeneficioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildBeneficioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildBeneficioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildBeneficioQuery leftJoinTipoDistribucion($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoDistribucion relation
 * @method     ChildBeneficioQuery rightJoinTipoDistribucion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoDistribucion relation
 * @method     ChildBeneficioQuery innerJoinTipoDistribucion($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoDistribucion relation
 *
 * @method     ChildBeneficioQuery joinWithTipoDistribucion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoDistribucion relation
 *
 * @method     ChildBeneficioQuery leftJoinWithTipoDistribucion() Adds a LEFT JOIN clause and with to the query using the TipoDistribucion relation
 * @method     ChildBeneficioQuery rightJoinWithTipoDistribucion() Adds a RIGHT JOIN clause and with to the query using the TipoDistribucion relation
 * @method     ChildBeneficioQuery innerJoinWithTipoDistribucion() Adds a INNER JOIN clause and with to the query using the TipoDistribucion relation
 *
 * @method     ChildBeneficioQuery leftJoinTipoBeneficio($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoBeneficio relation
 * @method     ChildBeneficioQuery rightJoinTipoBeneficio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoBeneficio relation
 * @method     ChildBeneficioQuery innerJoinTipoBeneficio($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoBeneficio relation
 *
 * @method     ChildBeneficioQuery joinWithTipoBeneficio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoBeneficio relation
 *
 * @method     ChildBeneficioQuery leftJoinWithTipoBeneficio() Adds a LEFT JOIN clause and with to the query using the TipoBeneficio relation
 * @method     ChildBeneficioQuery rightJoinWithTipoBeneficio() Adds a RIGHT JOIN clause and with to the query using the TipoBeneficio relation
 * @method     ChildBeneficioQuery innerJoinWithTipoBeneficio() Adds a INNER JOIN clause and with to the query using the TipoBeneficio relation
 *
 * @method     ChildBeneficioQuery leftJoinTipoRecurso($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoRecurso relation
 * @method     ChildBeneficioQuery rightJoinTipoRecurso($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoRecurso relation
 * @method     ChildBeneficioQuery innerJoinTipoRecurso($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoRecurso relation
 *
 * @method     ChildBeneficioQuery joinWithTipoRecurso($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoRecurso relation
 *
 * @method     ChildBeneficioQuery leftJoinWithTipoRecurso() Adds a LEFT JOIN clause and with to the query using the TipoRecurso relation
 * @method     ChildBeneficioQuery rightJoinWithTipoRecurso() Adds a RIGHT JOIN clause and with to the query using the TipoRecurso relation
 * @method     ChildBeneficioQuery innerJoinWithTipoRecurso() Adds a INNER JOIN clause and with to the query using the TipoRecurso relation
 *
 * @method     ChildBeneficioQuery leftJoinUnidadMedida($relationAlias = null) Adds a LEFT JOIN clause to the query using the UnidadMedida relation
 * @method     ChildBeneficioQuery rightJoinUnidadMedida($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UnidadMedida relation
 * @method     ChildBeneficioQuery innerJoinUnidadMedida($relationAlias = null) Adds a INNER JOIN clause to the query using the UnidadMedida relation
 *
 * @method     ChildBeneficioQuery joinWithUnidadMedida($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UnidadMedida relation
 *
 * @method     ChildBeneficioQuery leftJoinWithUnidadMedida() Adds a LEFT JOIN clause and with to the query using the UnidadMedida relation
 * @method     ChildBeneficioQuery rightJoinWithUnidadMedida() Adds a RIGHT JOIN clause and with to the query using the UnidadMedida relation
 * @method     ChildBeneficioQuery innerJoinWithUnidadMedida() Adds a INNER JOIN clause and with to the query using the UnidadMedida relation
 *
 * @method     ChildBeneficioQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildBeneficioQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildBeneficioQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildBeneficioQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildBeneficioQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildBeneficioQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildBeneficioQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     ChildBeneficioQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildBeneficioQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildBeneficioQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildBeneficioQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildBeneficioQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBeneficioQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildBeneficioQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \TipoDistribucionQuery|\TipoBeneficioQuery|\TipoRecursoQuery|\UnidadMedidaQuery|\ProgramaQuery|\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildBeneficio|null findOne(ConnectionInterface $con = null) Return the first ChildBeneficio matching the query
 * @method     ChildBeneficio findOneOrCreate(ConnectionInterface $con = null) Return the first ChildBeneficio matching the query, or a new ChildBeneficio object populated from the query conditions when no match is found
 *
 * @method     ChildBeneficio|null findOneByClave(int $clave) Return the first ChildBeneficio filtered by the clave column
 * @method     ChildBeneficio|null findOneByIdTipoDistribucion(int $id_tipo_distribucion) Return the first ChildBeneficio filtered by the id_tipo_distribucion column
 * @method     ChildBeneficio|null findOneByIdTipoBeneficio(int $id_tipo_beneficio) Return the first ChildBeneficio filtered by the id_tipo_beneficio column
 * @method     ChildBeneficio|null findOneByIdTipoRecurso(int $id_tipo_recurso) Return the first ChildBeneficio filtered by the id_tipo_recurso column
 * @method     ChildBeneficio|null findOneByIdUnidadMedida(int $id_unidad_medida) Return the first ChildBeneficio filtered by the id_unidad_medida column
 * @method     ChildBeneficio|null findOneByValor(string $valor) Return the first ChildBeneficio filtered by the valor column
 * @method     ChildBeneficio|null findOneByCantidad(int $cantidad) Return the first ChildBeneficio filtered by the cantidad column
 * @method     ChildBeneficio|null findOneByNombre(string $nombre) Return the first ChildBeneficio filtered by the nombre column
 * @method     ChildBeneficio|null findOneByIdPrograma(int $id_programa) Return the first ChildBeneficio filtered by the id_programa column
 * @method     ChildBeneficio|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildBeneficio filtered by the fecha_creacion column
 * @method     ChildBeneficio|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildBeneficio filtered by the fecha_modificacion column
 * @method     ChildBeneficio|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildBeneficio filtered by the id_usuario_modificacion column *

 * @method     ChildBeneficio requirePk($key, ConnectionInterface $con = null) Return the ChildBeneficio by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOne(ConnectionInterface $con = null) Return the first ChildBeneficio matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBeneficio requireOneByClave(int $clave) Return the first ChildBeneficio filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdTipoDistribucion(int $id_tipo_distribucion) Return the first ChildBeneficio filtered by the id_tipo_distribucion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdTipoBeneficio(int $id_tipo_beneficio) Return the first ChildBeneficio filtered by the id_tipo_beneficio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdTipoRecurso(int $id_tipo_recurso) Return the first ChildBeneficio filtered by the id_tipo_recurso column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdUnidadMedida(int $id_unidad_medida) Return the first ChildBeneficio filtered by the id_unidad_medida column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByValor(string $valor) Return the first ChildBeneficio filtered by the valor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByCantidad(int $cantidad) Return the first ChildBeneficio filtered by the cantidad column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByNombre(string $nombre) Return the first ChildBeneficio filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdPrograma(int $id_programa) Return the first ChildBeneficio filtered by the id_programa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildBeneficio filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildBeneficio filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildBeneficio requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildBeneficio filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildBeneficio[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildBeneficio objects based on current ModelCriteria
 * @method     ChildBeneficio[]|ObjectCollection findByClave(int $clave) Return ChildBeneficio objects filtered by the clave column
 * @method     ChildBeneficio[]|ObjectCollection findByIdTipoDistribucion(int $id_tipo_distribucion) Return ChildBeneficio objects filtered by the id_tipo_distribucion column
 * @method     ChildBeneficio[]|ObjectCollection findByIdTipoBeneficio(int $id_tipo_beneficio) Return ChildBeneficio objects filtered by the id_tipo_beneficio column
 * @method     ChildBeneficio[]|ObjectCollection findByIdTipoRecurso(int $id_tipo_recurso) Return ChildBeneficio objects filtered by the id_tipo_recurso column
 * @method     ChildBeneficio[]|ObjectCollection findByIdUnidadMedida(int $id_unidad_medida) Return ChildBeneficio objects filtered by the id_unidad_medida column
 * @method     ChildBeneficio[]|ObjectCollection findByValor(string $valor) Return ChildBeneficio objects filtered by the valor column
 * @method     ChildBeneficio[]|ObjectCollection findByCantidad(int $cantidad) Return ChildBeneficio objects filtered by the cantidad column
 * @method     ChildBeneficio[]|ObjectCollection findByNombre(string $nombre) Return ChildBeneficio objects filtered by the nombre column
 * @method     ChildBeneficio[]|ObjectCollection findByIdPrograma(int $id_programa) Return ChildBeneficio objects filtered by the id_programa column
 * @method     ChildBeneficio[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildBeneficio objects filtered by the fecha_creacion column
 * @method     ChildBeneficio[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildBeneficio objects filtered by the fecha_modificacion column
 * @method     ChildBeneficio[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildBeneficio objects filtered by the id_usuario_modificacion column
 * @method     ChildBeneficio[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class BeneficioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\BeneficioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Beneficio', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildBeneficioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildBeneficioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildBeneficioQuery) {
            return $criteria;
        }
        $query = new ChildBeneficioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildBeneficio|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(BeneficioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = BeneficioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficio A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, id_tipo_distribucion, id_tipo_beneficio, id_tipo_recurso, id_unidad_medida, valor, cantidad, nombre, id_programa, fecha_creacion, fecha_modificacion, id_usuario_modificacion FROM beneficio WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildBeneficio $obj */
            $obj = new ChildBeneficio();
            $obj->hydrate($row);
            BeneficioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildBeneficio|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the id_tipo_distribucion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTipoDistribucion(1234); // WHERE id_tipo_distribucion = 1234
     * $query->filterByIdTipoDistribucion(array(12, 34)); // WHERE id_tipo_distribucion IN (12, 34)
     * $query->filterByIdTipoDistribucion(array('min' => 12)); // WHERE id_tipo_distribucion > 12
     * </code>
     *
     * @see       filterByTipoDistribucion()
     *
     * @param     mixed $idTipoDistribucion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdTipoDistribucion($idTipoDistribucion = null, $comparison = null)
    {
        if (is_array($idTipoDistribucion)) {
            $useMinMax = false;
            if (isset($idTipoDistribucion['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, $idTipoDistribucion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTipoDistribucion['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, $idTipoDistribucion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, $idTipoDistribucion, $comparison);
    }

    /**
     * Filter the query on the id_tipo_beneficio column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTipoBeneficio(1234); // WHERE id_tipo_beneficio = 1234
     * $query->filterByIdTipoBeneficio(array(12, 34)); // WHERE id_tipo_beneficio IN (12, 34)
     * $query->filterByIdTipoBeneficio(array('min' => 12)); // WHERE id_tipo_beneficio > 12
     * </code>
     *
     * @see       filterByTipoBeneficio()
     *
     * @param     mixed $idTipoBeneficio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdTipoBeneficio($idTipoBeneficio = null, $comparison = null)
    {
        if (is_array($idTipoBeneficio)) {
            $useMinMax = false;
            if (isset($idTipoBeneficio['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_BENEFICIO, $idTipoBeneficio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTipoBeneficio['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_BENEFICIO, $idTipoBeneficio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_BENEFICIO, $idTipoBeneficio, $comparison);
    }

    /**
     * Filter the query on the id_tipo_recurso column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTipoRecurso(1234); // WHERE id_tipo_recurso = 1234
     * $query->filterByIdTipoRecurso(array(12, 34)); // WHERE id_tipo_recurso IN (12, 34)
     * $query->filterByIdTipoRecurso(array('min' => 12)); // WHERE id_tipo_recurso > 12
     * </code>
     *
     * @see       filterByTipoRecurso()
     *
     * @param     mixed $idTipoRecurso The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdTipoRecurso($idTipoRecurso = null, $comparison = null)
    {
        if (is_array($idTipoRecurso)) {
            $useMinMax = false;
            if (isset($idTipoRecurso['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_RECURSO, $idTipoRecurso['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTipoRecurso['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_RECURSO, $idTipoRecurso['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_RECURSO, $idTipoRecurso, $comparison);
    }

    /**
     * Filter the query on the id_unidad_medida column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUnidadMedida(1234); // WHERE id_unidad_medida = 1234
     * $query->filterByIdUnidadMedida(array(12, 34)); // WHERE id_unidad_medida IN (12, 34)
     * $query->filterByIdUnidadMedida(array('min' => 12)); // WHERE id_unidad_medida > 12
     * </code>
     *
     * @see       filterByUnidadMedida()
     *
     * @param     mixed $idUnidadMedida The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdUnidadMedida($idUnidadMedida = null, $comparison = null)
    {
        if (is_array($idUnidadMedida)) {
            $useMinMax = false;
            if (isset($idUnidadMedida['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, $idUnidadMedida['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUnidadMedida['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, $idUnidadMedida['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, $idUnidadMedida, $comparison);
    }

    /**
     * Filter the query on the valor column
     *
     * Example usage:
     * <code>
     * $query->filterByValor(1234); // WHERE valor = 1234
     * $query->filterByValor(array(12, 34)); // WHERE valor IN (12, 34)
     * $query->filterByValor(array('min' => 12)); // WHERE valor > 12
     * </code>
     *
     * @param     mixed $valor The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByValor($valor = null, $comparison = null)
    {
        if (is_array($valor)) {
            $useMinMax = false;
            if (isset($valor['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_VALOR, $valor['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($valor['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_VALOR, $valor['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_VALOR, $valor, $comparison);
    }

    /**
     * Filter the query on the cantidad column
     *
     * Example usage:
     * <code>
     * $query->filterByCantidad(1234); // WHERE cantidad = 1234
     * $query->filterByCantidad(array(12, 34)); // WHERE cantidad IN (12, 34)
     * $query->filterByCantidad(array('min' => 12)); // WHERE cantidad > 12
     * </code>
     *
     * @param     mixed $cantidad The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByCantidad($cantidad = null, $comparison = null)
    {
        if (is_array($cantidad)) {
            $useMinMax = false;
            if (isset($cantidad['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_CANTIDAD, $cantidad['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($cantidad['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_CANTIDAD, $cantidad['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_CANTIDAD, $cantidad, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the id_programa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrograma(1234); // WHERE id_programa = 1234
     * $query->filterByIdPrograma(array(12, 34)); // WHERE id_programa IN (12, 34)
     * $query->filterByIdPrograma(array('min' => 12)); // WHERE id_programa > 12
     * </code>
     *
     * @see       filterByPrograma()
     *
     * @param     mixed $idPrograma The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdPrograma($idPrograma = null, $comparison = null)
    {
        if (is_array($idPrograma)) {
            $useMinMax = false;
            if (isset($idPrograma['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_PROGRAMA, $idPrograma['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrograma['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_PROGRAMA, $idPrograma['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_PROGRAMA, $idPrograma, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query by a related \TipoDistribucion object
     *
     * @param \TipoDistribucion|ObjectCollection $tipoDistribucion The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByTipoDistribucion($tipoDistribucion, $comparison = null)
    {
        if ($tipoDistribucion instanceof \TipoDistribucion) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, $tipoDistribucion->getClave(), $comparison);
        } elseif ($tipoDistribucion instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, $tipoDistribucion->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByTipoDistribucion() only accepts arguments of type \TipoDistribucion or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoDistribucion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinTipoDistribucion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoDistribucion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoDistribucion');
        }

        return $this;
    }

    /**
     * Use the TipoDistribucion relation TipoDistribucion object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TipoDistribucionQuery A secondary query class using the current class as primary query
     */
    public function useTipoDistribucionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoDistribucion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoDistribucion', '\TipoDistribucionQuery');
    }

    /**
     * Use the TipoDistribucion relation TipoDistribucion object
     *
     * @param callable(\TipoDistribucionQuery):\TipoDistribucionQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTipoDistribucionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTipoDistribucionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \TipoBeneficio object
     *
     * @param \TipoBeneficio|ObjectCollection $tipoBeneficio The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByTipoBeneficio($tipoBeneficio, $comparison = null)
    {
        if ($tipoBeneficio instanceof \TipoBeneficio) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_BENEFICIO, $tipoBeneficio->getClave(), $comparison);
        } elseif ($tipoBeneficio instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_BENEFICIO, $tipoBeneficio->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByTipoBeneficio() only accepts arguments of type \TipoBeneficio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoBeneficio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinTipoBeneficio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoBeneficio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoBeneficio');
        }

        return $this;
    }

    /**
     * Use the TipoBeneficio relation TipoBeneficio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TipoBeneficioQuery A secondary query class using the current class as primary query
     */
    public function useTipoBeneficioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoBeneficio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoBeneficio', '\TipoBeneficioQuery');
    }

    /**
     * Use the TipoBeneficio relation TipoBeneficio object
     *
     * @param callable(\TipoBeneficioQuery):\TipoBeneficioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTipoBeneficioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTipoBeneficioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \TipoRecurso object
     *
     * @param \TipoRecurso|ObjectCollection $tipoRecurso The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByTipoRecurso($tipoRecurso, $comparison = null)
    {
        if ($tipoRecurso instanceof \TipoRecurso) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_RECURSO, $tipoRecurso->getClave(), $comparison);
        } elseif ($tipoRecurso instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_TIPO_RECURSO, $tipoRecurso->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByTipoRecurso() only accepts arguments of type \TipoRecurso or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoRecurso relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinTipoRecurso($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoRecurso');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoRecurso');
        }

        return $this;
    }

    /**
     * Use the TipoRecurso relation TipoRecurso object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TipoRecursoQuery A secondary query class using the current class as primary query
     */
    public function useTipoRecursoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoRecurso($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoRecurso', '\TipoRecursoQuery');
    }

    /**
     * Use the TipoRecurso relation TipoRecurso object
     *
     * @param callable(\TipoRecursoQuery):\TipoRecursoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTipoRecursoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useTipoRecursoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UnidadMedida object
     *
     * @param \UnidadMedida|ObjectCollection $unidadMedida The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByUnidadMedida($unidadMedida, $comparison = null)
    {
        if ($unidadMedida instanceof \UnidadMedida) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, $unidadMedida->getClave(), $comparison);
        } elseif ($unidadMedida instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, $unidadMedida->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUnidadMedida() only accepts arguments of type \UnidadMedida or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UnidadMedida relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinUnidadMedida($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UnidadMedida');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UnidadMedida');
        }

        return $this;
    }

    /**
     * Use the UnidadMedida relation UnidadMedida object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UnidadMedidaQuery A secondary query class using the current class as primary query
     */
    public function useUnidadMedidaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUnidadMedida($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UnidadMedida', '\UnidadMedidaQuery');
    }

    /**
     * Use the UnidadMedida relation UnidadMedida object
     *
     * @param callable(\UnidadMedidaQuery):\UnidadMedidaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUnidadMedidaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUnidadMedidaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_PROGRAMA, $programa->getClave(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_PROGRAMA, $programa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildBeneficioQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildBeneficio $beneficio Object to remove from the list of results
     *
     * @return $this|ChildBeneficioQuery The current query, for fluid interface
     */
    public function prune($beneficio = null)
    {
        if ($beneficio) {
            $this->addUsingAlias(BeneficioTableMap::COL_CLAVE, $beneficio->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the beneficio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BeneficioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            BeneficioTableMap::clearInstancePool();
            BeneficioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BeneficioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(BeneficioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            BeneficioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            BeneficioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // BeneficioQuery
