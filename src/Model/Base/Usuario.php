<?php

namespace Base;

use \Beneficio as ChildBeneficio;
use \BeneficioQuery as ChildBeneficioQuery;
use \EntidadOperativa as ChildEntidadOperativa;
use \EntidadOperativaQuery as ChildEntidadOperativaQuery;
use \Etapa as ChildEtapa;
use \EtapaQuery as ChildEtapaQuery;
use \FlujoEtapa as ChildFlujoEtapa;
use \FlujoEtapaQuery as ChildFlujoEtapaQuery;
use \Login as ChildLogin;
use \LoginQuery as ChildLoginQuery;
use \Programa as ChildPrograma;
use \ProgramaQuery as ChildProgramaQuery;
use \ProgramaRequisito as ChildProgramaRequisito;
use \ProgramaRequisitoQuery as ChildProgramaRequisitoQuery;
use \RestablecerPassword as ChildRestablecerPassword;
use \RestablecerPasswordQuery as ChildRestablecerPasswordQuery;
use \Rol as ChildRol;
use \RolPermiso as ChildRolPermiso;
use \RolPermisoQuery as ChildRolPermisoQuery;
use \RolQuery as ChildRolQuery;
use \RolUsuario as ChildRolUsuario;
use \RolUsuarioQuery as ChildRolUsuarioQuery;
use \Usuario as ChildUsuario;
use \UsuarioConfiguracionEtapa as ChildUsuarioConfiguracionEtapa;
use \UsuarioConfiguracionEtapaQuery as ChildUsuarioConfiguracionEtapaQuery;
use \UsuarioConfiguracionPrograma as ChildUsuarioConfiguracionPrograma;
use \UsuarioConfiguracionProgramaQuery as ChildUsuarioConfiguracionProgramaQuery;
use \UsuarioPermiso as ChildUsuarioPermiso;
use \UsuarioPermisoQuery as ChildUsuarioPermisoQuery;
use \UsuarioQuery as ChildUsuarioQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\BeneficioTableMap;
use Map\EtapaTableMap;
use Map\FlujoEtapaTableMap;
use Map\LoginTableMap;
use Map\ProgramaRequisitoTableMap;
use Map\ProgramaTableMap;
use Map\RestablecerPasswordTableMap;
use Map\RolPermisoTableMap;
use Map\RolTableMap;
use Map\RolUsuarioTableMap;
use Map\UsuarioConfiguracionEtapaTableMap;
use Map\UsuarioConfiguracionProgramaTableMap;
use Map\UsuarioPermisoTableMap;
use Map\UsuarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'usuario' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Usuario implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\UsuarioTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the clave field.
     *
     * @var        int
     */
    protected $clave;

    /**
     * The value for the usuario field.
     *
     * @var        string
     */
    protected $usuario;

    /**
     * The value for the password field.
     *
     * @var        string
     */
    protected $password;

    /**
     * The value for the vigencia field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $vigencia;

    /**
     * The value for the dias_vigencia field.
     *
     * @var        int|null
     */
    protected $dias_vigencia;

    /**
     * The value for the correo field.
     *
     * @var        string
     */
    protected $correo;

    /**
     * The value for the activo field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $activo;

    /**
     * The value for the nombre field.
     *
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the apaterno field.
     *
     * @var        string
     */
    protected $apaterno;

    /**
     * The value for the amaterno field.
     *
     * @var        string
     */
    protected $amaterno;

    /**
     * The value for the fecha_creacion field.
     *
     * @var        DateTime
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     *
     * @var        DateTime
     */
    protected $fecha_modificacion;

    /**
     * The value for the ultimo_acceso field.
     *
     * @var        DateTime
     */
    protected $ultimo_acceso;

    /**
     * The value for the password_antiguo field.
     *
     * Note: this column has a database default value of: ''
     * @var        string
     */
    protected $password_antiguo;

    /**
     * The value for the id_usuario_modificacion field.
     *
     * @var        int|null
     */
    protected $id_usuario_modificacion;

    /**
     * The value for the id_entidad_operativa field.
     *
     * @var        int
     */
    protected $id_entidad_operativa;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuarioRelatedByIdUsuarioModificacion;

    /**
     * @var        ChildEntidadOperativa
     */
    protected $aEntidadOperativa;

    /**
     * @var        ObjectCollection|ChildBeneficio[] Collection to store aggregation of ChildBeneficio objects.
     */
    protected $collBeneficios;
    protected $collBeneficiosPartial;

    /**
     * @var        ObjectCollection|ChildEtapa[] Collection to store aggregation of ChildEtapa objects.
     */
    protected $collEtapas;
    protected $collEtapasPartial;

    /**
     * @var        ObjectCollection|ChildFlujoEtapa[] Collection to store aggregation of ChildFlujoEtapa objects.
     */
    protected $collFlujoEtapas;
    protected $collFlujoEtapasPartial;

    /**
     * @var        ObjectCollection|ChildLogin[] Collection to store aggregation of ChildLogin objects.
     */
    protected $collLogins;
    protected $collLoginsPartial;

    /**
     * @var        ObjectCollection|ChildPrograma[] Collection to store aggregation of ChildPrograma objects.
     */
    protected $collProgramas;
    protected $collProgramasPartial;

    /**
     * @var        ObjectCollection|ChildProgramaRequisito[] Collection to store aggregation of ChildProgramaRequisito objects.
     */
    protected $collProgramaRequisitos;
    protected $collProgramaRequisitosPartial;

    /**
     * @var        ObjectCollection|ChildRestablecerPassword[] Collection to store aggregation of ChildRestablecerPassword objects.
     */
    protected $collRestablecerPasswords;
    protected $collRestablecerPasswordsPartial;

    /**
     * @var        ObjectCollection|ChildRol[] Collection to store aggregation of ChildRol objects.
     */
    protected $collRols;
    protected $collRolsPartial;

    /**
     * @var        ObjectCollection|ChildRolPermiso[] Collection to store aggregation of ChildRolPermiso objects.
     */
    protected $collRolPermisos;
    protected $collRolPermisosPartial;

    /**
     * @var        ObjectCollection|ChildRolUsuario[] Collection to store aggregation of ChildRolUsuario objects.
     */
    protected $collRolUsuariosRelatedByIdUsuario;
    protected $collRolUsuariosRelatedByIdUsuarioPartial;

    /**
     * @var        ObjectCollection|ChildRolUsuario[] Collection to store aggregation of ChildRolUsuario objects.
     */
    protected $collRolUsuariosRelatedByIdUsuarioModificacion;
    protected $collRolUsuariosRelatedByIdUsuarioModificacionPartial;

    /**
     * @var        ObjectCollection|ChildUsuario[] Collection to store aggregation of ChildUsuario objects.
     */
    protected $collUsuariosRelatedByClave;
    protected $collUsuariosRelatedByClavePartial;

    /**
     * @var        ObjectCollection|ChildUsuarioConfiguracionEtapa[] Collection to store aggregation of ChildUsuarioConfiguracionEtapa objects.
     */
    protected $collUsuarioConfiguracionEtapas;
    protected $collUsuarioConfiguracionEtapasPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioConfiguracionPrograma[] Collection to store aggregation of ChildUsuarioConfiguracionPrograma objects.
     */
    protected $collUsuarioConfiguracionProgramasRelatedByIdUsuario;
    protected $collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioConfiguracionPrograma[] Collection to store aggregation of ChildUsuarioConfiguracionPrograma objects.
     */
    protected $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
    protected $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioPermiso[] Collection to store aggregation of ChildUsuarioPermiso objects.
     */
    protected $collUsuarioPermisosRelatedByIdUsuario;
    protected $collUsuarioPermisosRelatedByIdUsuarioPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioPermiso[] Collection to store aggregation of ChildUsuarioPermiso objects.
     */
    protected $collUsuarioPermisosRelatedByIdUsuarioModificacion;
    protected $collUsuarioPermisosRelatedByIdUsuarioModificacionPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBeneficio[]
     */
    protected $beneficiosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEtapa[]
     */
    protected $etapasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFlujoEtapa[]
     */
    protected $flujoEtapasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLogin[]
     */
    protected $loginsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPrograma[]
     */
    protected $programasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProgramaRequisito[]
     */
    protected $programaRequisitosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRestablecerPassword[]
     */
    protected $restablecerPasswordsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRol[]
     */
    protected $rolsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRolPermiso[]
     */
    protected $rolPermisosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRolUsuario[]
     */
    protected $rolUsuariosRelatedByIdUsuarioScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRolUsuario[]
     */
    protected $rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuario[]
     */
    protected $usuariosRelatedByClaveScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioConfiguracionEtapa[]
     */
    protected $usuarioConfiguracionEtapasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioConfiguracionPrograma[]
     */
    protected $usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioConfiguracionPrograma[]
     */
    protected $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioPermiso[]
     */
    protected $usuarioPermisosRelatedByIdUsuarioScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioPermiso[]
     */
    protected $usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->vigencia = 0;
        $this->activo = 1;
        $this->password_antiguo = '';
    }

    /**
     * Initializes internal state of Base\Usuario object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Usuario</code> instance.  If
     * <code>obj</code> is an instance of <code>Usuario</code>, delegates to
     * <code>equals(Usuario)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param  string  $keyType                (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [clave] column value.
     *
     * @return int
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Get the [usuario] column value.
     *
     * @return string
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [vigencia] column value.
     *
     * @return int
     */
    public function getVigencia()
    {
        return $this->vigencia;
    }

    /**
     * Get the [dias_vigencia] column value.
     *
     * @return int|null
     */
    public function getDiasVigencia()
    {
        return $this->dias_vigencia;
    }

    /**
     * Get the [correo] column value.
     *
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Get the [activo] column value.
     *
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the [apaterno] column value.
     *
     * @return string
     */
    public function getApaterno()
    {
        return $this->apaterno;
    }

    /**
     * Get the [amaterno] column value.
     *
     * @return string
     */
    public function getAmaterno()
    {
        return $this->amaterno;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaCreacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_creacion;
        } else {
            return $this->fecha_creacion instanceof \DateTimeInterface ? $this->fecha_creacion->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaModificacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_modificacion;
        } else {
            return $this->fecha_modificacion instanceof \DateTimeInterface ? $this->fecha_modificacion->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [ultimo_acceso] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getUltimoAcceso($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->ultimo_acceso;
        } else {
            return $this->ultimo_acceso instanceof \DateTimeInterface ? $this->ultimo_acceso->format($format) : null;
        }
    }

    /**
     * Get the [password_antiguo] column value.
     *
     * @return string
     */
    public function getPasswordAntiguo()
    {
        return $this->password_antiguo;
    }

    /**
     * Get the [id_usuario_modificacion] column value.
     *
     * @return int|null
     */
    public function getIdUsuarioModificacion()
    {
        return $this->id_usuario_modificacion;
    }

    /**
     * Get the [id_entidad_operativa] column value.
     *
     * @return int
     */
    public function getIdEntidadOperativa()
    {
        return $this->id_entidad_operativa;
    }

    /**
     * Set the value of [clave] column.
     *
     * @param int $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setClave($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clave !== $v) {
            $this->clave = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_CLAVE] = true;
        }

        return $this;
    } // setClave()

    /**
     * Set the value of [usuario] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setUsuario($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->usuario !== $v) {
            $this->usuario = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_USUARIO] = true;
        }

        return $this;
    } // setUsuario()

    /**
     * Set the value of [password] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [vigencia] column.
     *
     * @param int $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setVigencia($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vigencia !== $v) {
            $this->vigencia = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_VIGENCIA] = true;
        }

        return $this;
    } // setVigencia()

    /**
     * Set the value of [dias_vigencia] column.
     *
     * @param int|null $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setDiasVigencia($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->dias_vigencia !== $v) {
            $this->dias_vigencia = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_DIAS_VIGENCIA] = true;
        }

        return $this;
    } // setDiasVigencia()

    /**
     * Set the value of [correo] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setCorreo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->correo !== $v) {
            $this->correo = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_CORREO] = true;
        }

        return $this;
    } // setCorreo()

    /**
     * Set the value of [activo] column.
     *
     * @param int $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setActivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->activo !== $v) {
            $this->activo = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_ACTIVO] = true;
        }

        return $this;
    } // setActivo()

    /**
     * Set the value of [nombre] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_NOMBRE] = true;
        }

        return $this;
    } // setNombre()

    /**
     * Set the value of [apaterno] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setApaterno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->apaterno !== $v) {
            $this->apaterno = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_APATERNO] = true;
        }

        return $this;
    } // setApaterno()

    /**
     * Set the value of [amaterno] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setAmaterno($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->amaterno !== $v) {
            $this->amaterno = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_AMATERNO] = true;
        }

        return $this;
    } // setAmaterno()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            if ($this->fecha_creacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_creacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_creacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsuarioTableMap::COL_FECHA_CREACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            if ($this->fecha_modificacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_modificacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_modificacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsuarioTableMap::COL_FECHA_MODIFICACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaModificacion()

    /**
     * Sets the value of [ultimo_acceso] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setUltimoAcceso($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ultimo_acceso !== null || $dt !== null) {
            if ($this->ultimo_acceso === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->ultimo_acceso->format("Y-m-d H:i:s.u")) {
                $this->ultimo_acceso = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UsuarioTableMap::COL_ULTIMO_ACCESO] = true;
            }
        } // if either are not null

        return $this;
    } // setUltimoAcceso()

    /**
     * Set the value of [password_antiguo] column.
     *
     * @param string $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setPasswordAntiguo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password_antiguo !== $v) {
            $this->password_antiguo = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_PASSWORD_ANTIGUO] = true;
        }

        return $this;
    } // setPasswordAntiguo()

    /**
     * Set the value of [id_usuario_modificacion] column.
     *
     * @param int|null $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setIdUsuarioModificacion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_usuario_modificacion !== $v) {
            $this->id_usuario_modificacion = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_ID_USUARIO_MODIFICACION] = true;
        }

        if ($this->aUsuarioRelatedByIdUsuarioModificacion !== null && $this->aUsuarioRelatedByIdUsuarioModificacion->getClave() !== $v) {
            $this->aUsuarioRelatedByIdUsuarioModificacion = null;
        }

        return $this;
    } // setIdUsuarioModificacion()

    /**
     * Set the value of [id_entidad_operativa] column.
     *
     * @param int $v New value
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function setIdEntidadOperativa($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_entidad_operativa !== $v) {
            $this->id_entidad_operativa = $v;
            $this->modifiedColumns[UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA] = true;
        }

        if ($this->aEntidadOperativa !== null && $this->aEntidadOperativa->getClave() !== $v) {
            $this->aEntidadOperativa = null;
        }

        return $this;
    } // setIdEntidadOperativa()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->vigencia !== 0) {
                return false;
            }

            if ($this->activo !== 1) {
                return false;
            }

            if ($this->password_antiguo !== '') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UsuarioTableMap::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clave = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UsuarioTableMap::translateFieldName('Usuario', TableMap::TYPE_PHPNAME, $indexType)];
            $this->usuario = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UsuarioTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UsuarioTableMap::translateFieldName('Vigencia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vigencia = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UsuarioTableMap::translateFieldName('DiasVigencia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dias_vigencia = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UsuarioTableMap::translateFieldName('Correo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->correo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UsuarioTableMap::translateFieldName('Activo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UsuarioTableMap::translateFieldName('Nombre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nombre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UsuarioTableMap::translateFieldName('Apaterno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->apaterno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UsuarioTableMap::translateFieldName('Amaterno', TableMap::TYPE_PHPNAME, $indexType)];
            $this->amaterno = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UsuarioTableMap::translateFieldName('FechaCreacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_creacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UsuarioTableMap::translateFieldName('FechaModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_modificacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UsuarioTableMap::translateFieldName('UltimoAcceso', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->ultimo_acceso = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UsuarioTableMap::translateFieldName('PasswordAntiguo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password_antiguo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : UsuarioTableMap::translateFieldName('IdUsuarioModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_usuario_modificacion = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : UsuarioTableMap::translateFieldName('IdEntidadOperativa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entidad_operativa = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 16; // 16 = UsuarioTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Usuario'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aUsuarioRelatedByIdUsuarioModificacion !== null && $this->id_usuario_modificacion !== $this->aUsuarioRelatedByIdUsuarioModificacion->getClave()) {
            $this->aUsuarioRelatedByIdUsuarioModificacion = null;
        }
        if ($this->aEntidadOperativa !== null && $this->id_entidad_operativa !== $this->aEntidadOperativa->getClave()) {
            $this->aEntidadOperativa = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsuarioTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUsuarioQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUsuarioRelatedByIdUsuarioModificacion = null;
            $this->aEntidadOperativa = null;
            $this->collBeneficios = null;

            $this->collEtapas = null;

            $this->collFlujoEtapas = null;

            $this->collLogins = null;

            $this->collProgramas = null;

            $this->collProgramaRequisitos = null;

            $this->collRestablecerPasswords = null;

            $this->collRols = null;

            $this->collRolPermisos = null;

            $this->collRolUsuariosRelatedByIdUsuario = null;

            $this->collRolUsuariosRelatedByIdUsuarioModificacion = null;

            $this->collUsuariosRelatedByClave = null;

            $this->collUsuarioConfiguracionEtapas = null;

            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = null;

            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = null;

            $this->collUsuarioPermisosRelatedByIdUsuario = null;

            $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Usuario::setDeleted()
     * @see Usuario::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUsuarioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsuarioTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUsuarioRelatedByIdUsuarioModificacion !== null) {
                if ($this->aUsuarioRelatedByIdUsuarioModificacion->isModified() || $this->aUsuarioRelatedByIdUsuarioModificacion->isNew()) {
                    $affectedRows += $this->aUsuarioRelatedByIdUsuarioModificacion->save($con);
                }
                $this->setUsuarioRelatedByIdUsuarioModificacion($this->aUsuarioRelatedByIdUsuarioModificacion);
            }

            if ($this->aEntidadOperativa !== null) {
                if ($this->aEntidadOperativa->isModified() || $this->aEntidadOperativa->isNew()) {
                    $affectedRows += $this->aEntidadOperativa->save($con);
                }
                $this->setEntidadOperativa($this->aEntidadOperativa);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->beneficiosScheduledForDeletion !== null) {
                if (!$this->beneficiosScheduledForDeletion->isEmpty()) {
                    \BeneficioQuery::create()
                        ->filterByPrimaryKeys($this->beneficiosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beneficiosScheduledForDeletion = null;
                }
            }

            if ($this->collBeneficios !== null) {
                foreach ($this->collBeneficios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->etapasScheduledForDeletion !== null) {
                if (!$this->etapasScheduledForDeletion->isEmpty()) {
                    \EtapaQuery::create()
                        ->filterByPrimaryKeys($this->etapasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->etapasScheduledForDeletion = null;
                }
            }

            if ($this->collEtapas !== null) {
                foreach ($this->collEtapas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->flujoEtapasScheduledForDeletion !== null) {
                if (!$this->flujoEtapasScheduledForDeletion->isEmpty()) {
                    \FlujoEtapaQuery::create()
                        ->filterByPrimaryKeys($this->flujoEtapasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->flujoEtapasScheduledForDeletion = null;
                }
            }

            if ($this->collFlujoEtapas !== null) {
                foreach ($this->collFlujoEtapas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->loginsScheduledForDeletion !== null) {
                if (!$this->loginsScheduledForDeletion->isEmpty()) {
                    \LoginQuery::create()
                        ->filterByPrimaryKeys($this->loginsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->loginsScheduledForDeletion = null;
                }
            }

            if ($this->collLogins !== null) {
                foreach ($this->collLogins as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->programasScheduledForDeletion !== null) {
                if (!$this->programasScheduledForDeletion->isEmpty()) {
                    \ProgramaQuery::create()
                        ->filterByPrimaryKeys($this->programasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->programasScheduledForDeletion = null;
                }
            }

            if ($this->collProgramas !== null) {
                foreach ($this->collProgramas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->programaRequisitosScheduledForDeletion !== null) {
                if (!$this->programaRequisitosScheduledForDeletion->isEmpty()) {
                    \ProgramaRequisitoQuery::create()
                        ->filterByPrimaryKeys($this->programaRequisitosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->programaRequisitosScheduledForDeletion = null;
                }
            }

            if ($this->collProgramaRequisitos !== null) {
                foreach ($this->collProgramaRequisitos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->restablecerPasswordsScheduledForDeletion !== null) {
                if (!$this->restablecerPasswordsScheduledForDeletion->isEmpty()) {
                    \RestablecerPasswordQuery::create()
                        ->filterByPrimaryKeys($this->restablecerPasswordsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->restablecerPasswordsScheduledForDeletion = null;
                }
            }

            if ($this->collRestablecerPasswords !== null) {
                foreach ($this->collRestablecerPasswords as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolsScheduledForDeletion !== null) {
                if (!$this->rolsScheduledForDeletion->isEmpty()) {
                    \RolQuery::create()
                        ->filterByPrimaryKeys($this->rolsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolsScheduledForDeletion = null;
                }
            }

            if ($this->collRols !== null) {
                foreach ($this->collRols as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolPermisosScheduledForDeletion !== null) {
                if (!$this->rolPermisosScheduledForDeletion->isEmpty()) {
                    \RolPermisoQuery::create()
                        ->filterByPrimaryKeys($this->rolPermisosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolPermisosScheduledForDeletion = null;
                }
            }

            if ($this->collRolPermisos !== null) {
                foreach ($this->collRolPermisos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion !== null) {
                if (!$this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->isEmpty()) {
                    \RolUsuarioQuery::create()
                        ->filterByPrimaryKeys($this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion = null;
                }
            }

            if ($this->collRolUsuariosRelatedByIdUsuario !== null) {
                foreach ($this->collRolUsuariosRelatedByIdUsuario as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion !== null) {
                if (!$this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->isEmpty()) {
                    \RolUsuarioQuery::create()
                        ->filterByPrimaryKeys($this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion = null;
                }
            }

            if ($this->collRolUsuariosRelatedByIdUsuarioModificacion !== null) {
                foreach ($this->collRolUsuariosRelatedByIdUsuarioModificacion as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuariosRelatedByClaveScheduledForDeletion !== null) {
                if (!$this->usuariosRelatedByClaveScheduledForDeletion->isEmpty()) {
                    foreach ($this->usuariosRelatedByClaveScheduledForDeletion as $usuarioRelatedByClave) {
                        // need to save related object because we set the relation to null
                        $usuarioRelatedByClave->save($con);
                    }
                    $this->usuariosRelatedByClaveScheduledForDeletion = null;
                }
            }

            if ($this->collUsuariosRelatedByClave !== null) {
                foreach ($this->collUsuariosRelatedByClave as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioConfiguracionEtapasScheduledForDeletion !== null) {
                if (!$this->usuarioConfiguracionEtapasScheduledForDeletion->isEmpty()) {
                    \UsuarioConfiguracionEtapaQuery::create()
                        ->filterByPrimaryKeys($this->usuarioConfiguracionEtapasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioConfiguracionEtapasScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioConfiguracionEtapas !== null) {
                foreach ($this->collUsuarioConfiguracionEtapas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion !== null) {
                if (!$this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->isEmpty()) {
                    \UsuarioConfiguracionProgramaQuery::create()
                        ->filterByPrimaryKeys($this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario !== null) {
                foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion !== null) {
                if (!$this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->isEmpty()) {
                    \UsuarioConfiguracionProgramaQuery::create()
                        ->filterByPrimaryKeys($this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion !== null) {
                foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion !== null) {
                if (!$this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->isEmpty()) {
                    \UsuarioPermisoQuery::create()
                        ->filterByPrimaryKeys($this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioPermisosRelatedByIdUsuario !== null) {
                foreach ($this->collUsuarioPermisosRelatedByIdUsuario as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion !== null) {
                if (!$this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->isEmpty()) {
                    \UsuarioPermisoQuery::create()
                        ->filterByPrimaryKeys($this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion !== null) {
                foreach ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UsuarioTableMap::COL_CLAVE] = true;
        if (null !== $this->clave) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsuarioTableMap::COL_CLAVE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsuarioTableMap::COL_CLAVE)) {
            $modifiedColumns[':p' . $index++]  = 'clave';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_USUARIO)) {
            $modifiedColumns[':p' . $index++]  = 'usuario';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_VIGENCIA)) {
            $modifiedColumns[':p' . $index++]  = 'vigencia';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DIAS_VIGENCIA)) {
            $modifiedColumns[':p' . $index++]  = 'dias_vigencia';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_CORREO)) {
            $modifiedColumns[':p' . $index++]  = 'correo';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ACTIVO)) {
            $modifiedColumns[':p' . $index++]  = 'activo';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = 'nombre';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_APATERNO)) {
            $modifiedColumns[':p' . $index++]  = 'apaterno';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_AMATERNO)) {
            $modifiedColumns[':p' . $index++]  = 'amaterno';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_creacion';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_modificacion';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ULTIMO_ACCESO)) {
            $modifiedColumns[':p' . $index++]  = 'ultimo_acceso';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PASSWORD_ANTIGUO)) {
            $modifiedColumns[':p' . $index++]  = 'password_antiguo';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'id_usuario_modificacion';
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA)) {
            $modifiedColumns[':p' . $index++]  = 'id_entidad_operativa';
        }

        $sql = sprintf(
            'INSERT INTO usuario (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'clave':
                        $stmt->bindValue($identifier, $this->clave, PDO::PARAM_INT);
                        break;
                    case 'usuario':
                        $stmt->bindValue($identifier, $this->usuario, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'vigencia':
                        $stmt->bindValue($identifier, $this->vigencia, PDO::PARAM_INT);
                        break;
                    case 'dias_vigencia':
                        $stmt->bindValue($identifier, $this->dias_vigencia, PDO::PARAM_INT);
                        break;
                    case 'correo':
                        $stmt->bindValue($identifier, $this->correo, PDO::PARAM_STR);
                        break;
                    case 'activo':
                        $stmt->bindValue($identifier, $this->activo, PDO::PARAM_INT);
                        break;
                    case 'nombre':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case 'apaterno':
                        $stmt->bindValue($identifier, $this->apaterno, PDO::PARAM_STR);
                        break;
                    case 'amaterno':
                        $stmt->bindValue($identifier, $this->amaterno, PDO::PARAM_STR);
                        break;
                    case 'fecha_creacion':
                        $stmt->bindValue($identifier, $this->fecha_creacion ? $this->fecha_creacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'fecha_modificacion':
                        $stmt->bindValue($identifier, $this->fecha_modificacion ? $this->fecha_modificacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'ultimo_acceso':
                        $stmt->bindValue($identifier, $this->ultimo_acceso ? $this->ultimo_acceso->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'password_antiguo':
                        $stmt->bindValue($identifier, $this->password_antiguo, PDO::PARAM_STR);
                        break;
                    case 'id_usuario_modificacion':
                        $stmt->bindValue($identifier, $this->id_usuario_modificacion, PDO::PARAM_INT);
                        break;
                    case 'id_entidad_operativa':
                        $stmt->bindValue($identifier, $this->id_entidad_operativa, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setClave($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsuarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getClave();
                break;
            case 1:
                return $this->getUsuario();
                break;
            case 2:
                return $this->getPassword();
                break;
            case 3:
                return $this->getVigencia();
                break;
            case 4:
                return $this->getDiasVigencia();
                break;
            case 5:
                return $this->getCorreo();
                break;
            case 6:
                return $this->getActivo();
                break;
            case 7:
                return $this->getNombre();
                break;
            case 8:
                return $this->getApaterno();
                break;
            case 9:
                return $this->getAmaterno();
                break;
            case 10:
                return $this->getFechaCreacion();
                break;
            case 11:
                return $this->getFechaModificacion();
                break;
            case 12:
                return $this->getUltimoAcceso();
                break;
            case 13:
                return $this->getPasswordAntiguo();
                break;
            case 14:
                return $this->getIdUsuarioModificacion();
                break;
            case 15:
                return $this->getIdEntidadOperativa();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Usuario'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Usuario'][$this->hashCode()] = true;
        $keys = UsuarioTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getClave(),
            $keys[1] => $this->getUsuario(),
            $keys[2] => $this->getPassword(),
            $keys[3] => $this->getVigencia(),
            $keys[4] => $this->getDiasVigencia(),
            $keys[5] => $this->getCorreo(),
            $keys[6] => $this->getActivo(),
            $keys[7] => $this->getNombre(),
            $keys[8] => $this->getApaterno(),
            $keys[9] => $this->getAmaterno(),
            $keys[10] => $this->getFechaCreacion(),
            $keys[11] => $this->getFechaModificacion(),
            $keys[12] => $this->getUltimoAcceso(),
            $keys[13] => $this->getPasswordAntiguo(),
            $keys[14] => $this->getIdUsuarioModificacion(),
            $keys[15] => $this->getIdEntidadOperativa(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUsuarioRelatedByIdUsuarioModificacion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuarioRelatedByIdUsuarioModificacion->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEntidadOperativa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entidadOperativa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'entidad_operativa';
                        break;
                    default:
                        $key = 'EntidadOperativa';
                }

                $result[$key] = $this->aEntidadOperativa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBeneficios) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'beneficios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'beneficios';
                        break;
                    default:
                        $key = 'Beneficios';
                }

                $result[$key] = $this->collBeneficios->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEtapas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etapas';
                        break;
                    default:
                        $key = 'Etapas';
                }

                $result[$key] = $this->collEtapas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFlujoEtapas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'flujoEtapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'flujo_etapas';
                        break;
                    default:
                        $key = 'FlujoEtapas';
                }

                $result[$key] = $this->collFlujoEtapas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collLogins) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'logins';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'logins';
                        break;
                    default:
                        $key = 'Logins';
                }

                $result[$key] = $this->collLogins->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProgramas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'programas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'programas';
                        break;
                    default:
                        $key = 'Programas';
                }

                $result[$key] = $this->collProgramas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProgramaRequisitos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'programaRequisitos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'programa_requisitos';
                        break;
                    default:
                        $key = 'ProgramaRequisitos';
                }

                $result[$key] = $this->collProgramaRequisitos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRestablecerPasswords) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'restablecerPasswords';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'restablecer_passwords';
                        break;
                    default:
                        $key = 'RestablecerPasswords';
                }

                $result[$key] = $this->collRestablecerPasswords->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRols) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rols';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rols';
                        break;
                    default:
                        $key = 'Rols';
                }

                $result[$key] = $this->collRols->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRolPermisos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolPermisos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rol_permisos';
                        break;
                    default:
                        $key = 'RolPermisos';
                }

                $result[$key] = $this->collRolPermisos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRolUsuariosRelatedByIdUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolUsuarios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rol_usuarios';
                        break;
                    default:
                        $key = 'RolUsuarios';
                }

                $result[$key] = $this->collRolUsuariosRelatedByIdUsuario->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRolUsuariosRelatedByIdUsuarioModificacion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolUsuarios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rol_usuarios';
                        break;
                    default:
                        $key = 'RolUsuarios';
                }

                $result[$key] = $this->collRolUsuariosRelatedByIdUsuarioModificacion->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuariosRelatedByClave) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuarios';
                        break;
                    default:
                        $key = 'Usuarios';
                }

                $result[$key] = $this->collUsuariosRelatedByClave->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioConfiguracionEtapas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioConfiguracionEtapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_configuracion_etapas';
                        break;
                    default:
                        $key = 'UsuarioConfiguracionEtapas';
                }

                $result[$key] = $this->collUsuarioConfiguracionEtapas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioConfiguracionProgramas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_configuracion_programas';
                        break;
                    default:
                        $key = 'UsuarioConfiguracionProgramas';
                }

                $result[$key] = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioConfiguracionProgramas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_configuracion_programas';
                        break;
                    default:
                        $key = 'UsuarioConfiguracionProgramas';
                }

                $result[$key] = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioPermisosRelatedByIdUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioPermisos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_permisos';
                        break;
                    default:
                        $key = 'UsuarioPermisos';
                }

                $result[$key] = $this->collUsuarioPermisosRelatedByIdUsuario->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioPermisosRelatedByIdUsuarioModificacion) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioPermisos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_permisos';
                        break;
                    default:
                        $key = 'UsuarioPermisos';
                }

                $result[$key] = $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Usuario
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsuarioTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Usuario
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setClave($value);
                break;
            case 1:
                $this->setUsuario($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
            case 3:
                $this->setVigencia($value);
                break;
            case 4:
                $this->setDiasVigencia($value);
                break;
            case 5:
                $this->setCorreo($value);
                break;
            case 6:
                $this->setActivo($value);
                break;
            case 7:
                $this->setNombre($value);
                break;
            case 8:
                $this->setApaterno($value);
                break;
            case 9:
                $this->setAmaterno($value);
                break;
            case 10:
                $this->setFechaCreacion($value);
                break;
            case 11:
                $this->setFechaModificacion($value);
                break;
            case 12:
                $this->setUltimoAcceso($value);
                break;
            case 13:
                $this->setPasswordAntiguo($value);
                break;
            case 14:
                $this->setIdUsuarioModificacion($value);
                break;
            case 15:
                $this->setIdEntidadOperativa($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     $this|\Usuario
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UsuarioTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setClave($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setUsuario($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPassword($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setVigencia($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDiasVigencia($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCorreo($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setActivo($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setNombre($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setApaterno($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAmaterno($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setFechaCreacion($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setFechaModificacion($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setUltimoAcceso($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPasswordAntiguo($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIdUsuarioModificacion($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setIdEntidadOperativa($arr[$keys[15]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Usuario The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsuarioTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UsuarioTableMap::COL_CLAVE)) {
            $criteria->add(UsuarioTableMap::COL_CLAVE, $this->clave);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_USUARIO)) {
            $criteria->add(UsuarioTableMap::COL_USUARIO, $this->usuario);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PASSWORD)) {
            $criteria->add(UsuarioTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_VIGENCIA)) {
            $criteria->add(UsuarioTableMap::COL_VIGENCIA, $this->vigencia);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_DIAS_VIGENCIA)) {
            $criteria->add(UsuarioTableMap::COL_DIAS_VIGENCIA, $this->dias_vigencia);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_CORREO)) {
            $criteria->add(UsuarioTableMap::COL_CORREO, $this->correo);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ACTIVO)) {
            $criteria->add(UsuarioTableMap::COL_ACTIVO, $this->activo);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_NOMBRE)) {
            $criteria->add(UsuarioTableMap::COL_NOMBRE, $this->nombre);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_APATERNO)) {
            $criteria->add(UsuarioTableMap::COL_APATERNO, $this->apaterno);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_AMATERNO)) {
            $criteria->add(UsuarioTableMap::COL_AMATERNO, $this->amaterno);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_FECHA_CREACION)) {
            $criteria->add(UsuarioTableMap::COL_FECHA_CREACION, $this->fecha_creacion);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_FECHA_MODIFICACION)) {
            $criteria->add(UsuarioTableMap::COL_FECHA_MODIFICACION, $this->fecha_modificacion);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ULTIMO_ACCESO)) {
            $criteria->add(UsuarioTableMap::COL_ULTIMO_ACCESO, $this->ultimo_acceso);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_PASSWORD_ANTIGUO)) {
            $criteria->add(UsuarioTableMap::COL_PASSWORD_ANTIGUO, $this->password_antiguo);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $criteria->add(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $this->id_usuario_modificacion);
        }
        if ($this->isColumnModified(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA)) {
            $criteria->add(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $this->id_entidad_operativa);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUsuarioQuery::create();
        $criteria->add(UsuarioTableMap::COL_CLAVE, $this->clave);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getClave();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getClave();
    }

    /**
     * Generic method to set the primary key (clave column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setClave($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getClave();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Usuario (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsuario($this->getUsuario());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setVigencia($this->getVigencia());
        $copyObj->setDiasVigencia($this->getDiasVigencia());
        $copyObj->setCorreo($this->getCorreo());
        $copyObj->setActivo($this->getActivo());
        $copyObj->setNombre($this->getNombre());
        $copyObj->setApaterno($this->getApaterno());
        $copyObj->setAmaterno($this->getAmaterno());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setUltimoAcceso($this->getUltimoAcceso());
        $copyObj->setPasswordAntiguo($this->getPasswordAntiguo());
        $copyObj->setIdUsuarioModificacion($this->getIdUsuarioModificacion());
        $copyObj->setIdEntidadOperativa($this->getIdEntidadOperativa());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBeneficios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeneficio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEtapas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEtapa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFlujoEtapas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlujoEtapa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getLogins() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLogin($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProgramas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPrograma($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProgramaRequisitos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProgramaRequisito($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRestablecerPasswords() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRestablecerPassword($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRols() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRol($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRolPermisos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolPermiso($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRolUsuariosRelatedByIdUsuario() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolUsuarioRelatedByIdUsuario($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRolUsuariosRelatedByIdUsuarioModificacion() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolUsuarioRelatedByIdUsuarioModificacion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuariosRelatedByClave() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioRelatedByClave($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioConfiguracionEtapas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioConfiguracionEtapa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioConfiguracionProgramasRelatedByIdUsuario() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioConfiguracionProgramaRelatedByIdUsuario($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioPermisosRelatedByIdUsuario() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioPermisoRelatedByIdUsuario($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioPermisosRelatedByIdUsuarioModificacion() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioPermisoRelatedByIdUsuarioModificacion($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setClave(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Usuario Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario|null $v
     * @return $this|\Usuario The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuarioRelatedByIdUsuarioModificacion(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setIdUsuarioModificacion(NULL);
        } else {
            $this->setIdUsuarioModificacion($v->getClave());
        }

        $this->aUsuarioRelatedByIdUsuarioModificacion = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addUsuarioRelatedByClave($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario|null The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuarioRelatedByIdUsuarioModificacion(ConnectionInterface $con = null)
    {
        if ($this->aUsuarioRelatedByIdUsuarioModificacion === null && ($this->id_usuario_modificacion != 0)) {
            $this->aUsuarioRelatedByIdUsuarioModificacion = ChildUsuarioQuery::create()->findPk($this->id_usuario_modificacion, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuarioRelatedByIdUsuarioModificacion->addUsuariosRelatedByClave($this);
             */
        }

        return $this->aUsuarioRelatedByIdUsuarioModificacion;
    }

    /**
     * Declares an association between this object and a ChildEntidadOperativa object.
     *
     * @param  ChildEntidadOperativa $v
     * @return $this|\Usuario The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntidadOperativa(ChildEntidadOperativa $v = null)
    {
        if ($v === null) {
            $this->setIdEntidadOperativa(NULL);
        } else {
            $this->setIdEntidadOperativa($v->getClave());
        }

        $this->aEntidadOperativa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntidadOperativa object, it will not be re-added.
        if ($v !== null) {
            $v->addUsuario($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntidadOperativa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntidadOperativa The associated ChildEntidadOperativa object.
     * @throws PropelException
     */
    public function getEntidadOperativa(ConnectionInterface $con = null)
    {
        if ($this->aEntidadOperativa === null && ($this->id_entidad_operativa != 0)) {
            $this->aEntidadOperativa = ChildEntidadOperativaQuery::create()->findPk($this->id_entidad_operativa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntidadOperativa->addUsuarios($this);
             */
        }

        return $this->aEntidadOperativa;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Beneficio' === $relationName) {
            $this->initBeneficios();
            return;
        }
        if ('Etapa' === $relationName) {
            $this->initEtapas();
            return;
        }
        if ('FlujoEtapa' === $relationName) {
            $this->initFlujoEtapas();
            return;
        }
        if ('Login' === $relationName) {
            $this->initLogins();
            return;
        }
        if ('Programa' === $relationName) {
            $this->initProgramas();
            return;
        }
        if ('ProgramaRequisito' === $relationName) {
            $this->initProgramaRequisitos();
            return;
        }
        if ('RestablecerPassword' === $relationName) {
            $this->initRestablecerPasswords();
            return;
        }
        if ('Rol' === $relationName) {
            $this->initRols();
            return;
        }
        if ('RolPermiso' === $relationName) {
            $this->initRolPermisos();
            return;
        }
        if ('RolUsuarioRelatedByIdUsuario' === $relationName) {
            $this->initRolUsuariosRelatedByIdUsuario();
            return;
        }
        if ('RolUsuarioRelatedByIdUsuarioModificacion' === $relationName) {
            $this->initRolUsuariosRelatedByIdUsuarioModificacion();
            return;
        }
        if ('UsuarioRelatedByClave' === $relationName) {
            $this->initUsuariosRelatedByClave();
            return;
        }
        if ('UsuarioConfiguracionEtapa' === $relationName) {
            $this->initUsuarioConfiguracionEtapas();
            return;
        }
        if ('UsuarioConfiguracionProgramaRelatedByIdUsuario' === $relationName) {
            $this->initUsuarioConfiguracionProgramasRelatedByIdUsuario();
            return;
        }
        if ('UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion' === $relationName) {
            $this->initUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion();
            return;
        }
        if ('UsuarioPermisoRelatedByIdUsuario' === $relationName) {
            $this->initUsuarioPermisosRelatedByIdUsuario();
            return;
        }
        if ('UsuarioPermisoRelatedByIdUsuarioModificacion' === $relationName) {
            $this->initUsuarioPermisosRelatedByIdUsuarioModificacion();
            return;
        }
    }

    /**
     * Clears out the collBeneficios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBeneficios()
     */
    public function clearBeneficios()
    {
        $this->collBeneficios = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBeneficios collection loaded partially.
     */
    public function resetPartialBeneficios($v = true)
    {
        $this->collBeneficiosPartial = $v;
    }

    /**
     * Initializes the collBeneficios collection.
     *
     * By default this just sets the collBeneficios collection to an empty array (like clearcollBeneficios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeneficios($overrideExisting = true)
    {
        if (null !== $this->collBeneficios && !$overrideExisting) {
            return;
        }

        $collectionClassName = BeneficioTableMap::getTableMap()->getCollectionClassName();

        $this->collBeneficios = new $collectionClassName;
        $this->collBeneficios->setModel('\Beneficio');
    }

    /**
     * Gets an array of ChildBeneficio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     * @throws PropelException
     */
    public function getBeneficios(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBeneficiosPartial && !$this->isNew();
        if (null === $this->collBeneficios || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collBeneficios) {
                    $this->initBeneficios();
                } else {
                    $collectionClassName = BeneficioTableMap::getTableMap()->getCollectionClassName();

                    $collBeneficios = new $collectionClassName;
                    $collBeneficios->setModel('\Beneficio');

                    return $collBeneficios;
                }
            } else {
                $collBeneficios = ChildBeneficioQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBeneficiosPartial && count($collBeneficios)) {
                        $this->initBeneficios(false);

                        foreach ($collBeneficios as $obj) {
                            if (false == $this->collBeneficios->contains($obj)) {
                                $this->collBeneficios->append($obj);
                            }
                        }

                        $this->collBeneficiosPartial = true;
                    }

                    return $collBeneficios;
                }

                if ($partial && $this->collBeneficios) {
                    foreach ($this->collBeneficios as $obj) {
                        if ($obj->isNew()) {
                            $collBeneficios[] = $obj;
                        }
                    }
                }

                $this->collBeneficios = $collBeneficios;
                $this->collBeneficiosPartial = false;
            }
        }

        return $this->collBeneficios;
    }

    /**
     * Sets a collection of ChildBeneficio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $beneficios A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setBeneficios(Collection $beneficios, ConnectionInterface $con = null)
    {
        /** @var ChildBeneficio[] $beneficiosToDelete */
        $beneficiosToDelete = $this->getBeneficios(new Criteria(), $con)->diff($beneficios);


        $this->beneficiosScheduledForDeletion = $beneficiosToDelete;

        foreach ($beneficiosToDelete as $beneficioRemoved) {
            $beneficioRemoved->setUsuario(null);
        }

        $this->collBeneficios = null;
        foreach ($beneficios as $beneficio) {
            $this->addBeneficio($beneficio);
        }

        $this->collBeneficios = $beneficios;
        $this->collBeneficiosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Beneficio objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Beneficio objects.
     * @throws PropelException
     */
    public function countBeneficios(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBeneficiosPartial && !$this->isNew();
        if (null === $this->collBeneficios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeneficios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBeneficios());
            }

            $query = ChildBeneficioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collBeneficios);
    }

    /**
     * Method called to associate a ChildBeneficio object to this object
     * through the ChildBeneficio foreign key attribute.
     *
     * @param  ChildBeneficio $l ChildBeneficio
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addBeneficio(ChildBeneficio $l)
    {
        if ($this->collBeneficios === null) {
            $this->initBeneficios();
            $this->collBeneficiosPartial = true;
        }

        if (!$this->collBeneficios->contains($l)) {
            $this->doAddBeneficio($l);

            if ($this->beneficiosScheduledForDeletion and $this->beneficiosScheduledForDeletion->contains($l)) {
                $this->beneficiosScheduledForDeletion->remove($this->beneficiosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBeneficio $beneficio The ChildBeneficio object to add.
     */
    protected function doAddBeneficio(ChildBeneficio $beneficio)
    {
        $this->collBeneficios[]= $beneficio;
        $beneficio->setUsuario($this);
    }

    /**
     * @param  ChildBeneficio $beneficio The ChildBeneficio object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeBeneficio(ChildBeneficio $beneficio)
    {
        if ($this->getBeneficios()->contains($beneficio)) {
            $pos = $this->collBeneficios->search($beneficio);
            $this->collBeneficios->remove($pos);
            if (null === $this->beneficiosScheduledForDeletion) {
                $this->beneficiosScheduledForDeletion = clone $this->collBeneficios;
                $this->beneficiosScheduledForDeletion->clear();
            }
            $this->beneficiosScheduledForDeletion[]= clone $beneficio;
            $beneficio->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoDistribucion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoDistribucion', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoBeneficio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoBeneficio', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoRecurso(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoRecurso', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinUnidadMedida(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('UnidadMedida', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }

    /**
     * Clears out the collEtapas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEtapas()
     */
    public function clearEtapas()
    {
        $this->collEtapas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEtapas collection loaded partially.
     */
    public function resetPartialEtapas($v = true)
    {
        $this->collEtapasPartial = $v;
    }

    /**
     * Initializes the collEtapas collection.
     *
     * By default this just sets the collEtapas collection to an empty array (like clearcollEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEtapas($overrideExisting = true)
    {
        if (null !== $this->collEtapas && !$overrideExisting) {
            return;
        }

        $collectionClassName = EtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collEtapas = new $collectionClassName;
        $this->collEtapas->setModel('\Etapa');
    }

    /**
     * Gets an array of ChildEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEtapa[] List of ChildEtapa objects
     * @throws PropelException
     */
    public function getEtapas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEtapasPartial && !$this->isNew();
        if (null === $this->collEtapas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collEtapas) {
                    $this->initEtapas();
                } else {
                    $collectionClassName = EtapaTableMap::getTableMap()->getCollectionClassName();

                    $collEtapas = new $collectionClassName;
                    $collEtapas->setModel('\Etapa');

                    return $collEtapas;
                }
            } else {
                $collEtapas = ChildEtapaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEtapasPartial && count($collEtapas)) {
                        $this->initEtapas(false);

                        foreach ($collEtapas as $obj) {
                            if (false == $this->collEtapas->contains($obj)) {
                                $this->collEtapas->append($obj);
                            }
                        }

                        $this->collEtapasPartial = true;
                    }

                    return $collEtapas;
                }

                if ($partial && $this->collEtapas) {
                    foreach ($this->collEtapas as $obj) {
                        if ($obj->isNew()) {
                            $collEtapas[] = $obj;
                        }
                    }
                }

                $this->collEtapas = $collEtapas;
                $this->collEtapasPartial = false;
            }
        }

        return $this->collEtapas;
    }

    /**
     * Sets a collection of ChildEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $etapas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setEtapas(Collection $etapas, ConnectionInterface $con = null)
    {
        /** @var ChildEtapa[] $etapasToDelete */
        $etapasToDelete = $this->getEtapas(new Criteria(), $con)->diff($etapas);


        $this->etapasScheduledForDeletion = $etapasToDelete;

        foreach ($etapasToDelete as $etapaRemoved) {
            $etapaRemoved->setUsuario(null);
        }

        $this->collEtapas = null;
        foreach ($etapas as $etapa) {
            $this->addEtapa($etapa);
        }

        $this->collEtapas = $etapas;
        $this->collEtapasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Etapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Etapa objects.
     * @throws PropelException
     */
    public function countEtapas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEtapasPartial && !$this->isNew();
        if (null === $this->collEtapas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEtapas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEtapas());
            }

            $query = ChildEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collEtapas);
    }

    /**
     * Method called to associate a ChildEtapa object to this object
     * through the ChildEtapa foreign key attribute.
     *
     * @param  ChildEtapa $l ChildEtapa
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addEtapa(ChildEtapa $l)
    {
        if ($this->collEtapas === null) {
            $this->initEtapas();
            $this->collEtapasPartial = true;
        }

        if (!$this->collEtapas->contains($l)) {
            $this->doAddEtapa($l);

            if ($this->etapasScheduledForDeletion and $this->etapasScheduledForDeletion->contains($l)) {
                $this->etapasScheduledForDeletion->remove($this->etapasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEtapa $etapa The ChildEtapa object to add.
     */
    protected function doAddEtapa(ChildEtapa $etapa)
    {
        $this->collEtapas[]= $etapa;
        $etapa->setUsuario($this);
    }

    /**
     * @param  ChildEtapa $etapa The ChildEtapa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeEtapa(ChildEtapa $etapa)
    {
        if ($this->getEtapas()->contains($etapa)) {
            $pos = $this->collEtapas->search($etapa);
            $this->collEtapas->remove($pos);
            if (null === $this->etapasScheduledForDeletion) {
                $this->etapasScheduledForDeletion = clone $this->collEtapas;
                $this->etapasScheduledForDeletion->clear();
            }
            $this->etapasScheduledForDeletion[]= clone $etapa;
            $etapa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Etapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEtapa[] List of ChildEtapa objects
     */
    public function getEtapasJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEtapaQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getEtapas($query, $con);
    }

    /**
     * Clears out the collFlujoEtapas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFlujoEtapas()
     */
    public function clearFlujoEtapas()
    {
        $this->collFlujoEtapas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFlujoEtapas collection loaded partially.
     */
    public function resetPartialFlujoEtapas($v = true)
    {
        $this->collFlujoEtapasPartial = $v;
    }

    /**
     * Initializes the collFlujoEtapas collection.
     *
     * By default this just sets the collFlujoEtapas collection to an empty array (like clearcollFlujoEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlujoEtapas($overrideExisting = true)
    {
        if (null !== $this->collFlujoEtapas && !$overrideExisting) {
            return;
        }

        $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collFlujoEtapas = new $collectionClassName;
        $this->collFlujoEtapas->setModel('\FlujoEtapa');
    }

    /**
     * Gets an array of ChildFlujoEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     * @throws PropelException
     */
    public function getFlujoEtapas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasPartial && !$this->isNew();
        if (null === $this->collFlujoEtapas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFlujoEtapas) {
                    $this->initFlujoEtapas();
                } else {
                    $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

                    $collFlujoEtapas = new $collectionClassName;
                    $collFlujoEtapas->setModel('\FlujoEtapa');

                    return $collFlujoEtapas;
                }
            } else {
                $collFlujoEtapas = ChildFlujoEtapaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFlujoEtapasPartial && count($collFlujoEtapas)) {
                        $this->initFlujoEtapas(false);

                        foreach ($collFlujoEtapas as $obj) {
                            if (false == $this->collFlujoEtapas->contains($obj)) {
                                $this->collFlujoEtapas->append($obj);
                            }
                        }

                        $this->collFlujoEtapasPartial = true;
                    }

                    return $collFlujoEtapas;
                }

                if ($partial && $this->collFlujoEtapas) {
                    foreach ($this->collFlujoEtapas as $obj) {
                        if ($obj->isNew()) {
                            $collFlujoEtapas[] = $obj;
                        }
                    }
                }

                $this->collFlujoEtapas = $collFlujoEtapas;
                $this->collFlujoEtapasPartial = false;
            }
        }

        return $this->collFlujoEtapas;
    }

    /**
     * Sets a collection of ChildFlujoEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $flujoEtapas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setFlujoEtapas(Collection $flujoEtapas, ConnectionInterface $con = null)
    {
        /** @var ChildFlujoEtapa[] $flujoEtapasToDelete */
        $flujoEtapasToDelete = $this->getFlujoEtapas(new Criteria(), $con)->diff($flujoEtapas);


        $this->flujoEtapasScheduledForDeletion = $flujoEtapasToDelete;

        foreach ($flujoEtapasToDelete as $flujoEtapaRemoved) {
            $flujoEtapaRemoved->setUsuario(null);
        }

        $this->collFlujoEtapas = null;
        foreach ($flujoEtapas as $flujoEtapa) {
            $this->addFlujoEtapa($flujoEtapa);
        }

        $this->collFlujoEtapas = $flujoEtapas;
        $this->collFlujoEtapasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlujoEtapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FlujoEtapa objects.
     * @throws PropelException
     */
    public function countFlujoEtapas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasPartial && !$this->isNew();
        if (null === $this->collFlujoEtapas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlujoEtapas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlujoEtapas());
            }

            $query = ChildFlujoEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collFlujoEtapas);
    }

    /**
     * Method called to associate a ChildFlujoEtapa object to this object
     * through the ChildFlujoEtapa foreign key attribute.
     *
     * @param  ChildFlujoEtapa $l ChildFlujoEtapa
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addFlujoEtapa(ChildFlujoEtapa $l)
    {
        if ($this->collFlujoEtapas === null) {
            $this->initFlujoEtapas();
            $this->collFlujoEtapasPartial = true;
        }

        if (!$this->collFlujoEtapas->contains($l)) {
            $this->doAddFlujoEtapa($l);

            if ($this->flujoEtapasScheduledForDeletion and $this->flujoEtapasScheduledForDeletion->contains($l)) {
                $this->flujoEtapasScheduledForDeletion->remove($this->flujoEtapasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFlujoEtapa $flujoEtapa The ChildFlujoEtapa object to add.
     */
    protected function doAddFlujoEtapa(ChildFlujoEtapa $flujoEtapa)
    {
        $this->collFlujoEtapas[]= $flujoEtapa;
        $flujoEtapa->setUsuario($this);
    }

    /**
     * @param  ChildFlujoEtapa $flujoEtapa The ChildFlujoEtapa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeFlujoEtapa(ChildFlujoEtapa $flujoEtapa)
    {
        if ($this->getFlujoEtapas()->contains($flujoEtapa)) {
            $pos = $this->collFlujoEtapas->search($flujoEtapa);
            $this->collFlujoEtapas->remove($pos);
            if (null === $this->flujoEtapasScheduledForDeletion) {
                $this->flujoEtapasScheduledForDeletion = clone $this->collFlujoEtapas;
                $this->flujoEtapasScheduledForDeletion->clear();
            }
            $this->flujoEtapasScheduledForDeletion[]= clone $flujoEtapa;
            $flujoEtapa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related FlujoEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     */
    public function getFlujoEtapasJoinEtapaRelatedByIdEtapa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFlujoEtapaQuery::create(null, $criteria);
        $query->joinWith('EtapaRelatedByIdEtapa', $joinBehavior);

        return $this->getFlujoEtapas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related FlujoEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     */
    public function getFlujoEtapasJoinEtapaRelatedByIdEtapaSiguiente(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFlujoEtapaQuery::create(null, $criteria);
        $query->joinWith('EtapaRelatedByIdEtapaSiguiente', $joinBehavior);

        return $this->getFlujoEtapas($query, $con);
    }

    /**
     * Clears out the collLogins collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLogins()
     */
    public function clearLogins()
    {
        $this->collLogins = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLogins collection loaded partially.
     */
    public function resetPartialLogins($v = true)
    {
        $this->collLoginsPartial = $v;
    }

    /**
     * Initializes the collLogins collection.
     *
     * By default this just sets the collLogins collection to an empty array (like clearcollLogins());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLogins($overrideExisting = true)
    {
        if (null !== $this->collLogins && !$overrideExisting) {
            return;
        }

        $collectionClassName = LoginTableMap::getTableMap()->getCollectionClassName();

        $this->collLogins = new $collectionClassName;
        $this->collLogins->setModel('\Login');
    }

    /**
     * Gets an array of ChildLogin objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLogin[] List of ChildLogin objects
     * @throws PropelException
     */
    public function getLogins(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLoginsPartial && !$this->isNew();
        if (null === $this->collLogins || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collLogins) {
                    $this->initLogins();
                } else {
                    $collectionClassName = LoginTableMap::getTableMap()->getCollectionClassName();

                    $collLogins = new $collectionClassName;
                    $collLogins->setModel('\Login');

                    return $collLogins;
                }
            } else {
                $collLogins = ChildLoginQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLoginsPartial && count($collLogins)) {
                        $this->initLogins(false);

                        foreach ($collLogins as $obj) {
                            if (false == $this->collLogins->contains($obj)) {
                                $this->collLogins->append($obj);
                            }
                        }

                        $this->collLoginsPartial = true;
                    }

                    return $collLogins;
                }

                if ($partial && $this->collLogins) {
                    foreach ($this->collLogins as $obj) {
                        if ($obj->isNew()) {
                            $collLogins[] = $obj;
                        }
                    }
                }

                $this->collLogins = $collLogins;
                $this->collLoginsPartial = false;
            }
        }

        return $this->collLogins;
    }

    /**
     * Sets a collection of ChildLogin objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $logins A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setLogins(Collection $logins, ConnectionInterface $con = null)
    {
        /** @var ChildLogin[] $loginsToDelete */
        $loginsToDelete = $this->getLogins(new Criteria(), $con)->diff($logins);


        $this->loginsScheduledForDeletion = $loginsToDelete;

        foreach ($loginsToDelete as $loginRemoved) {
            $loginRemoved->setUsuario(null);
        }

        $this->collLogins = null;
        foreach ($logins as $login) {
            $this->addLogin($login);
        }

        $this->collLogins = $logins;
        $this->collLoginsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Login objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Login objects.
     * @throws PropelException
     */
    public function countLogins(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLoginsPartial && !$this->isNew();
        if (null === $this->collLogins || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLogins) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLogins());
            }

            $query = ChildLoginQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collLogins);
    }

    /**
     * Method called to associate a ChildLogin object to this object
     * through the ChildLogin foreign key attribute.
     *
     * @param  ChildLogin $l ChildLogin
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addLogin(ChildLogin $l)
    {
        if ($this->collLogins === null) {
            $this->initLogins();
            $this->collLoginsPartial = true;
        }

        if (!$this->collLogins->contains($l)) {
            $this->doAddLogin($l);

            if ($this->loginsScheduledForDeletion and $this->loginsScheduledForDeletion->contains($l)) {
                $this->loginsScheduledForDeletion->remove($this->loginsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLogin $login The ChildLogin object to add.
     */
    protected function doAddLogin(ChildLogin $login)
    {
        $this->collLogins[]= $login;
        $login->setUsuario($this);
    }

    /**
     * @param  ChildLogin $login The ChildLogin object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeLogin(ChildLogin $login)
    {
        if ($this->getLogins()->contains($login)) {
            $pos = $this->collLogins->search($login);
            $this->collLogins->remove($pos);
            if (null === $this->loginsScheduledForDeletion) {
                $this->loginsScheduledForDeletion = clone $this->collLogins;
                $this->loginsScheduledForDeletion->clear();
            }
            $this->loginsScheduledForDeletion[]= clone $login;
            $login->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collProgramas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProgramas()
     */
    public function clearProgramas()
    {
        $this->collProgramas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProgramas collection loaded partially.
     */
    public function resetPartialProgramas($v = true)
    {
        $this->collProgramasPartial = $v;
    }

    /**
     * Initializes the collProgramas collection.
     *
     * By default this just sets the collProgramas collection to an empty array (like clearcollProgramas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProgramas($overrideExisting = true)
    {
        if (null !== $this->collProgramas && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProgramaTableMap::getTableMap()->getCollectionClassName();

        $this->collProgramas = new $collectionClassName;
        $this->collProgramas->setModel('\Programa');
    }

    /**
     * Gets an array of ChildPrograma objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPrograma[] List of ChildPrograma objects
     * @throws PropelException
     */
    public function getProgramas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramasPartial && !$this->isNew();
        if (null === $this->collProgramas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProgramas) {
                    $this->initProgramas();
                } else {
                    $collectionClassName = ProgramaTableMap::getTableMap()->getCollectionClassName();

                    $collProgramas = new $collectionClassName;
                    $collProgramas->setModel('\Programa');

                    return $collProgramas;
                }
            } else {
                $collProgramas = ChildProgramaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProgramasPartial && count($collProgramas)) {
                        $this->initProgramas(false);

                        foreach ($collProgramas as $obj) {
                            if (false == $this->collProgramas->contains($obj)) {
                                $this->collProgramas->append($obj);
                            }
                        }

                        $this->collProgramasPartial = true;
                    }

                    return $collProgramas;
                }

                if ($partial && $this->collProgramas) {
                    foreach ($this->collProgramas as $obj) {
                        if ($obj->isNew()) {
                            $collProgramas[] = $obj;
                        }
                    }
                }

                $this->collProgramas = $collProgramas;
                $this->collProgramasPartial = false;
            }
        }

        return $this->collProgramas;
    }

    /**
     * Sets a collection of ChildPrograma objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $programas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setProgramas(Collection $programas, ConnectionInterface $con = null)
    {
        /** @var ChildPrograma[] $programasToDelete */
        $programasToDelete = $this->getProgramas(new Criteria(), $con)->diff($programas);


        $this->programasScheduledForDeletion = $programasToDelete;

        foreach ($programasToDelete as $programaRemoved) {
            $programaRemoved->setUsuario(null);
        }

        $this->collProgramas = null;
        foreach ($programas as $programa) {
            $this->addPrograma($programa);
        }

        $this->collProgramas = $programas;
        $this->collProgramasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Programa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Programa objects.
     * @throws PropelException
     */
    public function countProgramas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramasPartial && !$this->isNew();
        if (null === $this->collProgramas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProgramas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProgramas());
            }

            $query = ChildProgramaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collProgramas);
    }

    /**
     * Method called to associate a ChildPrograma object to this object
     * through the ChildPrograma foreign key attribute.
     *
     * @param  ChildPrograma $l ChildPrograma
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addPrograma(ChildPrograma $l)
    {
        if ($this->collProgramas === null) {
            $this->initProgramas();
            $this->collProgramasPartial = true;
        }

        if (!$this->collProgramas->contains($l)) {
            $this->doAddPrograma($l);

            if ($this->programasScheduledForDeletion and $this->programasScheduledForDeletion->contains($l)) {
                $this->programasScheduledForDeletion->remove($this->programasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPrograma $programa The ChildPrograma object to add.
     */
    protected function doAddPrograma(ChildPrograma $programa)
    {
        $this->collProgramas[]= $programa;
        $programa->setUsuario($this);
    }

    /**
     * @param  ChildPrograma $programa The ChildPrograma object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removePrograma(ChildPrograma $programa)
    {
        if ($this->getProgramas()->contains($programa)) {
            $pos = $this->collProgramas->search($programa);
            $this->collProgramas->remove($pos);
            if (null === $this->programasScheduledForDeletion) {
                $this->programasScheduledForDeletion = clone $this->collProgramas;
                $this->programasScheduledForDeletion->clear();
            }
            $this->programasScheduledForDeletion[]= clone $programa;
            $programa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Programas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrograma[] List of ChildPrograma objects
     */
    public function getProgramasJoinEntidadOperativa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaQuery::create(null, $criteria);
        $query->joinWith('EntidadOperativa', $joinBehavior);

        return $this->getProgramas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Programas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrograma[] List of ChildPrograma objects
     */
    public function getProgramasJoinEstatusPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaQuery::create(null, $criteria);
        $query->joinWith('EstatusPrograma', $joinBehavior);

        return $this->getProgramas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related Programas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPrograma[] List of ChildPrograma objects
     */
    public function getProgramasJoinTipoBeneficiario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaQuery::create(null, $criteria);
        $query->joinWith('TipoBeneficiario', $joinBehavior);

        return $this->getProgramas($query, $con);
    }

    /**
     * Clears out the collProgramaRequisitos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProgramaRequisitos()
     */
    public function clearProgramaRequisitos()
    {
        $this->collProgramaRequisitos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProgramaRequisitos collection loaded partially.
     */
    public function resetPartialProgramaRequisitos($v = true)
    {
        $this->collProgramaRequisitosPartial = $v;
    }

    /**
     * Initializes the collProgramaRequisitos collection.
     *
     * By default this just sets the collProgramaRequisitos collection to an empty array (like clearcollProgramaRequisitos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProgramaRequisitos($overrideExisting = true)
    {
        if (null !== $this->collProgramaRequisitos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

        $this->collProgramaRequisitos = new $collectionClassName;
        $this->collProgramaRequisitos->setModel('\ProgramaRequisito');
    }

    /**
     * Gets an array of ChildProgramaRequisito objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     * @throws PropelException
     */
    public function getProgramaRequisitos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProgramaRequisitos) {
                    $this->initProgramaRequisitos();
                } else {
                    $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

                    $collProgramaRequisitos = new $collectionClassName;
                    $collProgramaRequisitos->setModel('\ProgramaRequisito');

                    return $collProgramaRequisitos;
                }
            } else {
                $collProgramaRequisitos = ChildProgramaRequisitoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProgramaRequisitosPartial && count($collProgramaRequisitos)) {
                        $this->initProgramaRequisitos(false);

                        foreach ($collProgramaRequisitos as $obj) {
                            if (false == $this->collProgramaRequisitos->contains($obj)) {
                                $this->collProgramaRequisitos->append($obj);
                            }
                        }

                        $this->collProgramaRequisitosPartial = true;
                    }

                    return $collProgramaRequisitos;
                }

                if ($partial && $this->collProgramaRequisitos) {
                    foreach ($this->collProgramaRequisitos as $obj) {
                        if ($obj->isNew()) {
                            $collProgramaRequisitos[] = $obj;
                        }
                    }
                }

                $this->collProgramaRequisitos = $collProgramaRequisitos;
                $this->collProgramaRequisitosPartial = false;
            }
        }

        return $this->collProgramaRequisitos;
    }

    /**
     * Sets a collection of ChildProgramaRequisito objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $programaRequisitos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setProgramaRequisitos(Collection $programaRequisitos, ConnectionInterface $con = null)
    {
        /** @var ChildProgramaRequisito[] $programaRequisitosToDelete */
        $programaRequisitosToDelete = $this->getProgramaRequisitos(new Criteria(), $con)->diff($programaRequisitos);


        $this->programaRequisitosScheduledForDeletion = $programaRequisitosToDelete;

        foreach ($programaRequisitosToDelete as $programaRequisitoRemoved) {
            $programaRequisitoRemoved->setUsuario(null);
        }

        $this->collProgramaRequisitos = null;
        foreach ($programaRequisitos as $programaRequisito) {
            $this->addProgramaRequisito($programaRequisito);
        }

        $this->collProgramaRequisitos = $programaRequisitos;
        $this->collProgramaRequisitosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProgramaRequisito objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProgramaRequisito objects.
     * @throws PropelException
     */
    public function countProgramaRequisitos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProgramaRequisitos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProgramaRequisitos());
            }

            $query = ChildProgramaRequisitoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collProgramaRequisitos);
    }

    /**
     * Method called to associate a ChildProgramaRequisito object to this object
     * through the ChildProgramaRequisito foreign key attribute.
     *
     * @param  ChildProgramaRequisito $l ChildProgramaRequisito
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addProgramaRequisito(ChildProgramaRequisito $l)
    {
        if ($this->collProgramaRequisitos === null) {
            $this->initProgramaRequisitos();
            $this->collProgramaRequisitosPartial = true;
        }

        if (!$this->collProgramaRequisitos->contains($l)) {
            $this->doAddProgramaRequisito($l);

            if ($this->programaRequisitosScheduledForDeletion and $this->programaRequisitosScheduledForDeletion->contains($l)) {
                $this->programaRequisitosScheduledForDeletion->remove($this->programaRequisitosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to add.
     */
    protected function doAddProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        $this->collProgramaRequisitos[]= $programaRequisito;
        $programaRequisito->setUsuario($this);
    }

    /**
     * @param  ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        if ($this->getProgramaRequisitos()->contains($programaRequisito)) {
            $pos = $this->collProgramaRequisitos->search($programaRequisito);
            $this->collProgramaRequisitos->remove($pos);
            if (null === $this->programaRequisitosScheduledForDeletion) {
                $this->programaRequisitosScheduledForDeletion = clone $this->collProgramaRequisitos;
                $this->programaRequisitosScheduledForDeletion->clear();
            }
            $this->programaRequisitosScheduledForDeletion[]= clone $programaRequisito;
            $programaRequisito->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinEtapa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Etapa', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinTipoRequisito(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('TipoRequisito', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }

    /**
     * Clears out the collRestablecerPasswords collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRestablecerPasswords()
     */
    public function clearRestablecerPasswords()
    {
        $this->collRestablecerPasswords = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRestablecerPasswords collection loaded partially.
     */
    public function resetPartialRestablecerPasswords($v = true)
    {
        $this->collRestablecerPasswordsPartial = $v;
    }

    /**
     * Initializes the collRestablecerPasswords collection.
     *
     * By default this just sets the collRestablecerPasswords collection to an empty array (like clearcollRestablecerPasswords());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRestablecerPasswords($overrideExisting = true)
    {
        if (null !== $this->collRestablecerPasswords && !$overrideExisting) {
            return;
        }

        $collectionClassName = RestablecerPasswordTableMap::getTableMap()->getCollectionClassName();

        $this->collRestablecerPasswords = new $collectionClassName;
        $this->collRestablecerPasswords->setModel('\RestablecerPassword');
    }

    /**
     * Gets an array of ChildRestablecerPassword objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRestablecerPassword[] List of ChildRestablecerPassword objects
     * @throws PropelException
     */
    public function getRestablecerPasswords(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRestablecerPasswordsPartial && !$this->isNew();
        if (null === $this->collRestablecerPasswords || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRestablecerPasswords) {
                    $this->initRestablecerPasswords();
                } else {
                    $collectionClassName = RestablecerPasswordTableMap::getTableMap()->getCollectionClassName();

                    $collRestablecerPasswords = new $collectionClassName;
                    $collRestablecerPasswords->setModel('\RestablecerPassword');

                    return $collRestablecerPasswords;
                }
            } else {
                $collRestablecerPasswords = ChildRestablecerPasswordQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRestablecerPasswordsPartial && count($collRestablecerPasswords)) {
                        $this->initRestablecerPasswords(false);

                        foreach ($collRestablecerPasswords as $obj) {
                            if (false == $this->collRestablecerPasswords->contains($obj)) {
                                $this->collRestablecerPasswords->append($obj);
                            }
                        }

                        $this->collRestablecerPasswordsPartial = true;
                    }

                    return $collRestablecerPasswords;
                }

                if ($partial && $this->collRestablecerPasswords) {
                    foreach ($this->collRestablecerPasswords as $obj) {
                        if ($obj->isNew()) {
                            $collRestablecerPasswords[] = $obj;
                        }
                    }
                }

                $this->collRestablecerPasswords = $collRestablecerPasswords;
                $this->collRestablecerPasswordsPartial = false;
            }
        }

        return $this->collRestablecerPasswords;
    }

    /**
     * Sets a collection of ChildRestablecerPassword objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $restablecerPasswords A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setRestablecerPasswords(Collection $restablecerPasswords, ConnectionInterface $con = null)
    {
        /** @var ChildRestablecerPassword[] $restablecerPasswordsToDelete */
        $restablecerPasswordsToDelete = $this->getRestablecerPasswords(new Criteria(), $con)->diff($restablecerPasswords);


        $this->restablecerPasswordsScheduledForDeletion = $restablecerPasswordsToDelete;

        foreach ($restablecerPasswordsToDelete as $restablecerPasswordRemoved) {
            $restablecerPasswordRemoved->setUsuario(null);
        }

        $this->collRestablecerPasswords = null;
        foreach ($restablecerPasswords as $restablecerPassword) {
            $this->addRestablecerPassword($restablecerPassword);
        }

        $this->collRestablecerPasswords = $restablecerPasswords;
        $this->collRestablecerPasswordsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RestablecerPassword objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RestablecerPassword objects.
     * @throws PropelException
     */
    public function countRestablecerPasswords(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRestablecerPasswordsPartial && !$this->isNew();
        if (null === $this->collRestablecerPasswords || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRestablecerPasswords) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRestablecerPasswords());
            }

            $query = ChildRestablecerPasswordQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRestablecerPasswords);
    }

    /**
     * Method called to associate a ChildRestablecerPassword object to this object
     * through the ChildRestablecerPassword foreign key attribute.
     *
     * @param  ChildRestablecerPassword $l ChildRestablecerPassword
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addRestablecerPassword(ChildRestablecerPassword $l)
    {
        if ($this->collRestablecerPasswords === null) {
            $this->initRestablecerPasswords();
            $this->collRestablecerPasswordsPartial = true;
        }

        if (!$this->collRestablecerPasswords->contains($l)) {
            $this->doAddRestablecerPassword($l);

            if ($this->restablecerPasswordsScheduledForDeletion and $this->restablecerPasswordsScheduledForDeletion->contains($l)) {
                $this->restablecerPasswordsScheduledForDeletion->remove($this->restablecerPasswordsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRestablecerPassword $restablecerPassword The ChildRestablecerPassword object to add.
     */
    protected function doAddRestablecerPassword(ChildRestablecerPassword $restablecerPassword)
    {
        $this->collRestablecerPasswords[]= $restablecerPassword;
        $restablecerPassword->setUsuario($this);
    }

    /**
     * @param  ChildRestablecerPassword $restablecerPassword The ChildRestablecerPassword object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeRestablecerPassword(ChildRestablecerPassword $restablecerPassword)
    {
        if ($this->getRestablecerPasswords()->contains($restablecerPassword)) {
            $pos = $this->collRestablecerPasswords->search($restablecerPassword);
            $this->collRestablecerPasswords->remove($pos);
            if (null === $this->restablecerPasswordsScheduledForDeletion) {
                $this->restablecerPasswordsScheduledForDeletion = clone $this->collRestablecerPasswords;
                $this->restablecerPasswordsScheduledForDeletion->clear();
            }
            $this->restablecerPasswordsScheduledForDeletion[]= clone $restablecerPassword;
            $restablecerPassword->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collRols collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRols()
     */
    public function clearRols()
    {
        $this->collRols = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRols collection loaded partially.
     */
    public function resetPartialRols($v = true)
    {
        $this->collRolsPartial = $v;
    }

    /**
     * Initializes the collRols collection.
     *
     * By default this just sets the collRols collection to an empty array (like clearcollRols());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRols($overrideExisting = true)
    {
        if (null !== $this->collRols && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolTableMap::getTableMap()->getCollectionClassName();

        $this->collRols = new $collectionClassName;
        $this->collRols->setModel('\Rol');
    }

    /**
     * Gets an array of ChildRol objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRol[] List of ChildRol objects
     * @throws PropelException
     */
    public function getRols(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolsPartial && !$this->isNew();
        if (null === $this->collRols || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRols) {
                    $this->initRols();
                } else {
                    $collectionClassName = RolTableMap::getTableMap()->getCollectionClassName();

                    $collRols = new $collectionClassName;
                    $collRols->setModel('\Rol');

                    return $collRols;
                }
            } else {
                $collRols = ChildRolQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolsPartial && count($collRols)) {
                        $this->initRols(false);

                        foreach ($collRols as $obj) {
                            if (false == $this->collRols->contains($obj)) {
                                $this->collRols->append($obj);
                            }
                        }

                        $this->collRolsPartial = true;
                    }

                    return $collRols;
                }

                if ($partial && $this->collRols) {
                    foreach ($this->collRols as $obj) {
                        if ($obj->isNew()) {
                            $collRols[] = $obj;
                        }
                    }
                }

                $this->collRols = $collRols;
                $this->collRolsPartial = false;
            }
        }

        return $this->collRols;
    }

    /**
     * Sets a collection of ChildRol objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rols A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setRols(Collection $rols, ConnectionInterface $con = null)
    {
        /** @var ChildRol[] $rolsToDelete */
        $rolsToDelete = $this->getRols(new Criteria(), $con)->diff($rols);


        $this->rolsScheduledForDeletion = $rolsToDelete;

        foreach ($rolsToDelete as $rolRemoved) {
            $rolRemoved->setUsuario(null);
        }

        $this->collRols = null;
        foreach ($rols as $rol) {
            $this->addRol($rol);
        }

        $this->collRols = $rols;
        $this->collRolsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Rol objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Rol objects.
     * @throws PropelException
     */
    public function countRols(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolsPartial && !$this->isNew();
        if (null === $this->collRols || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRols) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRols());
            }

            $query = ChildRolQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRols);
    }

    /**
     * Method called to associate a ChildRol object to this object
     * through the ChildRol foreign key attribute.
     *
     * @param  ChildRol $l ChildRol
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addRol(ChildRol $l)
    {
        if ($this->collRols === null) {
            $this->initRols();
            $this->collRolsPartial = true;
        }

        if (!$this->collRols->contains($l)) {
            $this->doAddRol($l);

            if ($this->rolsScheduledForDeletion and $this->rolsScheduledForDeletion->contains($l)) {
                $this->rolsScheduledForDeletion->remove($this->rolsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRol $rol The ChildRol object to add.
     */
    protected function doAddRol(ChildRol $rol)
    {
        $this->collRols[]= $rol;
        $rol->setUsuario($this);
    }

    /**
     * @param  ChildRol $rol The ChildRol object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeRol(ChildRol $rol)
    {
        if ($this->getRols()->contains($rol)) {
            $pos = $this->collRols->search($rol);
            $this->collRols->remove($pos);
            if (null === $this->rolsScheduledForDeletion) {
                $this->rolsScheduledForDeletion = clone $this->collRols;
                $this->rolsScheduledForDeletion->clear();
            }
            $this->rolsScheduledForDeletion[]= clone $rol;
            $rol->setUsuario(null);
        }

        return $this;
    }

    /**
     * Clears out the collRolPermisos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRolPermisos()
     */
    public function clearRolPermisos()
    {
        $this->collRolPermisos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRolPermisos collection loaded partially.
     */
    public function resetPartialRolPermisos($v = true)
    {
        $this->collRolPermisosPartial = $v;
    }

    /**
     * Initializes the collRolPermisos collection.
     *
     * By default this just sets the collRolPermisos collection to an empty array (like clearcollRolPermisos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolPermisos($overrideExisting = true)
    {
        if (null !== $this->collRolPermisos && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolPermisoTableMap::getTableMap()->getCollectionClassName();

        $this->collRolPermisos = new $collectionClassName;
        $this->collRolPermisos->setModel('\RolPermiso');
    }

    /**
     * Gets an array of ChildRolPermiso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     * @throws PropelException
     */
    public function getRolPermisos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolPermisosPartial && !$this->isNew();
        if (null === $this->collRolPermisos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRolPermisos) {
                    $this->initRolPermisos();
                } else {
                    $collectionClassName = RolPermisoTableMap::getTableMap()->getCollectionClassName();

                    $collRolPermisos = new $collectionClassName;
                    $collRolPermisos->setModel('\RolPermiso');

                    return $collRolPermisos;
                }
            } else {
                $collRolPermisos = ChildRolPermisoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolPermisosPartial && count($collRolPermisos)) {
                        $this->initRolPermisos(false);

                        foreach ($collRolPermisos as $obj) {
                            if (false == $this->collRolPermisos->contains($obj)) {
                                $this->collRolPermisos->append($obj);
                            }
                        }

                        $this->collRolPermisosPartial = true;
                    }

                    return $collRolPermisos;
                }

                if ($partial && $this->collRolPermisos) {
                    foreach ($this->collRolPermisos as $obj) {
                        if ($obj->isNew()) {
                            $collRolPermisos[] = $obj;
                        }
                    }
                }

                $this->collRolPermisos = $collRolPermisos;
                $this->collRolPermisosPartial = false;
            }
        }

        return $this->collRolPermisos;
    }

    /**
     * Sets a collection of ChildRolPermiso objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rolPermisos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setRolPermisos(Collection $rolPermisos, ConnectionInterface $con = null)
    {
        /** @var ChildRolPermiso[] $rolPermisosToDelete */
        $rolPermisosToDelete = $this->getRolPermisos(new Criteria(), $con)->diff($rolPermisos);


        $this->rolPermisosScheduledForDeletion = $rolPermisosToDelete;

        foreach ($rolPermisosToDelete as $rolPermisoRemoved) {
            $rolPermisoRemoved->setUsuario(null);
        }

        $this->collRolPermisos = null;
        foreach ($rolPermisos as $rolPermiso) {
            $this->addRolPermiso($rolPermiso);
        }

        $this->collRolPermisos = $rolPermisos;
        $this->collRolPermisosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RolPermiso objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RolPermiso objects.
     * @throws PropelException
     */
    public function countRolPermisos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolPermisosPartial && !$this->isNew();
        if (null === $this->collRolPermisos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolPermisos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolPermisos());
            }

            $query = ChildRolPermisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRolPermisos);
    }

    /**
     * Method called to associate a ChildRolPermiso object to this object
     * through the ChildRolPermiso foreign key attribute.
     *
     * @param  ChildRolPermiso $l ChildRolPermiso
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addRolPermiso(ChildRolPermiso $l)
    {
        if ($this->collRolPermisos === null) {
            $this->initRolPermisos();
            $this->collRolPermisosPartial = true;
        }

        if (!$this->collRolPermisos->contains($l)) {
            $this->doAddRolPermiso($l);

            if ($this->rolPermisosScheduledForDeletion and $this->rolPermisosScheduledForDeletion->contains($l)) {
                $this->rolPermisosScheduledForDeletion->remove($this->rolPermisosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRolPermiso $rolPermiso The ChildRolPermiso object to add.
     */
    protected function doAddRolPermiso(ChildRolPermiso $rolPermiso)
    {
        $this->collRolPermisos[]= $rolPermiso;
        $rolPermiso->setUsuario($this);
    }

    /**
     * @param  ChildRolPermiso $rolPermiso The ChildRolPermiso object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeRolPermiso(ChildRolPermiso $rolPermiso)
    {
        if ($this->getRolPermisos()->contains($rolPermiso)) {
            $pos = $this->collRolPermisos->search($rolPermiso);
            $this->collRolPermisos->remove($pos);
            if (null === $this->rolPermisosScheduledForDeletion) {
                $this->rolPermisosScheduledForDeletion = clone $this->collRolPermisos;
                $this->rolPermisosScheduledForDeletion->clear();
            }
            $this->rolPermisosScheduledForDeletion[]= clone $rolPermiso;
            $rolPermiso->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RolPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     */
    public function getRolPermisosJoinRol(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolPermisoQuery::create(null, $criteria);
        $query->joinWith('Rol', $joinBehavior);

        return $this->getRolPermisos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RolPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     */
    public function getRolPermisosJoinPermisos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolPermisoQuery::create(null, $criteria);
        $query->joinWith('Permisos', $joinBehavior);

        return $this->getRolPermisos($query, $con);
    }

    /**
     * Clears out the collRolUsuariosRelatedByIdUsuario collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRolUsuariosRelatedByIdUsuario()
     */
    public function clearRolUsuariosRelatedByIdUsuario()
    {
        $this->collRolUsuariosRelatedByIdUsuario = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRolUsuariosRelatedByIdUsuario collection loaded partially.
     */
    public function resetPartialRolUsuariosRelatedByIdUsuario($v = true)
    {
        $this->collRolUsuariosRelatedByIdUsuarioPartial = $v;
    }

    /**
     * Initializes the collRolUsuariosRelatedByIdUsuario collection.
     *
     * By default this just sets the collRolUsuariosRelatedByIdUsuario collection to an empty array (like clearcollRolUsuariosRelatedByIdUsuario());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolUsuariosRelatedByIdUsuario($overrideExisting = true)
    {
        if (null !== $this->collRolUsuariosRelatedByIdUsuario && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolUsuarioTableMap::getTableMap()->getCollectionClassName();

        $this->collRolUsuariosRelatedByIdUsuario = new $collectionClassName;
        $this->collRolUsuariosRelatedByIdUsuario->setModel('\RolUsuario');
    }

    /**
     * Gets an array of ChildRolUsuario objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRolUsuario[] List of ChildRolUsuario objects
     * @throws PropelException
     */
    public function getRolUsuariosRelatedByIdUsuario(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolUsuariosRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collRolUsuariosRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRolUsuariosRelatedByIdUsuario) {
                    $this->initRolUsuariosRelatedByIdUsuario();
                } else {
                    $collectionClassName = RolUsuarioTableMap::getTableMap()->getCollectionClassName();

                    $collRolUsuariosRelatedByIdUsuario = new $collectionClassName;
                    $collRolUsuariosRelatedByIdUsuario->setModel('\RolUsuario');

                    return $collRolUsuariosRelatedByIdUsuario;
                }
            } else {
                $collRolUsuariosRelatedByIdUsuario = ChildRolUsuarioQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolUsuariosRelatedByIdUsuarioPartial && count($collRolUsuariosRelatedByIdUsuario)) {
                        $this->initRolUsuariosRelatedByIdUsuario(false);

                        foreach ($collRolUsuariosRelatedByIdUsuario as $obj) {
                            if (false == $this->collRolUsuariosRelatedByIdUsuario->contains($obj)) {
                                $this->collRolUsuariosRelatedByIdUsuario->append($obj);
                            }
                        }

                        $this->collRolUsuariosRelatedByIdUsuarioPartial = true;
                    }

                    return $collRolUsuariosRelatedByIdUsuario;
                }

                if ($partial && $this->collRolUsuariosRelatedByIdUsuario) {
                    foreach ($this->collRolUsuariosRelatedByIdUsuario as $obj) {
                        if ($obj->isNew()) {
                            $collRolUsuariosRelatedByIdUsuario[] = $obj;
                        }
                    }
                }

                $this->collRolUsuariosRelatedByIdUsuario = $collRolUsuariosRelatedByIdUsuario;
                $this->collRolUsuariosRelatedByIdUsuarioPartial = false;
            }
        }

        return $this->collRolUsuariosRelatedByIdUsuario;
    }

    /**
     * Sets a collection of ChildRolUsuario objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rolUsuariosRelatedByIdUsuario A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setRolUsuariosRelatedByIdUsuario(Collection $rolUsuariosRelatedByIdUsuario, ConnectionInterface $con = null)
    {
        /** @var ChildRolUsuario[] $rolUsuariosRelatedByIdUsuarioToDelete */
        $rolUsuariosRelatedByIdUsuarioToDelete = $this->getRolUsuariosRelatedByIdUsuario(new Criteria(), $con)->diff($rolUsuariosRelatedByIdUsuario);


        $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion = $rolUsuariosRelatedByIdUsuarioToDelete;

        foreach ($rolUsuariosRelatedByIdUsuarioToDelete as $rolUsuarioRelatedByIdUsuarioRemoved) {
            $rolUsuarioRelatedByIdUsuarioRemoved->setUsuarioRelatedByIdUsuario(null);
        }

        $this->collRolUsuariosRelatedByIdUsuario = null;
        foreach ($rolUsuariosRelatedByIdUsuario as $rolUsuarioRelatedByIdUsuario) {
            $this->addRolUsuarioRelatedByIdUsuario($rolUsuarioRelatedByIdUsuario);
        }

        $this->collRolUsuariosRelatedByIdUsuario = $rolUsuariosRelatedByIdUsuario;
        $this->collRolUsuariosRelatedByIdUsuarioPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RolUsuario objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RolUsuario objects.
     * @throws PropelException
     */
    public function countRolUsuariosRelatedByIdUsuario(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolUsuariosRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collRolUsuariosRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolUsuariosRelatedByIdUsuario) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolUsuariosRelatedByIdUsuario());
            }

            $query = ChildRolUsuarioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuario($this)
                ->count($con);
        }

        return count($this->collRolUsuariosRelatedByIdUsuario);
    }

    /**
     * Method called to associate a ChildRolUsuario object to this object
     * through the ChildRolUsuario foreign key attribute.
     *
     * @param  ChildRolUsuario $l ChildRolUsuario
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addRolUsuarioRelatedByIdUsuario(ChildRolUsuario $l)
    {
        if ($this->collRolUsuariosRelatedByIdUsuario === null) {
            $this->initRolUsuariosRelatedByIdUsuario();
            $this->collRolUsuariosRelatedByIdUsuarioPartial = true;
        }

        if (!$this->collRolUsuariosRelatedByIdUsuario->contains($l)) {
            $this->doAddRolUsuarioRelatedByIdUsuario($l);

            if ($this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion and $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->contains($l)) {
                $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->remove($this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRolUsuario $rolUsuarioRelatedByIdUsuario The ChildRolUsuario object to add.
     */
    protected function doAddRolUsuarioRelatedByIdUsuario(ChildRolUsuario $rolUsuarioRelatedByIdUsuario)
    {
        $this->collRolUsuariosRelatedByIdUsuario[]= $rolUsuarioRelatedByIdUsuario;
        $rolUsuarioRelatedByIdUsuario->setUsuarioRelatedByIdUsuario($this);
    }

    /**
     * @param  ChildRolUsuario $rolUsuarioRelatedByIdUsuario The ChildRolUsuario object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeRolUsuarioRelatedByIdUsuario(ChildRolUsuario $rolUsuarioRelatedByIdUsuario)
    {
        if ($this->getRolUsuariosRelatedByIdUsuario()->contains($rolUsuarioRelatedByIdUsuario)) {
            $pos = $this->collRolUsuariosRelatedByIdUsuario->search($rolUsuarioRelatedByIdUsuario);
            $this->collRolUsuariosRelatedByIdUsuario->remove($pos);
            if (null === $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion) {
                $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion = clone $this->collRolUsuariosRelatedByIdUsuario;
                $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion->clear();
            }
            $this->rolUsuariosRelatedByIdUsuarioScheduledForDeletion[]= clone $rolUsuarioRelatedByIdUsuario;
            $rolUsuarioRelatedByIdUsuario->setUsuarioRelatedByIdUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RolUsuariosRelatedByIdUsuario from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolUsuario[] List of ChildRolUsuario objects
     */
    public function getRolUsuariosRelatedByIdUsuarioJoinRol(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolUsuarioQuery::create(null, $criteria);
        $query->joinWith('Rol', $joinBehavior);

        return $this->getRolUsuariosRelatedByIdUsuario($query, $con);
    }

    /**
     * Clears out the collRolUsuariosRelatedByIdUsuarioModificacion collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRolUsuariosRelatedByIdUsuarioModificacion()
     */
    public function clearRolUsuariosRelatedByIdUsuarioModificacion()
    {
        $this->collRolUsuariosRelatedByIdUsuarioModificacion = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRolUsuariosRelatedByIdUsuarioModificacion collection loaded partially.
     */
    public function resetPartialRolUsuariosRelatedByIdUsuarioModificacion($v = true)
    {
        $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial = $v;
    }

    /**
     * Initializes the collRolUsuariosRelatedByIdUsuarioModificacion collection.
     *
     * By default this just sets the collRolUsuariosRelatedByIdUsuarioModificacion collection to an empty array (like clearcollRolUsuariosRelatedByIdUsuarioModificacion());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolUsuariosRelatedByIdUsuarioModificacion($overrideExisting = true)
    {
        if (null !== $this->collRolUsuariosRelatedByIdUsuarioModificacion && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolUsuarioTableMap::getTableMap()->getCollectionClassName();

        $this->collRolUsuariosRelatedByIdUsuarioModificacion = new $collectionClassName;
        $this->collRolUsuariosRelatedByIdUsuarioModificacion->setModel('\RolUsuario');
    }

    /**
     * Gets an array of ChildRolUsuario objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRolUsuario[] List of ChildRolUsuario objects
     * @throws PropelException
     */
    public function getRolUsuariosRelatedByIdUsuarioModificacion(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collRolUsuariosRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRolUsuariosRelatedByIdUsuarioModificacion) {
                    $this->initRolUsuariosRelatedByIdUsuarioModificacion();
                } else {
                    $collectionClassName = RolUsuarioTableMap::getTableMap()->getCollectionClassName();

                    $collRolUsuariosRelatedByIdUsuarioModificacion = new $collectionClassName;
                    $collRolUsuariosRelatedByIdUsuarioModificacion->setModel('\RolUsuario');

                    return $collRolUsuariosRelatedByIdUsuarioModificacion;
                }
            } else {
                $collRolUsuariosRelatedByIdUsuarioModificacion = ChildRolUsuarioQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial && count($collRolUsuariosRelatedByIdUsuarioModificacion)) {
                        $this->initRolUsuariosRelatedByIdUsuarioModificacion(false);

                        foreach ($collRolUsuariosRelatedByIdUsuarioModificacion as $obj) {
                            if (false == $this->collRolUsuariosRelatedByIdUsuarioModificacion->contains($obj)) {
                                $this->collRolUsuariosRelatedByIdUsuarioModificacion->append($obj);
                            }
                        }

                        $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial = true;
                    }

                    return $collRolUsuariosRelatedByIdUsuarioModificacion;
                }

                if ($partial && $this->collRolUsuariosRelatedByIdUsuarioModificacion) {
                    foreach ($this->collRolUsuariosRelatedByIdUsuarioModificacion as $obj) {
                        if ($obj->isNew()) {
                            $collRolUsuariosRelatedByIdUsuarioModificacion[] = $obj;
                        }
                    }
                }

                $this->collRolUsuariosRelatedByIdUsuarioModificacion = $collRolUsuariosRelatedByIdUsuarioModificacion;
                $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial = false;
            }
        }

        return $this->collRolUsuariosRelatedByIdUsuarioModificacion;
    }

    /**
     * Sets a collection of ChildRolUsuario objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rolUsuariosRelatedByIdUsuarioModificacion A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setRolUsuariosRelatedByIdUsuarioModificacion(Collection $rolUsuariosRelatedByIdUsuarioModificacion, ConnectionInterface $con = null)
    {
        /** @var ChildRolUsuario[] $rolUsuariosRelatedByIdUsuarioModificacionToDelete */
        $rolUsuariosRelatedByIdUsuarioModificacionToDelete = $this->getRolUsuariosRelatedByIdUsuarioModificacion(new Criteria(), $con)->diff($rolUsuariosRelatedByIdUsuarioModificacion);


        $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion = $rolUsuariosRelatedByIdUsuarioModificacionToDelete;

        foreach ($rolUsuariosRelatedByIdUsuarioModificacionToDelete as $rolUsuarioRelatedByIdUsuarioModificacionRemoved) {
            $rolUsuarioRelatedByIdUsuarioModificacionRemoved->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        $this->collRolUsuariosRelatedByIdUsuarioModificacion = null;
        foreach ($rolUsuariosRelatedByIdUsuarioModificacion as $rolUsuarioRelatedByIdUsuarioModificacion) {
            $this->addRolUsuarioRelatedByIdUsuarioModificacion($rolUsuarioRelatedByIdUsuarioModificacion);
        }

        $this->collRolUsuariosRelatedByIdUsuarioModificacion = $rolUsuariosRelatedByIdUsuarioModificacion;
        $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RolUsuario objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RolUsuario objects.
     * @throws PropelException
     */
    public function countRolUsuariosRelatedByIdUsuarioModificacion(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collRolUsuariosRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolUsuariosRelatedByIdUsuarioModificacion) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolUsuariosRelatedByIdUsuarioModificacion());
            }

            $query = ChildRolUsuarioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                ->count($con);
        }

        return count($this->collRolUsuariosRelatedByIdUsuarioModificacion);
    }

    /**
     * Method called to associate a ChildRolUsuario object to this object
     * through the ChildRolUsuario foreign key attribute.
     *
     * @param  ChildRolUsuario $l ChildRolUsuario
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addRolUsuarioRelatedByIdUsuarioModificacion(ChildRolUsuario $l)
    {
        if ($this->collRolUsuariosRelatedByIdUsuarioModificacion === null) {
            $this->initRolUsuariosRelatedByIdUsuarioModificacion();
            $this->collRolUsuariosRelatedByIdUsuarioModificacionPartial = true;
        }

        if (!$this->collRolUsuariosRelatedByIdUsuarioModificacion->contains($l)) {
            $this->doAddRolUsuarioRelatedByIdUsuarioModificacion($l);

            if ($this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion and $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->contains($l)) {
                $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->remove($this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRolUsuario $rolUsuarioRelatedByIdUsuarioModificacion The ChildRolUsuario object to add.
     */
    protected function doAddRolUsuarioRelatedByIdUsuarioModificacion(ChildRolUsuario $rolUsuarioRelatedByIdUsuarioModificacion)
    {
        $this->collRolUsuariosRelatedByIdUsuarioModificacion[]= $rolUsuarioRelatedByIdUsuarioModificacion;
        $rolUsuarioRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion($this);
    }

    /**
     * @param  ChildRolUsuario $rolUsuarioRelatedByIdUsuarioModificacion The ChildRolUsuario object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeRolUsuarioRelatedByIdUsuarioModificacion(ChildRolUsuario $rolUsuarioRelatedByIdUsuarioModificacion)
    {
        if ($this->getRolUsuariosRelatedByIdUsuarioModificacion()->contains($rolUsuarioRelatedByIdUsuarioModificacion)) {
            $pos = $this->collRolUsuariosRelatedByIdUsuarioModificacion->search($rolUsuarioRelatedByIdUsuarioModificacion);
            $this->collRolUsuariosRelatedByIdUsuarioModificacion->remove($pos);
            if (null === $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion) {
                $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion = clone $this->collRolUsuariosRelatedByIdUsuarioModificacion;
                $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion->clear();
            }
            $this->rolUsuariosRelatedByIdUsuarioModificacionScheduledForDeletion[]= clone $rolUsuarioRelatedByIdUsuarioModificacion;
            $rolUsuarioRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RolUsuariosRelatedByIdUsuarioModificacion from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolUsuario[] List of ChildRolUsuario objects
     */
    public function getRolUsuariosRelatedByIdUsuarioModificacionJoinRol(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolUsuarioQuery::create(null, $criteria);
        $query->joinWith('Rol', $joinBehavior);

        return $this->getRolUsuariosRelatedByIdUsuarioModificacion($query, $con);
    }

    /**
     * Clears out the collUsuariosRelatedByClave collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuariosRelatedByClave()
     */
    public function clearUsuariosRelatedByClave()
    {
        $this->collUsuariosRelatedByClave = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuariosRelatedByClave collection loaded partially.
     */
    public function resetPartialUsuariosRelatedByClave($v = true)
    {
        $this->collUsuariosRelatedByClavePartial = $v;
    }

    /**
     * Initializes the collUsuariosRelatedByClave collection.
     *
     * By default this just sets the collUsuariosRelatedByClave collection to an empty array (like clearcollUsuariosRelatedByClave());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuariosRelatedByClave($overrideExisting = true)
    {
        if (null !== $this->collUsuariosRelatedByClave && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuariosRelatedByClave = new $collectionClassName;
        $this->collUsuariosRelatedByClave->setModel('\Usuario');
    }

    /**
     * Gets an array of ChildUsuario objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuario[] List of ChildUsuario objects
     * @throws PropelException
     */
    public function getUsuariosRelatedByClave(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuariosRelatedByClavePartial && !$this->isNew();
        if (null === $this->collUsuariosRelatedByClave || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuariosRelatedByClave) {
                    $this->initUsuariosRelatedByClave();
                } else {
                    $collectionClassName = UsuarioTableMap::getTableMap()->getCollectionClassName();

                    $collUsuariosRelatedByClave = new $collectionClassName;
                    $collUsuariosRelatedByClave->setModel('\Usuario');

                    return $collUsuariosRelatedByClave;
                }
            } else {
                $collUsuariosRelatedByClave = ChildUsuarioQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuariosRelatedByClavePartial && count($collUsuariosRelatedByClave)) {
                        $this->initUsuariosRelatedByClave(false);

                        foreach ($collUsuariosRelatedByClave as $obj) {
                            if (false == $this->collUsuariosRelatedByClave->contains($obj)) {
                                $this->collUsuariosRelatedByClave->append($obj);
                            }
                        }

                        $this->collUsuariosRelatedByClavePartial = true;
                    }

                    return $collUsuariosRelatedByClave;
                }

                if ($partial && $this->collUsuariosRelatedByClave) {
                    foreach ($this->collUsuariosRelatedByClave as $obj) {
                        if ($obj->isNew()) {
                            $collUsuariosRelatedByClave[] = $obj;
                        }
                    }
                }

                $this->collUsuariosRelatedByClave = $collUsuariosRelatedByClave;
                $this->collUsuariosRelatedByClavePartial = false;
            }
        }

        return $this->collUsuariosRelatedByClave;
    }

    /**
     * Sets a collection of ChildUsuario objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuariosRelatedByClave A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuariosRelatedByClave(Collection $usuariosRelatedByClave, ConnectionInterface $con = null)
    {
        /** @var ChildUsuario[] $usuariosRelatedByClaveToDelete */
        $usuariosRelatedByClaveToDelete = $this->getUsuariosRelatedByClave(new Criteria(), $con)->diff($usuariosRelatedByClave);


        $this->usuariosRelatedByClaveScheduledForDeletion = $usuariosRelatedByClaveToDelete;

        foreach ($usuariosRelatedByClaveToDelete as $usuarioRelatedByClaveRemoved) {
            $usuarioRelatedByClaveRemoved->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        $this->collUsuariosRelatedByClave = null;
        foreach ($usuariosRelatedByClave as $usuarioRelatedByClave) {
            $this->addUsuarioRelatedByClave($usuarioRelatedByClave);
        }

        $this->collUsuariosRelatedByClave = $usuariosRelatedByClave;
        $this->collUsuariosRelatedByClavePartial = false;

        return $this;
    }

    /**
     * Returns the number of related Usuario objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Usuario objects.
     * @throws PropelException
     */
    public function countUsuariosRelatedByClave(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuariosRelatedByClavePartial && !$this->isNew();
        if (null === $this->collUsuariosRelatedByClave || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuariosRelatedByClave) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuariosRelatedByClave());
            }

            $query = ChildUsuarioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                ->count($con);
        }

        return count($this->collUsuariosRelatedByClave);
    }

    /**
     * Method called to associate a ChildUsuario object to this object
     * through the ChildUsuario foreign key attribute.
     *
     * @param  ChildUsuario $l ChildUsuario
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioRelatedByClave(ChildUsuario $l)
    {
        if ($this->collUsuariosRelatedByClave === null) {
            $this->initUsuariosRelatedByClave();
            $this->collUsuariosRelatedByClavePartial = true;
        }

        if (!$this->collUsuariosRelatedByClave->contains($l)) {
            $this->doAddUsuarioRelatedByClave($l);

            if ($this->usuariosRelatedByClaveScheduledForDeletion and $this->usuariosRelatedByClaveScheduledForDeletion->contains($l)) {
                $this->usuariosRelatedByClaveScheduledForDeletion->remove($this->usuariosRelatedByClaveScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuario $usuarioRelatedByClave The ChildUsuario object to add.
     */
    protected function doAddUsuarioRelatedByClave(ChildUsuario $usuarioRelatedByClave)
    {
        $this->collUsuariosRelatedByClave[]= $usuarioRelatedByClave;
        $usuarioRelatedByClave->setUsuarioRelatedByIdUsuarioModificacion($this);
    }

    /**
     * @param  ChildUsuario $usuarioRelatedByClave The ChildUsuario object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioRelatedByClave(ChildUsuario $usuarioRelatedByClave)
    {
        if ($this->getUsuariosRelatedByClave()->contains($usuarioRelatedByClave)) {
            $pos = $this->collUsuariosRelatedByClave->search($usuarioRelatedByClave);
            $this->collUsuariosRelatedByClave->remove($pos);
            if (null === $this->usuariosRelatedByClaveScheduledForDeletion) {
                $this->usuariosRelatedByClaveScheduledForDeletion = clone $this->collUsuariosRelatedByClave;
                $this->usuariosRelatedByClaveScheduledForDeletion->clear();
            }
            $this->usuariosRelatedByClaveScheduledForDeletion[]= $usuarioRelatedByClave;
            $usuarioRelatedByClave->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuariosRelatedByClave from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuario[] List of ChildUsuario objects
     */
    public function getUsuariosRelatedByClaveJoinEntidadOperativa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioQuery::create(null, $criteria);
        $query->joinWith('EntidadOperativa', $joinBehavior);

        return $this->getUsuariosRelatedByClave($query, $con);
    }

    /**
     * Clears out the collUsuarioConfiguracionEtapas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioConfiguracionEtapas()
     */
    public function clearUsuarioConfiguracionEtapas()
    {
        $this->collUsuarioConfiguracionEtapas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioConfiguracionEtapas collection loaded partially.
     */
    public function resetPartialUsuarioConfiguracionEtapas($v = true)
    {
        $this->collUsuarioConfiguracionEtapasPartial = $v;
    }

    /**
     * Initializes the collUsuarioConfiguracionEtapas collection.
     *
     * By default this just sets the collUsuarioConfiguracionEtapas collection to an empty array (like clearcollUsuarioConfiguracionEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionEtapas($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionEtapas && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioConfiguracionEtapas = new $collectionClassName;
        $this->collUsuarioConfiguracionEtapas->setModel('\UsuarioConfiguracionEtapa');
    }

    /**
     * Gets an array of ChildUsuarioConfiguracionEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     * @throws PropelException
     */
    public function getUsuarioConfiguracionEtapas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionEtapasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionEtapas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioConfiguracionEtapas) {
                    $this->initUsuarioConfiguracionEtapas();
                } else {
                    $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioConfiguracionEtapas = new $collectionClassName;
                    $collUsuarioConfiguracionEtapas->setModel('\UsuarioConfiguracionEtapa');

                    return $collUsuarioConfiguracionEtapas;
                }
            } else {
                $collUsuarioConfiguracionEtapas = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioConfiguracionEtapasPartial && count($collUsuarioConfiguracionEtapas)) {
                        $this->initUsuarioConfiguracionEtapas(false);

                        foreach ($collUsuarioConfiguracionEtapas as $obj) {
                            if (false == $this->collUsuarioConfiguracionEtapas->contains($obj)) {
                                $this->collUsuarioConfiguracionEtapas->append($obj);
                            }
                        }

                        $this->collUsuarioConfiguracionEtapasPartial = true;
                    }

                    return $collUsuarioConfiguracionEtapas;
                }

                if ($partial && $this->collUsuarioConfiguracionEtapas) {
                    foreach ($this->collUsuarioConfiguracionEtapas as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioConfiguracionEtapas[] = $obj;
                        }
                    }
                }

                $this->collUsuarioConfiguracionEtapas = $collUsuarioConfiguracionEtapas;
                $this->collUsuarioConfiguracionEtapasPartial = false;
            }
        }

        return $this->collUsuarioConfiguracionEtapas;
    }

    /**
     * Sets a collection of ChildUsuarioConfiguracionEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioConfiguracionEtapas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuarioConfiguracionEtapas(Collection $usuarioConfiguracionEtapas, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioConfiguracionEtapa[] $usuarioConfiguracionEtapasToDelete */
        $usuarioConfiguracionEtapasToDelete = $this->getUsuarioConfiguracionEtapas(new Criteria(), $con)->diff($usuarioConfiguracionEtapas);


        $this->usuarioConfiguracionEtapasScheduledForDeletion = $usuarioConfiguracionEtapasToDelete;

        foreach ($usuarioConfiguracionEtapasToDelete as $usuarioConfiguracionEtapaRemoved) {
            $usuarioConfiguracionEtapaRemoved->setUsuario(null);
        }

        $this->collUsuarioConfiguracionEtapas = null;
        foreach ($usuarioConfiguracionEtapas as $usuarioConfiguracionEtapa) {
            $this->addUsuarioConfiguracionEtapa($usuarioConfiguracionEtapa);
        }

        $this->collUsuarioConfiguracionEtapas = $usuarioConfiguracionEtapas;
        $this->collUsuarioConfiguracionEtapasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioConfiguracionEtapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioConfiguracionEtapa objects.
     * @throws PropelException
     */
    public function countUsuarioConfiguracionEtapas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionEtapasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionEtapas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioConfiguracionEtapas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioConfiguracionEtapas());
            }

            $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collUsuarioConfiguracionEtapas);
    }

    /**
     * Method called to associate a ChildUsuarioConfiguracionEtapa object to this object
     * through the ChildUsuarioConfiguracionEtapa foreign key attribute.
     *
     * @param  ChildUsuarioConfiguracionEtapa $l ChildUsuarioConfiguracionEtapa
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $l)
    {
        if ($this->collUsuarioConfiguracionEtapas === null) {
            $this->initUsuarioConfiguracionEtapas();
            $this->collUsuarioConfiguracionEtapasPartial = true;
        }

        if (!$this->collUsuarioConfiguracionEtapas->contains($l)) {
            $this->doAddUsuarioConfiguracionEtapa($l);

            if ($this->usuarioConfiguracionEtapasScheduledForDeletion and $this->usuarioConfiguracionEtapasScheduledForDeletion->contains($l)) {
                $this->usuarioConfiguracionEtapasScheduledForDeletion->remove($this->usuarioConfiguracionEtapasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa The ChildUsuarioConfiguracionEtapa object to add.
     */
    protected function doAddUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa)
    {
        $this->collUsuarioConfiguracionEtapas[]= $usuarioConfiguracionEtapa;
        $usuarioConfiguracionEtapa->setUsuario($this);
    }

    /**
     * @param  ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa The ChildUsuarioConfiguracionEtapa object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa)
    {
        if ($this->getUsuarioConfiguracionEtapas()->contains($usuarioConfiguracionEtapa)) {
            $pos = $this->collUsuarioConfiguracionEtapas->search($usuarioConfiguracionEtapa);
            $this->collUsuarioConfiguracionEtapas->remove($pos);
            if (null === $this->usuarioConfiguracionEtapasScheduledForDeletion) {
                $this->usuarioConfiguracionEtapasScheduledForDeletion = clone $this->collUsuarioConfiguracionEtapas;
                $this->usuarioConfiguracionEtapasScheduledForDeletion->clear();
            }
            $this->usuarioConfiguracionEtapasScheduledForDeletion[]= clone $usuarioConfiguracionEtapa;
            $usuarioConfiguracionEtapa->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioConfiguracionEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     */
    public function getUsuarioConfiguracionEtapasJoinEtapa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
        $query->joinWith('Etapa', $joinBehavior);

        return $this->getUsuarioConfiguracionEtapas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioConfiguracionEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     */
    public function getUsuarioConfiguracionEtapasJoinUsuarioConfiguracionPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
        $query->joinWith('UsuarioConfiguracionPrograma', $joinBehavior);

        return $this->getUsuarioConfiguracionEtapas($query, $con);
    }

    /**
     * Clears out the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioConfiguracionProgramasRelatedByIdUsuario()
     */
    public function clearUsuarioConfiguracionProgramasRelatedByIdUsuario()
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection loaded partially.
     */
    public function resetPartialUsuarioConfiguracionProgramasRelatedByIdUsuario($v = true)
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial = $v;
    }

    /**
     * Initializes the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection.
     *
     * By default this just sets the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection to an empty array (like clearcollUsuarioConfiguracionProgramasRelatedByIdUsuario());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionProgramasRelatedByIdUsuario($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = new $collectionClassName;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->setModel('\UsuarioConfiguracionPrograma');
    }

    /**
     * Gets an array of ChildUsuarioConfiguracionPrograma objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     * @throws PropelException
     */
    public function getUsuarioConfiguracionProgramasRelatedByIdUsuario(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario) {
                    $this->initUsuarioConfiguracionProgramasRelatedByIdUsuario();
                } else {
                    $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioConfiguracionProgramasRelatedByIdUsuario = new $collectionClassName;
                    $collUsuarioConfiguracionProgramasRelatedByIdUsuario->setModel('\UsuarioConfiguracionPrograma');

                    return $collUsuarioConfiguracionProgramasRelatedByIdUsuario;
                }
            } else {
                $collUsuarioConfiguracionProgramasRelatedByIdUsuario = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial && count($collUsuarioConfiguracionProgramasRelatedByIdUsuario)) {
                        $this->initUsuarioConfiguracionProgramasRelatedByIdUsuario(false);

                        foreach ($collUsuarioConfiguracionProgramasRelatedByIdUsuario as $obj) {
                            if (false == $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->contains($obj)) {
                                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->append($obj);
                            }
                        }

                        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial = true;
                    }

                    return $collUsuarioConfiguracionProgramasRelatedByIdUsuario;
                }

                if ($partial && $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario) {
                    foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioConfiguracionProgramasRelatedByIdUsuario[] = $obj;
                        }
                    }
                }

                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = $collUsuarioConfiguracionProgramasRelatedByIdUsuario;
                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial = false;
            }
        }

        return $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario;
    }

    /**
     * Sets a collection of ChildUsuarioConfiguracionPrograma objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioConfiguracionProgramasRelatedByIdUsuario A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuarioConfiguracionProgramasRelatedByIdUsuario(Collection $usuarioConfiguracionProgramasRelatedByIdUsuario, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioConfiguracionPrograma[] $usuarioConfiguracionProgramasRelatedByIdUsuarioToDelete */
        $usuarioConfiguracionProgramasRelatedByIdUsuarioToDelete = $this->getUsuarioConfiguracionProgramasRelatedByIdUsuario(new Criteria(), $con)->diff($usuarioConfiguracionProgramasRelatedByIdUsuario);


        $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion = $usuarioConfiguracionProgramasRelatedByIdUsuarioToDelete;

        foreach ($usuarioConfiguracionProgramasRelatedByIdUsuarioToDelete as $usuarioConfiguracionProgramaRelatedByIdUsuarioRemoved) {
            $usuarioConfiguracionProgramaRelatedByIdUsuarioRemoved->setUsuarioRelatedByIdUsuario(null);
        }

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = null;
        foreach ($usuarioConfiguracionProgramasRelatedByIdUsuario as $usuarioConfiguracionProgramaRelatedByIdUsuario) {
            $this->addUsuarioConfiguracionProgramaRelatedByIdUsuario($usuarioConfiguracionProgramaRelatedByIdUsuario);
        }

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = $usuarioConfiguracionProgramasRelatedByIdUsuario;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioConfiguracionPrograma objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioConfiguracionPrograma objects.
     * @throws PropelException
     */
    public function countUsuarioConfiguracionProgramasRelatedByIdUsuario(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioConfiguracionProgramasRelatedByIdUsuario());
            }

            $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuario($this)
                ->count($con);
        }

        return count($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario);
    }

    /**
     * Method called to associate a ChildUsuarioConfiguracionPrograma object to this object
     * through the ChildUsuarioConfiguracionPrograma foreign key attribute.
     *
     * @param  ChildUsuarioConfiguracionPrograma $l ChildUsuarioConfiguracionPrograma
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioConfiguracionProgramaRelatedByIdUsuario(ChildUsuarioConfiguracionPrograma $l)
    {
        if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario === null) {
            $this->initUsuarioConfiguracionProgramasRelatedByIdUsuario();
            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioPartial = true;
        }

        if (!$this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->contains($l)) {
            $this->doAddUsuarioConfiguracionProgramaRelatedByIdUsuario($l);

            if ($this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion and $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->contains($l)) {
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->remove($this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuario The ChildUsuarioConfiguracionPrograma object to add.
     */
    protected function doAddUsuarioConfiguracionProgramaRelatedByIdUsuario(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuario)
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario[]= $usuarioConfiguracionProgramaRelatedByIdUsuario;
        $usuarioConfiguracionProgramaRelatedByIdUsuario->setUsuarioRelatedByIdUsuario($this);
    }

    /**
     * @param  ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuario The ChildUsuarioConfiguracionPrograma object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioConfiguracionProgramaRelatedByIdUsuario(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuario)
    {
        if ($this->getUsuarioConfiguracionProgramasRelatedByIdUsuario()->contains($usuarioConfiguracionProgramaRelatedByIdUsuario)) {
            $pos = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->search($usuarioConfiguracionProgramaRelatedByIdUsuario);
            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->remove($pos);
            if (null === $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion) {
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion = clone $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario;
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion->clear();
            }
            $this->usuarioConfiguracionProgramasRelatedByIdUsuarioScheduledForDeletion[]= clone $usuarioConfiguracionProgramaRelatedByIdUsuario;
            $usuarioConfiguracionProgramaRelatedByIdUsuario->setUsuarioRelatedByIdUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioConfiguracionProgramasRelatedByIdUsuario from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     */
    public function getUsuarioConfiguracionProgramasRelatedByIdUsuarioJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getUsuarioConfiguracionProgramasRelatedByIdUsuario($query, $con);
    }

    /**
     * Clears out the collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion()
     */
    public function clearUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion()
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion collection loaded partially.
     */
    public function resetPartialUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion($v = true)
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial = $v;
    }

    /**
     * Initializes the collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion collection.
     *
     * By default this just sets the collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion collection to an empty array (like clearcollUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = new $collectionClassName;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->setModel('\UsuarioConfiguracionPrograma');
    }

    /**
     * Gets an array of ChildUsuarioConfiguracionPrograma objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     * @throws PropelException
     */
    public function getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion) {
                    $this->initUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion();
                } else {
                    $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = new $collectionClassName;
                    $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->setModel('\UsuarioConfiguracionPrograma');

                    return $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
                }
            } else {
                $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial && count($collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion)) {
                        $this->initUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion(false);

                        foreach ($collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion as $obj) {
                            if (false == $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->contains($obj)) {
                                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->append($obj);
                            }
                        }

                        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial = true;
                    }

                    return $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
                }

                if ($partial && $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion) {
                    foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion[] = $obj;
                        }
                    }
                }

                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = $collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
                $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial = false;
            }
        }

        return $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
    }

    /**
     * Sets a collection of ChildUsuarioConfiguracionPrograma objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacion A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion(Collection $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacion, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioConfiguracionPrograma[] $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionToDelete */
        $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionToDelete = $this->getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion(new Criteria(), $con)->diff($usuarioConfiguracionProgramasRelatedByIdUsuarioModificacion);


        $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion = $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionToDelete;

        foreach ($usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionToDelete as $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacionRemoved) {
            $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacionRemoved->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = null;
        foreach ($usuarioConfiguracionProgramasRelatedByIdUsuarioModificacion as $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion) {
            $this->addUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion);
        }

        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = $usuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioConfiguracionPrograma objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioConfiguracionPrograma objects.
     * @throws PropelException
     */
    public function countUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion());
            }

            $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                ->count($con);
        }

        return count($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion);
    }

    /**
     * Method called to associate a ChildUsuarioConfiguracionPrograma object to this object
     * through the ChildUsuarioConfiguracionPrograma foreign key attribute.
     *
     * @param  ChildUsuarioConfiguracionPrograma $l ChildUsuarioConfiguracionPrograma
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion(ChildUsuarioConfiguracionPrograma $l)
    {
        if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion === null) {
            $this->initUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion();
            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionPartial = true;
        }

        if (!$this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->contains($l)) {
            $this->doAddUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($l);

            if ($this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion and $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->contains($l)) {
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->remove($this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion The ChildUsuarioConfiguracionPrograma object to add.
     */
    protected function doAddUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion)
    {
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion[]= $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion;
        $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion($this);
    }

    /**
     * @param  ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion The ChildUsuarioConfiguracionPrograma object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion)
    {
        if ($this->getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion()->contains($usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion)) {
            $pos = $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->search($usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion);
            $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion->remove($pos);
            if (null === $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion) {
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion = clone $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion;
                $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion->clear();
            }
            $this->usuarioConfiguracionProgramasRelatedByIdUsuarioModificacionScheduledForDeletion[]= clone $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion;
            $usuarioConfiguracionProgramaRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     */
    public function getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacionJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion($query, $con);
    }

    /**
     * Clears out the collUsuarioPermisosRelatedByIdUsuario collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioPermisosRelatedByIdUsuario()
     */
    public function clearUsuarioPermisosRelatedByIdUsuario()
    {
        $this->collUsuarioPermisosRelatedByIdUsuario = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioPermisosRelatedByIdUsuario collection loaded partially.
     */
    public function resetPartialUsuarioPermisosRelatedByIdUsuario($v = true)
    {
        $this->collUsuarioPermisosRelatedByIdUsuarioPartial = $v;
    }

    /**
     * Initializes the collUsuarioPermisosRelatedByIdUsuario collection.
     *
     * By default this just sets the collUsuarioPermisosRelatedByIdUsuario collection to an empty array (like clearcollUsuarioPermisosRelatedByIdUsuario());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioPermisosRelatedByIdUsuario($overrideExisting = true)
    {
        if (null !== $this->collUsuarioPermisosRelatedByIdUsuario && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioPermisosRelatedByIdUsuario = new $collectionClassName;
        $this->collUsuarioPermisosRelatedByIdUsuario->setModel('\UsuarioPermiso');
    }

    /**
     * Gets an array of ChildUsuarioPermiso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     * @throws PropelException
     */
    public function getUsuarioPermisosRelatedByIdUsuario(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisosRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioPermisosRelatedByIdUsuario) {
                    $this->initUsuarioPermisosRelatedByIdUsuario();
                } else {
                    $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioPermisosRelatedByIdUsuario = new $collectionClassName;
                    $collUsuarioPermisosRelatedByIdUsuario->setModel('\UsuarioPermiso');

                    return $collUsuarioPermisosRelatedByIdUsuario;
                }
            } else {
                $collUsuarioPermisosRelatedByIdUsuario = ChildUsuarioPermisoQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuario($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioPermisosRelatedByIdUsuarioPartial && count($collUsuarioPermisosRelatedByIdUsuario)) {
                        $this->initUsuarioPermisosRelatedByIdUsuario(false);

                        foreach ($collUsuarioPermisosRelatedByIdUsuario as $obj) {
                            if (false == $this->collUsuarioPermisosRelatedByIdUsuario->contains($obj)) {
                                $this->collUsuarioPermisosRelatedByIdUsuario->append($obj);
                            }
                        }

                        $this->collUsuarioPermisosRelatedByIdUsuarioPartial = true;
                    }

                    return $collUsuarioPermisosRelatedByIdUsuario;
                }

                if ($partial && $this->collUsuarioPermisosRelatedByIdUsuario) {
                    foreach ($this->collUsuarioPermisosRelatedByIdUsuario as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioPermisosRelatedByIdUsuario[] = $obj;
                        }
                    }
                }

                $this->collUsuarioPermisosRelatedByIdUsuario = $collUsuarioPermisosRelatedByIdUsuario;
                $this->collUsuarioPermisosRelatedByIdUsuarioPartial = false;
            }
        }

        return $this->collUsuarioPermisosRelatedByIdUsuario;
    }

    /**
     * Sets a collection of ChildUsuarioPermiso objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioPermisosRelatedByIdUsuario A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuarioPermisosRelatedByIdUsuario(Collection $usuarioPermisosRelatedByIdUsuario, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioPermiso[] $usuarioPermisosRelatedByIdUsuarioToDelete */
        $usuarioPermisosRelatedByIdUsuarioToDelete = $this->getUsuarioPermisosRelatedByIdUsuario(new Criteria(), $con)->diff($usuarioPermisosRelatedByIdUsuario);


        $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion = $usuarioPermisosRelatedByIdUsuarioToDelete;

        foreach ($usuarioPermisosRelatedByIdUsuarioToDelete as $usuarioPermisoRelatedByIdUsuarioRemoved) {
            $usuarioPermisoRelatedByIdUsuarioRemoved->setUsuarioRelatedByIdUsuario(null);
        }

        $this->collUsuarioPermisosRelatedByIdUsuario = null;
        foreach ($usuarioPermisosRelatedByIdUsuario as $usuarioPermisoRelatedByIdUsuario) {
            $this->addUsuarioPermisoRelatedByIdUsuario($usuarioPermisoRelatedByIdUsuario);
        }

        $this->collUsuarioPermisosRelatedByIdUsuario = $usuarioPermisosRelatedByIdUsuario;
        $this->collUsuarioPermisosRelatedByIdUsuarioPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioPermiso objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioPermiso objects.
     * @throws PropelException
     */
    public function countUsuarioPermisosRelatedByIdUsuario(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosRelatedByIdUsuarioPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisosRelatedByIdUsuario || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioPermisosRelatedByIdUsuario) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioPermisosRelatedByIdUsuario());
            }

            $query = ChildUsuarioPermisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuario($this)
                ->count($con);
        }

        return count($this->collUsuarioPermisosRelatedByIdUsuario);
    }

    /**
     * Method called to associate a ChildUsuarioPermiso object to this object
     * through the ChildUsuarioPermiso foreign key attribute.
     *
     * @param  ChildUsuarioPermiso $l ChildUsuarioPermiso
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioPermisoRelatedByIdUsuario(ChildUsuarioPermiso $l)
    {
        if ($this->collUsuarioPermisosRelatedByIdUsuario === null) {
            $this->initUsuarioPermisosRelatedByIdUsuario();
            $this->collUsuarioPermisosRelatedByIdUsuarioPartial = true;
        }

        if (!$this->collUsuarioPermisosRelatedByIdUsuario->contains($l)) {
            $this->doAddUsuarioPermisoRelatedByIdUsuario($l);

            if ($this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion and $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->contains($l)) {
                $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->remove($this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuario The ChildUsuarioPermiso object to add.
     */
    protected function doAddUsuarioPermisoRelatedByIdUsuario(ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuario)
    {
        $this->collUsuarioPermisosRelatedByIdUsuario[]= $usuarioPermisoRelatedByIdUsuario;
        $usuarioPermisoRelatedByIdUsuario->setUsuarioRelatedByIdUsuario($this);
    }

    /**
     * @param  ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuario The ChildUsuarioPermiso object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioPermisoRelatedByIdUsuario(ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuario)
    {
        if ($this->getUsuarioPermisosRelatedByIdUsuario()->contains($usuarioPermisoRelatedByIdUsuario)) {
            $pos = $this->collUsuarioPermisosRelatedByIdUsuario->search($usuarioPermisoRelatedByIdUsuario);
            $this->collUsuarioPermisosRelatedByIdUsuario->remove($pos);
            if (null === $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion) {
                $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion = clone $this->collUsuarioPermisosRelatedByIdUsuario;
                $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion->clear();
            }
            $this->usuarioPermisosRelatedByIdUsuarioScheduledForDeletion[]= clone $usuarioPermisoRelatedByIdUsuario;
            $usuarioPermisoRelatedByIdUsuario->setUsuarioRelatedByIdUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioPermisosRelatedByIdUsuario from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     */
    public function getUsuarioPermisosRelatedByIdUsuarioJoinPermisos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioPermisoQuery::create(null, $criteria);
        $query->joinWith('Permisos', $joinBehavior);

        return $this->getUsuarioPermisosRelatedByIdUsuario($query, $con);
    }

    /**
     * Clears out the collUsuarioPermisosRelatedByIdUsuarioModificacion collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioPermisosRelatedByIdUsuarioModificacion()
     */
    public function clearUsuarioPermisosRelatedByIdUsuarioModificacion()
    {
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioPermisosRelatedByIdUsuarioModificacion collection loaded partially.
     */
    public function resetPartialUsuarioPermisosRelatedByIdUsuarioModificacion($v = true)
    {
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial = $v;
    }

    /**
     * Initializes the collUsuarioPermisosRelatedByIdUsuarioModificacion collection.
     *
     * By default this just sets the collUsuarioPermisosRelatedByIdUsuarioModificacion collection to an empty array (like clearcollUsuarioPermisosRelatedByIdUsuarioModificacion());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioPermisosRelatedByIdUsuarioModificacion($overrideExisting = true)
    {
        if (null !== $this->collUsuarioPermisosRelatedByIdUsuarioModificacion && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = new $collectionClassName;
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->setModel('\UsuarioPermiso');
    }

    /**
     * Gets an array of ChildUsuarioPermiso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     * @throws PropelException
     */
    public function getUsuarioPermisosRelatedByIdUsuarioModificacion(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisosRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioPermisosRelatedByIdUsuarioModificacion) {
                    $this->initUsuarioPermisosRelatedByIdUsuarioModificacion();
                } else {
                    $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioPermisosRelatedByIdUsuarioModificacion = new $collectionClassName;
                    $collUsuarioPermisosRelatedByIdUsuarioModificacion->setModel('\UsuarioPermiso');

                    return $collUsuarioPermisosRelatedByIdUsuarioModificacion;
                }
            } else {
                $collUsuarioPermisosRelatedByIdUsuarioModificacion = ChildUsuarioPermisoQuery::create(null, $criteria)
                    ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial && count($collUsuarioPermisosRelatedByIdUsuarioModificacion)) {
                        $this->initUsuarioPermisosRelatedByIdUsuarioModificacion(false);

                        foreach ($collUsuarioPermisosRelatedByIdUsuarioModificacion as $obj) {
                            if (false == $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->contains($obj)) {
                                $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->append($obj);
                            }
                        }

                        $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial = true;
                    }

                    return $collUsuarioPermisosRelatedByIdUsuarioModificacion;
                }

                if ($partial && $this->collUsuarioPermisosRelatedByIdUsuarioModificacion) {
                    foreach ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioPermisosRelatedByIdUsuarioModificacion[] = $obj;
                        }
                    }
                }

                $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = $collUsuarioPermisosRelatedByIdUsuarioModificacion;
                $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial = false;
            }
        }

        return $this->collUsuarioPermisosRelatedByIdUsuarioModificacion;
    }

    /**
     * Sets a collection of ChildUsuarioPermiso objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioPermisosRelatedByIdUsuarioModificacion A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function setUsuarioPermisosRelatedByIdUsuarioModificacion(Collection $usuarioPermisosRelatedByIdUsuarioModificacion, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioPermiso[] $usuarioPermisosRelatedByIdUsuarioModificacionToDelete */
        $usuarioPermisosRelatedByIdUsuarioModificacionToDelete = $this->getUsuarioPermisosRelatedByIdUsuarioModificacion(new Criteria(), $con)->diff($usuarioPermisosRelatedByIdUsuarioModificacion);


        $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion = $usuarioPermisosRelatedByIdUsuarioModificacionToDelete;

        foreach ($usuarioPermisosRelatedByIdUsuarioModificacionToDelete as $usuarioPermisoRelatedByIdUsuarioModificacionRemoved) {
            $usuarioPermisoRelatedByIdUsuarioModificacionRemoved->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = null;
        foreach ($usuarioPermisosRelatedByIdUsuarioModificacion as $usuarioPermisoRelatedByIdUsuarioModificacion) {
            $this->addUsuarioPermisoRelatedByIdUsuarioModificacion($usuarioPermisoRelatedByIdUsuarioModificacion);
        }

        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = $usuarioPermisosRelatedByIdUsuarioModificacion;
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioPermiso objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioPermiso objects.
     * @throws PropelException
     */
    public function countUsuarioPermisosRelatedByIdUsuarioModificacion(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisosRelatedByIdUsuarioModificacion || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioPermisosRelatedByIdUsuarioModificacion) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioPermisosRelatedByIdUsuarioModificacion());
            }

            $query = ChildUsuarioPermisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuarioRelatedByIdUsuarioModificacion($this)
                ->count($con);
        }

        return count($this->collUsuarioPermisosRelatedByIdUsuarioModificacion);
    }

    /**
     * Method called to associate a ChildUsuarioPermiso object to this object
     * through the ChildUsuarioPermiso foreign key attribute.
     *
     * @param  ChildUsuarioPermiso $l ChildUsuarioPermiso
     * @return $this|\Usuario The current object (for fluent API support)
     */
    public function addUsuarioPermisoRelatedByIdUsuarioModificacion(ChildUsuarioPermiso $l)
    {
        if ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion === null) {
            $this->initUsuarioPermisosRelatedByIdUsuarioModificacion();
            $this->collUsuarioPermisosRelatedByIdUsuarioModificacionPartial = true;
        }

        if (!$this->collUsuarioPermisosRelatedByIdUsuarioModificacion->contains($l)) {
            $this->doAddUsuarioPermisoRelatedByIdUsuarioModificacion($l);

            if ($this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion and $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->contains($l)) {
                $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->remove($this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuarioModificacion The ChildUsuarioPermiso object to add.
     */
    protected function doAddUsuarioPermisoRelatedByIdUsuarioModificacion(ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuarioModificacion)
    {
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion[]= $usuarioPermisoRelatedByIdUsuarioModificacion;
        $usuarioPermisoRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion($this);
    }

    /**
     * @param  ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuarioModificacion The ChildUsuarioPermiso object to remove.
     * @return $this|ChildUsuario The current object (for fluent API support)
     */
    public function removeUsuarioPermisoRelatedByIdUsuarioModificacion(ChildUsuarioPermiso $usuarioPermisoRelatedByIdUsuarioModificacion)
    {
        if ($this->getUsuarioPermisosRelatedByIdUsuarioModificacion()->contains($usuarioPermisoRelatedByIdUsuarioModificacion)) {
            $pos = $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->search($usuarioPermisoRelatedByIdUsuarioModificacion);
            $this->collUsuarioPermisosRelatedByIdUsuarioModificacion->remove($pos);
            if (null === $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion) {
                $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion = clone $this->collUsuarioPermisosRelatedByIdUsuarioModificacion;
                $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion->clear();
            }
            $this->usuarioPermisosRelatedByIdUsuarioModificacionScheduledForDeletion[]= clone $usuarioPermisoRelatedByIdUsuarioModificacion;
            $usuarioPermisoRelatedByIdUsuarioModificacion->setUsuarioRelatedByIdUsuarioModificacion(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related UsuarioPermisosRelatedByIdUsuarioModificacion from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     */
    public function getUsuarioPermisosRelatedByIdUsuarioModificacionJoinPermisos(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioPermisoQuery::create(null, $criteria);
        $query->joinWith('Permisos', $joinBehavior);

        return $this->getUsuarioPermisosRelatedByIdUsuarioModificacion($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUsuarioRelatedByIdUsuarioModificacion) {
            $this->aUsuarioRelatedByIdUsuarioModificacion->removeUsuarioRelatedByClave($this);
        }
        if (null !== $this->aEntidadOperativa) {
            $this->aEntidadOperativa->removeUsuario($this);
        }
        $this->clave = null;
        $this->usuario = null;
        $this->password = null;
        $this->vigencia = null;
        $this->dias_vigencia = null;
        $this->correo = null;
        $this->activo = null;
        $this->nombre = null;
        $this->apaterno = null;
        $this->amaterno = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->ultimo_acceso = null;
        $this->password_antiguo = null;
        $this->id_usuario_modificacion = null;
        $this->id_entidad_operativa = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBeneficios) {
                foreach ($this->collBeneficios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEtapas) {
                foreach ($this->collEtapas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFlujoEtapas) {
                foreach ($this->collFlujoEtapas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collLogins) {
                foreach ($this->collLogins as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProgramas) {
                foreach ($this->collProgramas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProgramaRequisitos) {
                foreach ($this->collProgramaRequisitos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRestablecerPasswords) {
                foreach ($this->collRestablecerPasswords as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRols) {
                foreach ($this->collRols as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRolPermisos) {
                foreach ($this->collRolPermisos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRolUsuariosRelatedByIdUsuario) {
                foreach ($this->collRolUsuariosRelatedByIdUsuario as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRolUsuariosRelatedByIdUsuarioModificacion) {
                foreach ($this->collRolUsuariosRelatedByIdUsuarioModificacion as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuariosRelatedByClave) {
                foreach ($this->collUsuariosRelatedByClave as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioConfiguracionEtapas) {
                foreach ($this->collUsuarioConfiguracionEtapas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario) {
                foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuario as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion) {
                foreach ($this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioPermisosRelatedByIdUsuario) {
                foreach ($this->collUsuarioPermisosRelatedByIdUsuario as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion) {
                foreach ($this->collUsuarioPermisosRelatedByIdUsuarioModificacion as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBeneficios = null;
        $this->collEtapas = null;
        $this->collFlujoEtapas = null;
        $this->collLogins = null;
        $this->collProgramas = null;
        $this->collProgramaRequisitos = null;
        $this->collRestablecerPasswords = null;
        $this->collRols = null;
        $this->collRolPermisos = null;
        $this->collRolUsuariosRelatedByIdUsuario = null;
        $this->collRolUsuariosRelatedByIdUsuarioModificacion = null;
        $this->collUsuariosRelatedByClave = null;
        $this->collUsuarioConfiguracionEtapas = null;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = null;
        $this->collUsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion = null;
        $this->collUsuarioPermisosRelatedByIdUsuario = null;
        $this->collUsuarioPermisosRelatedByIdUsuarioModificacion = null;
        $this->aUsuarioRelatedByIdUsuarioModificacion = null;
        $this->aEntidadOperativa = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsuarioTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
