<?php

namespace Base;

use \UsuarioConfiguracionPrograma as ChildUsuarioConfiguracionPrograma;
use \UsuarioConfiguracionProgramaQuery as ChildUsuarioConfiguracionProgramaQuery;
use \Exception;
use \PDO;
use Map\UsuarioConfiguracionProgramaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'usuario_configuracion_programa' table.
 *
 *
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByIdPrograma($order = Criteria::ASC) Order by the id_programa column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByIdUsuario($order = Criteria::ASC) Order by the id_usuario column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByPPermisos($order = Criteria::ASC) Order by the p_permisos column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByPConsulta($order = Criteria::ASC) Order by the p_consulta column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByPEdicion($order = Criteria::ASC) Order by the p_edicion column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByPEstatus($order = Criteria::ASC) Order by the p_estatus column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByPDesactivar($order = Criteria::ASC) Order by the p_desactivar column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildUsuarioConfiguracionProgramaQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByClave() Group by the clave column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByIdPrograma() Group by the id_programa column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByIdUsuario() Group by the id_usuario column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByActivo() Group by the activo column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByPPermisos() Group by the p_permisos column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByPConsulta() Group by the p_consulta column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByPEdicion() Group by the p_edicion column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByPEstatus() Group by the p_estatus column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByPDesactivar() Group by the p_desactivar column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildUsuarioConfiguracionProgramaQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinUsuarioRelatedByIdUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinUsuarioRelatedByIdUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinUsuarioRelatedByIdUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery joinWithUsuarioRelatedByIdUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinWithUsuarioRelatedByIdUsuario() Adds a LEFT JOIN clause and with to the query using the UsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinWithUsuarioRelatedByIdUsuario() Adds a RIGHT JOIN clause and with to the query using the UsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinWithUsuarioRelatedByIdUsuario() Adds a INNER JOIN clause and with to the query using the UsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery joinWithUsuarioRelatedByIdUsuarioModificacion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a LEFT JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a RIGHT JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a INNER JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery joinWithUsuarioConfiguracionEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildUsuarioConfiguracionProgramaQuery leftJoinWithUsuarioConfiguracionEtapa() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery rightJoinWithUsuarioConfiguracionEtapa() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioConfiguracionProgramaQuery innerJoinWithUsuarioConfiguracionEtapa() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     \ProgramaQuery|\UsuarioQuery|\UsuarioConfiguracionEtapaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsuarioConfiguracionPrograma|null findOne(ConnectionInterface $con = null) Return the first ChildUsuarioConfiguracionPrograma matching the query
 * @method     ChildUsuarioConfiguracionPrograma findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsuarioConfiguracionPrograma matching the query, or a new ChildUsuarioConfiguracionPrograma object populated from the query conditions when no match is found
 *
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByClave(int $clave) Return the first ChildUsuarioConfiguracionPrograma filtered by the clave column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByIdPrograma(int $id_programa) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_programa column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByIdUsuario(int $id_usuario) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_usuario column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByActivo(int $activo) Return the first ChildUsuarioConfiguracionPrograma filtered by the activo column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByPPermisos(int $p_permisos) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_permisos column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByPConsulta(int $p_consulta) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_consulta column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByPEdicion(int $p_edicion) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_edicion column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByPEstatus(int $p_estatus) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_estatus column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByPDesactivar(int $p_desactivar) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_desactivar column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the fecha_creacion column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the fecha_modificacion column
 * @method     ChildUsuarioConfiguracionPrograma|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_usuario_modificacion column *

 * @method     ChildUsuarioConfiguracionPrograma requirePk($key, ConnectionInterface $con = null) Return the ChildUsuarioConfiguracionPrograma by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOne(ConnectionInterface $con = null) Return the first ChildUsuarioConfiguracionPrograma matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuarioConfiguracionPrograma requireOneByClave(int $clave) Return the first ChildUsuarioConfiguracionPrograma filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByIdPrograma(int $id_programa) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_programa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByIdUsuario(int $id_usuario) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_usuario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByActivo(int $activo) Return the first ChildUsuarioConfiguracionPrograma filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByPPermisos(int $p_permisos) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_permisos column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByPConsulta(int $p_consulta) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_consulta column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByPEdicion(int $p_edicion) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_edicion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByPEstatus(int $p_estatus) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_estatus column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByPDesactivar(int $p_desactivar) Return the first ChildUsuarioConfiguracionPrograma filtered by the p_desactivar column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuarioConfiguracionPrograma requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildUsuarioConfiguracionPrograma filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsuarioConfiguracionPrograma objects based on current ModelCriteria
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByClave(int $clave) Return ChildUsuarioConfiguracionPrograma objects filtered by the clave column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByIdPrograma(int $id_programa) Return ChildUsuarioConfiguracionPrograma objects filtered by the id_programa column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByIdUsuario(int $id_usuario) Return ChildUsuarioConfiguracionPrograma objects filtered by the id_usuario column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByActivo(int $activo) Return ChildUsuarioConfiguracionPrograma objects filtered by the activo column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByPPermisos(int $p_permisos) Return ChildUsuarioConfiguracionPrograma objects filtered by the p_permisos column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByPConsulta(int $p_consulta) Return ChildUsuarioConfiguracionPrograma objects filtered by the p_consulta column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByPEdicion(int $p_edicion) Return ChildUsuarioConfiguracionPrograma objects filtered by the p_edicion column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByPEstatus(int $p_estatus) Return ChildUsuarioConfiguracionPrograma objects filtered by the p_estatus column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByPDesactivar(int $p_desactivar) Return ChildUsuarioConfiguracionPrograma objects filtered by the p_desactivar column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildUsuarioConfiguracionPrograma objects filtered by the fecha_creacion column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildUsuarioConfiguracionPrograma objects filtered by the fecha_modificacion column
 * @method     ChildUsuarioConfiguracionPrograma[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildUsuarioConfiguracionPrograma objects filtered by the id_usuario_modificacion column
 * @method     ChildUsuarioConfiguracionPrograma[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsuarioConfiguracionProgramaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UsuarioConfiguracionProgramaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\UsuarioConfiguracionPrograma', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsuarioConfiguracionProgramaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsuarioConfiguracionProgramaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsuarioConfiguracionProgramaQuery) {
            return $criteria;
        }
        $query = new ChildUsuarioConfiguracionProgramaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsuarioConfiguracionPrograma|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsuarioConfiguracionProgramaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioConfiguracionPrograma A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, id_programa, id_usuario, activo, p_permisos, p_consulta, p_edicion, p_estatus, p_desactivar, fecha_creacion, fecha_modificacion, id_usuario_modificacion FROM usuario_configuracion_programa WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsuarioConfiguracionPrograma $obj */
            $obj = new ChildUsuarioConfiguracionPrograma();
            $obj->hydrate($row);
            UsuarioConfiguracionProgramaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsuarioConfiguracionPrograma|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the id_programa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrograma(1234); // WHERE id_programa = 1234
     * $query->filterByIdPrograma(array(12, 34)); // WHERE id_programa IN (12, 34)
     * $query->filterByIdPrograma(array('min' => 12)); // WHERE id_programa > 12
     * </code>
     *
     * @see       filterByPrograma()
     *
     * @param     mixed $idPrograma The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByIdPrograma($idPrograma = null, $comparison = null)
    {
        if (is_array($idPrograma)) {
            $useMinMax = false;
            if (isset($idPrograma['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, $idPrograma['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrograma['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, $idPrograma['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, $idPrograma, $comparison);
    }

    /**
     * Filter the query on the id_usuario column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuario(1234); // WHERE id_usuario = 1234
     * $query->filterByIdUsuario(array(12, 34)); // WHERE id_usuario IN (12, 34)
     * $query->filterByIdUsuario(array('min' => 12)); // WHERE id_usuario > 12
     * </code>
     *
     * @see       filterByUsuarioRelatedByIdUsuario()
     *
     * @param     mixed $idUsuario The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByIdUsuario($idUsuario = null, $comparison = null)
    {
        if (is_array($idUsuario)) {
            $useMinMax = false;
            if (isset($idUsuario['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, $idUsuario['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuario['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, $idUsuario['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, $idUsuario, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the p_permisos column
     *
     * Example usage:
     * <code>
     * $query->filterByPPermisos(1234); // WHERE p_permisos = 1234
     * $query->filterByPPermisos(array(12, 34)); // WHERE p_permisos IN (12, 34)
     * $query->filterByPPermisos(array('min' => 12)); // WHERE p_permisos > 12
     * </code>
     *
     * @param     mixed $pPermisos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPPermisos($pPermisos = null, $comparison = null)
    {
        if (is_array($pPermisos)) {
            $useMinMax = false;
            if (isset($pPermisos['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS, $pPermisos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pPermisos['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS, $pPermisos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS, $pPermisos, $comparison);
    }

    /**
     * Filter the query on the p_consulta column
     *
     * Example usage:
     * <code>
     * $query->filterByPConsulta(1234); // WHERE p_consulta = 1234
     * $query->filterByPConsulta(array(12, 34)); // WHERE p_consulta IN (12, 34)
     * $query->filterByPConsulta(array('min' => 12)); // WHERE p_consulta > 12
     * </code>
     *
     * @param     mixed $pConsulta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPConsulta($pConsulta = null, $comparison = null)
    {
        if (is_array($pConsulta)) {
            $useMinMax = false;
            if (isset($pConsulta['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA, $pConsulta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pConsulta['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA, $pConsulta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA, $pConsulta, $comparison);
    }

    /**
     * Filter the query on the p_edicion column
     *
     * Example usage:
     * <code>
     * $query->filterByPEdicion(1234); // WHERE p_edicion = 1234
     * $query->filterByPEdicion(array(12, 34)); // WHERE p_edicion IN (12, 34)
     * $query->filterByPEdicion(array('min' => 12)); // WHERE p_edicion > 12
     * </code>
     *
     * @param     mixed $pEdicion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPEdicion($pEdicion = null, $comparison = null)
    {
        if (is_array($pEdicion)) {
            $useMinMax = false;
            if (isset($pEdicion['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_EDICION, $pEdicion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pEdicion['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_EDICION, $pEdicion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_EDICION, $pEdicion, $comparison);
    }

    /**
     * Filter the query on the p_estatus column
     *
     * Example usage:
     * <code>
     * $query->filterByPEstatus(1234); // WHERE p_estatus = 1234
     * $query->filterByPEstatus(array(12, 34)); // WHERE p_estatus IN (12, 34)
     * $query->filterByPEstatus(array('min' => 12)); // WHERE p_estatus > 12
     * </code>
     *
     * @param     mixed $pEstatus The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPEstatus($pEstatus = null, $comparison = null)
    {
        if (is_array($pEstatus)) {
            $useMinMax = false;
            if (isset($pEstatus['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS, $pEstatus['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pEstatus['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS, $pEstatus['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS, $pEstatus, $comparison);
    }

    /**
     * Filter the query on the p_desactivar column
     *
     * Example usage:
     * <code>
     * $query->filterByPDesactivar(1234); // WHERE p_desactivar = 1234
     * $query->filterByPDesactivar(array(12, 34)); // WHERE p_desactivar IN (12, 34)
     * $query->filterByPDesactivar(array('min' => 12)); // WHERE p_desactivar > 12
     * </code>
     *
     * @param     mixed $pDesactivar The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPDesactivar($pDesactivar = null, $comparison = null)
    {
        if (is_array($pDesactivar)) {
            $useMinMax = false;
            if (isset($pDesactivar['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR, $pDesactivar['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pDesactivar['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR, $pDesactivar['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR, $pDesactivar, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuarioRelatedByIdUsuarioModificacion()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, $programa->getClave(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, $programa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByUsuarioRelatedByIdUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuarioRelatedByIdUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioRelatedByIdUsuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function joinUsuarioRelatedByIdUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioRelatedByIdUsuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioRelatedByIdUsuario');
        }

        return $this;
    }

    /**
     * Use the UsuarioRelatedByIdUsuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioRelatedByIdUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioRelatedByIdUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioRelatedByIdUsuario', '\UsuarioQuery');
    }

    /**
     * Use the UsuarioRelatedByIdUsuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioRelatedByIdUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioRelatedByIdUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByUsuarioRelatedByIdUsuarioModificacion($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuarioRelatedByIdUsuarioModificacion() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function joinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioRelatedByIdUsuarioModificacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioRelatedByIdUsuarioModificacion');
        }

        return $this;
    }

    /**
     * Use the UsuarioRelatedByIdUsuarioModificacion relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioRelatedByIdUsuarioModificacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioRelatedByIdUsuarioModificacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioRelatedByIdUsuarioModificacion', '\UsuarioQuery');
    }

    /**
     * Use the UsuarioRelatedByIdUsuarioModificacion relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioRelatedByIdUsuarioModificacionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioRelatedByIdUsuarioModificacionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionEtapa object
     *
     * @param \UsuarioConfiguracionEtapa|ObjectCollection $usuarioConfiguracionEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionEtapa($usuarioConfiguracionEtapa, $comparison = null)
    {
        if ($usuarioConfiguracionEtapa instanceof \UsuarioConfiguracionEtapa) {
            return $this
                ->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $usuarioConfiguracionEtapa->getIdUsuarioConfiguracionPrograma(), $comparison);
        } elseif ($usuarioConfiguracionEtapa instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionEtapaQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionEtapa() only accepts arguments of type \UsuarioConfiguracionEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionEtapa');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionEtapaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionEtapa', '\UsuarioConfiguracionEtapaQuery');
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @param callable(\UsuarioConfiguracionEtapaQuery):\UsuarioConfiguracionEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsuarioConfiguracionPrograma $usuarioConfiguracionPrograma Object to remove from the list of results
     *
     * @return $this|ChildUsuarioConfiguracionProgramaQuery The current query, for fluid interface
     */
    public function prune($usuarioConfiguracionPrograma = null)
    {
        if ($usuarioConfiguracionPrograma) {
            $this->addUsingAlias(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, $usuarioConfiguracionPrograma->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the usuario_configuracion_programa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsuarioConfiguracionProgramaTableMap::clearInstancePool();
            UsuarioConfiguracionProgramaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsuarioConfiguracionProgramaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsuarioConfiguracionProgramaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsuarioConfiguracionProgramaQuery
