<?php

namespace Base;

use \Programa as ChildPrograma;
use \ProgramaQuery as ChildProgramaQuery;
use \Exception;
use \PDO;
use Map\ProgramaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'programa' table.
 *
 *
 *
 * @method     ChildProgramaQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildProgramaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildProgramaQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method     ChildProgramaQuery orderByFechaInicio($order = Criteria::ASC) Order by the fecha_inicio column
 * @method     ChildProgramaQuery orderByFechaFin($order = Criteria::ASC) Order by the fecha_fin column
 * @method     ChildProgramaQuery orderByIdEntidadOperativa($order = Criteria::ASC) Order by the id_entidad_operativa column
 * @method     ChildProgramaQuery orderByIdEstatusPrograma($order = Criteria::ASC) Order by the id_estatus_programa column
 * @method     ChildProgramaQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildProgramaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildProgramaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildProgramaQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 * @method     ChildProgramaQuery orderByIdTipoBebeficiario($order = Criteria::ASC) Order by the id_tipo_bebeficiario column
 * @method     ChildProgramaQuery orderByPersonaMoral($order = Criteria::ASC) Order by the persona_moral column
 * @method     ChildProgramaQuery orderByDigramaYucatan($order = Criteria::ASC) Order by the digrama_yucatan column
 *
 * @method     ChildProgramaQuery groupByClave() Group by the clave column
 * @method     ChildProgramaQuery groupByNombre() Group by the nombre column
 * @method     ChildProgramaQuery groupByDescripcion() Group by the descripcion column
 * @method     ChildProgramaQuery groupByFechaInicio() Group by the fecha_inicio column
 * @method     ChildProgramaQuery groupByFechaFin() Group by the fecha_fin column
 * @method     ChildProgramaQuery groupByIdEntidadOperativa() Group by the id_entidad_operativa column
 * @method     ChildProgramaQuery groupByIdEstatusPrograma() Group by the id_estatus_programa column
 * @method     ChildProgramaQuery groupByActivo() Group by the activo column
 * @method     ChildProgramaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildProgramaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildProgramaQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 * @method     ChildProgramaQuery groupByIdTipoBebeficiario() Group by the id_tipo_bebeficiario column
 * @method     ChildProgramaQuery groupByPersonaMoral() Group by the persona_moral column
 * @method     ChildProgramaQuery groupByDigramaYucatan() Group by the digrama_yucatan column
 *
 * @method     ChildProgramaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProgramaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProgramaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProgramaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProgramaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProgramaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProgramaQuery leftJoinEntidadOperativa($relationAlias = null) Adds a LEFT JOIN clause to the query using the EntidadOperativa relation
 * @method     ChildProgramaQuery rightJoinEntidadOperativa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EntidadOperativa relation
 * @method     ChildProgramaQuery innerJoinEntidadOperativa($relationAlias = null) Adds a INNER JOIN clause to the query using the EntidadOperativa relation
 *
 * @method     ChildProgramaQuery joinWithEntidadOperativa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EntidadOperativa relation
 *
 * @method     ChildProgramaQuery leftJoinWithEntidadOperativa() Adds a LEFT JOIN clause and with to the query using the EntidadOperativa relation
 * @method     ChildProgramaQuery rightJoinWithEntidadOperativa() Adds a RIGHT JOIN clause and with to the query using the EntidadOperativa relation
 * @method     ChildProgramaQuery innerJoinWithEntidadOperativa() Adds a INNER JOIN clause and with to the query using the EntidadOperativa relation
 *
 * @method     ChildProgramaQuery leftJoinEstatusPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the EstatusPrograma relation
 * @method     ChildProgramaQuery rightJoinEstatusPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EstatusPrograma relation
 * @method     ChildProgramaQuery innerJoinEstatusPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the EstatusPrograma relation
 *
 * @method     ChildProgramaQuery joinWithEstatusPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EstatusPrograma relation
 *
 * @method     ChildProgramaQuery leftJoinWithEstatusPrograma() Adds a LEFT JOIN clause and with to the query using the EstatusPrograma relation
 * @method     ChildProgramaQuery rightJoinWithEstatusPrograma() Adds a RIGHT JOIN clause and with to the query using the EstatusPrograma relation
 * @method     ChildProgramaQuery innerJoinWithEstatusPrograma() Adds a INNER JOIN clause and with to the query using the EstatusPrograma relation
 *
 * @method     ChildProgramaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildProgramaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildProgramaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildProgramaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildProgramaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildProgramaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildProgramaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildProgramaQuery leftJoinTipoBeneficiario($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoBeneficiario relation
 * @method     ChildProgramaQuery rightJoinTipoBeneficiario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoBeneficiario relation
 * @method     ChildProgramaQuery innerJoinTipoBeneficiario($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoBeneficiario relation
 *
 * @method     ChildProgramaQuery joinWithTipoBeneficiario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the TipoBeneficiario relation
 *
 * @method     ChildProgramaQuery leftJoinWithTipoBeneficiario() Adds a LEFT JOIN clause and with to the query using the TipoBeneficiario relation
 * @method     ChildProgramaQuery rightJoinWithTipoBeneficiario() Adds a RIGHT JOIN clause and with to the query using the TipoBeneficiario relation
 * @method     ChildProgramaQuery innerJoinWithTipoBeneficiario() Adds a INNER JOIN clause and with to the query using the TipoBeneficiario relation
 *
 * @method     ChildProgramaQuery leftJoinBeneficio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Beneficio relation
 * @method     ChildProgramaQuery rightJoinBeneficio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Beneficio relation
 * @method     ChildProgramaQuery innerJoinBeneficio($relationAlias = null) Adds a INNER JOIN clause to the query using the Beneficio relation
 *
 * @method     ChildProgramaQuery joinWithBeneficio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Beneficio relation
 *
 * @method     ChildProgramaQuery leftJoinWithBeneficio() Adds a LEFT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildProgramaQuery rightJoinWithBeneficio() Adds a RIGHT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildProgramaQuery innerJoinWithBeneficio() Adds a INNER JOIN clause and with to the query using the Beneficio relation
 *
 * @method     ChildProgramaQuery leftJoinEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Etapa relation
 * @method     ChildProgramaQuery rightJoinEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Etapa relation
 * @method     ChildProgramaQuery innerJoinEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the Etapa relation
 *
 * @method     ChildProgramaQuery joinWithEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Etapa relation
 *
 * @method     ChildProgramaQuery leftJoinWithEtapa() Adds a LEFT JOIN clause and with to the query using the Etapa relation
 * @method     ChildProgramaQuery rightJoinWithEtapa() Adds a RIGHT JOIN clause and with to the query using the Etapa relation
 * @method     ChildProgramaQuery innerJoinWithEtapa() Adds a INNER JOIN clause and with to the query using the Etapa relation
 *
 * @method     ChildProgramaQuery leftJoinProgramaRequisito($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildProgramaQuery rightJoinProgramaRequisito($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildProgramaQuery innerJoinProgramaRequisito($relationAlias = null) Adds a INNER JOIN clause to the query using the ProgramaRequisito relation
 *
 * @method     ChildProgramaQuery joinWithProgramaRequisito($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildProgramaQuery leftJoinWithProgramaRequisito() Adds a LEFT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildProgramaQuery rightJoinWithProgramaRequisito() Adds a RIGHT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildProgramaQuery innerJoinWithProgramaRequisito() Adds a INNER JOIN clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildProgramaQuery leftJoinUsuarioConfiguracionPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionPrograma relation
 * @method     ChildProgramaQuery rightJoinUsuarioConfiguracionPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionPrograma relation
 * @method     ChildProgramaQuery innerJoinUsuarioConfiguracionPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionPrograma relation
 *
 * @method     ChildProgramaQuery joinWithUsuarioConfiguracionPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionPrograma relation
 *
 * @method     ChildProgramaQuery leftJoinWithUsuarioConfiguracionPrograma() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionPrograma relation
 * @method     ChildProgramaQuery rightJoinWithUsuarioConfiguracionPrograma() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionPrograma relation
 * @method     ChildProgramaQuery innerJoinWithUsuarioConfiguracionPrograma() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionPrograma relation
 *
 * @method     \EntidadOperativaQuery|\EstatusProgramaQuery|\UsuarioQuery|\TipoBeneficiarioQuery|\BeneficioQuery|\EtapaQuery|\ProgramaRequisitoQuery|\UsuarioConfiguracionProgramaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPrograma|null findOne(ConnectionInterface $con = null) Return the first ChildPrograma matching the query
 * @method     ChildPrograma findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPrograma matching the query, or a new ChildPrograma object populated from the query conditions when no match is found
 *
 * @method     ChildPrograma|null findOneByClave(int $clave) Return the first ChildPrograma filtered by the clave column
 * @method     ChildPrograma|null findOneByNombre(string $nombre) Return the first ChildPrograma filtered by the nombre column
 * @method     ChildPrograma|null findOneByDescripcion(string $descripcion) Return the first ChildPrograma filtered by the descripcion column
 * @method     ChildPrograma|null findOneByFechaInicio(string $fecha_inicio) Return the first ChildPrograma filtered by the fecha_inicio column
 * @method     ChildPrograma|null findOneByFechaFin(string $fecha_fin) Return the first ChildPrograma filtered by the fecha_fin column
 * @method     ChildPrograma|null findOneByIdEntidadOperativa(int $id_entidad_operativa) Return the first ChildPrograma filtered by the id_entidad_operativa column
 * @method     ChildPrograma|null findOneByIdEstatusPrograma(int $id_estatus_programa) Return the first ChildPrograma filtered by the id_estatus_programa column
 * @method     ChildPrograma|null findOneByActivo(int $activo) Return the first ChildPrograma filtered by the activo column
 * @method     ChildPrograma|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildPrograma filtered by the fecha_creacion column
 * @method     ChildPrograma|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildPrograma filtered by the fecha_modificacion column
 * @method     ChildPrograma|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildPrograma filtered by the id_usuario_modificacion column
 * @method     ChildPrograma|null findOneByIdTipoBebeficiario(int $id_tipo_bebeficiario) Return the first ChildPrograma filtered by the id_tipo_bebeficiario column
 * @method     ChildPrograma|null findOneByPersonaMoral(int $persona_moral) Return the first ChildPrograma filtered by the persona_moral column
 * @method     ChildPrograma|null findOneByDigramaYucatan(int $digrama_yucatan) Return the first ChildPrograma filtered by the digrama_yucatan column *

 * @method     ChildPrograma requirePk($key, ConnectionInterface $con = null) Return the ChildPrograma by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOne(ConnectionInterface $con = null) Return the first ChildPrograma matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrograma requireOneByClave(int $clave) Return the first ChildPrograma filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByNombre(string $nombre) Return the first ChildPrograma filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByDescripcion(string $descripcion) Return the first ChildPrograma filtered by the descripcion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByFechaInicio(string $fecha_inicio) Return the first ChildPrograma filtered by the fecha_inicio column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByFechaFin(string $fecha_fin) Return the first ChildPrograma filtered by the fecha_fin column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByIdEntidadOperativa(int $id_entidad_operativa) Return the first ChildPrograma filtered by the id_entidad_operativa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByIdEstatusPrograma(int $id_estatus_programa) Return the first ChildPrograma filtered by the id_estatus_programa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByActivo(int $activo) Return the first ChildPrograma filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildPrograma filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildPrograma filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildPrograma filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByIdTipoBebeficiario(int $id_tipo_bebeficiario) Return the first ChildPrograma filtered by the id_tipo_bebeficiario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByPersonaMoral(int $persona_moral) Return the first ChildPrograma filtered by the persona_moral column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPrograma requireOneByDigramaYucatan(int $digrama_yucatan) Return the first ChildPrograma filtered by the digrama_yucatan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPrograma[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPrograma objects based on current ModelCriteria
 * @method     ChildPrograma[]|ObjectCollection findByClave(int $clave) Return ChildPrograma objects filtered by the clave column
 * @method     ChildPrograma[]|ObjectCollection findByNombre(string $nombre) Return ChildPrograma objects filtered by the nombre column
 * @method     ChildPrograma[]|ObjectCollection findByDescripcion(string $descripcion) Return ChildPrograma objects filtered by the descripcion column
 * @method     ChildPrograma[]|ObjectCollection findByFechaInicio(string $fecha_inicio) Return ChildPrograma objects filtered by the fecha_inicio column
 * @method     ChildPrograma[]|ObjectCollection findByFechaFin(string $fecha_fin) Return ChildPrograma objects filtered by the fecha_fin column
 * @method     ChildPrograma[]|ObjectCollection findByIdEntidadOperativa(int $id_entidad_operativa) Return ChildPrograma objects filtered by the id_entidad_operativa column
 * @method     ChildPrograma[]|ObjectCollection findByIdEstatusPrograma(int $id_estatus_programa) Return ChildPrograma objects filtered by the id_estatus_programa column
 * @method     ChildPrograma[]|ObjectCollection findByActivo(int $activo) Return ChildPrograma objects filtered by the activo column
 * @method     ChildPrograma[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildPrograma objects filtered by the fecha_creacion column
 * @method     ChildPrograma[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildPrograma objects filtered by the fecha_modificacion column
 * @method     ChildPrograma[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildPrograma objects filtered by the id_usuario_modificacion column
 * @method     ChildPrograma[]|ObjectCollection findByIdTipoBebeficiario(int $id_tipo_bebeficiario) Return ChildPrograma objects filtered by the id_tipo_bebeficiario column
 * @method     ChildPrograma[]|ObjectCollection findByPersonaMoral(int $persona_moral) Return ChildPrograma objects filtered by the persona_moral column
 * @method     ChildPrograma[]|ObjectCollection findByDigramaYucatan(int $digrama_yucatan) Return ChildPrograma objects filtered by the digrama_yucatan column
 * @method     ChildPrograma[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProgramaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\ProgramaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Programa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProgramaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProgramaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProgramaQuery) {
            return $criteria;
        }
        $query = new ChildProgramaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPrograma|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProgramaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProgramaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPrograma A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, nombre, descripcion, fecha_inicio, fecha_fin, id_entidad_operativa, id_estatus_programa, activo, fecha_creacion, fecha_modificacion, id_usuario_modificacion, id_tipo_bebeficiario, persona_moral, digrama_yucatan FROM programa WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPrograma $obj */
            $obj = new ChildPrograma();
            $obj->hydrate($row);
            ProgramaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPrograma|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%', Criteria::LIKE); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the fecha_inicio column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaInicio('2011-03-14'); // WHERE fecha_inicio = '2011-03-14'
     * $query->filterByFechaInicio('now'); // WHERE fecha_inicio = '2011-03-14'
     * $query->filterByFechaInicio(array('max' => 'yesterday')); // WHERE fecha_inicio > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaInicio The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaInicio($fechaInicio = null, $comparison = null)
    {
        if (is_array($fechaInicio)) {
            $useMinMax = false;
            if (isset($fechaInicio['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_INICIO, $fechaInicio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaInicio['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_INICIO, $fechaInicio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_FECHA_INICIO, $fechaInicio, $comparison);
    }

    /**
     * Filter the query on the fecha_fin column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaFin('2011-03-14'); // WHERE fecha_fin = '2011-03-14'
     * $query->filterByFechaFin('now'); // WHERE fecha_fin = '2011-03-14'
     * $query->filterByFechaFin(array('max' => 'yesterday')); // WHERE fecha_fin > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaFin The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaFin($fechaFin = null, $comparison = null)
    {
        if (is_array($fechaFin)) {
            $useMinMax = false;
            if (isset($fechaFin['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_FIN, $fechaFin['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaFin['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_FIN, $fechaFin['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_FECHA_FIN, $fechaFin, $comparison);
    }

    /**
     * Filter the query on the id_entidad_operativa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntidadOperativa(1234); // WHERE id_entidad_operativa = 1234
     * $query->filterByIdEntidadOperativa(array(12, 34)); // WHERE id_entidad_operativa IN (12, 34)
     * $query->filterByIdEntidadOperativa(array('min' => 12)); // WHERE id_entidad_operativa > 12
     * </code>
     *
     * @see       filterByEntidadOperativa()
     *
     * @param     mixed $idEntidadOperativa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByIdEntidadOperativa($idEntidadOperativa = null, $comparison = null)
    {
        if (is_array($idEntidadOperativa)) {
            $useMinMax = false;
            if (isset($idEntidadOperativa['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntidadOperativa['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa, $comparison);
    }

    /**
     * Filter the query on the id_estatus_programa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEstatusPrograma(1234); // WHERE id_estatus_programa = 1234
     * $query->filterByIdEstatusPrograma(array(12, 34)); // WHERE id_estatus_programa IN (12, 34)
     * $query->filterByIdEstatusPrograma(array('min' => 12)); // WHERE id_estatus_programa > 12
     * </code>
     *
     * @see       filterByEstatusPrograma()
     *
     * @param     mixed $idEstatusPrograma The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByIdEstatusPrograma($idEstatusPrograma = null, $comparison = null)
    {
        if (is_array($idEstatusPrograma)) {
            $useMinMax = false;
            if (isset($idEstatusPrograma['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $idEstatusPrograma['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEstatusPrograma['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $idEstatusPrograma['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $idEstatusPrograma, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query on the id_tipo_bebeficiario column
     *
     * Example usage:
     * <code>
     * $query->filterByIdTipoBebeficiario(1234); // WHERE id_tipo_bebeficiario = 1234
     * $query->filterByIdTipoBebeficiario(array(12, 34)); // WHERE id_tipo_bebeficiario IN (12, 34)
     * $query->filterByIdTipoBebeficiario(array('min' => 12)); // WHERE id_tipo_bebeficiario > 12
     * </code>
     *
     * @see       filterByTipoBeneficiario()
     *
     * @param     mixed $idTipoBebeficiario The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByIdTipoBebeficiario($idTipoBebeficiario = null, $comparison = null)
    {
        if (is_array($idTipoBebeficiario)) {
            $useMinMax = false;
            if (isset($idTipoBebeficiario['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $idTipoBebeficiario['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idTipoBebeficiario['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $idTipoBebeficiario['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $idTipoBebeficiario, $comparison);
    }

    /**
     * Filter the query on the persona_moral column
     *
     * Example usage:
     * <code>
     * $query->filterByPersonaMoral(1234); // WHERE persona_moral = 1234
     * $query->filterByPersonaMoral(array(12, 34)); // WHERE persona_moral IN (12, 34)
     * $query->filterByPersonaMoral(array('min' => 12)); // WHERE persona_moral > 12
     * </code>
     *
     * @param     mixed $personaMoral The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByPersonaMoral($personaMoral = null, $comparison = null)
    {
        if (is_array($personaMoral)) {
            $useMinMax = false;
            if (isset($personaMoral['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_PERSONA_MORAL, $personaMoral['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($personaMoral['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_PERSONA_MORAL, $personaMoral['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_PERSONA_MORAL, $personaMoral, $comparison);
    }

    /**
     * Filter the query on the digrama_yucatan column
     *
     * Example usage:
     * <code>
     * $query->filterByDigramaYucatan(1234); // WHERE digrama_yucatan = 1234
     * $query->filterByDigramaYucatan(array(12, 34)); // WHERE digrama_yucatan IN (12, 34)
     * $query->filterByDigramaYucatan(array('min' => 12)); // WHERE digrama_yucatan > 12
     * </code>
     *
     * @param     mixed $digramaYucatan The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByDigramaYucatan($digramaYucatan = null, $comparison = null)
    {
        if (is_array($digramaYucatan)) {
            $useMinMax = false;
            if (isset($digramaYucatan['min'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_DIGRAMA_YUCATAN, $digramaYucatan['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($digramaYucatan['max'])) {
                $this->addUsingAlias(ProgramaTableMap::COL_DIGRAMA_YUCATAN, $digramaYucatan['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProgramaTableMap::COL_DIGRAMA_YUCATAN, $digramaYucatan, $comparison);
    }

    /**
     * Filter the query by a related \EntidadOperativa object
     *
     * @param \EntidadOperativa|ObjectCollection $entidadOperativa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByEntidadOperativa($entidadOperativa, $comparison = null)
    {
        if ($entidadOperativa instanceof \EntidadOperativa) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $entidadOperativa->getClave(), $comparison);
        } elseif ($entidadOperativa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $entidadOperativa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEntidadOperativa() only accepts arguments of type \EntidadOperativa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EntidadOperativa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinEntidadOperativa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EntidadOperativa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EntidadOperativa');
        }

        return $this;
    }

    /**
     * Use the EntidadOperativa relation EntidadOperativa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntidadOperativaQuery A secondary query class using the current class as primary query
     */
    public function useEntidadOperativaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntidadOperativa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EntidadOperativa', '\EntidadOperativaQuery');
    }

    /**
     * Use the EntidadOperativa relation EntidadOperativa object
     *
     * @param callable(\EntidadOperativaQuery):\EntidadOperativaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEntidadOperativaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEntidadOperativaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \EstatusPrograma object
     *
     * @param \EstatusPrograma|ObjectCollection $estatusPrograma The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByEstatusPrograma($estatusPrograma, $comparison = null)
    {
        if ($estatusPrograma instanceof \EstatusPrograma) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $estatusPrograma->getClave(), $comparison);
        } elseif ($estatusPrograma instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $estatusPrograma->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEstatusPrograma() only accepts arguments of type \EstatusPrograma or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EstatusPrograma relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinEstatusPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EstatusPrograma');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EstatusPrograma');
        }

        return $this;
    }

    /**
     * Use the EstatusPrograma relation EstatusPrograma object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EstatusProgramaQuery A secondary query class using the current class as primary query
     */
    public function useEstatusProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEstatusPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EstatusPrograma', '\EstatusProgramaQuery');
    }

    /**
     * Use the EstatusPrograma relation EstatusPrograma object
     *
     * @param callable(\EstatusProgramaQuery):\EstatusProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEstatusProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEstatusProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \TipoBeneficiario object
     *
     * @param \TipoBeneficiario|ObjectCollection $tipoBeneficiario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByTipoBeneficiario($tipoBeneficiario, $comparison = null)
    {
        if ($tipoBeneficiario instanceof \TipoBeneficiario) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $tipoBeneficiario->getClave(), $comparison);
        } elseif ($tipoBeneficiario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $tipoBeneficiario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByTipoBeneficiario() only accepts arguments of type \TipoBeneficiario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoBeneficiario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinTipoBeneficiario($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoBeneficiario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoBeneficiario');
        }

        return $this;
    }

    /**
     * Use the TipoBeneficiario relation TipoBeneficiario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \TipoBeneficiarioQuery A secondary query class using the current class as primary query
     */
    public function useTipoBeneficiarioQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinTipoBeneficiario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoBeneficiario', '\TipoBeneficiarioQuery');
    }

    /**
     * Use the TipoBeneficiario relation TipoBeneficiario object
     *
     * @param callable(\TipoBeneficiarioQuery):\TipoBeneficiarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withTipoBeneficiarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useTipoBeneficiarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Beneficio object
     *
     * @param \Beneficio|ObjectCollection $beneficio the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByBeneficio($beneficio, $comparison = null)
    {
        if ($beneficio instanceof \Beneficio) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_CLAVE, $beneficio->getIdPrograma(), $comparison);
        } elseif ($beneficio instanceof ObjectCollection) {
            return $this
                ->useBeneficioQuery()
                ->filterByPrimaryKeys($beneficio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBeneficio() only accepts arguments of type \Beneficio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Beneficio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinBeneficio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Beneficio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Beneficio');
        }

        return $this;
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BeneficioQuery A secondary query class using the current class as primary query
     */
    public function useBeneficioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBeneficio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Beneficio', '\BeneficioQuery');
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @param callable(\BeneficioQuery):\BeneficioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withBeneficioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useBeneficioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Etapa object
     *
     * @param \Etapa|ObjectCollection $etapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByEtapa($etapa, $comparison = null)
    {
        if ($etapa instanceof \Etapa) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_CLAVE, $etapa->getIdPrograma(), $comparison);
        } elseif ($etapa instanceof ObjectCollection) {
            return $this
                ->useEtapaQuery()
                ->filterByPrimaryKeys($etapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEtapa() only accepts arguments of type \Etapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Etapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Etapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Etapa');
        }

        return $this;
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EtapaQuery A secondary query class using the current class as primary query
     */
    public function useEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Etapa', '\EtapaQuery');
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @param callable(\EtapaQuery):\EtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \ProgramaRequisito object
     *
     * @param \ProgramaRequisito|ObjectCollection $programaRequisito the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByProgramaRequisito($programaRequisito, $comparison = null)
    {
        if ($programaRequisito instanceof \ProgramaRequisito) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_CLAVE, $programaRequisito->getIdPrograma(), $comparison);
        } elseif ($programaRequisito instanceof ObjectCollection) {
            return $this
                ->useProgramaRequisitoQuery()
                ->filterByPrimaryKeys($programaRequisito->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProgramaRequisito() only accepts arguments of type \ProgramaRequisito or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProgramaRequisito relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinProgramaRequisito($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProgramaRequisito');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProgramaRequisito');
        }

        return $this;
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaRequisitoQuery A secondary query class using the current class as primary query
     */
    public function useProgramaRequisitoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProgramaRequisito($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProgramaRequisito', '\ProgramaRequisitoQuery');
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @param callable(\ProgramaRequisitoQuery):\ProgramaRequisitoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaRequisitoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaRequisitoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionPrograma object
     *
     * @param \UsuarioConfiguracionPrograma|ObjectCollection $usuarioConfiguracionPrograma the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProgramaQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionPrograma($usuarioConfiguracionPrograma, $comparison = null)
    {
        if ($usuarioConfiguracionPrograma instanceof \UsuarioConfiguracionPrograma) {
            return $this
                ->addUsingAlias(ProgramaTableMap::COL_CLAVE, $usuarioConfiguracionPrograma->getIdPrograma(), $comparison);
        } elseif ($usuarioConfiguracionPrograma instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionProgramaQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionPrograma->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionPrograma() only accepts arguments of type \UsuarioConfiguracionPrograma or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionPrograma relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionPrograma');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionPrograma');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionPrograma relation UsuarioConfiguracionPrograma object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionProgramaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionPrograma', '\UsuarioConfiguracionProgramaQuery');
    }

    /**
     * Use the UsuarioConfiguracionPrograma relation UsuarioConfiguracionPrograma object
     *
     * @param callable(\UsuarioConfiguracionProgramaQuery):\UsuarioConfiguracionProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPrograma $programa Object to remove from the list of results
     *
     * @return $this|ChildProgramaQuery The current query, for fluid interface
     */
    public function prune($programa = null)
    {
        if ($programa) {
            $this->addUsingAlias(ProgramaTableMap::COL_CLAVE, $programa->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the programa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProgramaTableMap::clearInstancePool();
            ProgramaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProgramaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProgramaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProgramaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProgramaQuery
