<?php

namespace Base;

use \Beneficio as ChildBeneficio;
use \BeneficioQuery as ChildBeneficioQuery;
use \EntidadOperativa as ChildEntidadOperativa;
use \EntidadOperativaQuery as ChildEntidadOperativaQuery;
use \EstatusPrograma as ChildEstatusPrograma;
use \EstatusProgramaQuery as ChildEstatusProgramaQuery;
use \Etapa as ChildEtapa;
use \EtapaQuery as ChildEtapaQuery;
use \Programa as ChildPrograma;
use \ProgramaQuery as ChildProgramaQuery;
use \ProgramaRequisito as ChildProgramaRequisito;
use \ProgramaRequisitoQuery as ChildProgramaRequisitoQuery;
use \TipoBeneficiario as ChildTipoBeneficiario;
use \TipoBeneficiarioQuery as ChildTipoBeneficiarioQuery;
use \Usuario as ChildUsuario;
use \UsuarioConfiguracionPrograma as ChildUsuarioConfiguracionPrograma;
use \UsuarioConfiguracionProgramaQuery as ChildUsuarioConfiguracionProgramaQuery;
use \UsuarioQuery as ChildUsuarioQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\BeneficioTableMap;
use Map\EtapaTableMap;
use Map\ProgramaRequisitoTableMap;
use Map\ProgramaTableMap;
use Map\UsuarioConfiguracionProgramaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'programa' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Programa implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\ProgramaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the clave field.
     *
     * @var        int
     */
    protected $clave;

    /**
     * The value for the nombre field.
     *
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the descripcion field.
     *
     * @var        string|null
     */
    protected $descripcion;

    /**
     * The value for the fecha_inicio field.
     *
     * @var        DateTime
     */
    protected $fecha_inicio;

    /**
     * The value for the fecha_fin field.
     *
     * @var        DateTime|null
     */
    protected $fecha_fin;

    /**
     * The value for the id_entidad_operativa field.
     *
     * @var        int
     */
    protected $id_entidad_operativa;

    /**
     * The value for the id_estatus_programa field.
     *
     * @var        int
     */
    protected $id_estatus_programa;

    /**
     * The value for the activo field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $activo;

    /**
     * The value for the fecha_creacion field.
     *
     * @var        DateTime
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     *
     * @var        DateTime
     */
    protected $fecha_modificacion;

    /**
     * The value for the id_usuario_modificacion field.
     *
     * @var        int
     */
    protected $id_usuario_modificacion;

    /**
     * The value for the id_tipo_bebeficiario field.
     *
     * @var        int|null
     */
    protected $id_tipo_bebeficiario;

    /**
     * The value for the persona_moral field.
     *
     * @var        int|null
     */
    protected $persona_moral;

    /**
     * The value for the digrama_yucatan field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $digrama_yucatan;

    /**
     * @var        ChildEntidadOperativa
     */
    protected $aEntidadOperativa;

    /**
     * @var        ChildEstatusPrograma
     */
    protected $aEstatusPrograma;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ChildTipoBeneficiario
     */
    protected $aTipoBeneficiario;

    /**
     * @var        ObjectCollection|ChildBeneficio[] Collection to store aggregation of ChildBeneficio objects.
     */
    protected $collBeneficios;
    protected $collBeneficiosPartial;

    /**
     * @var        ObjectCollection|ChildEtapa[] Collection to store aggregation of ChildEtapa objects.
     */
    protected $collEtapas;
    protected $collEtapasPartial;

    /**
     * @var        ObjectCollection|ChildProgramaRequisito[] Collection to store aggregation of ChildProgramaRequisito objects.
     */
    protected $collProgramaRequisitos;
    protected $collProgramaRequisitosPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioConfiguracionPrograma[] Collection to store aggregation of ChildUsuarioConfiguracionPrograma objects.
     */
    protected $collUsuarioConfiguracionProgramas;
    protected $collUsuarioConfiguracionProgramasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildBeneficio[]
     */
    protected $beneficiosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEtapa[]
     */
    protected $etapasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProgramaRequisito[]
     */
    protected $programaRequisitosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioConfiguracionPrograma[]
     */
    protected $usuarioConfiguracionProgramasScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->activo = 1;
        $this->digrama_yucatan = 0;
    }

    /**
     * Initializes internal state of Base\Programa object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Programa</code> instance.  If
     * <code>obj</code> is an instance of <code>Programa</code>, delegates to
     * <code>equals(Programa)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param  string  $keyType                (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [clave] column value.
     *
     * @return int
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the [descripcion] column value.
     *
     * @return string|null
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_inicio] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaInicio($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_inicio;
        } else {
            return $this->fecha_inicio instanceof \DateTimeInterface ? $this->fecha_inicio->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [fecha_fin] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime|null : string|null)
     */
    public function getFechaFin($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_fin;
        } else {
            return $this->fecha_fin instanceof \DateTimeInterface ? $this->fecha_fin->format($format) : null;
        }
    }

    /**
     * Get the [id_entidad_operativa] column value.
     *
     * @return int
     */
    public function getIdEntidadOperativa()
    {
        return $this->id_entidad_operativa;
    }

    /**
     * Get the [id_estatus_programa] column value.
     *
     * @return int
     */
    public function getIdEstatusPrograma()
    {
        return $this->id_estatus_programa;
    }

    /**
     * Get the [activo] column value.
     *
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaCreacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_creacion;
        } else {
            return $this->fecha_creacion instanceof \DateTimeInterface ? $this->fecha_creacion->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaModificacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_modificacion;
        } else {
            return $this->fecha_modificacion instanceof \DateTimeInterface ? $this->fecha_modificacion->format($format) : null;
        }
    }

    /**
     * Get the [id_usuario_modificacion] column value.
     *
     * @return int
     */
    public function getIdUsuarioModificacion()
    {
        return $this->id_usuario_modificacion;
    }

    /**
     * Get the [id_tipo_bebeficiario] column value.
     *
     * @return int|null
     */
    public function getIdTipoBebeficiario()
    {
        return $this->id_tipo_bebeficiario;
    }

    /**
     * Get the [persona_moral] column value.
     *
     * @return int|null
     */
    public function getPersonaMoral()
    {
        return $this->persona_moral;
    }

    /**
     * Get the [digrama_yucatan] column value.
     *
     * @return int
     */
    public function getDigramaYucatan()
    {
        return $this->digrama_yucatan;
    }

    /**
     * Set the value of [clave] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setClave($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clave !== $v) {
            $this->clave = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_CLAVE] = true;
        }

        return $this;
    } // setClave()

    /**
     * Set the value of [nombre] column.
     *
     * @param string $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_NOMBRE] = true;
        }

        return $this;
    } // setNombre()

    /**
     * Set the value of [descripcion] column.
     *
     * @param string|null $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_DESCRIPCION] = true;
        }

        return $this;
    } // setDescripcion()

    /**
     * Sets the value of [fecha_inicio] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setFechaInicio($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_inicio !== null || $dt !== null) {
            if ($this->fecha_inicio === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_inicio->format("Y-m-d H:i:s.u")) {
                $this->fecha_inicio = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProgramaTableMap::COL_FECHA_INICIO] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaInicio()

    /**
     * Sets the value of [fecha_fin] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setFechaFin($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_fin !== null || $dt !== null) {
            if ($this->fecha_fin === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_fin->format("Y-m-d H:i:s.u")) {
                $this->fecha_fin = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProgramaTableMap::COL_FECHA_FIN] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaFin()

    /**
     * Set the value of [id_entidad_operativa] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setIdEntidadOperativa($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_entidad_operativa !== $v) {
            $this->id_entidad_operativa = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA] = true;
        }

        if ($this->aEntidadOperativa !== null && $this->aEntidadOperativa->getClave() !== $v) {
            $this->aEntidadOperativa = null;
        }

        return $this;
    } // setIdEntidadOperativa()

    /**
     * Set the value of [id_estatus_programa] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setIdEstatusPrograma($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_estatus_programa !== $v) {
            $this->id_estatus_programa = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA] = true;
        }

        if ($this->aEstatusPrograma !== null && $this->aEstatusPrograma->getClave() !== $v) {
            $this->aEstatusPrograma = null;
        }

        return $this;
    } // setIdEstatusPrograma()

    /**
     * Set the value of [activo] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setActivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->activo !== $v) {
            $this->activo = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_ACTIVO] = true;
        }

        return $this;
    } // setActivo()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            if ($this->fecha_creacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_creacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_creacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProgramaTableMap::COL_FECHA_CREACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            if ($this->fecha_modificacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_modificacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_modificacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProgramaTableMap::COL_FECHA_MODIFICACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaModificacion()

    /**
     * Set the value of [id_usuario_modificacion] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setIdUsuarioModificacion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_usuario_modificacion !== $v) {
            $this->id_usuario_modificacion = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_ID_USUARIO_MODIFICACION] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getClave() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setIdUsuarioModificacion()

    /**
     * Set the value of [id_tipo_bebeficiario] column.
     *
     * @param int|null $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setIdTipoBebeficiario($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_tipo_bebeficiario !== $v) {
            $this->id_tipo_bebeficiario = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO] = true;
        }

        if ($this->aTipoBeneficiario !== null && $this->aTipoBeneficiario->getClave() !== $v) {
            $this->aTipoBeneficiario = null;
        }

        return $this;
    } // setIdTipoBebeficiario()

    /**
     * Set the value of [persona_moral] column.
     *
     * @param int|null $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setPersonaMoral($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->persona_moral !== $v) {
            $this->persona_moral = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_PERSONA_MORAL] = true;
        }

        return $this;
    } // setPersonaMoral()

    /**
     * Set the value of [digrama_yucatan] column.
     *
     * @param int $v New value
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function setDigramaYucatan($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->digrama_yucatan !== $v) {
            $this->digrama_yucatan = $v;
            $this->modifiedColumns[ProgramaTableMap::COL_DIGRAMA_YUCATAN] = true;
        }

        return $this;
    } // setDigramaYucatan()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->activo !== 1) {
                return false;
            }

            if ($this->digrama_yucatan !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProgramaTableMap::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clave = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProgramaTableMap::translateFieldName('Nombre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nombre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProgramaTableMap::translateFieldName('Descripcion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descripcion = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProgramaTableMap::translateFieldName('FechaInicio', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_inicio = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProgramaTableMap::translateFieldName('FechaFin', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_fin = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProgramaTableMap::translateFieldName('IdEntidadOperativa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_entidad_operativa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProgramaTableMap::translateFieldName('IdEstatusPrograma', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_estatus_programa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ProgramaTableMap::translateFieldName('Activo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ProgramaTableMap::translateFieldName('FechaCreacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_creacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ProgramaTableMap::translateFieldName('FechaModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_modificacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ProgramaTableMap::translateFieldName('IdUsuarioModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_usuario_modificacion = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ProgramaTableMap::translateFieldName('IdTipoBebeficiario', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_tipo_bebeficiario = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ProgramaTableMap::translateFieldName('PersonaMoral', TableMap::TYPE_PHPNAME, $indexType)];
            $this->persona_moral = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ProgramaTableMap::translateFieldName('DigramaYucatan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->digrama_yucatan = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = ProgramaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Programa'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEntidadOperativa !== null && $this->id_entidad_operativa !== $this->aEntidadOperativa->getClave()) {
            $this->aEntidadOperativa = null;
        }
        if ($this->aEstatusPrograma !== null && $this->id_estatus_programa !== $this->aEstatusPrograma->getClave()) {
            $this->aEstatusPrograma = null;
        }
        if ($this->aUsuario !== null && $this->id_usuario_modificacion !== $this->aUsuario->getClave()) {
            $this->aUsuario = null;
        }
        if ($this->aTipoBeneficiario !== null && $this->id_tipo_bebeficiario !== $this->aTipoBeneficiario->getClave()) {
            $this->aTipoBeneficiario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProgramaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProgramaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEntidadOperativa = null;
            $this->aEstatusPrograma = null;
            $this->aUsuario = null;
            $this->aTipoBeneficiario = null;
            $this->collBeneficios = null;

            $this->collEtapas = null;

            $this->collProgramaRequisitos = null;

            $this->collUsuarioConfiguracionProgramas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Programa::setDeleted()
     * @see Programa::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProgramaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProgramaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEntidadOperativa !== null) {
                if ($this->aEntidadOperativa->isModified() || $this->aEntidadOperativa->isNew()) {
                    $affectedRows += $this->aEntidadOperativa->save($con);
                }
                $this->setEntidadOperativa($this->aEntidadOperativa);
            }

            if ($this->aEstatusPrograma !== null) {
                if ($this->aEstatusPrograma->isModified() || $this->aEstatusPrograma->isNew()) {
                    $affectedRows += $this->aEstatusPrograma->save($con);
                }
                $this->setEstatusPrograma($this->aEstatusPrograma);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->aTipoBeneficiario !== null) {
                if ($this->aTipoBeneficiario->isModified() || $this->aTipoBeneficiario->isNew()) {
                    $affectedRows += $this->aTipoBeneficiario->save($con);
                }
                $this->setTipoBeneficiario($this->aTipoBeneficiario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->beneficiosScheduledForDeletion !== null) {
                if (!$this->beneficiosScheduledForDeletion->isEmpty()) {
                    \BeneficioQuery::create()
                        ->filterByPrimaryKeys($this->beneficiosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->beneficiosScheduledForDeletion = null;
                }
            }

            if ($this->collBeneficios !== null) {
                foreach ($this->collBeneficios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->etapasScheduledForDeletion !== null) {
                if (!$this->etapasScheduledForDeletion->isEmpty()) {
                    \EtapaQuery::create()
                        ->filterByPrimaryKeys($this->etapasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->etapasScheduledForDeletion = null;
                }
            }

            if ($this->collEtapas !== null) {
                foreach ($this->collEtapas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->programaRequisitosScheduledForDeletion !== null) {
                if (!$this->programaRequisitosScheduledForDeletion->isEmpty()) {
                    \ProgramaRequisitoQuery::create()
                        ->filterByPrimaryKeys($this->programaRequisitosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->programaRequisitosScheduledForDeletion = null;
                }
            }

            if ($this->collProgramaRequisitos !== null) {
                foreach ($this->collProgramaRequisitos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioConfiguracionProgramasScheduledForDeletion !== null) {
                if (!$this->usuarioConfiguracionProgramasScheduledForDeletion->isEmpty()) {
                    \UsuarioConfiguracionProgramaQuery::create()
                        ->filterByPrimaryKeys($this->usuarioConfiguracionProgramasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioConfiguracionProgramasScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioConfiguracionProgramas !== null) {
                foreach ($this->collUsuarioConfiguracionProgramas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProgramaTableMap::COL_CLAVE] = true;
        if (null !== $this->clave) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProgramaTableMap::COL_CLAVE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProgramaTableMap::COL_CLAVE)) {
            $modifiedColumns[':p' . $index++]  = 'clave';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = 'nombre';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = 'descripcion';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_INICIO)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_inicio';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_FIN)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_fin';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA)) {
            $modifiedColumns[':p' . $index++]  = 'id_entidad_operativa';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA)) {
            $modifiedColumns[':p' . $index++]  = 'id_estatus_programa';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ACTIVO)) {
            $modifiedColumns[':p' . $index++]  = 'activo';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_creacion';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_modificacion';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'id_usuario_modificacion';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO)) {
            $modifiedColumns[':p' . $index++]  = 'id_tipo_bebeficiario';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_PERSONA_MORAL)) {
            $modifiedColumns[':p' . $index++]  = 'persona_moral';
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_DIGRAMA_YUCATAN)) {
            $modifiedColumns[':p' . $index++]  = 'digrama_yucatan';
        }

        $sql = sprintf(
            'INSERT INTO programa (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'clave':
                        $stmt->bindValue($identifier, $this->clave, PDO::PARAM_INT);
                        break;
                    case 'nombre':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case 'descripcion':
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case 'fecha_inicio':
                        $stmt->bindValue($identifier, $this->fecha_inicio ? $this->fecha_inicio->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'fecha_fin':
                        $stmt->bindValue($identifier, $this->fecha_fin ? $this->fecha_fin->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'id_entidad_operativa':
                        $stmt->bindValue($identifier, $this->id_entidad_operativa, PDO::PARAM_INT);
                        break;
                    case 'id_estatus_programa':
                        $stmt->bindValue($identifier, $this->id_estatus_programa, PDO::PARAM_INT);
                        break;
                    case 'activo':
                        $stmt->bindValue($identifier, $this->activo, PDO::PARAM_INT);
                        break;
                    case 'fecha_creacion':
                        $stmt->bindValue($identifier, $this->fecha_creacion ? $this->fecha_creacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'fecha_modificacion':
                        $stmt->bindValue($identifier, $this->fecha_modificacion ? $this->fecha_modificacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'id_usuario_modificacion':
                        $stmt->bindValue($identifier, $this->id_usuario_modificacion, PDO::PARAM_INT);
                        break;
                    case 'id_tipo_bebeficiario':
                        $stmt->bindValue($identifier, $this->id_tipo_bebeficiario, PDO::PARAM_INT);
                        break;
                    case 'persona_moral':
                        $stmt->bindValue($identifier, $this->persona_moral, PDO::PARAM_INT);
                        break;
                    case 'digrama_yucatan':
                        $stmt->bindValue($identifier, $this->digrama_yucatan, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setClave($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProgramaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getClave();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDescripcion();
                break;
            case 3:
                return $this->getFechaInicio();
                break;
            case 4:
                return $this->getFechaFin();
                break;
            case 5:
                return $this->getIdEntidadOperativa();
                break;
            case 6:
                return $this->getIdEstatusPrograma();
                break;
            case 7:
                return $this->getActivo();
                break;
            case 8:
                return $this->getFechaCreacion();
                break;
            case 9:
                return $this->getFechaModificacion();
                break;
            case 10:
                return $this->getIdUsuarioModificacion();
                break;
            case 11:
                return $this->getIdTipoBebeficiario();
                break;
            case 12:
                return $this->getPersonaMoral();
                break;
            case 13:
                return $this->getDigramaYucatan();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Programa'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Programa'][$this->hashCode()] = true;
        $keys = ProgramaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getClave(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDescripcion(),
            $keys[3] => $this->getFechaInicio(),
            $keys[4] => $this->getFechaFin(),
            $keys[5] => $this->getIdEntidadOperativa(),
            $keys[6] => $this->getIdEstatusPrograma(),
            $keys[7] => $this->getActivo(),
            $keys[8] => $this->getFechaCreacion(),
            $keys[9] => $this->getFechaModificacion(),
            $keys[10] => $this->getIdUsuarioModificacion(),
            $keys[11] => $this->getIdTipoBebeficiario(),
            $keys[12] => $this->getPersonaMoral(),
            $keys[13] => $this->getDigramaYucatan(),
        );
        if ($result[$keys[3]] instanceof \DateTimeInterface) {
            $result[$keys[3]] = $result[$keys[3]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEntidadOperativa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'entidadOperativa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'entidad_operativa';
                        break;
                    default:
                        $key = 'EntidadOperativa';
                }

                $result[$key] = $this->aEntidadOperativa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEstatusPrograma) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'estatusPrograma';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'estatus_programa';
                        break;
                    default:
                        $key = 'EstatusPrograma';
                }

                $result[$key] = $this->aEstatusPrograma->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aTipoBeneficiario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'tipoBeneficiario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'tipo_beneficiario';
                        break;
                    default:
                        $key = 'TipoBeneficiario';
                }

                $result[$key] = $this->aTipoBeneficiario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBeneficios) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'beneficios';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'beneficios';
                        break;
                    default:
                        $key = 'Beneficios';
                }

                $result[$key] = $this->collBeneficios->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEtapas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etapas';
                        break;
                    default:
                        $key = 'Etapas';
                }

                $result[$key] = $this->collEtapas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProgramaRequisitos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'programaRequisitos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'programa_requisitos';
                        break;
                    default:
                        $key = 'ProgramaRequisitos';
                }

                $result[$key] = $this->collProgramaRequisitos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioConfiguracionProgramas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioConfiguracionProgramas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_configuracion_programas';
                        break;
                    default:
                        $key = 'UsuarioConfiguracionProgramas';
                }

                $result[$key] = $this->collUsuarioConfiguracionProgramas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Programa
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProgramaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Programa
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setClave($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDescripcion($value);
                break;
            case 3:
                $this->setFechaInicio($value);
                break;
            case 4:
                $this->setFechaFin($value);
                break;
            case 5:
                $this->setIdEntidadOperativa($value);
                break;
            case 6:
                $this->setIdEstatusPrograma($value);
                break;
            case 7:
                $this->setActivo($value);
                break;
            case 8:
                $this->setFechaCreacion($value);
                break;
            case 9:
                $this->setFechaModificacion($value);
                break;
            case 10:
                $this->setIdUsuarioModificacion($value);
                break;
            case 11:
                $this->setIdTipoBebeficiario($value);
                break;
            case 12:
                $this->setPersonaMoral($value);
                break;
            case 13:
                $this->setDigramaYucatan($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     $this|\Programa
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProgramaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setClave($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNombre($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDescripcion($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFechaInicio($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setFechaFin($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIdEntidadOperativa($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdEstatusPrograma($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setActivo($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFechaCreacion($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setFechaModificacion($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setIdUsuarioModificacion($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setIdTipoBebeficiario($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setPersonaMoral($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDigramaYucatan($arr[$keys[13]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Programa The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProgramaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProgramaTableMap::COL_CLAVE)) {
            $criteria->add(ProgramaTableMap::COL_CLAVE, $this->clave);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_NOMBRE)) {
            $criteria->add(ProgramaTableMap::COL_NOMBRE, $this->nombre);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_DESCRIPCION)) {
            $criteria->add(ProgramaTableMap::COL_DESCRIPCION, $this->descripcion);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_INICIO)) {
            $criteria->add(ProgramaTableMap::COL_FECHA_INICIO, $this->fecha_inicio);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_FIN)) {
            $criteria->add(ProgramaTableMap::COL_FECHA_FIN, $this->fecha_fin);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA)) {
            $criteria->add(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, $this->id_entidad_operativa);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA)) {
            $criteria->add(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, $this->id_estatus_programa);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ACTIVO)) {
            $criteria->add(ProgramaTableMap::COL_ACTIVO, $this->activo);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_CREACION)) {
            $criteria->add(ProgramaTableMap::COL_FECHA_CREACION, $this->fecha_creacion);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_FECHA_MODIFICACION)) {
            $criteria->add(ProgramaTableMap::COL_FECHA_MODIFICACION, $this->fecha_modificacion);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $criteria->add(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, $this->id_usuario_modificacion);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO)) {
            $criteria->add(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, $this->id_tipo_bebeficiario);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_PERSONA_MORAL)) {
            $criteria->add(ProgramaTableMap::COL_PERSONA_MORAL, $this->persona_moral);
        }
        if ($this->isColumnModified(ProgramaTableMap::COL_DIGRAMA_YUCATAN)) {
            $criteria->add(ProgramaTableMap::COL_DIGRAMA_YUCATAN, $this->digrama_yucatan);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProgramaQuery::create();
        $criteria->add(ProgramaTableMap::COL_CLAVE, $this->clave);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getClave();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getClave();
    }

    /**
     * Generic method to set the primary key (clave column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setClave($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getClave();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Programa (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setFechaInicio($this->getFechaInicio());
        $copyObj->setFechaFin($this->getFechaFin());
        $copyObj->setIdEntidadOperativa($this->getIdEntidadOperativa());
        $copyObj->setIdEstatusPrograma($this->getIdEstatusPrograma());
        $copyObj->setActivo($this->getActivo());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setIdUsuarioModificacion($this->getIdUsuarioModificacion());
        $copyObj->setIdTipoBebeficiario($this->getIdTipoBebeficiario());
        $copyObj->setPersonaMoral($this->getPersonaMoral());
        $copyObj->setDigramaYucatan($this->getDigramaYucatan());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBeneficios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBeneficio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEtapas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEtapa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProgramaRequisitos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProgramaRequisito($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioConfiguracionProgramas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioConfiguracionPrograma($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setClave(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Programa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEntidadOperativa object.
     *
     * @param  ChildEntidadOperativa $v
     * @return $this|\Programa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEntidadOperativa(ChildEntidadOperativa $v = null)
    {
        if ($v === null) {
            $this->setIdEntidadOperativa(NULL);
        } else {
            $this->setIdEntidadOperativa($v->getClave());
        }

        $this->aEntidadOperativa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEntidadOperativa object, it will not be re-added.
        if ($v !== null) {
            $v->addPrograma($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEntidadOperativa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEntidadOperativa The associated ChildEntidadOperativa object.
     * @throws PropelException
     */
    public function getEntidadOperativa(ConnectionInterface $con = null)
    {
        if ($this->aEntidadOperativa === null && ($this->id_entidad_operativa != 0)) {
            $this->aEntidadOperativa = ChildEntidadOperativaQuery::create()->findPk($this->id_entidad_operativa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEntidadOperativa->addProgramas($this);
             */
        }

        return $this->aEntidadOperativa;
    }

    /**
     * Declares an association between this object and a ChildEstatusPrograma object.
     *
     * @param  ChildEstatusPrograma $v
     * @return $this|\Programa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEstatusPrograma(ChildEstatusPrograma $v = null)
    {
        if ($v === null) {
            $this->setIdEstatusPrograma(NULL);
        } else {
            $this->setIdEstatusPrograma($v->getClave());
        }

        $this->aEstatusPrograma = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEstatusPrograma object, it will not be re-added.
        if ($v !== null) {
            $v->addPrograma($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEstatusPrograma object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEstatusPrograma The associated ChildEstatusPrograma object.
     * @throws PropelException
     */
    public function getEstatusPrograma(ConnectionInterface $con = null)
    {
        if ($this->aEstatusPrograma === null && ($this->id_estatus_programa != 0)) {
            $this->aEstatusPrograma = ChildEstatusProgramaQuery::create()->findPk($this->id_estatus_programa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEstatusPrograma->addProgramas($this);
             */
        }

        return $this->aEstatusPrograma;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\Programa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setIdUsuarioModificacion(NULL);
        } else {
            $this->setIdUsuarioModificacion($v->getClave());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addPrograma($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->id_usuario_modificacion != 0)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->id_usuario_modificacion, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addProgramas($this);
             */
        }

        return $this->aUsuario;
    }

    /**
     * Declares an association between this object and a ChildTipoBeneficiario object.
     *
     * @param  ChildTipoBeneficiario|null $v
     * @return $this|\Programa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setTipoBeneficiario(ChildTipoBeneficiario $v = null)
    {
        if ($v === null) {
            $this->setIdTipoBebeficiario(NULL);
        } else {
            $this->setIdTipoBebeficiario($v->getClave());
        }

        $this->aTipoBeneficiario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildTipoBeneficiario object, it will not be re-added.
        if ($v !== null) {
            $v->addPrograma($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildTipoBeneficiario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildTipoBeneficiario|null The associated ChildTipoBeneficiario object.
     * @throws PropelException
     */
    public function getTipoBeneficiario(ConnectionInterface $con = null)
    {
        if ($this->aTipoBeneficiario === null && ($this->id_tipo_bebeficiario != 0)) {
            $this->aTipoBeneficiario = ChildTipoBeneficiarioQuery::create()->findPk($this->id_tipo_bebeficiario, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aTipoBeneficiario->addProgramas($this);
             */
        }

        return $this->aTipoBeneficiario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Beneficio' === $relationName) {
            $this->initBeneficios();
            return;
        }
        if ('Etapa' === $relationName) {
            $this->initEtapas();
            return;
        }
        if ('ProgramaRequisito' === $relationName) {
            $this->initProgramaRequisitos();
            return;
        }
        if ('UsuarioConfiguracionPrograma' === $relationName) {
            $this->initUsuarioConfiguracionProgramas();
            return;
        }
    }

    /**
     * Clears out the collBeneficios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBeneficios()
     */
    public function clearBeneficios()
    {
        $this->collBeneficios = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBeneficios collection loaded partially.
     */
    public function resetPartialBeneficios($v = true)
    {
        $this->collBeneficiosPartial = $v;
    }

    /**
     * Initializes the collBeneficios collection.
     *
     * By default this just sets the collBeneficios collection to an empty array (like clearcollBeneficios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBeneficios($overrideExisting = true)
    {
        if (null !== $this->collBeneficios && !$overrideExisting) {
            return;
        }

        $collectionClassName = BeneficioTableMap::getTableMap()->getCollectionClassName();

        $this->collBeneficios = new $collectionClassName;
        $this->collBeneficios->setModel('\Beneficio');
    }

    /**
     * Gets an array of ChildBeneficio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrograma is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     * @throws PropelException
     */
    public function getBeneficios(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBeneficiosPartial && !$this->isNew();
        if (null === $this->collBeneficios || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collBeneficios) {
                    $this->initBeneficios();
                } else {
                    $collectionClassName = BeneficioTableMap::getTableMap()->getCollectionClassName();

                    $collBeneficios = new $collectionClassName;
                    $collBeneficios->setModel('\Beneficio');

                    return $collBeneficios;
                }
            } else {
                $collBeneficios = ChildBeneficioQuery::create(null, $criteria)
                    ->filterByPrograma($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBeneficiosPartial && count($collBeneficios)) {
                        $this->initBeneficios(false);

                        foreach ($collBeneficios as $obj) {
                            if (false == $this->collBeneficios->contains($obj)) {
                                $this->collBeneficios->append($obj);
                            }
                        }

                        $this->collBeneficiosPartial = true;
                    }

                    return $collBeneficios;
                }

                if ($partial && $this->collBeneficios) {
                    foreach ($this->collBeneficios as $obj) {
                        if ($obj->isNew()) {
                            $collBeneficios[] = $obj;
                        }
                    }
                }

                $this->collBeneficios = $collBeneficios;
                $this->collBeneficiosPartial = false;
            }
        }

        return $this->collBeneficios;
    }

    /**
     * Sets a collection of ChildBeneficio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $beneficios A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function setBeneficios(Collection $beneficios, ConnectionInterface $con = null)
    {
        /** @var ChildBeneficio[] $beneficiosToDelete */
        $beneficiosToDelete = $this->getBeneficios(new Criteria(), $con)->diff($beneficios);


        $this->beneficiosScheduledForDeletion = $beneficiosToDelete;

        foreach ($beneficiosToDelete as $beneficioRemoved) {
            $beneficioRemoved->setPrograma(null);
        }

        $this->collBeneficios = null;
        foreach ($beneficios as $beneficio) {
            $this->addBeneficio($beneficio);
        }

        $this->collBeneficios = $beneficios;
        $this->collBeneficiosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Beneficio objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Beneficio objects.
     * @throws PropelException
     */
    public function countBeneficios(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBeneficiosPartial && !$this->isNew();
        if (null === $this->collBeneficios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBeneficios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBeneficios());
            }

            $query = ChildBeneficioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrograma($this)
                ->count($con);
        }

        return count($this->collBeneficios);
    }

    /**
     * Method called to associate a ChildBeneficio object to this object
     * through the ChildBeneficio foreign key attribute.
     *
     * @param  ChildBeneficio $l ChildBeneficio
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function addBeneficio(ChildBeneficio $l)
    {
        if ($this->collBeneficios === null) {
            $this->initBeneficios();
            $this->collBeneficiosPartial = true;
        }

        if (!$this->collBeneficios->contains($l)) {
            $this->doAddBeneficio($l);

            if ($this->beneficiosScheduledForDeletion and $this->beneficiosScheduledForDeletion->contains($l)) {
                $this->beneficiosScheduledForDeletion->remove($this->beneficiosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildBeneficio $beneficio The ChildBeneficio object to add.
     */
    protected function doAddBeneficio(ChildBeneficio $beneficio)
    {
        $this->collBeneficios[]= $beneficio;
        $beneficio->setPrograma($this);
    }

    /**
     * @param  ChildBeneficio $beneficio The ChildBeneficio object to remove.
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function removeBeneficio(ChildBeneficio $beneficio)
    {
        if ($this->getBeneficios()->contains($beneficio)) {
            $pos = $this->collBeneficios->search($beneficio);
            $this->collBeneficios->remove($pos);
            if (null === $this->beneficiosScheduledForDeletion) {
                $this->beneficiosScheduledForDeletion = clone $this->collBeneficios;
                $this->beneficiosScheduledForDeletion->clear();
            }
            $this->beneficiosScheduledForDeletion[]= clone $beneficio;
            $beneficio->setPrograma(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoDistribucion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoDistribucion', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoBeneficio(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoBeneficio', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinTipoRecurso(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('TipoRecurso', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinUnidadMedida(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('UnidadMedida', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Beneficios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildBeneficio[] List of ChildBeneficio objects
     */
    public function getBeneficiosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildBeneficioQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getBeneficios($query, $con);
    }

    /**
     * Clears out the collEtapas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEtapas()
     */
    public function clearEtapas()
    {
        $this->collEtapas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEtapas collection loaded partially.
     */
    public function resetPartialEtapas($v = true)
    {
        $this->collEtapasPartial = $v;
    }

    /**
     * Initializes the collEtapas collection.
     *
     * By default this just sets the collEtapas collection to an empty array (like clearcollEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEtapas($overrideExisting = true)
    {
        if (null !== $this->collEtapas && !$overrideExisting) {
            return;
        }

        $collectionClassName = EtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collEtapas = new $collectionClassName;
        $this->collEtapas->setModel('\Etapa');
    }

    /**
     * Gets an array of ChildEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrograma is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEtapa[] List of ChildEtapa objects
     * @throws PropelException
     */
    public function getEtapas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEtapasPartial && !$this->isNew();
        if (null === $this->collEtapas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collEtapas) {
                    $this->initEtapas();
                } else {
                    $collectionClassName = EtapaTableMap::getTableMap()->getCollectionClassName();

                    $collEtapas = new $collectionClassName;
                    $collEtapas->setModel('\Etapa');

                    return $collEtapas;
                }
            } else {
                $collEtapas = ChildEtapaQuery::create(null, $criteria)
                    ->filterByPrograma($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEtapasPartial && count($collEtapas)) {
                        $this->initEtapas(false);

                        foreach ($collEtapas as $obj) {
                            if (false == $this->collEtapas->contains($obj)) {
                                $this->collEtapas->append($obj);
                            }
                        }

                        $this->collEtapasPartial = true;
                    }

                    return $collEtapas;
                }

                if ($partial && $this->collEtapas) {
                    foreach ($this->collEtapas as $obj) {
                        if ($obj->isNew()) {
                            $collEtapas[] = $obj;
                        }
                    }
                }

                $this->collEtapas = $collEtapas;
                $this->collEtapasPartial = false;
            }
        }

        return $this->collEtapas;
    }

    /**
     * Sets a collection of ChildEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $etapas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function setEtapas(Collection $etapas, ConnectionInterface $con = null)
    {
        /** @var ChildEtapa[] $etapasToDelete */
        $etapasToDelete = $this->getEtapas(new Criteria(), $con)->diff($etapas);


        $this->etapasScheduledForDeletion = $etapasToDelete;

        foreach ($etapasToDelete as $etapaRemoved) {
            $etapaRemoved->setPrograma(null);
        }

        $this->collEtapas = null;
        foreach ($etapas as $etapa) {
            $this->addEtapa($etapa);
        }

        $this->collEtapas = $etapas;
        $this->collEtapasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Etapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Etapa objects.
     * @throws PropelException
     */
    public function countEtapas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEtapasPartial && !$this->isNew();
        if (null === $this->collEtapas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEtapas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEtapas());
            }

            $query = ChildEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrograma($this)
                ->count($con);
        }

        return count($this->collEtapas);
    }

    /**
     * Method called to associate a ChildEtapa object to this object
     * through the ChildEtapa foreign key attribute.
     *
     * @param  ChildEtapa $l ChildEtapa
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function addEtapa(ChildEtapa $l)
    {
        if ($this->collEtapas === null) {
            $this->initEtapas();
            $this->collEtapasPartial = true;
        }

        if (!$this->collEtapas->contains($l)) {
            $this->doAddEtapa($l);

            if ($this->etapasScheduledForDeletion and $this->etapasScheduledForDeletion->contains($l)) {
                $this->etapasScheduledForDeletion->remove($this->etapasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEtapa $etapa The ChildEtapa object to add.
     */
    protected function doAddEtapa(ChildEtapa $etapa)
    {
        $this->collEtapas[]= $etapa;
        $etapa->setPrograma($this);
    }

    /**
     * @param  ChildEtapa $etapa The ChildEtapa object to remove.
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function removeEtapa(ChildEtapa $etapa)
    {
        if ($this->getEtapas()->contains($etapa)) {
            $pos = $this->collEtapas->search($etapa);
            $this->collEtapas->remove($pos);
            if (null === $this->etapasScheduledForDeletion) {
                $this->etapasScheduledForDeletion = clone $this->collEtapas;
                $this->etapasScheduledForDeletion->clear();
            }
            $this->etapasScheduledForDeletion[]= clone $etapa;
            $etapa->setPrograma(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related Etapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEtapa[] List of ChildEtapa objects
     */
    public function getEtapasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEtapaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getEtapas($query, $con);
    }

    /**
     * Clears out the collProgramaRequisitos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProgramaRequisitos()
     */
    public function clearProgramaRequisitos()
    {
        $this->collProgramaRequisitos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProgramaRequisitos collection loaded partially.
     */
    public function resetPartialProgramaRequisitos($v = true)
    {
        $this->collProgramaRequisitosPartial = $v;
    }

    /**
     * Initializes the collProgramaRequisitos collection.
     *
     * By default this just sets the collProgramaRequisitos collection to an empty array (like clearcollProgramaRequisitos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProgramaRequisitos($overrideExisting = true)
    {
        if (null !== $this->collProgramaRequisitos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

        $this->collProgramaRequisitos = new $collectionClassName;
        $this->collProgramaRequisitos->setModel('\ProgramaRequisito');
    }

    /**
     * Gets an array of ChildProgramaRequisito objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrograma is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     * @throws PropelException
     */
    public function getProgramaRequisitos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProgramaRequisitos) {
                    $this->initProgramaRequisitos();
                } else {
                    $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

                    $collProgramaRequisitos = new $collectionClassName;
                    $collProgramaRequisitos->setModel('\ProgramaRequisito');

                    return $collProgramaRequisitos;
                }
            } else {
                $collProgramaRequisitos = ChildProgramaRequisitoQuery::create(null, $criteria)
                    ->filterByPrograma($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProgramaRequisitosPartial && count($collProgramaRequisitos)) {
                        $this->initProgramaRequisitos(false);

                        foreach ($collProgramaRequisitos as $obj) {
                            if (false == $this->collProgramaRequisitos->contains($obj)) {
                                $this->collProgramaRequisitos->append($obj);
                            }
                        }

                        $this->collProgramaRequisitosPartial = true;
                    }

                    return $collProgramaRequisitos;
                }

                if ($partial && $this->collProgramaRequisitos) {
                    foreach ($this->collProgramaRequisitos as $obj) {
                        if ($obj->isNew()) {
                            $collProgramaRequisitos[] = $obj;
                        }
                    }
                }

                $this->collProgramaRequisitos = $collProgramaRequisitos;
                $this->collProgramaRequisitosPartial = false;
            }
        }

        return $this->collProgramaRequisitos;
    }

    /**
     * Sets a collection of ChildProgramaRequisito objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $programaRequisitos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function setProgramaRequisitos(Collection $programaRequisitos, ConnectionInterface $con = null)
    {
        /** @var ChildProgramaRequisito[] $programaRequisitosToDelete */
        $programaRequisitosToDelete = $this->getProgramaRequisitos(new Criteria(), $con)->diff($programaRequisitos);


        $this->programaRequisitosScheduledForDeletion = $programaRequisitosToDelete;

        foreach ($programaRequisitosToDelete as $programaRequisitoRemoved) {
            $programaRequisitoRemoved->setPrograma(null);
        }

        $this->collProgramaRequisitos = null;
        foreach ($programaRequisitos as $programaRequisito) {
            $this->addProgramaRequisito($programaRequisito);
        }

        $this->collProgramaRequisitos = $programaRequisitos;
        $this->collProgramaRequisitosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProgramaRequisito objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProgramaRequisito objects.
     * @throws PropelException
     */
    public function countProgramaRequisitos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProgramaRequisitos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProgramaRequisitos());
            }

            $query = ChildProgramaRequisitoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrograma($this)
                ->count($con);
        }

        return count($this->collProgramaRequisitos);
    }

    /**
     * Method called to associate a ChildProgramaRequisito object to this object
     * through the ChildProgramaRequisito foreign key attribute.
     *
     * @param  ChildProgramaRequisito $l ChildProgramaRequisito
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function addProgramaRequisito(ChildProgramaRequisito $l)
    {
        if ($this->collProgramaRequisitos === null) {
            $this->initProgramaRequisitos();
            $this->collProgramaRequisitosPartial = true;
        }

        if (!$this->collProgramaRequisitos->contains($l)) {
            $this->doAddProgramaRequisito($l);

            if ($this->programaRequisitosScheduledForDeletion and $this->programaRequisitosScheduledForDeletion->contains($l)) {
                $this->programaRequisitosScheduledForDeletion->remove($this->programaRequisitosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to add.
     */
    protected function doAddProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        $this->collProgramaRequisitos[]= $programaRequisito;
        $programaRequisito->setPrograma($this);
    }

    /**
     * @param  ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to remove.
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function removeProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        if ($this->getProgramaRequisitos()->contains($programaRequisito)) {
            $pos = $this->collProgramaRequisitos->search($programaRequisito);
            $this->collProgramaRequisitos->remove($pos);
            if (null === $this->programaRequisitosScheduledForDeletion) {
                $this->programaRequisitosScheduledForDeletion = clone $this->collProgramaRequisitos;
                $this->programaRequisitosScheduledForDeletion->clear();
            }
            $this->programaRequisitosScheduledForDeletion[]= clone $programaRequisito;
            $programaRequisito->setPrograma(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinEtapa(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Etapa', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinTipoRequisito(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('TipoRequisito', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }

    /**
     * Clears out the collUsuarioConfiguracionProgramas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioConfiguracionProgramas()
     */
    public function clearUsuarioConfiguracionProgramas()
    {
        $this->collUsuarioConfiguracionProgramas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioConfiguracionProgramas collection loaded partially.
     */
    public function resetPartialUsuarioConfiguracionProgramas($v = true)
    {
        $this->collUsuarioConfiguracionProgramasPartial = $v;
    }

    /**
     * Initializes the collUsuarioConfiguracionProgramas collection.
     *
     * By default this just sets the collUsuarioConfiguracionProgramas collection to an empty array (like clearcollUsuarioConfiguracionProgramas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionProgramas($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionProgramas && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioConfiguracionProgramas = new $collectionClassName;
        $this->collUsuarioConfiguracionProgramas->setModel('\UsuarioConfiguracionPrograma');
    }

    /**
     * Gets an array of ChildUsuarioConfiguracionPrograma objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPrograma is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     * @throws PropelException
     */
    public function getUsuarioConfiguracionProgramas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioConfiguracionProgramas) {
                    $this->initUsuarioConfiguracionProgramas();
                } else {
                    $collectionClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioConfiguracionProgramas = new $collectionClassName;
                    $collUsuarioConfiguracionProgramas->setModel('\UsuarioConfiguracionPrograma');

                    return $collUsuarioConfiguracionProgramas;
                }
            } else {
                $collUsuarioConfiguracionProgramas = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria)
                    ->filterByPrograma($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioConfiguracionProgramasPartial && count($collUsuarioConfiguracionProgramas)) {
                        $this->initUsuarioConfiguracionProgramas(false);

                        foreach ($collUsuarioConfiguracionProgramas as $obj) {
                            if (false == $this->collUsuarioConfiguracionProgramas->contains($obj)) {
                                $this->collUsuarioConfiguracionProgramas->append($obj);
                            }
                        }

                        $this->collUsuarioConfiguracionProgramasPartial = true;
                    }

                    return $collUsuarioConfiguracionProgramas;
                }

                if ($partial && $this->collUsuarioConfiguracionProgramas) {
                    foreach ($this->collUsuarioConfiguracionProgramas as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioConfiguracionProgramas[] = $obj;
                        }
                    }
                }

                $this->collUsuarioConfiguracionProgramas = $collUsuarioConfiguracionProgramas;
                $this->collUsuarioConfiguracionProgramasPartial = false;
            }
        }

        return $this->collUsuarioConfiguracionProgramas;
    }

    /**
     * Sets a collection of ChildUsuarioConfiguracionPrograma objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioConfiguracionProgramas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function setUsuarioConfiguracionProgramas(Collection $usuarioConfiguracionProgramas, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioConfiguracionPrograma[] $usuarioConfiguracionProgramasToDelete */
        $usuarioConfiguracionProgramasToDelete = $this->getUsuarioConfiguracionProgramas(new Criteria(), $con)->diff($usuarioConfiguracionProgramas);


        $this->usuarioConfiguracionProgramasScheduledForDeletion = $usuarioConfiguracionProgramasToDelete;

        foreach ($usuarioConfiguracionProgramasToDelete as $usuarioConfiguracionProgramaRemoved) {
            $usuarioConfiguracionProgramaRemoved->setPrograma(null);
        }

        $this->collUsuarioConfiguracionProgramas = null;
        foreach ($usuarioConfiguracionProgramas as $usuarioConfiguracionPrograma) {
            $this->addUsuarioConfiguracionPrograma($usuarioConfiguracionPrograma);
        }

        $this->collUsuarioConfiguracionProgramas = $usuarioConfiguracionProgramas;
        $this->collUsuarioConfiguracionProgramasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioConfiguracionPrograma objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioConfiguracionPrograma objects.
     * @throws PropelException
     */
    public function countUsuarioConfiguracionProgramas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionProgramasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionProgramas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioConfiguracionProgramas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioConfiguracionProgramas());
            }

            $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPrograma($this)
                ->count($con);
        }

        return count($this->collUsuarioConfiguracionProgramas);
    }

    /**
     * Method called to associate a ChildUsuarioConfiguracionPrograma object to this object
     * through the ChildUsuarioConfiguracionPrograma foreign key attribute.
     *
     * @param  ChildUsuarioConfiguracionPrograma $l ChildUsuarioConfiguracionPrograma
     * @return $this|\Programa The current object (for fluent API support)
     */
    public function addUsuarioConfiguracionPrograma(ChildUsuarioConfiguracionPrograma $l)
    {
        if ($this->collUsuarioConfiguracionProgramas === null) {
            $this->initUsuarioConfiguracionProgramas();
            $this->collUsuarioConfiguracionProgramasPartial = true;
        }

        if (!$this->collUsuarioConfiguracionProgramas->contains($l)) {
            $this->doAddUsuarioConfiguracionPrograma($l);

            if ($this->usuarioConfiguracionProgramasScheduledForDeletion and $this->usuarioConfiguracionProgramasScheduledForDeletion->contains($l)) {
                $this->usuarioConfiguracionProgramasScheduledForDeletion->remove($this->usuarioConfiguracionProgramasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioConfiguracionPrograma $usuarioConfiguracionPrograma The ChildUsuarioConfiguracionPrograma object to add.
     */
    protected function doAddUsuarioConfiguracionPrograma(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionPrograma)
    {
        $this->collUsuarioConfiguracionProgramas[]= $usuarioConfiguracionPrograma;
        $usuarioConfiguracionPrograma->setPrograma($this);
    }

    /**
     * @param  ChildUsuarioConfiguracionPrograma $usuarioConfiguracionPrograma The ChildUsuarioConfiguracionPrograma object to remove.
     * @return $this|ChildPrograma The current object (for fluent API support)
     */
    public function removeUsuarioConfiguracionPrograma(ChildUsuarioConfiguracionPrograma $usuarioConfiguracionPrograma)
    {
        if ($this->getUsuarioConfiguracionProgramas()->contains($usuarioConfiguracionPrograma)) {
            $pos = $this->collUsuarioConfiguracionProgramas->search($usuarioConfiguracionPrograma);
            $this->collUsuarioConfiguracionProgramas->remove($pos);
            if (null === $this->usuarioConfiguracionProgramasScheduledForDeletion) {
                $this->usuarioConfiguracionProgramasScheduledForDeletion = clone $this->collUsuarioConfiguracionProgramas;
                $this->usuarioConfiguracionProgramasScheduledForDeletion->clear();
            }
            $this->usuarioConfiguracionProgramasScheduledForDeletion[]= clone $usuarioConfiguracionPrograma;
            $usuarioConfiguracionPrograma->setPrograma(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related UsuarioConfiguracionProgramas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     */
    public function getUsuarioConfiguracionProgramasJoinUsuarioRelatedByIdUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
        $query->joinWith('UsuarioRelatedByIdUsuario', $joinBehavior);

        return $this->getUsuarioConfiguracionProgramas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Programa is new, it will return
     * an empty collection; or if this Programa has previously
     * been saved, it will retrieve related UsuarioConfiguracionProgramas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Programa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionPrograma[] List of ChildUsuarioConfiguracionPrograma objects
     */
    public function getUsuarioConfiguracionProgramasJoinUsuarioRelatedByIdUsuarioModificacion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionProgramaQuery::create(null, $criteria);
        $query->joinWith('UsuarioRelatedByIdUsuarioModificacion', $joinBehavior);

        return $this->getUsuarioConfiguracionProgramas($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEntidadOperativa) {
            $this->aEntidadOperativa->removePrograma($this);
        }
        if (null !== $this->aEstatusPrograma) {
            $this->aEstatusPrograma->removePrograma($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removePrograma($this);
        }
        if (null !== $this->aTipoBeneficiario) {
            $this->aTipoBeneficiario->removePrograma($this);
        }
        $this->clave = null;
        $this->nombre = null;
        $this->descripcion = null;
        $this->fecha_inicio = null;
        $this->fecha_fin = null;
        $this->id_entidad_operativa = null;
        $this->id_estatus_programa = null;
        $this->activo = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->id_usuario_modificacion = null;
        $this->id_tipo_bebeficiario = null;
        $this->persona_moral = null;
        $this->digrama_yucatan = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBeneficios) {
                foreach ($this->collBeneficios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEtapas) {
                foreach ($this->collEtapas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProgramaRequisitos) {
                foreach ($this->collProgramaRequisitos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioConfiguracionProgramas) {
                foreach ($this->collUsuarioConfiguracionProgramas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBeneficios = null;
        $this->collEtapas = null;
        $this->collProgramaRequisitos = null;
        $this->collUsuarioConfiguracionProgramas = null;
        $this->aEntidadOperativa = null;
        $this->aEstatusPrograma = null;
        $this->aUsuario = null;
        $this->aTipoBeneficiario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProgramaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
