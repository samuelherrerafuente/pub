<?php

namespace Base;

use \Etapa as ChildEtapa;
use \EtapaQuery as ChildEtapaQuery;
use \FlujoEtapa as ChildFlujoEtapa;
use \FlujoEtapaQuery as ChildFlujoEtapaQuery;
use \Programa as ChildPrograma;
use \ProgramaQuery as ChildProgramaQuery;
use \ProgramaRequisito as ChildProgramaRequisito;
use \ProgramaRequisitoQuery as ChildProgramaRequisitoQuery;
use \Usuario as ChildUsuario;
use \UsuarioConfiguracionEtapa as ChildUsuarioConfiguracionEtapa;
use \UsuarioConfiguracionEtapaQuery as ChildUsuarioConfiguracionEtapaQuery;
use \UsuarioQuery as ChildUsuarioQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\EtapaTableMap;
use Map\FlujoEtapaTableMap;
use Map\ProgramaRequisitoTableMap;
use Map\UsuarioConfiguracionEtapaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'etapa' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Etapa implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\EtapaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the clave field.
     *
     * @var        int
     */
    protected $clave;

    /**
     * The value for the id_programa field.
     *
     * @var        int
     */
    protected $id_programa;

    /**
     * The value for the nombre field.
     *
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the siglas field.
     *
     * @var        string
     */
    protected $siglas;

    /**
     * The value for the activo field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $activo;

    /**
     * The value for the fecha_creacion field.
     *
     * @var        DateTime
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     *
     * @var        DateTime
     */
    protected $fecha_modificacion;

    /**
     * The value for the id_usuario_modificacion field.
     *
     * @var        int
     */
    protected $id_usuario_modificacion;

    /**
     * @var        ChildPrograma
     */
    protected $aPrograma;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * @var        ObjectCollection|ChildFlujoEtapa[] Collection to store aggregation of ChildFlujoEtapa objects.
     */
    protected $collFlujoEtapasRelatedByIdEtapa;
    protected $collFlujoEtapasRelatedByIdEtapaPartial;

    /**
     * @var        ObjectCollection|ChildFlujoEtapa[] Collection to store aggregation of ChildFlujoEtapa objects.
     */
    protected $collFlujoEtapasRelatedByIdEtapaSiguiente;
    protected $collFlujoEtapasRelatedByIdEtapaSiguientePartial;

    /**
     * @var        ObjectCollection|ChildProgramaRequisito[] Collection to store aggregation of ChildProgramaRequisito objects.
     */
    protected $collProgramaRequisitos;
    protected $collProgramaRequisitosPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioConfiguracionEtapa[] Collection to store aggregation of ChildUsuarioConfiguracionEtapa objects.
     */
    protected $collUsuarioConfiguracionEtapas;
    protected $collUsuarioConfiguracionEtapasPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFlujoEtapa[]
     */
    protected $flujoEtapasRelatedByIdEtapaScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFlujoEtapa[]
     */
    protected $flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProgramaRequisito[]
     */
    protected $programaRequisitosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioConfiguracionEtapa[]
     */
    protected $usuarioConfiguracionEtapasScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->activo = 1;
    }

    /**
     * Initializes internal state of Base\Etapa object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Etapa</code> instance.  If
     * <code>obj</code> is an instance of <code>Etapa</code>, delegates to
     * <code>equals(Etapa)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param  string  $keyType                (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [clave] column value.
     *
     * @return int
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Get the [id_programa] column value.
     *
     * @return int
     */
    public function getIdPrograma()
    {
        return $this->id_programa;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the [siglas] column value.
     *
     * @return string
     */
    public function getSiglas()
    {
        return $this->siglas;
    }

    /**
     * Get the [activo] column value.
     *
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaCreacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_creacion;
        } else {
            return $this->fecha_creacion instanceof \DateTimeInterface ? $this->fecha_creacion->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaModificacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_modificacion;
        } else {
            return $this->fecha_modificacion instanceof \DateTimeInterface ? $this->fecha_modificacion->format($format) : null;
        }
    }

    /**
     * Get the [id_usuario_modificacion] column value.
     *
     * @return int
     */
    public function getIdUsuarioModificacion()
    {
        return $this->id_usuario_modificacion;
    }

    /**
     * Set the value of [clave] column.
     *
     * @param int $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setClave($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clave !== $v) {
            $this->clave = $v;
            $this->modifiedColumns[EtapaTableMap::COL_CLAVE] = true;
        }

        return $this;
    } // setClave()

    /**
     * Set the value of [id_programa] column.
     *
     * @param int $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setIdPrograma($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_programa !== $v) {
            $this->id_programa = $v;
            $this->modifiedColumns[EtapaTableMap::COL_ID_PROGRAMA] = true;
        }

        if ($this->aPrograma !== null && $this->aPrograma->getClave() !== $v) {
            $this->aPrograma = null;
        }

        return $this;
    } // setIdPrograma()

    /**
     * Set the value of [nombre] column.
     *
     * @param string $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[EtapaTableMap::COL_NOMBRE] = true;
        }

        return $this;
    } // setNombre()

    /**
     * Set the value of [siglas] column.
     *
     * @param string $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setSiglas($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->siglas !== $v) {
            $this->siglas = $v;
            $this->modifiedColumns[EtapaTableMap::COL_SIGLAS] = true;
        }

        return $this;
    } // setSiglas()

    /**
     * Set the value of [activo] column.
     *
     * @param int $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setActivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->activo !== $v) {
            $this->activo = $v;
            $this->modifiedColumns[EtapaTableMap::COL_ACTIVO] = true;
        }

        return $this;
    } // setActivo()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            if ($this->fecha_creacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_creacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_creacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[EtapaTableMap::COL_FECHA_CREACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            if ($this->fecha_modificacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_modificacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_modificacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[EtapaTableMap::COL_FECHA_MODIFICACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaModificacion()

    /**
     * Set the value of [id_usuario_modificacion] column.
     *
     * @param int $v New value
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function setIdUsuarioModificacion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_usuario_modificacion !== $v) {
            $this->id_usuario_modificacion = $v;
            $this->modifiedColumns[EtapaTableMap::COL_ID_USUARIO_MODIFICACION] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getClave() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setIdUsuarioModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->activo !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : EtapaTableMap::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clave = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : EtapaTableMap::translateFieldName('IdPrograma', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_programa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : EtapaTableMap::translateFieldName('Nombre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nombre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : EtapaTableMap::translateFieldName('Siglas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->siglas = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : EtapaTableMap::translateFieldName('Activo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : EtapaTableMap::translateFieldName('FechaCreacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_creacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : EtapaTableMap::translateFieldName('FechaModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_modificacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : EtapaTableMap::translateFieldName('IdUsuarioModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_usuario_modificacion = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = EtapaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Etapa'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aPrograma !== null && $this->id_programa !== $this->aPrograma->getClave()) {
            $this->aPrograma = null;
        }
        if ($this->aUsuario !== null && $this->id_usuario_modificacion !== $this->aUsuario->getClave()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EtapaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildEtapaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPrograma = null;
            $this->aUsuario = null;
            $this->collFlujoEtapasRelatedByIdEtapa = null;

            $this->collFlujoEtapasRelatedByIdEtapaSiguiente = null;

            $this->collProgramaRequisitos = null;

            $this->collUsuarioConfiguracionEtapas = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Etapa::setDeleted()
     * @see Etapa::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildEtapaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                EtapaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPrograma !== null) {
                if ($this->aPrograma->isModified() || $this->aPrograma->isNew()) {
                    $affectedRows += $this->aPrograma->save($con);
                }
                $this->setPrograma($this->aPrograma);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->flujoEtapasRelatedByIdEtapaScheduledForDeletion !== null) {
                if (!$this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->isEmpty()) {
                    \FlujoEtapaQuery::create()
                        ->filterByPrimaryKeys($this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion = null;
                }
            }

            if ($this->collFlujoEtapasRelatedByIdEtapa !== null) {
                foreach ($this->collFlujoEtapasRelatedByIdEtapa as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion !== null) {
                if (!$this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->isEmpty()) {
                    \FlujoEtapaQuery::create()
                        ->filterByPrimaryKeys($this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion = null;
                }
            }

            if ($this->collFlujoEtapasRelatedByIdEtapaSiguiente !== null) {
                foreach ($this->collFlujoEtapasRelatedByIdEtapaSiguiente as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->programaRequisitosScheduledForDeletion !== null) {
                if (!$this->programaRequisitosScheduledForDeletion->isEmpty()) {
                    \ProgramaRequisitoQuery::create()
                        ->filterByPrimaryKeys($this->programaRequisitosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->programaRequisitosScheduledForDeletion = null;
                }
            }

            if ($this->collProgramaRequisitos !== null) {
                foreach ($this->collProgramaRequisitos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioConfiguracionEtapasScheduledForDeletion !== null) {
                if (!$this->usuarioConfiguracionEtapasScheduledForDeletion->isEmpty()) {
                    \UsuarioConfiguracionEtapaQuery::create()
                        ->filterByPrimaryKeys($this->usuarioConfiguracionEtapasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioConfiguracionEtapasScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioConfiguracionEtapas !== null) {
                foreach ($this->collUsuarioConfiguracionEtapas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[EtapaTableMap::COL_CLAVE] = true;
        if (null !== $this->clave) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . EtapaTableMap::COL_CLAVE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(EtapaTableMap::COL_CLAVE)) {
            $modifiedColumns[':p' . $index++]  = 'clave';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ID_PROGRAMA)) {
            $modifiedColumns[':p' . $index++]  = 'id_programa';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = 'nombre';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_SIGLAS)) {
            $modifiedColumns[':p' . $index++]  = 'siglas';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ACTIVO)) {
            $modifiedColumns[':p' . $index++]  = 'activo';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_creacion';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_modificacion';
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'id_usuario_modificacion';
        }

        $sql = sprintf(
            'INSERT INTO etapa (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'clave':
                        $stmt->bindValue($identifier, $this->clave, PDO::PARAM_INT);
                        break;
                    case 'id_programa':
                        $stmt->bindValue($identifier, $this->id_programa, PDO::PARAM_INT);
                        break;
                    case 'nombre':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case 'siglas':
                        $stmt->bindValue($identifier, $this->siglas, PDO::PARAM_STR);
                        break;
                    case 'activo':
                        $stmt->bindValue($identifier, $this->activo, PDO::PARAM_INT);
                        break;
                    case 'fecha_creacion':
                        $stmt->bindValue($identifier, $this->fecha_creacion ? $this->fecha_creacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'fecha_modificacion':
                        $stmt->bindValue($identifier, $this->fecha_modificacion ? $this->fecha_modificacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'id_usuario_modificacion':
                        $stmt->bindValue($identifier, $this->id_usuario_modificacion, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setClave($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = EtapaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getClave();
                break;
            case 1:
                return $this->getIdPrograma();
                break;
            case 2:
                return $this->getNombre();
                break;
            case 3:
                return $this->getSiglas();
                break;
            case 4:
                return $this->getActivo();
                break;
            case 5:
                return $this->getFechaCreacion();
                break;
            case 6:
                return $this->getFechaModificacion();
                break;
            case 7:
                return $this->getIdUsuarioModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Etapa'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Etapa'][$this->hashCode()] = true;
        $keys = EtapaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getClave(),
            $keys[1] => $this->getIdPrograma(),
            $keys[2] => $this->getNombre(),
            $keys[3] => $this->getSiglas(),
            $keys[4] => $this->getActivo(),
            $keys[5] => $this->getFechaCreacion(),
            $keys[6] => $this->getFechaModificacion(),
            $keys[7] => $this->getIdUsuarioModificacion(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPrograma) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'programa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'programa';
                        break;
                    default:
                        $key = 'Programa';
                }

                $result[$key] = $this->aPrograma->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collFlujoEtapasRelatedByIdEtapa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'flujoEtapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'flujo_etapas';
                        break;
                    default:
                        $key = 'FlujoEtapas';
                }

                $result[$key] = $this->collFlujoEtapasRelatedByIdEtapa->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFlujoEtapasRelatedByIdEtapaSiguiente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'flujoEtapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'flujo_etapas';
                        break;
                    default:
                        $key = 'FlujoEtapas';
                }

                $result[$key] = $this->collFlujoEtapasRelatedByIdEtapaSiguiente->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProgramaRequisitos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'programaRequisitos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'programa_requisitos';
                        break;
                    default:
                        $key = 'ProgramaRequisitos';
                }

                $result[$key] = $this->collProgramaRequisitos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioConfiguracionEtapas) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioConfiguracionEtapas';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_configuracion_etapas';
                        break;
                    default:
                        $key = 'UsuarioConfiguracionEtapas';
                }

                $result[$key] = $this->collUsuarioConfiguracionEtapas->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Etapa
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = EtapaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Etapa
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setClave($value);
                break;
            case 1:
                $this->setIdPrograma($value);
                break;
            case 2:
                $this->setNombre($value);
                break;
            case 3:
                $this->setSiglas($value);
                break;
            case 4:
                $this->setActivo($value);
                break;
            case 5:
                $this->setFechaCreacion($value);
                break;
            case 6:
                $this->setFechaModificacion($value);
                break;
            case 7:
                $this->setIdUsuarioModificacion($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     $this|\Etapa
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = EtapaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setClave($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdPrograma($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNombre($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setSiglas($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setActivo($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setFechaCreacion($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setFechaModificacion($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIdUsuarioModificacion($arr[$keys[7]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Etapa The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(EtapaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(EtapaTableMap::COL_CLAVE)) {
            $criteria->add(EtapaTableMap::COL_CLAVE, $this->clave);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ID_PROGRAMA)) {
            $criteria->add(EtapaTableMap::COL_ID_PROGRAMA, $this->id_programa);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_NOMBRE)) {
            $criteria->add(EtapaTableMap::COL_NOMBRE, $this->nombre);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_SIGLAS)) {
            $criteria->add(EtapaTableMap::COL_SIGLAS, $this->siglas);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ACTIVO)) {
            $criteria->add(EtapaTableMap::COL_ACTIVO, $this->activo);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_FECHA_CREACION)) {
            $criteria->add(EtapaTableMap::COL_FECHA_CREACION, $this->fecha_creacion);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_FECHA_MODIFICACION)) {
            $criteria->add(EtapaTableMap::COL_FECHA_MODIFICACION, $this->fecha_modificacion);
        }
        if ($this->isColumnModified(EtapaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $criteria->add(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $this->id_usuario_modificacion);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildEtapaQuery::create();
        $criteria->add(EtapaTableMap::COL_CLAVE, $this->clave);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getClave();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getClave();
    }

    /**
     * Generic method to set the primary key (clave column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setClave($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getClave();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Etapa (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdPrograma($this->getIdPrograma());
        $copyObj->setNombre($this->getNombre());
        $copyObj->setSiglas($this->getSiglas());
        $copyObj->setActivo($this->getActivo());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setIdUsuarioModificacion($this->getIdUsuarioModificacion());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getFlujoEtapasRelatedByIdEtapa() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlujoEtapaRelatedByIdEtapa($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFlujoEtapasRelatedByIdEtapaSiguiente() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFlujoEtapaRelatedByIdEtapaSiguiente($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProgramaRequisitos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProgramaRequisito($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioConfiguracionEtapas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioConfiguracionEtapa($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setClave(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Etapa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildPrograma object.
     *
     * @param  ChildPrograma $v
     * @return $this|\Etapa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPrograma(ChildPrograma $v = null)
    {
        if ($v === null) {
            $this->setIdPrograma(NULL);
        } else {
            $this->setIdPrograma($v->getClave());
        }

        $this->aPrograma = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildPrograma object, it will not be re-added.
        if ($v !== null) {
            $v->addEtapa($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildPrograma object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildPrograma The associated ChildPrograma object.
     * @throws PropelException
     */
    public function getPrograma(ConnectionInterface $con = null)
    {
        if ($this->aPrograma === null && ($this->id_programa != 0)) {
            $this->aPrograma = ChildProgramaQuery::create()->findPk($this->id_programa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPrograma->addEtapas($this);
             */
        }

        return $this->aPrograma;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\Etapa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setIdUsuarioModificacion(NULL);
        } else {
            $this->setIdUsuarioModificacion($v->getClave());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addEtapa($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->id_usuario_modificacion != 0)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->id_usuario_modificacion, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addEtapas($this);
             */
        }

        return $this->aUsuario;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('FlujoEtapaRelatedByIdEtapa' === $relationName) {
            $this->initFlujoEtapasRelatedByIdEtapa();
            return;
        }
        if ('FlujoEtapaRelatedByIdEtapaSiguiente' === $relationName) {
            $this->initFlujoEtapasRelatedByIdEtapaSiguiente();
            return;
        }
        if ('ProgramaRequisito' === $relationName) {
            $this->initProgramaRequisitos();
            return;
        }
        if ('UsuarioConfiguracionEtapa' === $relationName) {
            $this->initUsuarioConfiguracionEtapas();
            return;
        }
    }

    /**
     * Clears out the collFlujoEtapasRelatedByIdEtapa collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFlujoEtapasRelatedByIdEtapa()
     */
    public function clearFlujoEtapasRelatedByIdEtapa()
    {
        $this->collFlujoEtapasRelatedByIdEtapa = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFlujoEtapasRelatedByIdEtapa collection loaded partially.
     */
    public function resetPartialFlujoEtapasRelatedByIdEtapa($v = true)
    {
        $this->collFlujoEtapasRelatedByIdEtapaPartial = $v;
    }

    /**
     * Initializes the collFlujoEtapasRelatedByIdEtapa collection.
     *
     * By default this just sets the collFlujoEtapasRelatedByIdEtapa collection to an empty array (like clearcollFlujoEtapasRelatedByIdEtapa());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlujoEtapasRelatedByIdEtapa($overrideExisting = true)
    {
        if (null !== $this->collFlujoEtapasRelatedByIdEtapa && !$overrideExisting) {
            return;
        }

        $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collFlujoEtapasRelatedByIdEtapa = new $collectionClassName;
        $this->collFlujoEtapasRelatedByIdEtapa->setModel('\FlujoEtapa');
    }

    /**
     * Gets an array of ChildFlujoEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEtapa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     * @throws PropelException
     */
    public function getFlujoEtapasRelatedByIdEtapa(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasRelatedByIdEtapaPartial && !$this->isNew();
        if (null === $this->collFlujoEtapasRelatedByIdEtapa || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFlujoEtapasRelatedByIdEtapa) {
                    $this->initFlujoEtapasRelatedByIdEtapa();
                } else {
                    $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

                    $collFlujoEtapasRelatedByIdEtapa = new $collectionClassName;
                    $collFlujoEtapasRelatedByIdEtapa->setModel('\FlujoEtapa');

                    return $collFlujoEtapasRelatedByIdEtapa;
                }
            } else {
                $collFlujoEtapasRelatedByIdEtapa = ChildFlujoEtapaQuery::create(null, $criteria)
                    ->filterByEtapaRelatedByIdEtapa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFlujoEtapasRelatedByIdEtapaPartial && count($collFlujoEtapasRelatedByIdEtapa)) {
                        $this->initFlujoEtapasRelatedByIdEtapa(false);

                        foreach ($collFlujoEtapasRelatedByIdEtapa as $obj) {
                            if (false == $this->collFlujoEtapasRelatedByIdEtapa->contains($obj)) {
                                $this->collFlujoEtapasRelatedByIdEtapa->append($obj);
                            }
                        }

                        $this->collFlujoEtapasRelatedByIdEtapaPartial = true;
                    }

                    return $collFlujoEtapasRelatedByIdEtapa;
                }

                if ($partial && $this->collFlujoEtapasRelatedByIdEtapa) {
                    foreach ($this->collFlujoEtapasRelatedByIdEtapa as $obj) {
                        if ($obj->isNew()) {
                            $collFlujoEtapasRelatedByIdEtapa[] = $obj;
                        }
                    }
                }

                $this->collFlujoEtapasRelatedByIdEtapa = $collFlujoEtapasRelatedByIdEtapa;
                $this->collFlujoEtapasRelatedByIdEtapaPartial = false;
            }
        }

        return $this->collFlujoEtapasRelatedByIdEtapa;
    }

    /**
     * Sets a collection of ChildFlujoEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $flujoEtapasRelatedByIdEtapa A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function setFlujoEtapasRelatedByIdEtapa(Collection $flujoEtapasRelatedByIdEtapa, ConnectionInterface $con = null)
    {
        /** @var ChildFlujoEtapa[] $flujoEtapasRelatedByIdEtapaToDelete */
        $flujoEtapasRelatedByIdEtapaToDelete = $this->getFlujoEtapasRelatedByIdEtapa(new Criteria(), $con)->diff($flujoEtapasRelatedByIdEtapa);


        $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion = $flujoEtapasRelatedByIdEtapaToDelete;

        foreach ($flujoEtapasRelatedByIdEtapaToDelete as $flujoEtapaRelatedByIdEtapaRemoved) {
            $flujoEtapaRelatedByIdEtapaRemoved->setEtapaRelatedByIdEtapa(null);
        }

        $this->collFlujoEtapasRelatedByIdEtapa = null;
        foreach ($flujoEtapasRelatedByIdEtapa as $flujoEtapaRelatedByIdEtapa) {
            $this->addFlujoEtapaRelatedByIdEtapa($flujoEtapaRelatedByIdEtapa);
        }

        $this->collFlujoEtapasRelatedByIdEtapa = $flujoEtapasRelatedByIdEtapa;
        $this->collFlujoEtapasRelatedByIdEtapaPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlujoEtapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FlujoEtapa objects.
     * @throws PropelException
     */
    public function countFlujoEtapasRelatedByIdEtapa(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasRelatedByIdEtapaPartial && !$this->isNew();
        if (null === $this->collFlujoEtapasRelatedByIdEtapa || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlujoEtapasRelatedByIdEtapa) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlujoEtapasRelatedByIdEtapa());
            }

            $query = ChildFlujoEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEtapaRelatedByIdEtapa($this)
                ->count($con);
        }

        return count($this->collFlujoEtapasRelatedByIdEtapa);
    }

    /**
     * Method called to associate a ChildFlujoEtapa object to this object
     * through the ChildFlujoEtapa foreign key attribute.
     *
     * @param  ChildFlujoEtapa $l ChildFlujoEtapa
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function addFlujoEtapaRelatedByIdEtapa(ChildFlujoEtapa $l)
    {
        if ($this->collFlujoEtapasRelatedByIdEtapa === null) {
            $this->initFlujoEtapasRelatedByIdEtapa();
            $this->collFlujoEtapasRelatedByIdEtapaPartial = true;
        }

        if (!$this->collFlujoEtapasRelatedByIdEtapa->contains($l)) {
            $this->doAddFlujoEtapaRelatedByIdEtapa($l);

            if ($this->flujoEtapasRelatedByIdEtapaScheduledForDeletion and $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->contains($l)) {
                $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->remove($this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFlujoEtapa $flujoEtapaRelatedByIdEtapa The ChildFlujoEtapa object to add.
     */
    protected function doAddFlujoEtapaRelatedByIdEtapa(ChildFlujoEtapa $flujoEtapaRelatedByIdEtapa)
    {
        $this->collFlujoEtapasRelatedByIdEtapa[]= $flujoEtapaRelatedByIdEtapa;
        $flujoEtapaRelatedByIdEtapa->setEtapaRelatedByIdEtapa($this);
    }

    /**
     * @param  ChildFlujoEtapa $flujoEtapaRelatedByIdEtapa The ChildFlujoEtapa object to remove.
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function removeFlujoEtapaRelatedByIdEtapa(ChildFlujoEtapa $flujoEtapaRelatedByIdEtapa)
    {
        if ($this->getFlujoEtapasRelatedByIdEtapa()->contains($flujoEtapaRelatedByIdEtapa)) {
            $pos = $this->collFlujoEtapasRelatedByIdEtapa->search($flujoEtapaRelatedByIdEtapa);
            $this->collFlujoEtapasRelatedByIdEtapa->remove($pos);
            if (null === $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion) {
                $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion = clone $this->collFlujoEtapasRelatedByIdEtapa;
                $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion->clear();
            }
            $this->flujoEtapasRelatedByIdEtapaScheduledForDeletion[]= clone $flujoEtapaRelatedByIdEtapa;
            $flujoEtapaRelatedByIdEtapa->setEtapaRelatedByIdEtapa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related FlujoEtapasRelatedByIdEtapa from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     */
    public function getFlujoEtapasRelatedByIdEtapaJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFlujoEtapaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getFlujoEtapasRelatedByIdEtapa($query, $con);
    }

    /**
     * Clears out the collFlujoEtapasRelatedByIdEtapaSiguiente collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFlujoEtapasRelatedByIdEtapaSiguiente()
     */
    public function clearFlujoEtapasRelatedByIdEtapaSiguiente()
    {
        $this->collFlujoEtapasRelatedByIdEtapaSiguiente = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFlujoEtapasRelatedByIdEtapaSiguiente collection loaded partially.
     */
    public function resetPartialFlujoEtapasRelatedByIdEtapaSiguiente($v = true)
    {
        $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial = $v;
    }

    /**
     * Initializes the collFlujoEtapasRelatedByIdEtapaSiguiente collection.
     *
     * By default this just sets the collFlujoEtapasRelatedByIdEtapaSiguiente collection to an empty array (like clearcollFlujoEtapasRelatedByIdEtapaSiguiente());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFlujoEtapasRelatedByIdEtapaSiguiente($overrideExisting = true)
    {
        if (null !== $this->collFlujoEtapasRelatedByIdEtapaSiguiente && !$overrideExisting) {
            return;
        }

        $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collFlujoEtapasRelatedByIdEtapaSiguiente = new $collectionClassName;
        $this->collFlujoEtapasRelatedByIdEtapaSiguiente->setModel('\FlujoEtapa');
    }

    /**
     * Gets an array of ChildFlujoEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEtapa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     * @throws PropelException
     */
    public function getFlujoEtapasRelatedByIdEtapaSiguiente(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial && !$this->isNew();
        if (null === $this->collFlujoEtapasRelatedByIdEtapaSiguiente || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFlujoEtapasRelatedByIdEtapaSiguiente) {
                    $this->initFlujoEtapasRelatedByIdEtapaSiguiente();
                } else {
                    $collectionClassName = FlujoEtapaTableMap::getTableMap()->getCollectionClassName();

                    $collFlujoEtapasRelatedByIdEtapaSiguiente = new $collectionClassName;
                    $collFlujoEtapasRelatedByIdEtapaSiguiente->setModel('\FlujoEtapa');

                    return $collFlujoEtapasRelatedByIdEtapaSiguiente;
                }
            } else {
                $collFlujoEtapasRelatedByIdEtapaSiguiente = ChildFlujoEtapaQuery::create(null, $criteria)
                    ->filterByEtapaRelatedByIdEtapaSiguiente($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial && count($collFlujoEtapasRelatedByIdEtapaSiguiente)) {
                        $this->initFlujoEtapasRelatedByIdEtapaSiguiente(false);

                        foreach ($collFlujoEtapasRelatedByIdEtapaSiguiente as $obj) {
                            if (false == $this->collFlujoEtapasRelatedByIdEtapaSiguiente->contains($obj)) {
                                $this->collFlujoEtapasRelatedByIdEtapaSiguiente->append($obj);
                            }
                        }

                        $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial = true;
                    }

                    return $collFlujoEtapasRelatedByIdEtapaSiguiente;
                }

                if ($partial && $this->collFlujoEtapasRelatedByIdEtapaSiguiente) {
                    foreach ($this->collFlujoEtapasRelatedByIdEtapaSiguiente as $obj) {
                        if ($obj->isNew()) {
                            $collFlujoEtapasRelatedByIdEtapaSiguiente[] = $obj;
                        }
                    }
                }

                $this->collFlujoEtapasRelatedByIdEtapaSiguiente = $collFlujoEtapasRelatedByIdEtapaSiguiente;
                $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial = false;
            }
        }

        return $this->collFlujoEtapasRelatedByIdEtapaSiguiente;
    }

    /**
     * Sets a collection of ChildFlujoEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $flujoEtapasRelatedByIdEtapaSiguiente A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function setFlujoEtapasRelatedByIdEtapaSiguiente(Collection $flujoEtapasRelatedByIdEtapaSiguiente, ConnectionInterface $con = null)
    {
        /** @var ChildFlujoEtapa[] $flujoEtapasRelatedByIdEtapaSiguienteToDelete */
        $flujoEtapasRelatedByIdEtapaSiguienteToDelete = $this->getFlujoEtapasRelatedByIdEtapaSiguiente(new Criteria(), $con)->diff($flujoEtapasRelatedByIdEtapaSiguiente);


        $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion = $flujoEtapasRelatedByIdEtapaSiguienteToDelete;

        foreach ($flujoEtapasRelatedByIdEtapaSiguienteToDelete as $flujoEtapaRelatedByIdEtapaSiguienteRemoved) {
            $flujoEtapaRelatedByIdEtapaSiguienteRemoved->setEtapaRelatedByIdEtapaSiguiente(null);
        }

        $this->collFlujoEtapasRelatedByIdEtapaSiguiente = null;
        foreach ($flujoEtapasRelatedByIdEtapaSiguiente as $flujoEtapaRelatedByIdEtapaSiguiente) {
            $this->addFlujoEtapaRelatedByIdEtapaSiguiente($flujoEtapaRelatedByIdEtapaSiguiente);
        }

        $this->collFlujoEtapasRelatedByIdEtapaSiguiente = $flujoEtapasRelatedByIdEtapaSiguiente;
        $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial = false;

        return $this;
    }

    /**
     * Returns the number of related FlujoEtapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FlujoEtapa objects.
     * @throws PropelException
     */
    public function countFlujoEtapasRelatedByIdEtapaSiguiente(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial && !$this->isNew();
        if (null === $this->collFlujoEtapasRelatedByIdEtapaSiguiente || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFlujoEtapasRelatedByIdEtapaSiguiente) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFlujoEtapasRelatedByIdEtapaSiguiente());
            }

            $query = ChildFlujoEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEtapaRelatedByIdEtapaSiguiente($this)
                ->count($con);
        }

        return count($this->collFlujoEtapasRelatedByIdEtapaSiguiente);
    }

    /**
     * Method called to associate a ChildFlujoEtapa object to this object
     * through the ChildFlujoEtapa foreign key attribute.
     *
     * @param  ChildFlujoEtapa $l ChildFlujoEtapa
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function addFlujoEtapaRelatedByIdEtapaSiguiente(ChildFlujoEtapa $l)
    {
        if ($this->collFlujoEtapasRelatedByIdEtapaSiguiente === null) {
            $this->initFlujoEtapasRelatedByIdEtapaSiguiente();
            $this->collFlujoEtapasRelatedByIdEtapaSiguientePartial = true;
        }

        if (!$this->collFlujoEtapasRelatedByIdEtapaSiguiente->contains($l)) {
            $this->doAddFlujoEtapaRelatedByIdEtapaSiguiente($l);

            if ($this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion and $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->contains($l)) {
                $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->remove($this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFlujoEtapa $flujoEtapaRelatedByIdEtapaSiguiente The ChildFlujoEtapa object to add.
     */
    protected function doAddFlujoEtapaRelatedByIdEtapaSiguiente(ChildFlujoEtapa $flujoEtapaRelatedByIdEtapaSiguiente)
    {
        $this->collFlujoEtapasRelatedByIdEtapaSiguiente[]= $flujoEtapaRelatedByIdEtapaSiguiente;
        $flujoEtapaRelatedByIdEtapaSiguiente->setEtapaRelatedByIdEtapaSiguiente($this);
    }

    /**
     * @param  ChildFlujoEtapa $flujoEtapaRelatedByIdEtapaSiguiente The ChildFlujoEtapa object to remove.
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function removeFlujoEtapaRelatedByIdEtapaSiguiente(ChildFlujoEtapa $flujoEtapaRelatedByIdEtapaSiguiente)
    {
        if ($this->getFlujoEtapasRelatedByIdEtapaSiguiente()->contains($flujoEtapaRelatedByIdEtapaSiguiente)) {
            $pos = $this->collFlujoEtapasRelatedByIdEtapaSiguiente->search($flujoEtapaRelatedByIdEtapaSiguiente);
            $this->collFlujoEtapasRelatedByIdEtapaSiguiente->remove($pos);
            if (null === $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion) {
                $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion = clone $this->collFlujoEtapasRelatedByIdEtapaSiguiente;
                $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion->clear();
            }
            $this->flujoEtapasRelatedByIdEtapaSiguienteScheduledForDeletion[]= clone $flujoEtapaRelatedByIdEtapaSiguiente;
            $flujoEtapaRelatedByIdEtapaSiguiente->setEtapaRelatedByIdEtapaSiguiente(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related FlujoEtapasRelatedByIdEtapaSiguiente from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFlujoEtapa[] List of ChildFlujoEtapa objects
     */
    public function getFlujoEtapasRelatedByIdEtapaSiguienteJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFlujoEtapaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getFlujoEtapasRelatedByIdEtapaSiguiente($query, $con);
    }

    /**
     * Clears out the collProgramaRequisitos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProgramaRequisitos()
     */
    public function clearProgramaRequisitos()
    {
        $this->collProgramaRequisitos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProgramaRequisitos collection loaded partially.
     */
    public function resetPartialProgramaRequisitos($v = true)
    {
        $this->collProgramaRequisitosPartial = $v;
    }

    /**
     * Initializes the collProgramaRequisitos collection.
     *
     * By default this just sets the collProgramaRequisitos collection to an empty array (like clearcollProgramaRequisitos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProgramaRequisitos($overrideExisting = true)
    {
        if (null !== $this->collProgramaRequisitos && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

        $this->collProgramaRequisitos = new $collectionClassName;
        $this->collProgramaRequisitos->setModel('\ProgramaRequisito');
    }

    /**
     * Gets an array of ChildProgramaRequisito objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEtapa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     * @throws PropelException
     */
    public function getProgramaRequisitos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProgramaRequisitos) {
                    $this->initProgramaRequisitos();
                } else {
                    $collectionClassName = ProgramaRequisitoTableMap::getTableMap()->getCollectionClassName();

                    $collProgramaRequisitos = new $collectionClassName;
                    $collProgramaRequisitos->setModel('\ProgramaRequisito');

                    return $collProgramaRequisitos;
                }
            } else {
                $collProgramaRequisitos = ChildProgramaRequisitoQuery::create(null, $criteria)
                    ->filterByEtapa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProgramaRequisitosPartial && count($collProgramaRequisitos)) {
                        $this->initProgramaRequisitos(false);

                        foreach ($collProgramaRequisitos as $obj) {
                            if (false == $this->collProgramaRequisitos->contains($obj)) {
                                $this->collProgramaRequisitos->append($obj);
                            }
                        }

                        $this->collProgramaRequisitosPartial = true;
                    }

                    return $collProgramaRequisitos;
                }

                if ($partial && $this->collProgramaRequisitos) {
                    foreach ($this->collProgramaRequisitos as $obj) {
                        if ($obj->isNew()) {
                            $collProgramaRequisitos[] = $obj;
                        }
                    }
                }

                $this->collProgramaRequisitos = $collProgramaRequisitos;
                $this->collProgramaRequisitosPartial = false;
            }
        }

        return $this->collProgramaRequisitos;
    }

    /**
     * Sets a collection of ChildProgramaRequisito objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $programaRequisitos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function setProgramaRequisitos(Collection $programaRequisitos, ConnectionInterface $con = null)
    {
        /** @var ChildProgramaRequisito[] $programaRequisitosToDelete */
        $programaRequisitosToDelete = $this->getProgramaRequisitos(new Criteria(), $con)->diff($programaRequisitos);


        $this->programaRequisitosScheduledForDeletion = $programaRequisitosToDelete;

        foreach ($programaRequisitosToDelete as $programaRequisitoRemoved) {
            $programaRequisitoRemoved->setEtapa(null);
        }

        $this->collProgramaRequisitos = null;
        foreach ($programaRequisitos as $programaRequisito) {
            $this->addProgramaRequisito($programaRequisito);
        }

        $this->collProgramaRequisitos = $programaRequisitos;
        $this->collProgramaRequisitosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProgramaRequisito objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProgramaRequisito objects.
     * @throws PropelException
     */
    public function countProgramaRequisitos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProgramaRequisitosPartial && !$this->isNew();
        if (null === $this->collProgramaRequisitos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProgramaRequisitos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProgramaRequisitos());
            }

            $query = ChildProgramaRequisitoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEtapa($this)
                ->count($con);
        }

        return count($this->collProgramaRequisitos);
    }

    /**
     * Method called to associate a ChildProgramaRequisito object to this object
     * through the ChildProgramaRequisito foreign key attribute.
     *
     * @param  ChildProgramaRequisito $l ChildProgramaRequisito
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function addProgramaRequisito(ChildProgramaRequisito $l)
    {
        if ($this->collProgramaRequisitos === null) {
            $this->initProgramaRequisitos();
            $this->collProgramaRequisitosPartial = true;
        }

        if (!$this->collProgramaRequisitos->contains($l)) {
            $this->doAddProgramaRequisito($l);

            if ($this->programaRequisitosScheduledForDeletion and $this->programaRequisitosScheduledForDeletion->contains($l)) {
                $this->programaRequisitosScheduledForDeletion->remove($this->programaRequisitosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to add.
     */
    protected function doAddProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        $this->collProgramaRequisitos[]= $programaRequisito;
        $programaRequisito->setEtapa($this);
    }

    /**
     * @param  ChildProgramaRequisito $programaRequisito The ChildProgramaRequisito object to remove.
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function removeProgramaRequisito(ChildProgramaRequisito $programaRequisito)
    {
        if ($this->getProgramaRequisitos()->contains($programaRequisito)) {
            $pos = $this->collProgramaRequisitos->search($programaRequisito);
            $this->collProgramaRequisitos->remove($pos);
            if (null === $this->programaRequisitosScheduledForDeletion) {
                $this->programaRequisitosScheduledForDeletion = clone $this->collProgramaRequisitos;
                $this->programaRequisitosScheduledForDeletion->clear();
            }
            $this->programaRequisitosScheduledForDeletion[]= clone $programaRequisito;
            $programaRequisito->setEtapa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Programa', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinTipoRequisito(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('TipoRequisito', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related ProgramaRequisitos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProgramaRequisito[] List of ChildProgramaRequisito objects
     */
    public function getProgramaRequisitosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProgramaRequisitoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getProgramaRequisitos($query, $con);
    }

    /**
     * Clears out the collUsuarioConfiguracionEtapas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioConfiguracionEtapas()
     */
    public function clearUsuarioConfiguracionEtapas()
    {
        $this->collUsuarioConfiguracionEtapas = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioConfiguracionEtapas collection loaded partially.
     */
    public function resetPartialUsuarioConfiguracionEtapas($v = true)
    {
        $this->collUsuarioConfiguracionEtapasPartial = $v;
    }

    /**
     * Initializes the collUsuarioConfiguracionEtapas collection.
     *
     * By default this just sets the collUsuarioConfiguracionEtapas collection to an empty array (like clearcollUsuarioConfiguracionEtapas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioConfiguracionEtapas($overrideExisting = true)
    {
        if (null !== $this->collUsuarioConfiguracionEtapas && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioConfiguracionEtapas = new $collectionClassName;
        $this->collUsuarioConfiguracionEtapas->setModel('\UsuarioConfiguracionEtapa');
    }

    /**
     * Gets an array of ChildUsuarioConfiguracionEtapa objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEtapa is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     * @throws PropelException
     */
    public function getUsuarioConfiguracionEtapas(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionEtapasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionEtapas || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioConfiguracionEtapas) {
                    $this->initUsuarioConfiguracionEtapas();
                } else {
                    $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioConfiguracionEtapas = new $collectionClassName;
                    $collUsuarioConfiguracionEtapas->setModel('\UsuarioConfiguracionEtapa');

                    return $collUsuarioConfiguracionEtapas;
                }
            } else {
                $collUsuarioConfiguracionEtapas = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria)
                    ->filterByEtapa($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioConfiguracionEtapasPartial && count($collUsuarioConfiguracionEtapas)) {
                        $this->initUsuarioConfiguracionEtapas(false);

                        foreach ($collUsuarioConfiguracionEtapas as $obj) {
                            if (false == $this->collUsuarioConfiguracionEtapas->contains($obj)) {
                                $this->collUsuarioConfiguracionEtapas->append($obj);
                            }
                        }

                        $this->collUsuarioConfiguracionEtapasPartial = true;
                    }

                    return $collUsuarioConfiguracionEtapas;
                }

                if ($partial && $this->collUsuarioConfiguracionEtapas) {
                    foreach ($this->collUsuarioConfiguracionEtapas as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioConfiguracionEtapas[] = $obj;
                        }
                    }
                }

                $this->collUsuarioConfiguracionEtapas = $collUsuarioConfiguracionEtapas;
                $this->collUsuarioConfiguracionEtapasPartial = false;
            }
        }

        return $this->collUsuarioConfiguracionEtapas;
    }

    /**
     * Sets a collection of ChildUsuarioConfiguracionEtapa objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioConfiguracionEtapas A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function setUsuarioConfiguracionEtapas(Collection $usuarioConfiguracionEtapas, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioConfiguracionEtapa[] $usuarioConfiguracionEtapasToDelete */
        $usuarioConfiguracionEtapasToDelete = $this->getUsuarioConfiguracionEtapas(new Criteria(), $con)->diff($usuarioConfiguracionEtapas);


        $this->usuarioConfiguracionEtapasScheduledForDeletion = $usuarioConfiguracionEtapasToDelete;

        foreach ($usuarioConfiguracionEtapasToDelete as $usuarioConfiguracionEtapaRemoved) {
            $usuarioConfiguracionEtapaRemoved->setEtapa(null);
        }

        $this->collUsuarioConfiguracionEtapas = null;
        foreach ($usuarioConfiguracionEtapas as $usuarioConfiguracionEtapa) {
            $this->addUsuarioConfiguracionEtapa($usuarioConfiguracionEtapa);
        }

        $this->collUsuarioConfiguracionEtapas = $usuarioConfiguracionEtapas;
        $this->collUsuarioConfiguracionEtapasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioConfiguracionEtapa objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioConfiguracionEtapa objects.
     * @throws PropelException
     */
    public function countUsuarioConfiguracionEtapas(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioConfiguracionEtapasPartial && !$this->isNew();
        if (null === $this->collUsuarioConfiguracionEtapas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioConfiguracionEtapas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioConfiguracionEtapas());
            }

            $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEtapa($this)
                ->count($con);
        }

        return count($this->collUsuarioConfiguracionEtapas);
    }

    /**
     * Method called to associate a ChildUsuarioConfiguracionEtapa object to this object
     * through the ChildUsuarioConfiguracionEtapa foreign key attribute.
     *
     * @param  ChildUsuarioConfiguracionEtapa $l ChildUsuarioConfiguracionEtapa
     * @return $this|\Etapa The current object (for fluent API support)
     */
    public function addUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $l)
    {
        if ($this->collUsuarioConfiguracionEtapas === null) {
            $this->initUsuarioConfiguracionEtapas();
            $this->collUsuarioConfiguracionEtapasPartial = true;
        }

        if (!$this->collUsuarioConfiguracionEtapas->contains($l)) {
            $this->doAddUsuarioConfiguracionEtapa($l);

            if ($this->usuarioConfiguracionEtapasScheduledForDeletion and $this->usuarioConfiguracionEtapasScheduledForDeletion->contains($l)) {
                $this->usuarioConfiguracionEtapasScheduledForDeletion->remove($this->usuarioConfiguracionEtapasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa The ChildUsuarioConfiguracionEtapa object to add.
     */
    protected function doAddUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa)
    {
        $this->collUsuarioConfiguracionEtapas[]= $usuarioConfiguracionEtapa;
        $usuarioConfiguracionEtapa->setEtapa($this);
    }

    /**
     * @param  ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa The ChildUsuarioConfiguracionEtapa object to remove.
     * @return $this|ChildEtapa The current object (for fluent API support)
     */
    public function removeUsuarioConfiguracionEtapa(ChildUsuarioConfiguracionEtapa $usuarioConfiguracionEtapa)
    {
        if ($this->getUsuarioConfiguracionEtapas()->contains($usuarioConfiguracionEtapa)) {
            $pos = $this->collUsuarioConfiguracionEtapas->search($usuarioConfiguracionEtapa);
            $this->collUsuarioConfiguracionEtapas->remove($pos);
            if (null === $this->usuarioConfiguracionEtapasScheduledForDeletion) {
                $this->usuarioConfiguracionEtapasScheduledForDeletion = clone $this->collUsuarioConfiguracionEtapas;
                $this->usuarioConfiguracionEtapasScheduledForDeletion->clear();
            }
            $this->usuarioConfiguracionEtapasScheduledForDeletion[]= clone $usuarioConfiguracionEtapa;
            $usuarioConfiguracionEtapa->setEtapa(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related UsuarioConfiguracionEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     */
    public function getUsuarioConfiguracionEtapasJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getUsuarioConfiguracionEtapas($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Etapa is new, it will return
     * an empty collection; or if this Etapa has previously
     * been saved, it will retrieve related UsuarioConfiguracionEtapas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Etapa.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioConfiguracionEtapa[] List of ChildUsuarioConfiguracionEtapa objects
     */
    public function getUsuarioConfiguracionEtapasJoinUsuarioConfiguracionPrograma(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioConfiguracionEtapaQuery::create(null, $criteria);
        $query->joinWith('UsuarioConfiguracionPrograma', $joinBehavior);

        return $this->getUsuarioConfiguracionEtapas($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPrograma) {
            $this->aPrograma->removeEtapa($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeEtapa($this);
        }
        $this->clave = null;
        $this->id_programa = null;
        $this->nombre = null;
        $this->siglas = null;
        $this->activo = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->id_usuario_modificacion = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collFlujoEtapasRelatedByIdEtapa) {
                foreach ($this->collFlujoEtapasRelatedByIdEtapa as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFlujoEtapasRelatedByIdEtapaSiguiente) {
                foreach ($this->collFlujoEtapasRelatedByIdEtapaSiguiente as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProgramaRequisitos) {
                foreach ($this->collProgramaRequisitos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioConfiguracionEtapas) {
                foreach ($this->collUsuarioConfiguracionEtapas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collFlujoEtapasRelatedByIdEtapa = null;
        $this->collFlujoEtapasRelatedByIdEtapaSiguiente = null;
        $this->collProgramaRequisitos = null;
        $this->collUsuarioConfiguracionEtapas = null;
        $this->aPrograma = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(EtapaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
