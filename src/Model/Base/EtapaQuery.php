<?php

namespace Base;

use \Etapa as ChildEtapa;
use \EtapaQuery as ChildEtapaQuery;
use \Exception;
use \PDO;
use Map\EtapaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'etapa' table.
 *
 *
 *
 * @method     ChildEtapaQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildEtapaQuery orderByIdPrograma($order = Criteria::ASC) Order by the id_programa column
 * @method     ChildEtapaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildEtapaQuery orderBySiglas($order = Criteria::ASC) Order by the siglas column
 * @method     ChildEtapaQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildEtapaQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildEtapaQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildEtapaQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 *
 * @method     ChildEtapaQuery groupByClave() Group by the clave column
 * @method     ChildEtapaQuery groupByIdPrograma() Group by the id_programa column
 * @method     ChildEtapaQuery groupByNombre() Group by the nombre column
 * @method     ChildEtapaQuery groupBySiglas() Group by the siglas column
 * @method     ChildEtapaQuery groupByActivo() Group by the activo column
 * @method     ChildEtapaQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildEtapaQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildEtapaQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 *
 * @method     ChildEtapaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEtapaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEtapaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEtapaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEtapaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEtapaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEtapaQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildEtapaQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildEtapaQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildEtapaQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildEtapaQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildEtapaQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildEtapaQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     ChildEtapaQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildEtapaQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildEtapaQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildEtapaQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildEtapaQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEtapaQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildEtapaQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     ChildEtapaQuery leftJoinFlujoEtapaRelatedByIdEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlujoEtapaRelatedByIdEtapa relation
 * @method     ChildEtapaQuery rightJoinFlujoEtapaRelatedByIdEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlujoEtapaRelatedByIdEtapa relation
 * @method     ChildEtapaQuery innerJoinFlujoEtapaRelatedByIdEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the FlujoEtapaRelatedByIdEtapa relation
 *
 * @method     ChildEtapaQuery joinWithFlujoEtapaRelatedByIdEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FlujoEtapaRelatedByIdEtapa relation
 *
 * @method     ChildEtapaQuery leftJoinWithFlujoEtapaRelatedByIdEtapa() Adds a LEFT JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapa relation
 * @method     ChildEtapaQuery rightJoinWithFlujoEtapaRelatedByIdEtapa() Adds a RIGHT JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapa relation
 * @method     ChildEtapaQuery innerJoinWithFlujoEtapaRelatedByIdEtapa() Adds a INNER JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapa relation
 *
 * @method     ChildEtapaQuery leftJoinFlujoEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildEtapaQuery rightJoinFlujoEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildEtapaQuery innerJoinFlujoEtapaRelatedByIdEtapaSiguiente($relationAlias = null) Adds a INNER JOIN clause to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildEtapaQuery joinWithFlujoEtapaRelatedByIdEtapaSiguiente($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildEtapaQuery leftJoinWithFlujoEtapaRelatedByIdEtapaSiguiente() Adds a LEFT JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildEtapaQuery rightJoinWithFlujoEtapaRelatedByIdEtapaSiguiente() Adds a RIGHT JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 * @method     ChildEtapaQuery innerJoinWithFlujoEtapaRelatedByIdEtapaSiguiente() Adds a INNER JOIN clause and with to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
 *
 * @method     ChildEtapaQuery leftJoinProgramaRequisito($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildEtapaQuery rightJoinProgramaRequisito($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildEtapaQuery innerJoinProgramaRequisito($relationAlias = null) Adds a INNER JOIN clause to the query using the ProgramaRequisito relation
 *
 * @method     ChildEtapaQuery joinWithProgramaRequisito($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildEtapaQuery leftJoinWithProgramaRequisito() Adds a LEFT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildEtapaQuery rightJoinWithProgramaRequisito() Adds a RIGHT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildEtapaQuery innerJoinWithProgramaRequisito() Adds a INNER JOIN clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildEtapaQuery leftJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildEtapaQuery rightJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildEtapaQuery innerJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildEtapaQuery joinWithUsuarioConfiguracionEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildEtapaQuery leftJoinWithUsuarioConfiguracionEtapa() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildEtapaQuery rightJoinWithUsuarioConfiguracionEtapa() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildEtapaQuery innerJoinWithUsuarioConfiguracionEtapa() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     \ProgramaQuery|\UsuarioQuery|\FlujoEtapaQuery|\ProgramaRequisitoQuery|\UsuarioConfiguracionEtapaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEtapa|null findOne(ConnectionInterface $con = null) Return the first ChildEtapa matching the query
 * @method     ChildEtapa findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEtapa matching the query, or a new ChildEtapa object populated from the query conditions when no match is found
 *
 * @method     ChildEtapa|null findOneByClave(int $clave) Return the first ChildEtapa filtered by the clave column
 * @method     ChildEtapa|null findOneByIdPrograma(int $id_programa) Return the first ChildEtapa filtered by the id_programa column
 * @method     ChildEtapa|null findOneByNombre(string $nombre) Return the first ChildEtapa filtered by the nombre column
 * @method     ChildEtapa|null findOneBySiglas(string $siglas) Return the first ChildEtapa filtered by the siglas column
 * @method     ChildEtapa|null findOneByActivo(int $activo) Return the first ChildEtapa filtered by the activo column
 * @method     ChildEtapa|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildEtapa filtered by the fecha_creacion column
 * @method     ChildEtapa|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildEtapa filtered by the fecha_modificacion column
 * @method     ChildEtapa|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildEtapa filtered by the id_usuario_modificacion column *

 * @method     ChildEtapa requirePk($key, ConnectionInterface $con = null) Return the ChildEtapa by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOne(ConnectionInterface $con = null) Return the first ChildEtapa matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEtapa requireOneByClave(int $clave) Return the first ChildEtapa filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByIdPrograma(int $id_programa) Return the first ChildEtapa filtered by the id_programa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByNombre(string $nombre) Return the first ChildEtapa filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneBySiglas(string $siglas) Return the first ChildEtapa filtered by the siglas column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByActivo(int $activo) Return the first ChildEtapa filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildEtapa filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildEtapa filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtapa requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildEtapa filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEtapa[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEtapa objects based on current ModelCriteria
 * @method     ChildEtapa[]|ObjectCollection findByClave(int $clave) Return ChildEtapa objects filtered by the clave column
 * @method     ChildEtapa[]|ObjectCollection findByIdPrograma(int $id_programa) Return ChildEtapa objects filtered by the id_programa column
 * @method     ChildEtapa[]|ObjectCollection findByNombre(string $nombre) Return ChildEtapa objects filtered by the nombre column
 * @method     ChildEtapa[]|ObjectCollection findBySiglas(string $siglas) Return ChildEtapa objects filtered by the siglas column
 * @method     ChildEtapa[]|ObjectCollection findByActivo(int $activo) Return ChildEtapa objects filtered by the activo column
 * @method     ChildEtapa[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildEtapa objects filtered by the fecha_creacion column
 * @method     ChildEtapa[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildEtapa objects filtered by the fecha_modificacion column
 * @method     ChildEtapa[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildEtapa objects filtered by the id_usuario_modificacion column
 * @method     ChildEtapa[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EtapaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\EtapaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Etapa', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEtapaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEtapaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEtapaQuery) {
            return $criteria;
        }
        $query = new ChildEtapaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEtapa|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EtapaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EtapaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEtapa A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, id_programa, nombre, siglas, activo, fecha_creacion, fecha_modificacion, id_usuario_modificacion FROM etapa WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEtapa $obj */
            $obj = new ChildEtapa();
            $obj->hydrate($row);
            EtapaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEtapa|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the id_programa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPrograma(1234); // WHERE id_programa = 1234
     * $query->filterByIdPrograma(array(12, 34)); // WHERE id_programa IN (12, 34)
     * $query->filterByIdPrograma(array('min' => 12)); // WHERE id_programa > 12
     * </code>
     *
     * @see       filterByPrograma()
     *
     * @param     mixed $idPrograma The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByIdPrograma($idPrograma = null, $comparison = null)
    {
        if (is_array($idPrograma)) {
            $useMinMax = false;
            if (isset($idPrograma['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ID_PROGRAMA, $idPrograma['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPrograma['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ID_PROGRAMA, $idPrograma['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_ID_PROGRAMA, $idPrograma, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the siglas column
     *
     * Example usage:
     * <code>
     * $query->filterBySiglas('fooValue');   // WHERE siglas = 'fooValue'
     * $query->filterBySiglas('%fooValue%', Criteria::LIKE); // WHERE siglas LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siglas The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterBySiglas($siglas = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siglas)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_SIGLAS, $siglas, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_ID_PROGRAMA, $programa->getClave(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EtapaTableMap::COL_ID_PROGRAMA, $programa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EtapaTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \FlujoEtapa object
     *
     * @param \FlujoEtapa|ObjectCollection $flujoEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByFlujoEtapaRelatedByIdEtapa($flujoEtapa, $comparison = null)
    {
        if ($flujoEtapa instanceof \FlujoEtapa) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_CLAVE, $flujoEtapa->getIdEtapa(), $comparison);
        } elseif ($flujoEtapa instanceof ObjectCollection) {
            return $this
                ->useFlujoEtapaRelatedByIdEtapaQuery()
                ->filterByPrimaryKeys($flujoEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFlujoEtapaRelatedByIdEtapa() only accepts arguments of type \FlujoEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlujoEtapaRelatedByIdEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinFlujoEtapaRelatedByIdEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlujoEtapaRelatedByIdEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlujoEtapaRelatedByIdEtapa');
        }

        return $this;
    }

    /**
     * Use the FlujoEtapaRelatedByIdEtapa relation FlujoEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FlujoEtapaQuery A secondary query class using the current class as primary query
     */
    public function useFlujoEtapaRelatedByIdEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFlujoEtapaRelatedByIdEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FlujoEtapaRelatedByIdEtapa', '\FlujoEtapaQuery');
    }

    /**
     * Use the FlujoEtapaRelatedByIdEtapa relation FlujoEtapa object
     *
     * @param callable(\FlujoEtapaQuery):\FlujoEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withFlujoEtapaRelatedByIdEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useFlujoEtapaRelatedByIdEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \FlujoEtapa object
     *
     * @param \FlujoEtapa|ObjectCollection $flujoEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByFlujoEtapaRelatedByIdEtapaSiguiente($flujoEtapa, $comparison = null)
    {
        if ($flujoEtapa instanceof \FlujoEtapa) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_CLAVE, $flujoEtapa->getIdEtapaSiguiente(), $comparison);
        } elseif ($flujoEtapa instanceof ObjectCollection) {
            return $this
                ->useFlujoEtapaRelatedByIdEtapaSiguienteQuery()
                ->filterByPrimaryKeys($flujoEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFlujoEtapaRelatedByIdEtapaSiguiente() only accepts arguments of type \FlujoEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlujoEtapaRelatedByIdEtapaSiguiente relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinFlujoEtapaRelatedByIdEtapaSiguiente($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlujoEtapaRelatedByIdEtapaSiguiente');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlujoEtapaRelatedByIdEtapaSiguiente');
        }

        return $this;
    }

    /**
     * Use the FlujoEtapaRelatedByIdEtapaSiguiente relation FlujoEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FlujoEtapaQuery A secondary query class using the current class as primary query
     */
    public function useFlujoEtapaRelatedByIdEtapaSiguienteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFlujoEtapaRelatedByIdEtapaSiguiente($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FlujoEtapaRelatedByIdEtapaSiguiente', '\FlujoEtapaQuery');
    }

    /**
     * Use the FlujoEtapaRelatedByIdEtapaSiguiente relation FlujoEtapa object
     *
     * @param callable(\FlujoEtapaQuery):\FlujoEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withFlujoEtapaRelatedByIdEtapaSiguienteQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useFlujoEtapaRelatedByIdEtapaSiguienteQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \ProgramaRequisito object
     *
     * @param \ProgramaRequisito|ObjectCollection $programaRequisito the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByProgramaRequisito($programaRequisito, $comparison = null)
    {
        if ($programaRequisito instanceof \ProgramaRequisito) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_CLAVE, $programaRequisito->getIdEtapa(), $comparison);
        } elseif ($programaRequisito instanceof ObjectCollection) {
            return $this
                ->useProgramaRequisitoQuery()
                ->filterByPrimaryKeys($programaRequisito->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProgramaRequisito() only accepts arguments of type \ProgramaRequisito or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProgramaRequisito relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinProgramaRequisito($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProgramaRequisito');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProgramaRequisito');
        }

        return $this;
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaRequisitoQuery A secondary query class using the current class as primary query
     */
    public function useProgramaRequisitoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProgramaRequisito($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProgramaRequisito', '\ProgramaRequisitoQuery');
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @param callable(\ProgramaRequisitoQuery):\ProgramaRequisitoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaRequisitoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaRequisitoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionEtapa object
     *
     * @param \UsuarioConfiguracionEtapa|ObjectCollection $usuarioConfiguracionEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEtapaQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionEtapa($usuarioConfiguracionEtapa, $comparison = null)
    {
        if ($usuarioConfiguracionEtapa instanceof \UsuarioConfiguracionEtapa) {
            return $this
                ->addUsingAlias(EtapaTableMap::COL_CLAVE, $usuarioConfiguracionEtapa->getIdEtapa(), $comparison);
        } elseif ($usuarioConfiguracionEtapa instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionEtapaQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionEtapa() only accepts arguments of type \UsuarioConfiguracionEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionEtapa');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionEtapaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionEtapa', '\UsuarioConfiguracionEtapaQuery');
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @param callable(\UsuarioConfiguracionEtapaQuery):\UsuarioConfiguracionEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEtapa $etapa Object to remove from the list of results
     *
     * @return $this|ChildEtapaQuery The current query, for fluid interface
     */
    public function prune($etapa = null)
    {
        if ($etapa) {
            $this->addUsingAlias(EtapaTableMap::COL_CLAVE, $etapa->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the etapa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EtapaTableMap::clearInstancePool();
            EtapaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EtapaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EtapaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EtapaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EtapaQuery
