<?php

namespace Base;

use \Permisos as ChildPermisos;
use \PermisosQuery as ChildPermisosQuery;
use \RolPermiso as ChildRolPermiso;
use \RolPermisoQuery as ChildRolPermisoQuery;
use \UsuarioPermiso as ChildUsuarioPermiso;
use \UsuarioPermisoQuery as ChildUsuarioPermisoQuery;
use \Exception;
use \PDO;
use Map\PermisosTableMap;
use Map\RolPermisoTableMap;
use Map\UsuarioPermisoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'permisos' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Permisos implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\PermisosTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the clave field.
     *
     * @var        int
     */
    protected $clave;

    /**
     * The value for the seccion field.
     *
     * @var        string
     */
    protected $seccion;

    /**
     * The value for the nombre field.
     *
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the descripcion field.
     *
     * @var        string
     */
    protected $descripcion;

    /**
     * The value for the siglas field.
     *
     * @var        string
     */
    protected $siglas;

    /**
     * The value for the activo field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $activo;

    /**
     * @var        ObjectCollection|ChildRolPermiso[] Collection to store aggregation of ChildRolPermiso objects.
     */
    protected $collRolPermisos;
    protected $collRolPermisosPartial;

    /**
     * @var        ObjectCollection|ChildUsuarioPermiso[] Collection to store aggregation of ChildUsuarioPermiso objects.
     */
    protected $collUsuarioPermisos;
    protected $collUsuarioPermisosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildRolPermiso[]
     */
    protected $rolPermisosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsuarioPermiso[]
     */
    protected $usuarioPermisosScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->activo = 1;
    }

    /**
     * Initializes internal state of Base\Permisos object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Permisos</code> instance.  If
     * <code>obj</code> is an instance of <code>Permisos</code>, delegates to
     * <code>equals(Permisos)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param  string  $keyType                (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [clave] column value.
     *
     * @return int
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Get the [seccion] column value.
     *
     * @return string
     */
    public function getSeccion()
    {
        return $this->seccion;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Get the [descripcion] column value.
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Get the [siglas] column value.
     *
     * @return string
     */
    public function getSiglas()
    {
        return $this->siglas;
    }

    /**
     * Get the [activo] column value.
     *
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Set the value of [clave] column.
     *
     * @param int $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setClave($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clave !== $v) {
            $this->clave = $v;
            $this->modifiedColumns[PermisosTableMap::COL_CLAVE] = true;
        }

        return $this;
    } // setClave()

    /**
     * Set the value of [seccion] column.
     *
     * @param string $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setSeccion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->seccion !== $v) {
            $this->seccion = $v;
            $this->modifiedColumns[PermisosTableMap::COL_SECCION] = true;
        }

        return $this;
    } // setSeccion()

    /**
     * Set the value of [nombre] column.
     *
     * @param string $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[PermisosTableMap::COL_NOMBRE] = true;
        }

        return $this;
    } // setNombre()

    /**
     * Set the value of [descripcion] column.
     *
     * @param string $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[PermisosTableMap::COL_DESCRIPCION] = true;
        }

        return $this;
    } // setDescripcion()

    /**
     * Set the value of [siglas] column.
     *
     * @param string $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setSiglas($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->siglas !== $v) {
            $this->siglas = $v;
            $this->modifiedColumns[PermisosTableMap::COL_SIGLAS] = true;
        }

        return $this;
    } // setSiglas()

    /**
     * Set the value of [activo] column.
     *
     * @param int $v New value
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function setActivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->activo !== $v) {
            $this->activo = $v;
            $this->modifiedColumns[PermisosTableMap::COL_ACTIVO] = true;
        }

        return $this;
    } // setActivo()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->activo !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : PermisosTableMap::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clave = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : PermisosTableMap::translateFieldName('Seccion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->seccion = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : PermisosTableMap::translateFieldName('Nombre', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nombre = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : PermisosTableMap::translateFieldName('Descripcion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->descripcion = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : PermisosTableMap::translateFieldName('Siglas', TableMap::TYPE_PHPNAME, $indexType)];
            $this->siglas = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : PermisosTableMap::translateFieldName('Activo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activo = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = PermisosTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Permisos'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PermisosTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildPermisosQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRolPermisos = null;

            $this->collUsuarioPermisos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Permisos::setDeleted()
     * @see Permisos::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PermisosTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildPermisosQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(PermisosTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                PermisosTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->rolPermisosScheduledForDeletion !== null) {
                if (!$this->rolPermisosScheduledForDeletion->isEmpty()) {
                    \RolPermisoQuery::create()
                        ->filterByPrimaryKeys($this->rolPermisosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rolPermisosScheduledForDeletion = null;
                }
            }

            if ($this->collRolPermisos !== null) {
                foreach ($this->collRolPermisos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usuarioPermisosScheduledForDeletion !== null) {
                if (!$this->usuarioPermisosScheduledForDeletion->isEmpty()) {
                    \UsuarioPermisoQuery::create()
                        ->filterByPrimaryKeys($this->usuarioPermisosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usuarioPermisosScheduledForDeletion = null;
                }
            }

            if ($this->collUsuarioPermisos !== null) {
                foreach ($this->collUsuarioPermisos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(PermisosTableMap::COL_CLAVE)) {
            $modifiedColumns[':p' . $index++]  = 'clave';
        }
        if ($this->isColumnModified(PermisosTableMap::COL_SECCION)) {
            $modifiedColumns[':p' . $index++]  = 'seccion';
        }
        if ($this->isColumnModified(PermisosTableMap::COL_NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = 'nombre';
        }
        if ($this->isColumnModified(PermisosTableMap::COL_DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = 'descripcion';
        }
        if ($this->isColumnModified(PermisosTableMap::COL_SIGLAS)) {
            $modifiedColumns[':p' . $index++]  = 'siglas';
        }
        if ($this->isColumnModified(PermisosTableMap::COL_ACTIVO)) {
            $modifiedColumns[':p' . $index++]  = 'activo';
        }

        $sql = sprintf(
            'INSERT INTO permisos (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'clave':
                        $stmt->bindValue($identifier, $this->clave, PDO::PARAM_INT);
                        break;
                    case 'seccion':
                        $stmt->bindValue($identifier, $this->seccion, PDO::PARAM_STR);
                        break;
                    case 'nombre':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case 'descripcion':
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case 'siglas':
                        $stmt->bindValue($identifier, $this->siglas, PDO::PARAM_STR);
                        break;
                    case 'activo':
                        $stmt->bindValue($identifier, $this->activo, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PermisosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getClave();
                break;
            case 1:
                return $this->getSeccion();
                break;
            case 2:
                return $this->getNombre();
                break;
            case 3:
                return $this->getDescripcion();
                break;
            case 4:
                return $this->getSiglas();
                break;
            case 5:
                return $this->getActivo();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Permisos'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Permisos'][$this->hashCode()] = true;
        $keys = PermisosTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getClave(),
            $keys[1] => $this->getSeccion(),
            $keys[2] => $this->getNombre(),
            $keys[3] => $this->getDescripcion(),
            $keys[4] => $this->getSiglas(),
            $keys[5] => $this->getActivo(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRolPermisos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'rolPermisos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'rol_permisos';
                        break;
                    default:
                        $key = 'RolPermisos';
                }

                $result[$key] = $this->collRolPermisos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsuarioPermisos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuarioPermisos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario_permisos';
                        break;
                    default:
                        $key = 'UsuarioPermisos';
                }

                $result[$key] = $this->collUsuarioPermisos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Permisos
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = PermisosTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Permisos
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setClave($value);
                break;
            case 1:
                $this->setSeccion($value);
                break;
            case 2:
                $this->setNombre($value);
                break;
            case 3:
                $this->setDescripcion($value);
                break;
            case 4:
                $this->setSiglas($value);
                break;
            case 5:
                $this->setActivo($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     $this|\Permisos
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = PermisosTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setClave($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSeccion($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNombre($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescripcion($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSiglas($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setActivo($arr[$keys[5]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Permisos The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(PermisosTableMap::DATABASE_NAME);

        if ($this->isColumnModified(PermisosTableMap::COL_CLAVE)) {
            $criteria->add(PermisosTableMap::COL_CLAVE, $this->clave);
        }
        if ($this->isColumnModified(PermisosTableMap::COL_SECCION)) {
            $criteria->add(PermisosTableMap::COL_SECCION, $this->seccion);
        }
        if ($this->isColumnModified(PermisosTableMap::COL_NOMBRE)) {
            $criteria->add(PermisosTableMap::COL_NOMBRE, $this->nombre);
        }
        if ($this->isColumnModified(PermisosTableMap::COL_DESCRIPCION)) {
            $criteria->add(PermisosTableMap::COL_DESCRIPCION, $this->descripcion);
        }
        if ($this->isColumnModified(PermisosTableMap::COL_SIGLAS)) {
            $criteria->add(PermisosTableMap::COL_SIGLAS, $this->siglas);
        }
        if ($this->isColumnModified(PermisosTableMap::COL_ACTIVO)) {
            $criteria->add(PermisosTableMap::COL_ACTIVO, $this->activo);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildPermisosQuery::create();
        $criteria->add(PermisosTableMap::COL_CLAVE, $this->clave);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getClave();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getClave();
    }

    /**
     * Generic method to set the primary key (clave column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setClave($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getClave();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Permisos (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setClave($this->getClave());
        $copyObj->setSeccion($this->getSeccion());
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setSiglas($this->getSiglas());
        $copyObj->setActivo($this->getActivo());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getRolPermisos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRolPermiso($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsuarioPermisos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsuarioPermiso($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Permisos Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RolPermiso' === $relationName) {
            $this->initRolPermisos();
            return;
        }
        if ('UsuarioPermiso' === $relationName) {
            $this->initUsuarioPermisos();
            return;
        }
    }

    /**
     * Clears out the collRolPermisos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRolPermisos()
     */
    public function clearRolPermisos()
    {
        $this->collRolPermisos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRolPermisos collection loaded partially.
     */
    public function resetPartialRolPermisos($v = true)
    {
        $this->collRolPermisosPartial = $v;
    }

    /**
     * Initializes the collRolPermisos collection.
     *
     * By default this just sets the collRolPermisos collection to an empty array (like clearcollRolPermisos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRolPermisos($overrideExisting = true)
    {
        if (null !== $this->collRolPermisos && !$overrideExisting) {
            return;
        }

        $collectionClassName = RolPermisoTableMap::getTableMap()->getCollectionClassName();

        $this->collRolPermisos = new $collectionClassName;
        $this->collRolPermisos->setModel('\RolPermiso');
    }

    /**
     * Gets an array of ChildRolPermiso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPermisos is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     * @throws PropelException
     */
    public function getRolPermisos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRolPermisosPartial && !$this->isNew();
        if (null === $this->collRolPermisos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRolPermisos) {
                    $this->initRolPermisos();
                } else {
                    $collectionClassName = RolPermisoTableMap::getTableMap()->getCollectionClassName();

                    $collRolPermisos = new $collectionClassName;
                    $collRolPermisos->setModel('\RolPermiso');

                    return $collRolPermisos;
                }
            } else {
                $collRolPermisos = ChildRolPermisoQuery::create(null, $criteria)
                    ->filterByPermisos($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRolPermisosPartial && count($collRolPermisos)) {
                        $this->initRolPermisos(false);

                        foreach ($collRolPermisos as $obj) {
                            if (false == $this->collRolPermisos->contains($obj)) {
                                $this->collRolPermisos->append($obj);
                            }
                        }

                        $this->collRolPermisosPartial = true;
                    }

                    return $collRolPermisos;
                }

                if ($partial && $this->collRolPermisos) {
                    foreach ($this->collRolPermisos as $obj) {
                        if ($obj->isNew()) {
                            $collRolPermisos[] = $obj;
                        }
                    }
                }

                $this->collRolPermisos = $collRolPermisos;
                $this->collRolPermisosPartial = false;
            }
        }

        return $this->collRolPermisos;
    }

    /**
     * Sets a collection of ChildRolPermiso objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $rolPermisos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPermisos The current object (for fluent API support)
     */
    public function setRolPermisos(Collection $rolPermisos, ConnectionInterface $con = null)
    {
        /** @var ChildRolPermiso[] $rolPermisosToDelete */
        $rolPermisosToDelete = $this->getRolPermisos(new Criteria(), $con)->diff($rolPermisos);


        $this->rolPermisosScheduledForDeletion = $rolPermisosToDelete;

        foreach ($rolPermisosToDelete as $rolPermisoRemoved) {
            $rolPermisoRemoved->setPermisos(null);
        }

        $this->collRolPermisos = null;
        foreach ($rolPermisos as $rolPermiso) {
            $this->addRolPermiso($rolPermiso);
        }

        $this->collRolPermisos = $rolPermisos;
        $this->collRolPermisosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RolPermiso objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related RolPermiso objects.
     * @throws PropelException
     */
    public function countRolPermisos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRolPermisosPartial && !$this->isNew();
        if (null === $this->collRolPermisos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRolPermisos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRolPermisos());
            }

            $query = ChildRolPermisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPermisos($this)
                ->count($con);
        }

        return count($this->collRolPermisos);
    }

    /**
     * Method called to associate a ChildRolPermiso object to this object
     * through the ChildRolPermiso foreign key attribute.
     *
     * @param  ChildRolPermiso $l ChildRolPermiso
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function addRolPermiso(ChildRolPermiso $l)
    {
        if ($this->collRolPermisos === null) {
            $this->initRolPermisos();
            $this->collRolPermisosPartial = true;
        }

        if (!$this->collRolPermisos->contains($l)) {
            $this->doAddRolPermiso($l);

            if ($this->rolPermisosScheduledForDeletion and $this->rolPermisosScheduledForDeletion->contains($l)) {
                $this->rolPermisosScheduledForDeletion->remove($this->rolPermisosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildRolPermiso $rolPermiso The ChildRolPermiso object to add.
     */
    protected function doAddRolPermiso(ChildRolPermiso $rolPermiso)
    {
        $this->collRolPermisos[]= $rolPermiso;
        $rolPermiso->setPermisos($this);
    }

    /**
     * @param  ChildRolPermiso $rolPermiso The ChildRolPermiso object to remove.
     * @return $this|ChildPermisos The current object (for fluent API support)
     */
    public function removeRolPermiso(ChildRolPermiso $rolPermiso)
    {
        if ($this->getRolPermisos()->contains($rolPermiso)) {
            $pos = $this->collRolPermisos->search($rolPermiso);
            $this->collRolPermisos->remove($pos);
            if (null === $this->rolPermisosScheduledForDeletion) {
                $this->rolPermisosScheduledForDeletion = clone $this->collRolPermisos;
                $this->rolPermisosScheduledForDeletion->clear();
            }
            $this->rolPermisosScheduledForDeletion[]= clone $rolPermiso;
            $rolPermiso->setPermisos(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Permisos is new, it will return
     * an empty collection; or if this Permisos has previously
     * been saved, it will retrieve related RolPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Permisos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     */
    public function getRolPermisosJoinRol(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolPermisoQuery::create(null, $criteria);
        $query->joinWith('Rol', $joinBehavior);

        return $this->getRolPermisos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Permisos is new, it will return
     * an empty collection; or if this Permisos has previously
     * been saved, it will retrieve related RolPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Permisos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildRolPermiso[] List of ChildRolPermiso objects
     */
    public function getRolPermisosJoinUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildRolPermisoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $joinBehavior);

        return $this->getRolPermisos($query, $con);
    }

    /**
     * Clears out the collUsuarioPermisos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsuarioPermisos()
     */
    public function clearUsuarioPermisos()
    {
        $this->collUsuarioPermisos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsuarioPermisos collection loaded partially.
     */
    public function resetPartialUsuarioPermisos($v = true)
    {
        $this->collUsuarioPermisosPartial = $v;
    }

    /**
     * Initializes the collUsuarioPermisos collection.
     *
     * By default this just sets the collUsuarioPermisos collection to an empty array (like clearcollUsuarioPermisos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsuarioPermisos($overrideExisting = true)
    {
        if (null !== $this->collUsuarioPermisos && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

        $this->collUsuarioPermisos = new $collectionClassName;
        $this->collUsuarioPermisos->setModel('\UsuarioPermiso');
    }

    /**
     * Gets an array of ChildUsuarioPermiso objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildPermisos is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     * @throws PropelException
     */
    public function getUsuarioPermisos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsuarioPermisos) {
                    $this->initUsuarioPermisos();
                } else {
                    $collectionClassName = UsuarioPermisoTableMap::getTableMap()->getCollectionClassName();

                    $collUsuarioPermisos = new $collectionClassName;
                    $collUsuarioPermisos->setModel('\UsuarioPermiso');

                    return $collUsuarioPermisos;
                }
            } else {
                $collUsuarioPermisos = ChildUsuarioPermisoQuery::create(null, $criteria)
                    ->filterByPermisos($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsuarioPermisosPartial && count($collUsuarioPermisos)) {
                        $this->initUsuarioPermisos(false);

                        foreach ($collUsuarioPermisos as $obj) {
                            if (false == $this->collUsuarioPermisos->contains($obj)) {
                                $this->collUsuarioPermisos->append($obj);
                            }
                        }

                        $this->collUsuarioPermisosPartial = true;
                    }

                    return $collUsuarioPermisos;
                }

                if ($partial && $this->collUsuarioPermisos) {
                    foreach ($this->collUsuarioPermisos as $obj) {
                        if ($obj->isNew()) {
                            $collUsuarioPermisos[] = $obj;
                        }
                    }
                }

                $this->collUsuarioPermisos = $collUsuarioPermisos;
                $this->collUsuarioPermisosPartial = false;
            }
        }

        return $this->collUsuarioPermisos;
    }

    /**
     * Sets a collection of ChildUsuarioPermiso objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usuarioPermisos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildPermisos The current object (for fluent API support)
     */
    public function setUsuarioPermisos(Collection $usuarioPermisos, ConnectionInterface $con = null)
    {
        /** @var ChildUsuarioPermiso[] $usuarioPermisosToDelete */
        $usuarioPermisosToDelete = $this->getUsuarioPermisos(new Criteria(), $con)->diff($usuarioPermisos);


        $this->usuarioPermisosScheduledForDeletion = $usuarioPermisosToDelete;

        foreach ($usuarioPermisosToDelete as $usuarioPermisoRemoved) {
            $usuarioPermisoRemoved->setPermisos(null);
        }

        $this->collUsuarioPermisos = null;
        foreach ($usuarioPermisos as $usuarioPermiso) {
            $this->addUsuarioPermiso($usuarioPermiso);
        }

        $this->collUsuarioPermisos = $usuarioPermisos;
        $this->collUsuarioPermisosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsuarioPermiso objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsuarioPermiso objects.
     * @throws PropelException
     */
    public function countUsuarioPermisos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsuarioPermisosPartial && !$this->isNew();
        if (null === $this->collUsuarioPermisos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsuarioPermisos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsuarioPermisos());
            }

            $query = ChildUsuarioPermisoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPermisos($this)
                ->count($con);
        }

        return count($this->collUsuarioPermisos);
    }

    /**
     * Method called to associate a ChildUsuarioPermiso object to this object
     * through the ChildUsuarioPermiso foreign key attribute.
     *
     * @param  ChildUsuarioPermiso $l ChildUsuarioPermiso
     * @return $this|\Permisos The current object (for fluent API support)
     */
    public function addUsuarioPermiso(ChildUsuarioPermiso $l)
    {
        if ($this->collUsuarioPermisos === null) {
            $this->initUsuarioPermisos();
            $this->collUsuarioPermisosPartial = true;
        }

        if (!$this->collUsuarioPermisos->contains($l)) {
            $this->doAddUsuarioPermiso($l);

            if ($this->usuarioPermisosScheduledForDeletion and $this->usuarioPermisosScheduledForDeletion->contains($l)) {
                $this->usuarioPermisosScheduledForDeletion->remove($this->usuarioPermisosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsuarioPermiso $usuarioPermiso The ChildUsuarioPermiso object to add.
     */
    protected function doAddUsuarioPermiso(ChildUsuarioPermiso $usuarioPermiso)
    {
        $this->collUsuarioPermisos[]= $usuarioPermiso;
        $usuarioPermiso->setPermisos($this);
    }

    /**
     * @param  ChildUsuarioPermiso $usuarioPermiso The ChildUsuarioPermiso object to remove.
     * @return $this|ChildPermisos The current object (for fluent API support)
     */
    public function removeUsuarioPermiso(ChildUsuarioPermiso $usuarioPermiso)
    {
        if ($this->getUsuarioPermisos()->contains($usuarioPermiso)) {
            $pos = $this->collUsuarioPermisos->search($usuarioPermiso);
            $this->collUsuarioPermisos->remove($pos);
            if (null === $this->usuarioPermisosScheduledForDeletion) {
                $this->usuarioPermisosScheduledForDeletion = clone $this->collUsuarioPermisos;
                $this->usuarioPermisosScheduledForDeletion->clear();
            }
            $this->usuarioPermisosScheduledForDeletion[]= clone $usuarioPermiso;
            $usuarioPermiso->setPermisos(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Permisos is new, it will return
     * an empty collection; or if this Permisos has previously
     * been saved, it will retrieve related UsuarioPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Permisos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     */
    public function getUsuarioPermisosJoinUsuarioRelatedByIdUsuario(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioPermisoQuery::create(null, $criteria);
        $query->joinWith('UsuarioRelatedByIdUsuario', $joinBehavior);

        return $this->getUsuarioPermisos($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Permisos is new, it will return
     * an empty collection; or if this Permisos has previously
     * been saved, it will retrieve related UsuarioPermisos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Permisos.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsuarioPermiso[] List of ChildUsuarioPermiso objects
     */
    public function getUsuarioPermisosJoinUsuarioRelatedByIdUsuarioModificacion(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsuarioPermisoQuery::create(null, $criteria);
        $query->joinWith('UsuarioRelatedByIdUsuarioModificacion', $joinBehavior);

        return $this->getUsuarioPermisos($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->clave = null;
        $this->seccion = null;
        $this->nombre = null;
        $this->descripcion = null;
        $this->siglas = null;
        $this->activo = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collRolPermisos) {
                foreach ($this->collRolPermisos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsuarioPermisos) {
                foreach ($this->collUsuarioPermisos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collRolPermisos = null;
        $this->collUsuarioPermisos = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(PermisosTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
