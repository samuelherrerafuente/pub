<?php

namespace Base;

use \Etapa as ChildEtapa;
use \EtapaQuery as ChildEtapaQuery;
use \FlujoEtapaQuery as ChildFlujoEtapaQuery;
use \Usuario as ChildUsuario;
use \UsuarioQuery as ChildUsuarioQuery;
use \DateTime;
use \Exception;
use \PDO;
use Map\FlujoEtapaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'flujo_etapa' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class FlujoEtapa implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Map\\FlujoEtapaTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the clave field.
     *
     * @var        int
     */
    protected $clave;

    /**
     * The value for the id_etapa field.
     *
     * @var        int
     */
    protected $id_etapa;

    /**
     * The value for the id_etapa_siguiente field.
     *
     * @var        int
     */
    protected $id_etapa_siguiente;

    /**
     * The value for the activo field.
     *
     * Note: this column has a database default value of: 1
     * @var        int
     */
    protected $activo;

    /**
     * The value for the fecha_creacion field.
     *
     * @var        DateTime
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_modificacion field.
     *
     * @var        DateTime
     */
    protected $fecha_modificacion;

    /**
     * The value for the id_usuario_modificacion field.
     *
     * @var        int
     */
    protected $id_usuario_modificacion;

    /**
     * @var        ChildEtapa
     */
    protected $aEtapaRelatedByIdEtapa;

    /**
     * @var        ChildEtapa
     */
    protected $aEtapaRelatedByIdEtapaSiguiente;

    /**
     * @var        ChildUsuario
     */
    protected $aUsuario;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->activo = 1;
    }

    /**
     * Initializes internal state of Base\FlujoEtapa object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>FlujoEtapa</code> instance.  If
     * <code>obj</code> is an instance of <code>FlujoEtapa</code>, delegates to
     * <code>equals(FlujoEtapa)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param  string  $keyType                (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [clave] column value.
     *
     * @return int
     */
    public function getClave()
    {
        return $this->clave;
    }

    /**
     * Get the [id_etapa] column value.
     *
     * @return int
     */
    public function getIdEtapa()
    {
        return $this->id_etapa;
    }

    /**
     * Get the [id_etapa_siguiente] column value.
     *
     * @return int
     */
    public function getIdEtapaSiguiente()
    {
        return $this->id_etapa_siguiente;
    }

    /**
     * Get the [activo] column value.
     *
     * @return int
     */
    public function getActivo()
    {
        return $this->activo;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaCreacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_creacion;
        } else {
            return $this->fecha_creacion instanceof \DateTimeInterface ? $this->fecha_creacion->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [fecha_modificacion] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getFechaModificacion($format = 'Y-m-d H:i:s')
    {
        if ($format === null) {
            return $this->fecha_modificacion;
        } else {
            return $this->fecha_modificacion instanceof \DateTimeInterface ? $this->fecha_modificacion->format($format) : null;
        }
    }

    /**
     * Get the [id_usuario_modificacion] column value.
     *
     * @return int
     */
    public function getIdUsuarioModificacion()
    {
        return $this->id_usuario_modificacion;
    }

    /**
     * Set the value of [clave] column.
     *
     * @param int $v New value
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setClave($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->clave !== $v) {
            $this->clave = $v;
            $this->modifiedColumns[FlujoEtapaTableMap::COL_CLAVE] = true;
        }

        return $this;
    } // setClave()

    /**
     * Set the value of [id_etapa] column.
     *
     * @param int $v New value
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setIdEtapa($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_etapa !== $v) {
            $this->id_etapa = $v;
            $this->modifiedColumns[FlujoEtapaTableMap::COL_ID_ETAPA] = true;
        }

        if ($this->aEtapaRelatedByIdEtapa !== null && $this->aEtapaRelatedByIdEtapa->getClave() !== $v) {
            $this->aEtapaRelatedByIdEtapa = null;
        }

        return $this;
    } // setIdEtapa()

    /**
     * Set the value of [id_etapa_siguiente] column.
     *
     * @param int $v New value
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setIdEtapaSiguiente($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_etapa_siguiente !== $v) {
            $this->id_etapa_siguiente = $v;
            $this->modifiedColumns[FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE] = true;
        }

        if ($this->aEtapaRelatedByIdEtapaSiguiente !== null && $this->aEtapaRelatedByIdEtapaSiguiente->getClave() !== $v) {
            $this->aEtapaRelatedByIdEtapaSiguiente = null;
        }

        return $this;
    } // setIdEtapaSiguiente()

    /**
     * Set the value of [activo] column.
     *
     * @param int $v New value
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setActivo($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->activo !== $v) {
            $this->activo = $v;
            $this->modifiedColumns[FlujoEtapaTableMap::COL_ACTIVO] = true;
        }

        return $this;
    } // setActivo()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            if ($this->fecha_creacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_creacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_creacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FlujoEtapaTableMap::COL_FECHA_CREACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_modificacion] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setFechaModificacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_modificacion !== null || $dt !== null) {
            if ($this->fecha_modificacion === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->fecha_modificacion->format("Y-m-d H:i:s.u")) {
                $this->fecha_modificacion = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FlujoEtapaTableMap::COL_FECHA_MODIFICACION] = true;
            }
        } // if either are not null

        return $this;
    } // setFechaModificacion()

    /**
     * Set the value of [id_usuario_modificacion] column.
     *
     * @param int $v New value
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     */
    public function setIdUsuarioModificacion($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id_usuario_modificacion !== $v) {
            $this->id_usuario_modificacion = $v;
            $this->modifiedColumns[FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION] = true;
        }

        if ($this->aUsuario !== null && $this->aUsuario->getClave() !== $v) {
            $this->aUsuario = null;
        }

        return $this;
    } // setIdUsuarioModificacion()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->activo !== 1) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FlujoEtapaTableMap::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
            $this->clave = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FlujoEtapaTableMap::translateFieldName('IdEtapa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_etapa = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FlujoEtapaTableMap::translateFieldName('IdEtapaSiguiente', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_etapa_siguiente = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FlujoEtapaTableMap::translateFieldName('Activo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->activo = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FlujoEtapaTableMap::translateFieldName('FechaCreacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_creacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FlujoEtapaTableMap::translateFieldName('FechaModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->fecha_modificacion = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FlujoEtapaTableMap::translateFieldName('IdUsuarioModificacion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id_usuario_modificacion = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = FlujoEtapaTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\FlujoEtapa'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEtapaRelatedByIdEtapa !== null && $this->id_etapa !== $this->aEtapaRelatedByIdEtapa->getClave()) {
            $this->aEtapaRelatedByIdEtapa = null;
        }
        if ($this->aEtapaRelatedByIdEtapaSiguiente !== null && $this->id_etapa_siguiente !== $this->aEtapaRelatedByIdEtapaSiguiente->getClave()) {
            $this->aEtapaRelatedByIdEtapaSiguiente = null;
        }
        if ($this->aUsuario !== null && $this->id_usuario_modificacion !== $this->aUsuario->getClave()) {
            $this->aUsuario = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFlujoEtapaQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEtapaRelatedByIdEtapa = null;
            $this->aEtapaRelatedByIdEtapaSiguiente = null;
            $this->aUsuario = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see FlujoEtapa::setDeleted()
     * @see FlujoEtapa::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFlujoEtapaQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FlujoEtapaTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FlujoEtapaTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEtapaRelatedByIdEtapa !== null) {
                if ($this->aEtapaRelatedByIdEtapa->isModified() || $this->aEtapaRelatedByIdEtapa->isNew()) {
                    $affectedRows += $this->aEtapaRelatedByIdEtapa->save($con);
                }
                $this->setEtapaRelatedByIdEtapa($this->aEtapaRelatedByIdEtapa);
            }

            if ($this->aEtapaRelatedByIdEtapaSiguiente !== null) {
                if ($this->aEtapaRelatedByIdEtapaSiguiente->isModified() || $this->aEtapaRelatedByIdEtapaSiguiente->isNew()) {
                    $affectedRows += $this->aEtapaRelatedByIdEtapaSiguiente->save($con);
                }
                $this->setEtapaRelatedByIdEtapaSiguiente($this->aEtapaRelatedByIdEtapaSiguiente);
            }

            if ($this->aUsuario !== null) {
                if ($this->aUsuario->isModified() || $this->aUsuario->isNew()) {
                    $affectedRows += $this->aUsuario->save($con);
                }
                $this->setUsuario($this->aUsuario);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[FlujoEtapaTableMap::COL_CLAVE] = true;
        if (null !== $this->clave) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FlujoEtapaTableMap::COL_CLAVE . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_CLAVE)) {
            $modifiedColumns[':p' . $index++]  = 'clave';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_ETAPA)) {
            $modifiedColumns[':p' . $index++]  = 'id_etapa';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE)) {
            $modifiedColumns[':p' . $index++]  = 'id_etapa_siguiente';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ACTIVO)) {
            $modifiedColumns[':p' . $index++]  = 'activo';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_creacion';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_FECHA_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'fecha_modificacion';
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $modifiedColumns[':p' . $index++]  = 'id_usuario_modificacion';
        }

        $sql = sprintf(
            'INSERT INTO flujo_etapa (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'clave':
                        $stmt->bindValue($identifier, $this->clave, PDO::PARAM_INT);
                        break;
                    case 'id_etapa':
                        $stmt->bindValue($identifier, $this->id_etapa, PDO::PARAM_INT);
                        break;
                    case 'id_etapa_siguiente':
                        $stmt->bindValue($identifier, $this->id_etapa_siguiente, PDO::PARAM_INT);
                        break;
                    case 'activo':
                        $stmt->bindValue($identifier, $this->activo, PDO::PARAM_INT);
                        break;
                    case 'fecha_creacion':
                        $stmt->bindValue($identifier, $this->fecha_creacion ? $this->fecha_creacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'fecha_modificacion':
                        $stmt->bindValue($identifier, $this->fecha_modificacion ? $this->fecha_modificacion->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'id_usuario_modificacion':
                        $stmt->bindValue($identifier, $this->id_usuario_modificacion, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setClave($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FlujoEtapaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getClave();
                break;
            case 1:
                return $this->getIdEtapa();
                break;
            case 2:
                return $this->getIdEtapaSiguiente();
                break;
            case 3:
                return $this->getActivo();
                break;
            case 4:
                return $this->getFechaCreacion();
                break;
            case 5:
                return $this->getFechaModificacion();
                break;
            case 6:
                return $this->getIdUsuarioModificacion();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['FlujoEtapa'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['FlujoEtapa'][$this->hashCode()] = true;
        $keys = FlujoEtapaTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getClave(),
            $keys[1] => $this->getIdEtapa(),
            $keys[2] => $this->getIdEtapaSiguiente(),
            $keys[3] => $this->getActivo(),
            $keys[4] => $this->getFechaCreacion(),
            $keys[5] => $this->getFechaModificacion(),
            $keys[6] => $this->getIdUsuarioModificacion(),
        );
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('Y-m-d H:i:s.u');
        }

        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('Y-m-d H:i:s.u');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEtapaRelatedByIdEtapa) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etapa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etapa';
                        break;
                    default:
                        $key = 'Etapa';
                }

                $result[$key] = $this->aEtapaRelatedByIdEtapa->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEtapaRelatedByIdEtapaSiguiente) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etapa';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etapa';
                        break;
                    default:
                        $key = 'Etapa';
                }

                $result[$key] = $this->aEtapaRelatedByIdEtapaSiguiente->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUsuario) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usuario';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'usuario';
                        break;
                    default:
                        $key = 'Usuario';
                }

                $result[$key] = $this->aUsuario->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\FlujoEtapa
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FlujoEtapaTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\FlujoEtapa
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setClave($value);
                break;
            case 1:
                $this->setIdEtapa($value);
                break;
            case 2:
                $this->setIdEtapaSiguiente($value);
                break;
            case 3:
                $this->setActivo($value);
                break;
            case 4:
                $this->setFechaCreacion($value);
                break;
            case 5:
                $this->setFechaModificacion($value);
                break;
            case 6:
                $this->setIdUsuarioModificacion($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return     $this|\FlujoEtapa
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FlujoEtapaTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setClave($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIdEtapa($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIdEtapaSiguiente($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setActivo($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setFechaCreacion($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setFechaModificacion($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdUsuarioModificacion($arr[$keys[6]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\FlujoEtapa The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FlujoEtapaTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FlujoEtapaTableMap::COL_CLAVE)) {
            $criteria->add(FlujoEtapaTableMap::COL_CLAVE, $this->clave);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_ETAPA)) {
            $criteria->add(FlujoEtapaTableMap::COL_ID_ETAPA, $this->id_etapa);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE)) {
            $criteria->add(FlujoEtapaTableMap::COL_ID_ETAPA_SIGUIENTE, $this->id_etapa_siguiente);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ACTIVO)) {
            $criteria->add(FlujoEtapaTableMap::COL_ACTIVO, $this->activo);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_FECHA_CREACION)) {
            $criteria->add(FlujoEtapaTableMap::COL_FECHA_CREACION, $this->fecha_creacion);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_FECHA_MODIFICACION)) {
            $criteria->add(FlujoEtapaTableMap::COL_FECHA_MODIFICACION, $this->fecha_modificacion);
        }
        if ($this->isColumnModified(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION)) {
            $criteria->add(FlujoEtapaTableMap::COL_ID_USUARIO_MODIFICACION, $this->id_usuario_modificacion);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFlujoEtapaQuery::create();
        $criteria->add(FlujoEtapaTableMap::COL_CLAVE, $this->clave);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getClave();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getClave();
    }

    /**
     * Generic method to set the primary key (clave column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setClave($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getClave();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \FlujoEtapa (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIdEtapa($this->getIdEtapa());
        $copyObj->setIdEtapaSiguiente($this->getIdEtapaSiguiente());
        $copyObj->setActivo($this->getActivo());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaModificacion($this->getFechaModificacion());
        $copyObj->setIdUsuarioModificacion($this->getIdUsuarioModificacion());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setClave(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \FlujoEtapa Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEtapa object.
     *
     * @param  ChildEtapa $v
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEtapaRelatedByIdEtapa(ChildEtapa $v = null)
    {
        if ($v === null) {
            $this->setIdEtapa(NULL);
        } else {
            $this->setIdEtapa($v->getClave());
        }

        $this->aEtapaRelatedByIdEtapa = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEtapa object, it will not be re-added.
        if ($v !== null) {
            $v->addFlujoEtapaRelatedByIdEtapa($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEtapa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEtapa The associated ChildEtapa object.
     * @throws PropelException
     */
    public function getEtapaRelatedByIdEtapa(ConnectionInterface $con = null)
    {
        if ($this->aEtapaRelatedByIdEtapa === null && ($this->id_etapa != 0)) {
            $this->aEtapaRelatedByIdEtapa = ChildEtapaQuery::create()->findPk($this->id_etapa, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEtapaRelatedByIdEtapa->addFlujoEtapasRelatedByIdEtapa($this);
             */
        }

        return $this->aEtapaRelatedByIdEtapa;
    }

    /**
     * Declares an association between this object and a ChildEtapa object.
     *
     * @param  ChildEtapa $v
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEtapaRelatedByIdEtapaSiguiente(ChildEtapa $v = null)
    {
        if ($v === null) {
            $this->setIdEtapaSiguiente(NULL);
        } else {
            $this->setIdEtapaSiguiente($v->getClave());
        }

        $this->aEtapaRelatedByIdEtapaSiguiente = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEtapa object, it will not be re-added.
        if ($v !== null) {
            $v->addFlujoEtapaRelatedByIdEtapaSiguiente($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEtapa object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildEtapa The associated ChildEtapa object.
     * @throws PropelException
     */
    public function getEtapaRelatedByIdEtapaSiguiente(ConnectionInterface $con = null)
    {
        if ($this->aEtapaRelatedByIdEtapaSiguiente === null && ($this->id_etapa_siguiente != 0)) {
            $this->aEtapaRelatedByIdEtapaSiguiente = ChildEtapaQuery::create()->findPk($this->id_etapa_siguiente, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEtapaRelatedByIdEtapaSiguiente->addFlujoEtapasRelatedByIdEtapaSiguiente($this);
             */
        }

        return $this->aEtapaRelatedByIdEtapaSiguiente;
    }

    /**
     * Declares an association between this object and a ChildUsuario object.
     *
     * @param  ChildUsuario $v
     * @return $this|\FlujoEtapa The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUsuario(ChildUsuario $v = null)
    {
        if ($v === null) {
            $this->setIdUsuarioModificacion(NULL);
        } else {
            $this->setIdUsuarioModificacion($v->getClave());
        }

        $this->aUsuario = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUsuario object, it will not be re-added.
        if ($v !== null) {
            $v->addFlujoEtapa($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUsuario object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUsuario The associated ChildUsuario object.
     * @throws PropelException
     */
    public function getUsuario(ConnectionInterface $con = null)
    {
        if ($this->aUsuario === null && ($this->id_usuario_modificacion != 0)) {
            $this->aUsuario = ChildUsuarioQuery::create()->findPk($this->id_usuario_modificacion, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUsuario->addFlujoEtapas($this);
             */
        }

        return $this->aUsuario;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEtapaRelatedByIdEtapa) {
            $this->aEtapaRelatedByIdEtapa->removeFlujoEtapaRelatedByIdEtapa($this);
        }
        if (null !== $this->aEtapaRelatedByIdEtapaSiguiente) {
            $this->aEtapaRelatedByIdEtapaSiguiente->removeFlujoEtapaRelatedByIdEtapaSiguiente($this);
        }
        if (null !== $this->aUsuario) {
            $this->aUsuario->removeFlujoEtapa($this);
        }
        $this->clave = null;
        $this->id_etapa = null;
        $this->id_etapa_siguiente = null;
        $this->activo = null;
        $this->fecha_creacion = null;
        $this->fecha_modificacion = null;
        $this->id_usuario_modificacion = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aEtapaRelatedByIdEtapa = null;
        $this->aEtapaRelatedByIdEtapaSiguiente = null;
        $this->aUsuario = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FlujoEtapaTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
