<?php

namespace Base;

use \Usuario as ChildUsuario;
use \UsuarioQuery as ChildUsuarioQuery;
use \Exception;
use \PDO;
use Map\UsuarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'usuario' table.
 *
 *
 *
 * @method     ChildUsuarioQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildUsuarioQuery orderByUsuario($order = Criteria::ASC) Order by the usuario column
 * @method     ChildUsuarioQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUsuarioQuery orderByVigencia($order = Criteria::ASC) Order by the vigencia column
 * @method     ChildUsuarioQuery orderByDiasVigencia($order = Criteria::ASC) Order by the dias_vigencia column
 * @method     ChildUsuarioQuery orderByCorreo($order = Criteria::ASC) Order by the correo column
 * @method     ChildUsuarioQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 * @method     ChildUsuarioQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method     ChildUsuarioQuery orderByApaterno($order = Criteria::ASC) Order by the apaterno column
 * @method     ChildUsuarioQuery orderByAmaterno($order = Criteria::ASC) Order by the amaterno column
 * @method     ChildUsuarioQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildUsuarioQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildUsuarioQuery orderByUltimoAcceso($order = Criteria::ASC) Order by the ultimo_acceso column
 * @method     ChildUsuarioQuery orderByPasswordAntiguo($order = Criteria::ASC) Order by the password_antiguo column
 * @method     ChildUsuarioQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 * @method     ChildUsuarioQuery orderByIdEntidadOperativa($order = Criteria::ASC) Order by the id_entidad_operativa column
 *
 * @method     ChildUsuarioQuery groupByClave() Group by the clave column
 * @method     ChildUsuarioQuery groupByUsuario() Group by the usuario column
 * @method     ChildUsuarioQuery groupByPassword() Group by the password column
 * @method     ChildUsuarioQuery groupByVigencia() Group by the vigencia column
 * @method     ChildUsuarioQuery groupByDiasVigencia() Group by the dias_vigencia column
 * @method     ChildUsuarioQuery groupByCorreo() Group by the correo column
 * @method     ChildUsuarioQuery groupByActivo() Group by the activo column
 * @method     ChildUsuarioQuery groupByNombre() Group by the nombre column
 * @method     ChildUsuarioQuery groupByApaterno() Group by the apaterno column
 * @method     ChildUsuarioQuery groupByAmaterno() Group by the amaterno column
 * @method     ChildUsuarioQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildUsuarioQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildUsuarioQuery groupByUltimoAcceso() Group by the ultimo_acceso column
 * @method     ChildUsuarioQuery groupByPasswordAntiguo() Group by the password_antiguo column
 * @method     ChildUsuarioQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 * @method     ChildUsuarioQuery groupByIdEntidadOperativa() Group by the id_entidad_operativa column
 *
 * @method     ChildUsuarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsuarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsuarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsuarioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsuarioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsuarioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioRelatedByIdUsuarioModificacion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a LEFT JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a RIGHT JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioRelatedByIdUsuarioModificacion() Adds a INNER JOIN clause and with to the query using the UsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinEntidadOperativa($relationAlias = null) Adds a LEFT JOIN clause to the query using the EntidadOperativa relation
 * @method     ChildUsuarioQuery rightJoinEntidadOperativa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EntidadOperativa relation
 * @method     ChildUsuarioQuery innerJoinEntidadOperativa($relationAlias = null) Adds a INNER JOIN clause to the query using the EntidadOperativa relation
 *
 * @method     ChildUsuarioQuery joinWithEntidadOperativa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EntidadOperativa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEntidadOperativa() Adds a LEFT JOIN clause and with to the query using the EntidadOperativa relation
 * @method     ChildUsuarioQuery rightJoinWithEntidadOperativa() Adds a RIGHT JOIN clause and with to the query using the EntidadOperativa relation
 * @method     ChildUsuarioQuery innerJoinWithEntidadOperativa() Adds a INNER JOIN clause and with to the query using the EntidadOperativa relation
 *
 * @method     ChildUsuarioQuery leftJoinBeneficio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Beneficio relation
 * @method     ChildUsuarioQuery rightJoinBeneficio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Beneficio relation
 * @method     ChildUsuarioQuery innerJoinBeneficio($relationAlias = null) Adds a INNER JOIN clause to the query using the Beneficio relation
 *
 * @method     ChildUsuarioQuery joinWithBeneficio($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Beneficio relation
 *
 * @method     ChildUsuarioQuery leftJoinWithBeneficio() Adds a LEFT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildUsuarioQuery rightJoinWithBeneficio() Adds a RIGHT JOIN clause and with to the query using the Beneficio relation
 * @method     ChildUsuarioQuery innerJoinWithBeneficio() Adds a INNER JOIN clause and with to the query using the Beneficio relation
 *
 * @method     ChildUsuarioQuery leftJoinEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the Etapa relation
 * @method     ChildUsuarioQuery rightJoinEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Etapa relation
 * @method     ChildUsuarioQuery innerJoinEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the Etapa relation
 *
 * @method     ChildUsuarioQuery joinWithEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Etapa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithEtapa() Adds a LEFT JOIN clause and with to the query using the Etapa relation
 * @method     ChildUsuarioQuery rightJoinWithEtapa() Adds a RIGHT JOIN clause and with to the query using the Etapa relation
 * @method     ChildUsuarioQuery innerJoinWithEtapa() Adds a INNER JOIN clause and with to the query using the Etapa relation
 *
 * @method     ChildUsuarioQuery leftJoinFlujoEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the FlujoEtapa relation
 * @method     ChildUsuarioQuery rightJoinFlujoEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FlujoEtapa relation
 * @method     ChildUsuarioQuery innerJoinFlujoEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the FlujoEtapa relation
 *
 * @method     ChildUsuarioQuery joinWithFlujoEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FlujoEtapa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithFlujoEtapa() Adds a LEFT JOIN clause and with to the query using the FlujoEtapa relation
 * @method     ChildUsuarioQuery rightJoinWithFlujoEtapa() Adds a RIGHT JOIN clause and with to the query using the FlujoEtapa relation
 * @method     ChildUsuarioQuery innerJoinWithFlujoEtapa() Adds a INNER JOIN clause and with to the query using the FlujoEtapa relation
 *
 * @method     ChildUsuarioQuery leftJoinLogin($relationAlias = null) Adds a LEFT JOIN clause to the query using the Login relation
 * @method     ChildUsuarioQuery rightJoinLogin($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Login relation
 * @method     ChildUsuarioQuery innerJoinLogin($relationAlias = null) Adds a INNER JOIN clause to the query using the Login relation
 *
 * @method     ChildUsuarioQuery joinWithLogin($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Login relation
 *
 * @method     ChildUsuarioQuery leftJoinWithLogin() Adds a LEFT JOIN clause and with to the query using the Login relation
 * @method     ChildUsuarioQuery rightJoinWithLogin() Adds a RIGHT JOIN clause and with to the query using the Login relation
 * @method     ChildUsuarioQuery innerJoinWithLogin() Adds a INNER JOIN clause and with to the query using the Login relation
 *
 * @method     ChildUsuarioQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildUsuarioQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildUsuarioQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildUsuarioQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildUsuarioQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildUsuarioQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     ChildUsuarioQuery leftJoinProgramaRequisito($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildUsuarioQuery rightJoinProgramaRequisito($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProgramaRequisito relation
 * @method     ChildUsuarioQuery innerJoinProgramaRequisito($relationAlias = null) Adds a INNER JOIN clause to the query using the ProgramaRequisito relation
 *
 * @method     ChildUsuarioQuery joinWithProgramaRequisito($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildUsuarioQuery leftJoinWithProgramaRequisito() Adds a LEFT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildUsuarioQuery rightJoinWithProgramaRequisito() Adds a RIGHT JOIN clause and with to the query using the ProgramaRequisito relation
 * @method     ChildUsuarioQuery innerJoinWithProgramaRequisito() Adds a INNER JOIN clause and with to the query using the ProgramaRequisito relation
 *
 * @method     ChildUsuarioQuery leftJoinRestablecerPassword($relationAlias = null) Adds a LEFT JOIN clause to the query using the RestablecerPassword relation
 * @method     ChildUsuarioQuery rightJoinRestablecerPassword($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RestablecerPassword relation
 * @method     ChildUsuarioQuery innerJoinRestablecerPassword($relationAlias = null) Adds a INNER JOIN clause to the query using the RestablecerPassword relation
 *
 * @method     ChildUsuarioQuery joinWithRestablecerPassword($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RestablecerPassword relation
 *
 * @method     ChildUsuarioQuery leftJoinWithRestablecerPassword() Adds a LEFT JOIN clause and with to the query using the RestablecerPassword relation
 * @method     ChildUsuarioQuery rightJoinWithRestablecerPassword() Adds a RIGHT JOIN clause and with to the query using the RestablecerPassword relation
 * @method     ChildUsuarioQuery innerJoinWithRestablecerPassword() Adds a INNER JOIN clause and with to the query using the RestablecerPassword relation
 *
 * @method     ChildUsuarioQuery leftJoinRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rol relation
 * @method     ChildUsuarioQuery rightJoinRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rol relation
 * @method     ChildUsuarioQuery innerJoinRol($relationAlias = null) Adds a INNER JOIN clause to the query using the Rol relation
 *
 * @method     ChildUsuarioQuery joinWithRol($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rol relation
 *
 * @method     ChildUsuarioQuery leftJoinWithRol() Adds a LEFT JOIN clause and with to the query using the Rol relation
 * @method     ChildUsuarioQuery rightJoinWithRol() Adds a RIGHT JOIN clause and with to the query using the Rol relation
 * @method     ChildUsuarioQuery innerJoinWithRol() Adds a INNER JOIN clause and with to the query using the Rol relation
 *
 * @method     ChildUsuarioQuery leftJoinRolPermiso($relationAlias = null) Adds a LEFT JOIN clause to the query using the RolPermiso relation
 * @method     ChildUsuarioQuery rightJoinRolPermiso($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RolPermiso relation
 * @method     ChildUsuarioQuery innerJoinRolPermiso($relationAlias = null) Adds a INNER JOIN clause to the query using the RolPermiso relation
 *
 * @method     ChildUsuarioQuery joinWithRolPermiso($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RolPermiso relation
 *
 * @method     ChildUsuarioQuery leftJoinWithRolPermiso() Adds a LEFT JOIN clause and with to the query using the RolPermiso relation
 * @method     ChildUsuarioQuery rightJoinWithRolPermiso() Adds a RIGHT JOIN clause and with to the query using the RolPermiso relation
 * @method     ChildUsuarioQuery innerJoinWithRolPermiso() Adds a INNER JOIN clause and with to the query using the RolPermiso relation
 *
 * @method     ChildUsuarioQuery leftJoinRolUsuarioRelatedByIdUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the RolUsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinRolUsuarioRelatedByIdUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RolUsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinRolUsuarioRelatedByIdUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the RolUsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery joinWithRolUsuarioRelatedByIdUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RolUsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinWithRolUsuarioRelatedByIdUsuario() Adds a LEFT JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinWithRolUsuarioRelatedByIdUsuario() Adds a RIGHT JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinWithRolUsuarioRelatedByIdUsuario() Adds a INNER JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinRolUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinRolUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinRolUsuarioRelatedByIdUsuarioModificacion($relationAlias = null) Adds a INNER JOIN clause to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery joinWithRolUsuarioRelatedByIdUsuarioModificacion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinWithRolUsuarioRelatedByIdUsuarioModificacion() Adds a LEFT JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinWithRolUsuarioRelatedByIdUsuarioModificacion() Adds a RIGHT JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinWithRolUsuarioRelatedByIdUsuarioModificacion() Adds a INNER JOIN clause and with to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioRelatedByClave($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioRelatedByClave relation
 * @method     ChildUsuarioQuery rightJoinUsuarioRelatedByClave($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioRelatedByClave relation
 * @method     ChildUsuarioQuery innerJoinUsuarioRelatedByClave($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioRelatedByClave relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioRelatedByClave($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioRelatedByClave relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioRelatedByClave() Adds a LEFT JOIN clause and with to the query using the UsuarioRelatedByClave relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioRelatedByClave() Adds a RIGHT JOIN clause and with to the query using the UsuarioRelatedByClave relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioRelatedByClave() Adds a INNER JOIN clause and with to the query using the UsuarioRelatedByClave relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioQuery rightJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioQuery innerJoinUsuarioConfiguracionEtapa($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioConfiguracionEtapa($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioConfiguracionEtapa() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioConfiguracionEtapa() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioConfiguracionEtapa() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionEtapa relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioConfiguracionProgramaRelatedByIdUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinUsuarioConfiguracionProgramaRelatedByIdUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinUsuarioConfiguracionProgramaRelatedByIdUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioConfiguracionProgramaRelatedByIdUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuario() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuario() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuario() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion() Adds a LEFT JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion() Adds a RIGHT JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion() Adds a INNER JOIN clause and with to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioPermisoRelatedByIdUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinUsuarioPermisoRelatedByIdUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinUsuarioPermisoRelatedByIdUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioPermisoRelatedByIdUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioPermisoRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioPermisoRelatedByIdUsuario() Adds a LEFT JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioPermisoRelatedByIdUsuario() Adds a RIGHT JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuario relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioPermisoRelatedByIdUsuario() Adds a INNER JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuario relation
 *
 * @method     ChildUsuarioQuery leftJoinUsuarioPermisoRelatedByIdUsuarioModificacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinUsuarioPermisoRelatedByIdUsuarioModificacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinUsuarioPermisoRelatedByIdUsuarioModificacion($relationAlias = null) Adds a INNER JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery joinWithUsuarioPermisoRelatedByIdUsuarioModificacion($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 *
 * @method     ChildUsuarioQuery leftJoinWithUsuarioPermisoRelatedByIdUsuarioModificacion() Adds a LEFT JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery rightJoinWithUsuarioPermisoRelatedByIdUsuarioModificacion() Adds a RIGHT JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 * @method     ChildUsuarioQuery innerJoinWithUsuarioPermisoRelatedByIdUsuarioModificacion() Adds a INNER JOIN clause and with to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
 *
 * @method     \UsuarioQuery|\EntidadOperativaQuery|\BeneficioQuery|\EtapaQuery|\FlujoEtapaQuery|\LoginQuery|\ProgramaQuery|\ProgramaRequisitoQuery|\RestablecerPasswordQuery|\RolQuery|\RolPermisoQuery|\RolUsuarioQuery|\UsuarioConfiguracionEtapaQuery|\UsuarioConfiguracionProgramaQuery|\UsuarioPermisoQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsuario|null findOne(ConnectionInterface $con = null) Return the first ChildUsuario matching the query
 * @method     ChildUsuario findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsuario matching the query, or a new ChildUsuario object populated from the query conditions when no match is found
 *
 * @method     ChildUsuario|null findOneByClave(int $clave) Return the first ChildUsuario filtered by the clave column
 * @method     ChildUsuario|null findOneByUsuario(string $usuario) Return the first ChildUsuario filtered by the usuario column
 * @method     ChildUsuario|null findOneByPassword(string $password) Return the first ChildUsuario filtered by the password column
 * @method     ChildUsuario|null findOneByVigencia(int $vigencia) Return the first ChildUsuario filtered by the vigencia column
 * @method     ChildUsuario|null findOneByDiasVigencia(int $dias_vigencia) Return the first ChildUsuario filtered by the dias_vigencia column
 * @method     ChildUsuario|null findOneByCorreo(string $correo) Return the first ChildUsuario filtered by the correo column
 * @method     ChildUsuario|null findOneByActivo(int $activo) Return the first ChildUsuario filtered by the activo column
 * @method     ChildUsuario|null findOneByNombre(string $nombre) Return the first ChildUsuario filtered by the nombre column
 * @method     ChildUsuario|null findOneByApaterno(string $apaterno) Return the first ChildUsuario filtered by the apaterno column
 * @method     ChildUsuario|null findOneByAmaterno(string $amaterno) Return the first ChildUsuario filtered by the amaterno column
 * @method     ChildUsuario|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildUsuario filtered by the fecha_creacion column
 * @method     ChildUsuario|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildUsuario filtered by the fecha_modificacion column
 * @method     ChildUsuario|null findOneByUltimoAcceso(string $ultimo_acceso) Return the first ChildUsuario filtered by the ultimo_acceso column
 * @method     ChildUsuario|null findOneByPasswordAntiguo(string $password_antiguo) Return the first ChildUsuario filtered by the password_antiguo column
 * @method     ChildUsuario|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildUsuario filtered by the id_usuario_modificacion column
 * @method     ChildUsuario|null findOneByIdEntidadOperativa(int $id_entidad_operativa) Return the first ChildUsuario filtered by the id_entidad_operativa column *

 * @method     ChildUsuario requirePk($key, ConnectionInterface $con = null) Return the ChildUsuario by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOne(ConnectionInterface $con = null) Return the first ChildUsuario matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuario requireOneByClave(int $clave) Return the first ChildUsuario filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByUsuario(string $usuario) Return the first ChildUsuario filtered by the usuario column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByPassword(string $password) Return the first ChildUsuario filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByVigencia(int $vigencia) Return the first ChildUsuario filtered by the vigencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByDiasVigencia(int $dias_vigencia) Return the first ChildUsuario filtered by the dias_vigencia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByCorreo(string $correo) Return the first ChildUsuario filtered by the correo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByActivo(int $activo) Return the first ChildUsuario filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByNombre(string $nombre) Return the first ChildUsuario filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByApaterno(string $apaterno) Return the first ChildUsuario filtered by the apaterno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByAmaterno(string $amaterno) Return the first ChildUsuario filtered by the amaterno column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildUsuario filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildUsuario filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByUltimoAcceso(string $ultimo_acceso) Return the first ChildUsuario filtered by the ultimo_acceso column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByPasswordAntiguo(string $password_antiguo) Return the first ChildUsuario filtered by the password_antiguo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildUsuario filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsuario requireOneByIdEntidadOperativa(int $id_entidad_operativa) Return the first ChildUsuario filtered by the id_entidad_operativa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsuario[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsuario objects based on current ModelCriteria
 * @method     ChildUsuario[]|ObjectCollection findByClave(int $clave) Return ChildUsuario objects filtered by the clave column
 * @method     ChildUsuario[]|ObjectCollection findByUsuario(string $usuario) Return ChildUsuario objects filtered by the usuario column
 * @method     ChildUsuario[]|ObjectCollection findByPassword(string $password) Return ChildUsuario objects filtered by the password column
 * @method     ChildUsuario[]|ObjectCollection findByVigencia(int $vigencia) Return ChildUsuario objects filtered by the vigencia column
 * @method     ChildUsuario[]|ObjectCollection findByDiasVigencia(int $dias_vigencia) Return ChildUsuario objects filtered by the dias_vigencia column
 * @method     ChildUsuario[]|ObjectCollection findByCorreo(string $correo) Return ChildUsuario objects filtered by the correo column
 * @method     ChildUsuario[]|ObjectCollection findByActivo(int $activo) Return ChildUsuario objects filtered by the activo column
 * @method     ChildUsuario[]|ObjectCollection findByNombre(string $nombre) Return ChildUsuario objects filtered by the nombre column
 * @method     ChildUsuario[]|ObjectCollection findByApaterno(string $apaterno) Return ChildUsuario objects filtered by the apaterno column
 * @method     ChildUsuario[]|ObjectCollection findByAmaterno(string $amaterno) Return ChildUsuario objects filtered by the amaterno column
 * @method     ChildUsuario[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildUsuario objects filtered by the fecha_creacion column
 * @method     ChildUsuario[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildUsuario objects filtered by the fecha_modificacion column
 * @method     ChildUsuario[]|ObjectCollection findByUltimoAcceso(string $ultimo_acceso) Return ChildUsuario objects filtered by the ultimo_acceso column
 * @method     ChildUsuario[]|ObjectCollection findByPasswordAntiguo(string $password_antiguo) Return ChildUsuario objects filtered by the password_antiguo column
 * @method     ChildUsuario[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildUsuario objects filtered by the id_usuario_modificacion column
 * @method     ChildUsuario[]|ObjectCollection findByIdEntidadOperativa(int $id_entidad_operativa) Return ChildUsuario objects filtered by the id_entidad_operativa column
 * @method     ChildUsuario[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsuarioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\UsuarioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\Usuario', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsuarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsuarioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsuarioQuery) {
            return $criteria;
        }
        $query = new ChildUsuarioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsuario|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsuarioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuario A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, usuario, password, vigencia, dias_vigencia, correo, activo, nombre, apaterno, amaterno, fecha_creacion, fecha_modificacion, ultimo_acceso, password_antiguo, id_usuario_modificacion, id_entidad_operativa FROM usuario WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsuario $obj */
            $obj = new ChildUsuario();
            $obj->hydrate($row);
            UsuarioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsuario|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the usuario column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuario('fooValue');   // WHERE usuario = 'fooValue'
     * $query->filterByUsuario('%fooValue%', Criteria::LIKE); // WHERE usuario LIKE '%fooValue%'
     * </code>
     *
     * @param     string $usuario The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($usuario)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_USUARIO, $usuario, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the vigencia column
     *
     * Example usage:
     * <code>
     * $query->filterByVigencia(1234); // WHERE vigencia = 1234
     * $query->filterByVigencia(array(12, 34)); // WHERE vigencia IN (12, 34)
     * $query->filterByVigencia(array('min' => 12)); // WHERE vigencia > 12
     * </code>
     *
     * @param     mixed $vigencia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByVigencia($vigencia = null, $comparison = null)
    {
        if (is_array($vigencia)) {
            $useMinMax = false;
            if (isset($vigencia['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_VIGENCIA, $vigencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vigencia['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_VIGENCIA, $vigencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_VIGENCIA, $vigencia, $comparison);
    }

    /**
     * Filter the query on the dias_vigencia column
     *
     * Example usage:
     * <code>
     * $query->filterByDiasVigencia(1234); // WHERE dias_vigencia = 1234
     * $query->filterByDiasVigencia(array(12, 34)); // WHERE dias_vigencia IN (12, 34)
     * $query->filterByDiasVigencia(array('min' => 12)); // WHERE dias_vigencia > 12
     * </code>
     *
     * @param     mixed $diasVigencia The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByDiasVigencia($diasVigencia = null, $comparison = null)
    {
        if (is_array($diasVigencia)) {
            $useMinMax = false;
            if (isset($diasVigencia['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DIAS_VIGENCIA, $diasVigencia['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($diasVigencia['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_DIAS_VIGENCIA, $diasVigencia['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_DIAS_VIGENCIA, $diasVigencia, $comparison);
    }

    /**
     * Filter the query on the correo column
     *
     * Example usage:
     * <code>
     * $query->filterByCorreo('fooValue');   // WHERE correo = 'fooValue'
     * $query->filterByCorreo('%fooValue%', Criteria::LIKE); // WHERE correo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $correo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByCorreo($correo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($correo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_CORREO, $correo, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the apaterno column
     *
     * Example usage:
     * <code>
     * $query->filterByApaterno('fooValue');   // WHERE apaterno = 'fooValue'
     * $query->filterByApaterno('%fooValue%', Criteria::LIKE); // WHERE apaterno LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apaterno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByApaterno($apaterno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apaterno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_APATERNO, $apaterno, $comparison);
    }

    /**
     * Filter the query on the amaterno column
     *
     * Example usage:
     * <code>
     * $query->filterByAmaterno('fooValue');   // WHERE amaterno = 'fooValue'
     * $query->filterByAmaterno('%fooValue%', Criteria::LIKE); // WHERE amaterno LIKE '%fooValue%'
     * </code>
     *
     * @param     string $amaterno The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByAmaterno($amaterno = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($amaterno)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_AMATERNO, $amaterno, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the ultimo_acceso column
     *
     * Example usage:
     * <code>
     * $query->filterByUltimoAcceso('2011-03-14'); // WHERE ultimo_acceso = '2011-03-14'
     * $query->filterByUltimoAcceso('now'); // WHERE ultimo_acceso = '2011-03-14'
     * $query->filterByUltimoAcceso(array('max' => 'yesterday')); // WHERE ultimo_acceso > '2011-03-13'
     * </code>
     *
     * @param     mixed $ultimoAcceso The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUltimoAcceso($ultimoAcceso = null, $comparison = null)
    {
        if (is_array($ultimoAcceso)) {
            $useMinMax = false;
            if (isset($ultimoAcceso['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ULTIMO_ACCESO, $ultimoAcceso['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ultimoAcceso['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ULTIMO_ACCESO, $ultimoAcceso['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_ULTIMO_ACCESO, $ultimoAcceso, $comparison);
    }

    /**
     * Filter the query on the password_antiguo column
     *
     * Example usage:
     * <code>
     * $query->filterByPasswordAntiguo('fooValue');   // WHERE password_antiguo = 'fooValue'
     * $query->filterByPasswordAntiguo('%fooValue%', Criteria::LIKE); // WHERE password_antiguo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $passwordAntiguo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPasswordAntiguo($passwordAntiguo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($passwordAntiguo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_PASSWORD_ANTIGUO, $passwordAntiguo, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuarioRelatedByIdUsuarioModificacion()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query on the id_entidad_operativa column
     *
     * Example usage:
     * <code>
     * $query->filterByIdEntidadOperativa(1234); // WHERE id_entidad_operativa = 1234
     * $query->filterByIdEntidadOperativa(array(12, 34)); // WHERE id_entidad_operativa IN (12, 34)
     * $query->filterByIdEntidadOperativa(array('min' => 12)); // WHERE id_entidad_operativa > 12
     * </code>
     *
     * @see       filterByEntidadOperativa()
     *
     * @param     mixed $idEntidadOperativa The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByIdEntidadOperativa($idEntidadOperativa = null, $comparison = null)
    {
        if (is_array($idEntidadOperativa)) {
            $useMinMax = false;
            if (isset($idEntidadOperativa['min'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idEntidadOperativa['max'])) {
                $this->addUsingAlias(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $idEntidadOperativa, $comparison);
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioRelatedByIdUsuarioModificacion($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuarioRelatedByIdUsuarioModificacion() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioRelatedByIdUsuarioModificacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioRelatedByIdUsuarioModificacion($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioRelatedByIdUsuarioModificacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioRelatedByIdUsuarioModificacion');
        }

        return $this;
    }

    /**
     * Use the UsuarioRelatedByIdUsuarioModificacion relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioRelatedByIdUsuarioModificacionQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsuarioRelatedByIdUsuarioModificacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioRelatedByIdUsuarioModificacion', '\UsuarioQuery');
    }

    /**
     * Use the UsuarioRelatedByIdUsuarioModificacion relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioRelatedByIdUsuarioModificacionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useUsuarioRelatedByIdUsuarioModificacionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \EntidadOperativa object
     *
     * @param \EntidadOperativa|ObjectCollection $entidadOperativa The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEntidadOperativa($entidadOperativa, $comparison = null)
    {
        if ($entidadOperativa instanceof \EntidadOperativa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $entidadOperativa->getClave(), $comparison);
        } elseif ($entidadOperativa instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, $entidadOperativa->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByEntidadOperativa() only accepts arguments of type \EntidadOperativa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EntidadOperativa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEntidadOperativa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EntidadOperativa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EntidadOperativa');
        }

        return $this;
    }

    /**
     * Use the EntidadOperativa relation EntidadOperativa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EntidadOperativaQuery A secondary query class using the current class as primary query
     */
    public function useEntidadOperativaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEntidadOperativa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EntidadOperativa', '\EntidadOperativaQuery');
    }

    /**
     * Use the EntidadOperativa relation EntidadOperativa object
     *
     * @param callable(\EntidadOperativaQuery):\EntidadOperativaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEntidadOperativaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEntidadOperativaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Beneficio object
     *
     * @param \Beneficio|ObjectCollection $beneficio the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByBeneficio($beneficio, $comparison = null)
    {
        if ($beneficio instanceof \Beneficio) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $beneficio->getIdUsuarioModificacion(), $comparison);
        } elseif ($beneficio instanceof ObjectCollection) {
            return $this
                ->useBeneficioQuery()
                ->filterByPrimaryKeys($beneficio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBeneficio() only accepts arguments of type \Beneficio or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Beneficio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinBeneficio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Beneficio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Beneficio');
        }

        return $this;
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \BeneficioQuery A secondary query class using the current class as primary query
     */
    public function useBeneficioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBeneficio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Beneficio', '\BeneficioQuery');
    }

    /**
     * Use the Beneficio relation Beneficio object
     *
     * @param callable(\BeneficioQuery):\BeneficioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withBeneficioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useBeneficioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Etapa object
     *
     * @param \Etapa|ObjectCollection $etapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByEtapa($etapa, $comparison = null)
    {
        if ($etapa instanceof \Etapa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $etapa->getIdUsuarioModificacion(), $comparison);
        } elseif ($etapa instanceof ObjectCollection) {
            return $this
                ->useEtapaQuery()
                ->filterByPrimaryKeys($etapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEtapa() only accepts arguments of type \Etapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Etapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Etapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Etapa');
        }

        return $this;
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \EtapaQuery A secondary query class using the current class as primary query
     */
    public function useEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Etapa', '\EtapaQuery');
    }

    /**
     * Use the Etapa relation Etapa object
     *
     * @param callable(\EtapaQuery):\EtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \FlujoEtapa object
     *
     * @param \FlujoEtapa|ObjectCollection $flujoEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByFlujoEtapa($flujoEtapa, $comparison = null)
    {
        if ($flujoEtapa instanceof \FlujoEtapa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $flujoEtapa->getIdUsuarioModificacion(), $comparison);
        } elseif ($flujoEtapa instanceof ObjectCollection) {
            return $this
                ->useFlujoEtapaQuery()
                ->filterByPrimaryKeys($flujoEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFlujoEtapa() only accepts arguments of type \FlujoEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FlujoEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinFlujoEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FlujoEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FlujoEtapa');
        }

        return $this;
    }

    /**
     * Use the FlujoEtapa relation FlujoEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \FlujoEtapaQuery A secondary query class using the current class as primary query
     */
    public function useFlujoEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFlujoEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FlujoEtapa', '\FlujoEtapaQuery');
    }

    /**
     * Use the FlujoEtapa relation FlujoEtapa object
     *
     * @param callable(\FlujoEtapaQuery):\FlujoEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withFlujoEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useFlujoEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Login object
     *
     * @param \Login|ObjectCollection $login the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByLogin($login, $comparison = null)
    {
        if ($login instanceof \Login) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $login->getIdUsuario(), $comparison);
        } elseif ($login instanceof ObjectCollection) {
            return $this
                ->useLoginQuery()
                ->filterByPrimaryKeys($login->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLogin() only accepts arguments of type \Login or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Login relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinLogin($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Login');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Login');
        }

        return $this;
    }

    /**
     * Use the Login relation Login object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \LoginQuery A secondary query class using the current class as primary query
     */
    public function useLoginQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLogin($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Login', '\LoginQuery');
    }

    /**
     * Use the Login relation Login object
     *
     * @param callable(\LoginQuery):\LoginQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withLoginQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useLoginQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $programa->getIdUsuarioModificacion(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            return $this
                ->useProgramaQuery()
                ->filterByPrimaryKeys($programa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \ProgramaRequisito object
     *
     * @param \ProgramaRequisito|ObjectCollection $programaRequisito the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByProgramaRequisito($programaRequisito, $comparison = null)
    {
        if ($programaRequisito instanceof \ProgramaRequisito) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $programaRequisito->getIdUsuarioModificacion(), $comparison);
        } elseif ($programaRequisito instanceof ObjectCollection) {
            return $this
                ->useProgramaRequisitoQuery()
                ->filterByPrimaryKeys($programaRequisito->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProgramaRequisito() only accepts arguments of type \ProgramaRequisito or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProgramaRequisito relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinProgramaRequisito($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProgramaRequisito');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProgramaRequisito');
        }

        return $this;
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaRequisitoQuery A secondary query class using the current class as primary query
     */
    public function useProgramaRequisitoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProgramaRequisito($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProgramaRequisito', '\ProgramaRequisitoQuery');
    }

    /**
     * Use the ProgramaRequisito relation ProgramaRequisito object
     *
     * @param callable(\ProgramaRequisitoQuery):\ProgramaRequisitoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaRequisitoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useProgramaRequisitoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \RestablecerPassword object
     *
     * @param \RestablecerPassword|ObjectCollection $restablecerPassword the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByRestablecerPassword($restablecerPassword, $comparison = null)
    {
        if ($restablecerPassword instanceof \RestablecerPassword) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $restablecerPassword->getIdUsuario(), $comparison);
        } elseif ($restablecerPassword instanceof ObjectCollection) {
            return $this
                ->useRestablecerPasswordQuery()
                ->filterByPrimaryKeys($restablecerPassword->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRestablecerPassword() only accepts arguments of type \RestablecerPassword or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RestablecerPassword relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinRestablecerPassword($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RestablecerPassword');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RestablecerPassword');
        }

        return $this;
    }

    /**
     * Use the RestablecerPassword relation RestablecerPassword object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RestablecerPasswordQuery A secondary query class using the current class as primary query
     */
    public function useRestablecerPasswordQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRestablecerPassword($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RestablecerPassword', '\RestablecerPasswordQuery');
    }

    /**
     * Use the RestablecerPassword relation RestablecerPassword object
     *
     * @param callable(\RestablecerPasswordQuery):\RestablecerPasswordQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRestablecerPasswordQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRestablecerPasswordQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Rol object
     *
     * @param \Rol|ObjectCollection $rol the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByRol($rol, $comparison = null)
    {
        if ($rol instanceof \Rol) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $rol->getIdUsuarioModificacion(), $comparison);
        } elseif ($rol instanceof ObjectCollection) {
            return $this
                ->useRolQuery()
                ->filterByPrimaryKeys($rol->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRol() only accepts arguments of type \Rol or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rol');
        }

        return $this;
    }

    /**
     * Use the Rol relation Rol object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolQuery A secondary query class using the current class as primary query
     */
    public function useRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rol', '\RolQuery');
    }

    /**
     * Use the Rol relation Rol object
     *
     * @param callable(\RolQuery):\RolQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRolQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRolQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \RolPermiso object
     *
     * @param \RolPermiso|ObjectCollection $rolPermiso the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByRolPermiso($rolPermiso, $comparison = null)
    {
        if ($rolPermiso instanceof \RolPermiso) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $rolPermiso->getIdUsuarioModificacion(), $comparison);
        } elseif ($rolPermiso instanceof ObjectCollection) {
            return $this
                ->useRolPermisoQuery()
                ->filterByPrimaryKeys($rolPermiso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRolPermiso() only accepts arguments of type \RolPermiso or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RolPermiso relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinRolPermiso($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RolPermiso');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RolPermiso');
        }

        return $this;
    }

    /**
     * Use the RolPermiso relation RolPermiso object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolPermisoQuery A secondary query class using the current class as primary query
     */
    public function useRolPermisoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRolPermiso($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RolPermiso', '\RolPermisoQuery');
    }

    /**
     * Use the RolPermiso relation RolPermiso object
     *
     * @param callable(\RolPermisoQuery):\RolPermisoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRolPermisoQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRolPermisoQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \RolUsuario object
     *
     * @param \RolUsuario|ObjectCollection $rolUsuario the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByRolUsuarioRelatedByIdUsuario($rolUsuario, $comparison = null)
    {
        if ($rolUsuario instanceof \RolUsuario) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $rolUsuario->getIdUsuario(), $comparison);
        } elseif ($rolUsuario instanceof ObjectCollection) {
            return $this
                ->useRolUsuarioRelatedByIdUsuarioQuery()
                ->filterByPrimaryKeys($rolUsuario->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRolUsuarioRelatedByIdUsuario() only accepts arguments of type \RolUsuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RolUsuarioRelatedByIdUsuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinRolUsuarioRelatedByIdUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RolUsuarioRelatedByIdUsuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RolUsuarioRelatedByIdUsuario');
        }

        return $this;
    }

    /**
     * Use the RolUsuarioRelatedByIdUsuario relation RolUsuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolUsuarioQuery A secondary query class using the current class as primary query
     */
    public function useRolUsuarioRelatedByIdUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRolUsuarioRelatedByIdUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RolUsuarioRelatedByIdUsuario', '\RolUsuarioQuery');
    }

    /**
     * Use the RolUsuarioRelatedByIdUsuario relation RolUsuario object
     *
     * @param callable(\RolUsuarioQuery):\RolUsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRolUsuarioRelatedByIdUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRolUsuarioRelatedByIdUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \RolUsuario object
     *
     * @param \RolUsuario|ObjectCollection $rolUsuario the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByRolUsuarioRelatedByIdUsuarioModificacion($rolUsuario, $comparison = null)
    {
        if ($rolUsuario instanceof \RolUsuario) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $rolUsuario->getIdUsuarioModificacion(), $comparison);
        } elseif ($rolUsuario instanceof ObjectCollection) {
            return $this
                ->useRolUsuarioRelatedByIdUsuarioModificacionQuery()
                ->filterByPrimaryKeys($rolUsuario->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRolUsuarioRelatedByIdUsuarioModificacion() only accepts arguments of type \RolUsuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RolUsuarioRelatedByIdUsuarioModificacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinRolUsuarioRelatedByIdUsuarioModificacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RolUsuarioRelatedByIdUsuarioModificacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RolUsuarioRelatedByIdUsuarioModificacion');
        }

        return $this;
    }

    /**
     * Use the RolUsuarioRelatedByIdUsuarioModificacion relation RolUsuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolUsuarioQuery A secondary query class using the current class as primary query
     */
    public function useRolUsuarioRelatedByIdUsuarioModificacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRolUsuarioRelatedByIdUsuarioModificacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RolUsuarioRelatedByIdUsuarioModificacion', '\RolUsuarioQuery');
    }

    /**
     * Use the RolUsuarioRelatedByIdUsuarioModificacion relation RolUsuario object
     *
     * @param callable(\RolUsuarioQuery):\RolUsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRolUsuarioRelatedByIdUsuarioModificacionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRolUsuarioRelatedByIdUsuarioModificacionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioRelatedByClave($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuario->getIdUsuarioModificacion(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            return $this
                ->useUsuarioRelatedByClaveQuery()
                ->filterByPrimaryKeys($usuario->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioRelatedByClave() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioRelatedByClave relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioRelatedByClave($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioRelatedByClave');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioRelatedByClave');
        }

        return $this;
    }

    /**
     * Use the UsuarioRelatedByClave relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioRelatedByClaveQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsuarioRelatedByClave($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioRelatedByClave', '\UsuarioQuery');
    }

    /**
     * Use the UsuarioRelatedByClave relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioRelatedByClaveQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useUsuarioRelatedByClaveQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionEtapa object
     *
     * @param \UsuarioConfiguracionEtapa|ObjectCollection $usuarioConfiguracionEtapa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionEtapa($usuarioConfiguracionEtapa, $comparison = null)
    {
        if ($usuarioConfiguracionEtapa instanceof \UsuarioConfiguracionEtapa) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuarioConfiguracionEtapa->getIdUsuarioModificacion(), $comparison);
        } elseif ($usuarioConfiguracionEtapa instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionEtapaQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionEtapa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionEtapa() only accepts arguments of type \UsuarioConfiguracionEtapa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionEtapa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionEtapa($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionEtapa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionEtapa');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionEtapaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionEtapaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionEtapa($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionEtapa', '\UsuarioConfiguracionEtapaQuery');
    }

    /**
     * Use the UsuarioConfiguracionEtapa relation UsuarioConfiguracionEtapa object
     *
     * @param callable(\UsuarioConfiguracionEtapaQuery):\UsuarioConfiguracionEtapaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionEtapaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionEtapaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionPrograma object
     *
     * @param \UsuarioConfiguracionPrograma|ObjectCollection $usuarioConfiguracionPrograma the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionProgramaRelatedByIdUsuario($usuarioConfiguracionPrograma, $comparison = null)
    {
        if ($usuarioConfiguracionPrograma instanceof \UsuarioConfiguracionPrograma) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuarioConfiguracionPrograma->getIdUsuario(), $comparison);
        } elseif ($usuarioConfiguracionPrograma instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionProgramaRelatedByIdUsuarioQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionPrograma->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionProgramaRelatedByIdUsuario() only accepts arguments of type \UsuarioConfiguracionPrograma or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionProgramaRelatedByIdUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionProgramaRelatedByIdUsuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionProgramaRelatedByIdUsuario');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionProgramaRelatedByIdUsuario relation UsuarioConfiguracionPrograma object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionProgramaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionProgramaRelatedByIdUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionProgramaRelatedByIdUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionProgramaRelatedByIdUsuario', '\UsuarioConfiguracionProgramaQuery');
    }

    /**
     * Use the UsuarioConfiguracionProgramaRelatedByIdUsuario relation UsuarioConfiguracionPrograma object
     *
     * @param callable(\UsuarioConfiguracionProgramaQuery):\UsuarioConfiguracionProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionProgramaRelatedByIdUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionProgramaRelatedByIdUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioConfiguracionPrograma object
     *
     * @param \UsuarioConfiguracionPrograma|ObjectCollection $usuarioConfiguracionPrograma the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($usuarioConfiguracionPrograma, $comparison = null)
    {
        if ($usuarioConfiguracionPrograma instanceof \UsuarioConfiguracionPrograma) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuarioConfiguracionPrograma->getIdUsuarioModificacion(), $comparison);
        } elseif ($usuarioConfiguracionPrograma instanceof ObjectCollection) {
            return $this
                ->useUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacionQuery()
                ->filterByPrimaryKeys($usuarioConfiguracionPrograma->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion() only accepts arguments of type \UsuarioConfiguracionPrograma or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion');
        }

        return $this;
    }

    /**
     * Use the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation UsuarioConfiguracionPrograma object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioConfiguracionProgramaQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion', '\UsuarioConfiguracionProgramaQuery');
    }

    /**
     * Use the UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion relation UsuarioConfiguracionPrograma object
     *
     * @param callable(\UsuarioConfiguracionProgramaQuery):\UsuarioConfiguracionProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioConfiguracionProgramaRelatedByIdUsuarioModificacionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioPermiso object
     *
     * @param \UsuarioPermiso|ObjectCollection $usuarioPermiso the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioPermisoRelatedByIdUsuario($usuarioPermiso, $comparison = null)
    {
        if ($usuarioPermiso instanceof \UsuarioPermiso) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuarioPermiso->getIdUsuario(), $comparison);
        } elseif ($usuarioPermiso instanceof ObjectCollection) {
            return $this
                ->useUsuarioPermisoRelatedByIdUsuarioQuery()
                ->filterByPrimaryKeys($usuarioPermiso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioPermisoRelatedByIdUsuario() only accepts arguments of type \UsuarioPermiso or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioPermisoRelatedByIdUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioPermisoRelatedByIdUsuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioPermisoRelatedByIdUsuario');
        }

        return $this;
    }

    /**
     * Use the UsuarioPermisoRelatedByIdUsuario relation UsuarioPermiso object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioPermisoQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioPermisoRelatedByIdUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioPermisoRelatedByIdUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioPermisoRelatedByIdUsuario', '\UsuarioPermisoQuery');
    }

    /**
     * Use the UsuarioPermisoRelatedByIdUsuario relation UsuarioPermiso object
     *
     * @param callable(\UsuarioPermisoQuery):\UsuarioPermisoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioPermisoRelatedByIdUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioPermisoRelatedByIdUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \UsuarioPermiso object
     *
     * @param \UsuarioPermiso|ObjectCollection $usuarioPermiso the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioPermisoRelatedByIdUsuarioModificacion($usuarioPermiso, $comparison = null)
    {
        if ($usuarioPermiso instanceof \UsuarioPermiso) {
            return $this
                ->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuarioPermiso->getIdUsuarioModificacion(), $comparison);
        } elseif ($usuarioPermiso instanceof ObjectCollection) {
            return $this
                ->useUsuarioPermisoRelatedByIdUsuarioModificacionQuery()
                ->filterByPrimaryKeys($usuarioPermiso->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsuarioPermisoRelatedByIdUsuarioModificacion() only accepts arguments of type \UsuarioPermiso or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsuarioPermisoRelatedByIdUsuarioModificacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function joinUsuarioPermisoRelatedByIdUsuarioModificacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsuarioPermisoRelatedByIdUsuarioModificacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsuarioPermisoRelatedByIdUsuarioModificacion');
        }

        return $this;
    }

    /**
     * Use the UsuarioPermisoRelatedByIdUsuarioModificacion relation UsuarioPermiso object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioPermisoQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioPermisoRelatedByIdUsuarioModificacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuarioPermisoRelatedByIdUsuarioModificacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsuarioPermisoRelatedByIdUsuarioModificacion', '\UsuarioPermisoQuery');
    }

    /**
     * Use the UsuarioPermisoRelatedByIdUsuarioModificacion relation UsuarioPermiso object
     *
     * @param callable(\UsuarioPermisoQuery):\UsuarioPermisoQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioPermisoRelatedByIdUsuarioModificacionQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioPermisoRelatedByIdUsuarioModificacionQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsuario $usuario Object to remove from the list of results
     *
     * @return $this|ChildUsuarioQuery The current query, for fluid interface
     */
    public function prune($usuario = null)
    {
        if ($usuario) {
            $this->addUsingAlias(UsuarioTableMap::COL_CLAVE, $usuario->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the usuario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsuarioTableMap::clearInstancePool();
            UsuarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsuarioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsuarioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsuarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsuarioQuery
