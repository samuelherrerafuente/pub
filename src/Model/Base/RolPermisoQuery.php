<?php

namespace Base;

use \RolPermiso as ChildRolPermiso;
use \RolPermisoQuery as ChildRolPermisoQuery;
use \Exception;
use \PDO;
use Map\RolPermisoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'rol_permiso' table.
 *
 *
 *
 * @method     ChildRolPermisoQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildRolPermisoQuery orderByIdRol($order = Criteria::ASC) Order by the id_rol column
 * @method     ChildRolPermisoQuery orderByIdPermiso($order = Criteria::ASC) Order by the id_permiso column
 * @method     ChildRolPermisoQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method     ChildRolPermisoQuery orderByFechaModificacion($order = Criteria::ASC) Order by the fecha_modificacion column
 * @method     ChildRolPermisoQuery orderByIdUsuarioModificacion($order = Criteria::ASC) Order by the id_usuario_modificacion column
 * @method     ChildRolPermisoQuery orderByActivo($order = Criteria::ASC) Order by the activo column
 *
 * @method     ChildRolPermisoQuery groupByClave() Group by the clave column
 * @method     ChildRolPermisoQuery groupByIdRol() Group by the id_rol column
 * @method     ChildRolPermisoQuery groupByIdPermiso() Group by the id_permiso column
 * @method     ChildRolPermisoQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method     ChildRolPermisoQuery groupByFechaModificacion() Group by the fecha_modificacion column
 * @method     ChildRolPermisoQuery groupByIdUsuarioModificacion() Group by the id_usuario_modificacion column
 * @method     ChildRolPermisoQuery groupByActivo() Group by the activo column
 *
 * @method     ChildRolPermisoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildRolPermisoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildRolPermisoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildRolPermisoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildRolPermisoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildRolPermisoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildRolPermisoQuery leftJoinRol($relationAlias = null) Adds a LEFT JOIN clause to the query using the Rol relation
 * @method     ChildRolPermisoQuery rightJoinRol($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Rol relation
 * @method     ChildRolPermisoQuery innerJoinRol($relationAlias = null) Adds a INNER JOIN clause to the query using the Rol relation
 *
 * @method     ChildRolPermisoQuery joinWithRol($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Rol relation
 *
 * @method     ChildRolPermisoQuery leftJoinWithRol() Adds a LEFT JOIN clause and with to the query using the Rol relation
 * @method     ChildRolPermisoQuery rightJoinWithRol() Adds a RIGHT JOIN clause and with to the query using the Rol relation
 * @method     ChildRolPermisoQuery innerJoinWithRol() Adds a INNER JOIN clause and with to the query using the Rol relation
 *
 * @method     ChildRolPermisoQuery leftJoinPermisos($relationAlias = null) Adds a LEFT JOIN clause to the query using the Permisos relation
 * @method     ChildRolPermisoQuery rightJoinPermisos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Permisos relation
 * @method     ChildRolPermisoQuery innerJoinPermisos($relationAlias = null) Adds a INNER JOIN clause to the query using the Permisos relation
 *
 * @method     ChildRolPermisoQuery joinWithPermisos($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Permisos relation
 *
 * @method     ChildRolPermisoQuery leftJoinWithPermisos() Adds a LEFT JOIN clause and with to the query using the Permisos relation
 * @method     ChildRolPermisoQuery rightJoinWithPermisos() Adds a RIGHT JOIN clause and with to the query using the Permisos relation
 * @method     ChildRolPermisoQuery innerJoinWithPermisos() Adds a INNER JOIN clause and with to the query using the Permisos relation
 *
 * @method     ChildRolPermisoQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method     ChildRolPermisoQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method     ChildRolPermisoQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method     ChildRolPermisoQuery joinWithUsuario($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Usuario relation
 *
 * @method     ChildRolPermisoQuery leftJoinWithUsuario() Adds a LEFT JOIN clause and with to the query using the Usuario relation
 * @method     ChildRolPermisoQuery rightJoinWithUsuario() Adds a RIGHT JOIN clause and with to the query using the Usuario relation
 * @method     ChildRolPermisoQuery innerJoinWithUsuario() Adds a INNER JOIN clause and with to the query using the Usuario relation
 *
 * @method     \RolQuery|\PermisosQuery|\UsuarioQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildRolPermiso|null findOne(ConnectionInterface $con = null) Return the first ChildRolPermiso matching the query
 * @method     ChildRolPermiso findOneOrCreate(ConnectionInterface $con = null) Return the first ChildRolPermiso matching the query, or a new ChildRolPermiso object populated from the query conditions when no match is found
 *
 * @method     ChildRolPermiso|null findOneByClave(int $clave) Return the first ChildRolPermiso filtered by the clave column
 * @method     ChildRolPermiso|null findOneByIdRol(int $id_rol) Return the first ChildRolPermiso filtered by the id_rol column
 * @method     ChildRolPermiso|null findOneByIdPermiso(int $id_permiso) Return the first ChildRolPermiso filtered by the id_permiso column
 * @method     ChildRolPermiso|null findOneByFechaCreacion(string $fecha_creacion) Return the first ChildRolPermiso filtered by the fecha_creacion column
 * @method     ChildRolPermiso|null findOneByFechaModificacion(string $fecha_modificacion) Return the first ChildRolPermiso filtered by the fecha_modificacion column
 * @method     ChildRolPermiso|null findOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildRolPermiso filtered by the id_usuario_modificacion column
 * @method     ChildRolPermiso|null findOneByActivo(int $activo) Return the first ChildRolPermiso filtered by the activo column *

 * @method     ChildRolPermiso requirePk($key, ConnectionInterface $con = null) Return the ChildRolPermiso by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOne(ConnectionInterface $con = null) Return the first ChildRolPermiso matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRolPermiso requireOneByClave(int $clave) Return the first ChildRolPermiso filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByIdRol(int $id_rol) Return the first ChildRolPermiso filtered by the id_rol column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByIdPermiso(int $id_permiso) Return the first ChildRolPermiso filtered by the id_permiso column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByFechaCreacion(string $fecha_creacion) Return the first ChildRolPermiso filtered by the fecha_creacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByFechaModificacion(string $fecha_modificacion) Return the first ChildRolPermiso filtered by the fecha_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByIdUsuarioModificacion(int $id_usuario_modificacion) Return the first ChildRolPermiso filtered by the id_usuario_modificacion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildRolPermiso requireOneByActivo(int $activo) Return the first ChildRolPermiso filtered by the activo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildRolPermiso[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildRolPermiso objects based on current ModelCriteria
 * @method     ChildRolPermiso[]|ObjectCollection findByClave(int $clave) Return ChildRolPermiso objects filtered by the clave column
 * @method     ChildRolPermiso[]|ObjectCollection findByIdRol(int $id_rol) Return ChildRolPermiso objects filtered by the id_rol column
 * @method     ChildRolPermiso[]|ObjectCollection findByIdPermiso(int $id_permiso) Return ChildRolPermiso objects filtered by the id_permiso column
 * @method     ChildRolPermiso[]|ObjectCollection findByFechaCreacion(string $fecha_creacion) Return ChildRolPermiso objects filtered by the fecha_creacion column
 * @method     ChildRolPermiso[]|ObjectCollection findByFechaModificacion(string $fecha_modificacion) Return ChildRolPermiso objects filtered by the fecha_modificacion column
 * @method     ChildRolPermiso[]|ObjectCollection findByIdUsuarioModificacion(int $id_usuario_modificacion) Return ChildRolPermiso objects filtered by the id_usuario_modificacion column
 * @method     ChildRolPermiso[]|ObjectCollection findByActivo(int $activo) Return ChildRolPermiso objects filtered by the activo column
 * @method     ChildRolPermiso[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class RolPermisoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\RolPermisoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\RolPermiso', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildRolPermisoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildRolPermisoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildRolPermisoQuery) {
            return $criteria;
        }
        $query = new ChildRolPermisoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildRolPermiso|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(RolPermisoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = RolPermisoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolPermiso A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, id_rol, id_permiso, fecha_creacion, fecha_modificacion, id_usuario_modificacion, activo FROM rol_permiso WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildRolPermiso $obj */
            $obj = new ChildRolPermiso();
            $obj->hydrate($row);
            RolPermisoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildRolPermiso|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the id_rol column
     *
     * Example usage:
     * <code>
     * $query->filterByIdRol(1234); // WHERE id_rol = 1234
     * $query->filterByIdRol(array(12, 34)); // WHERE id_rol IN (12, 34)
     * $query->filterByIdRol(array('min' => 12)); // WHERE id_rol > 12
     * </code>
     *
     * @see       filterByRol()
     *
     * @param     mixed $idRol The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByIdRol($idRol = null, $comparison = null)
    {
        if (is_array($idRol)) {
            $useMinMax = false;
            if (isset($idRol['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_ROL, $idRol['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idRol['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_ROL, $idRol['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_ID_ROL, $idRol, $comparison);
    }

    /**
     * Filter the query on the id_permiso column
     *
     * Example usage:
     * <code>
     * $query->filterByIdPermiso(1234); // WHERE id_permiso = 1234
     * $query->filterByIdPermiso(array(12, 34)); // WHERE id_permiso IN (12, 34)
     * $query->filterByIdPermiso(array('min' => 12)); // WHERE id_permiso > 12
     * </code>
     *
     * @see       filterByPermisos()
     *
     * @param     mixed $idPermiso The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByIdPermiso($idPermiso = null, $comparison = null)
    {
        if (is_array($idPermiso)) {
            $useMinMax = false;
            if (isset($idPermiso['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_PERMISO, $idPermiso['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idPermiso['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_PERMISO, $idPermiso['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_ID_PERMISO, $idPermiso, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaModificacion('2011-03-14'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion('now'); // WHERE fecha_modificacion = '2011-03-14'
     * $query->filterByFechaModificacion(array('max' => 'yesterday')); // WHERE fecha_modificacion > '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaModificacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByFechaModificacion($fechaModificacion = null, $comparison = null)
    {
        if (is_array($fechaModificacion)) {
            $useMinMax = false;
            if (isset($fechaModificacion['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaModificacion['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_FECHA_MODIFICACION, $fechaModificacion, $comparison);
    }

    /**
     * Filter the query on the id_usuario_modificacion column
     *
     * Example usage:
     * <code>
     * $query->filterByIdUsuarioModificacion(1234); // WHERE id_usuario_modificacion = 1234
     * $query->filterByIdUsuarioModificacion(array(12, 34)); // WHERE id_usuario_modificacion IN (12, 34)
     * $query->filterByIdUsuarioModificacion(array('min' => 12)); // WHERE id_usuario_modificacion > 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $idUsuarioModificacion The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByIdUsuarioModificacion($idUsuarioModificacion = null, $comparison = null)
    {
        if (is_array($idUsuarioModificacion)) {
            $useMinMax = false;
            if (isset($idUsuarioModificacion['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idUsuarioModificacion['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, $idUsuarioModificacion, $comparison);
    }

    /**
     * Filter the query on the activo column
     *
     * Example usage:
     * <code>
     * $query->filterByActivo(1234); // WHERE activo = 1234
     * $query->filterByActivo(array(12, 34)); // WHERE activo IN (12, 34)
     * $query->filterByActivo(array('min' => 12)); // WHERE activo > 12
     * </code>
     *
     * @param     mixed $activo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByActivo($activo = null, $comparison = null)
    {
        if (is_array($activo)) {
            $useMinMax = false;
            if (isset($activo['min'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ACTIVO, $activo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($activo['max'])) {
                $this->addUsingAlias(RolPermisoTableMap::COL_ACTIVO, $activo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RolPermisoTableMap::COL_ACTIVO, $activo, $comparison);
    }

    /**
     * Filter the query by a related \Rol object
     *
     * @param \Rol|ObjectCollection $rol The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByRol($rol, $comparison = null)
    {
        if ($rol instanceof \Rol) {
            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_ROL, $rol->getClave(), $comparison);
        } elseif ($rol instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_ROL, $rol->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByRol() only accepts arguments of type \Rol or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Rol relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function joinRol($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Rol');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Rol');
        }

        return $this;
    }

    /**
     * Use the Rol relation Rol object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \RolQuery A secondary query class using the current class as primary query
     */
    public function useRolQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRol($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Rol', '\RolQuery');
    }

    /**
     * Use the Rol relation Rol object
     *
     * @param callable(\RolQuery):\RolQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withRolQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useRolQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Permisos object
     *
     * @param \Permisos|ObjectCollection $permisos The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByPermisos($permisos, $comparison = null)
    {
        if ($permisos instanceof \Permisos) {
            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_PERMISO, $permisos->getClave(), $comparison);
        } elseif ($permisos instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_PERMISO, $permisos->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByPermisos() only accepts arguments of type \Permisos or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Permisos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function joinPermisos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Permisos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Permisos');
        }

        return $this;
    }

    /**
     * Use the Permisos relation Permisos object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \PermisosQuery A secondary query class using the current class as primary query
     */
    public function usePermisosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPermisos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Permisos', '\PermisosQuery');
    }

    /**
     * Use the Permisos relation Permisos object
     *
     * @param callable(\PermisosQuery):\PermisosQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withPermisosQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->usePermisosQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Filter the query by a related \Usuario object
     *
     * @param \Usuario|ObjectCollection $usuario The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildRolPermisoQuery The current query, for fluid interface
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof \Usuario) {
            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->getClave(), $comparison);
        } elseif ($usuario instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, $usuario->toKeyValue('PrimaryKey', 'Clave'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type \Usuario or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', '\UsuarioQuery');
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @param callable(\UsuarioQuery):\UsuarioQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUsuarioQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUsuarioQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildRolPermiso $rolPermiso Object to remove from the list of results
     *
     * @return $this|ChildRolPermisoQuery The current query, for fluid interface
     */
    public function prune($rolPermiso = null)
    {
        if ($rolPermiso) {
            $this->addUsingAlias(RolPermisoTableMap::COL_CLAVE, $rolPermiso->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the rol_permiso table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolPermisoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            RolPermisoTableMap::clearInstancePool();
            RolPermisoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolPermisoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(RolPermisoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            RolPermisoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            RolPermisoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // RolPermisoQuery
