<?php

namespace Base;

use \TipoBeneficiario as ChildTipoBeneficiario;
use \TipoBeneficiarioQuery as ChildTipoBeneficiarioQuery;
use \Exception;
use \PDO;
use Map\TipoBeneficiarioTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'tipo_beneficiario' table.
 *
 *
 *
 * @method     ChildTipoBeneficiarioQuery orderByClave($order = Criteria::ASC) Order by the clave column
 * @method     ChildTipoBeneficiarioQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 *
 * @method     ChildTipoBeneficiarioQuery groupByClave() Group by the clave column
 * @method     ChildTipoBeneficiarioQuery groupByNombre() Group by the nombre column
 *
 * @method     ChildTipoBeneficiarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTipoBeneficiarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTipoBeneficiarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTipoBeneficiarioQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTipoBeneficiarioQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTipoBeneficiarioQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTipoBeneficiarioQuery leftJoinPrograma($relationAlias = null) Adds a LEFT JOIN clause to the query using the Programa relation
 * @method     ChildTipoBeneficiarioQuery rightJoinPrograma($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Programa relation
 * @method     ChildTipoBeneficiarioQuery innerJoinPrograma($relationAlias = null) Adds a INNER JOIN clause to the query using the Programa relation
 *
 * @method     ChildTipoBeneficiarioQuery joinWithPrograma($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Programa relation
 *
 * @method     ChildTipoBeneficiarioQuery leftJoinWithPrograma() Adds a LEFT JOIN clause and with to the query using the Programa relation
 * @method     ChildTipoBeneficiarioQuery rightJoinWithPrograma() Adds a RIGHT JOIN clause and with to the query using the Programa relation
 * @method     ChildTipoBeneficiarioQuery innerJoinWithPrograma() Adds a INNER JOIN clause and with to the query using the Programa relation
 *
 * @method     \ProgramaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildTipoBeneficiario|null findOne(ConnectionInterface $con = null) Return the first ChildTipoBeneficiario matching the query
 * @method     ChildTipoBeneficiario findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTipoBeneficiario matching the query, or a new ChildTipoBeneficiario object populated from the query conditions when no match is found
 *
 * @method     ChildTipoBeneficiario|null findOneByClave(int $clave) Return the first ChildTipoBeneficiario filtered by the clave column
 * @method     ChildTipoBeneficiario|null findOneByNombre(string $nombre) Return the first ChildTipoBeneficiario filtered by the nombre column *

 * @method     ChildTipoBeneficiario requirePk($key, ConnectionInterface $con = null) Return the ChildTipoBeneficiario by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoBeneficiario requireOne(ConnectionInterface $con = null) Return the first ChildTipoBeneficiario matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoBeneficiario requireOneByClave(int $clave) Return the first ChildTipoBeneficiario filtered by the clave column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTipoBeneficiario requireOneByNombre(string $nombre) Return the first ChildTipoBeneficiario filtered by the nombre column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTipoBeneficiario[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTipoBeneficiario objects based on current ModelCriteria
 * @method     ChildTipoBeneficiario[]|ObjectCollection findByClave(int $clave) Return ChildTipoBeneficiario objects filtered by the clave column
 * @method     ChildTipoBeneficiario[]|ObjectCollection findByNombre(string $nombre) Return ChildTipoBeneficiario objects filtered by the nombre column
 * @method     ChildTipoBeneficiario[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TipoBeneficiarioQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Base\TipoBeneficiarioQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\TipoBeneficiario', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTipoBeneficiarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTipoBeneficiarioQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTipoBeneficiarioQuery) {
            return $criteria;
        }
        $query = new ChildTipoBeneficiarioQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTipoBeneficiario|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TipoBeneficiarioTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TipoBeneficiarioTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTipoBeneficiario A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT clave, nombre FROM tipo_beneficiario WHERE clave = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTipoBeneficiario $obj */
            $obj = new ChildTipoBeneficiario();
            $obj->hydrate($row);
            TipoBeneficiarioTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTipoBeneficiario|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the clave column
     *
     * Example usage:
     * <code>
     * $query->filterByClave(1234); // WHERE clave = 1234
     * $query->filterByClave(array(12, 34)); // WHERE clave IN (12, 34)
     * $query->filterByClave(array('min' => 12)); // WHERE clave > 12
     * </code>
     *
     * @param     mixed $clave The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function filterByClave($clave = null, $comparison = null)
    {
        if (is_array($clave)) {
            $useMinMax = false;
            if (isset($clave['min'])) {
                $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $clave['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($clave['max'])) {
                $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $clave['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $clave, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%', Criteria::LIKE); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TipoBeneficiarioTableMap::COL_NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query by a related \Programa object
     *
     * @param \Programa|ObjectCollection $programa the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function filterByPrograma($programa, $comparison = null)
    {
        if ($programa instanceof \Programa) {
            return $this
                ->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $programa->getIdTipoBebeficiario(), $comparison);
        } elseif ($programa instanceof ObjectCollection) {
            return $this
                ->useProgramaQuery()
                ->filterByPrimaryKeys($programa->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPrograma() only accepts arguments of type \Programa or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Programa relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function joinPrograma($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Programa');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Programa');
        }

        return $this;
    }

    /**
     * Use the Programa relation Programa object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \ProgramaQuery A secondary query class using the current class as primary query
     */
    public function useProgramaQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPrograma($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Programa', '\ProgramaQuery');
    }

    /**
     * Use the Programa relation Programa object
     *
     * @param callable(\ProgramaQuery):\ProgramaQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withProgramaQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::LEFT_JOIN
    ) {
        $relatedQuery = $this->useProgramaQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTipoBeneficiario $tipoBeneficiario Object to remove from the list of results
     *
     * @return $this|ChildTipoBeneficiarioQuery The current query, for fluid interface
     */
    public function prune($tipoBeneficiario = null)
    {
        if ($tipoBeneficiario) {
            $this->addUsingAlias(TipoBeneficiarioTableMap::COL_CLAVE, $tipoBeneficiario->getClave(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the tipo_beneficiario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoBeneficiarioTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TipoBeneficiarioTableMap::clearInstancePool();
            TipoBeneficiarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TipoBeneficiarioTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TipoBeneficiarioTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TipoBeneficiarioTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TipoBeneficiarioTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TipoBeneficiarioQuery
