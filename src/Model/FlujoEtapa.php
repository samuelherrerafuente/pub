<?php

use Base\FlujoEtapa as BaseFlujoEtapa;

/**
 * Skeleton subclass for representing a row from the 'flujo_etapa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class FlujoEtapa extends BaseFlujoEtapa
{

}
