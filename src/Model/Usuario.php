<?php

use Base\Usuario as BaseUsuario;

/**
 * Skeleton subclass for representing a row from the 'usuario' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Usuario extends BaseUsuario
{


  /**
   * Initializes the collUsuarioPermisosRelatedByIdUsuarioModificacion collection.
   *
   * By default this just sets the collUsuarioPermisosRelatedByIdUsuarioModificacion collection to an empty array (like clearcollUsuarioPermisosRelatedByIdUsuarioModificacion());
   * however, you may wish to override this method in your stub class to provide setting appropriate
   * to your application -- for example, setting the initial array to the values stored in database.
   *
   * @param      boolean $overrideExisting If set to true, the method call initializes
   *                                        the collection even if it is not empty
   *
   * @return void
   */
  public function initUsuarioPermisosRelatedByIdUsuario($overrideExisting = true)
  {
    if (null !== $this->collUsuarioPermisosRelatedByIdUsuario && !$overrideExisting) {
      return;
    }
    $this->collUsuarioPermisosRelatedByIdUsuario = UsuarioPermisoQuery::create()->findByIdUsuario($this->getClave());
    $this->collUsuarioPermisosRelatedByIdUsuario->setModel('\UsuarioPermiso');
    foreach($this->collUsuarioPermisosRelatedByIdUsuario as &$p){
      $p->getPermisos();
    }
  }

  /**
   * Initializes the collRolUsuariosRelatedByIdUsuario collection.
   *
   * By default this just sets the collRolUsuariosRelatedByIdUsuario collection to an empty array (like clearcollRolUsuariosRelatedByIdUsuario());
   * however, you may wish to override this method in your stub class to provide setting appropriate
   * to your application -- for example, setting the initial array to the values stored in database.
   *
   * @param      boolean $overrideExisting If set to true, the method call initializes
   *                                        the collection even if it is not empty
   *
   * @return void
   */
  public function initRolPermisos($overrideExisting = true)
  {
    if (null !== $this->collRolPermisos && !$overrideExisting) {
      return;
    }
    $this->collRolPermisos = RolPermisoQuery::create()
      ->useRolQuery()
      ->useRolUsuarioQuery()
      ->filterByActivo(1)
      ->filterByIdUsuario($this->getClave())
      ->endUse()
      ->endUse()
      ->leftJoinWithPermisos()
      ->findByActivo(1);
    $this->collRolPermisos->setModel('\RolPermiso');
  }

  /**
   * Initializes the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection.
   *
   * By default this just sets the collUsuarioConfiguracionProgramasRelatedByIdUsuario collection to an empty array (like clearcollUsuarioConfiguracionProgramasRelatedByIdUsuario());
   * however, you may wish to override this method in your stub class to provide setting appropriate
   * to your application -- for example, setting the initial array to the values stored in database.
   *
   * @param      boolean $overrideExisting If set to true, the method call initializes
   *                                        the collection even if it is not empty
   *
   * @return void
   */
  public function initUsuarioConfiguracionProgramasRelatedByIdUsuario($overrideExisting = true)
  {
    if (null !== $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario && !$overrideExisting) {
      return;
    }

    $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario = UsuarioConfiguracionProgramaQuery::create()->leftJoinWithPrograma()->findByIdUsuario($this->getClave());
    $this->collUsuarioConfiguracionProgramasRelatedByIdUsuario->setModel('\UsuarioConfiguracionPrograma');
  }
}
