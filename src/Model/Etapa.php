<?php

use Base\Etapa as BaseEtapa;

/**
 * Skeleton subclass for representing a row from the 'etapa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Etapa extends BaseEtapa
{

      /**
     * Initializes the collFlujoEtapasRelatedByIdEtapa collection.
     *
     * By default this just sets the collFlujoEtapasRelatedByIdEtapa collection to an empty array (like clearcollFlujoEtapasRelatedByIdEtapa());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
  
    public function initFlujoEtapasRelatedByIdEtapa($overrideExisting = true)
    {
        if (null !== $this->collFlujoEtapasRelatedByIdEtapa && !$overrideExisting) {
            return;
        }
        $this->collFlujoEtapasRelatedByIdEtapa = FlujoEtapaQuery::create()->filterByIdEtapa($this->getClave())->find();
        $this->collFlujoEtapasRelatedByIdEtapa->setModel('\FlujoEtapa');

    }

}
