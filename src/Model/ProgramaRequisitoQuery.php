<?php

use Base\ProgramaRequisitoQuery as BaseProgramaRequisitoQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'programa_requisito' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class ProgramaRequisitoQuery extends BaseProgramaRequisitoQuery
{

}
