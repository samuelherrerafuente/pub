<?php

use Base\ProgramaQuery as BaseProgramaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'programa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class ProgramaQuery extends BaseProgramaQuery
{

}
