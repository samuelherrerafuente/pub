<?php

namespace Map;

use \Usuario;
use \UsuarioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'usuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UsuarioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.UsuarioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'usuario';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Usuario';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Usuario';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'usuario.clave';

    /**
     * the column name for the usuario field
     */
    const COL_USUARIO = 'usuario.usuario';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'usuario.password';

    /**
     * the column name for the vigencia field
     */
    const COL_VIGENCIA = 'usuario.vigencia';

    /**
     * the column name for the dias_vigencia field
     */
    const COL_DIAS_VIGENCIA = 'usuario.dias_vigencia';

    /**
     * the column name for the correo field
     */
    const COL_CORREO = 'usuario.correo';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'usuario.activo';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'usuario.nombre';

    /**
     * the column name for the apaterno field
     */
    const COL_APATERNO = 'usuario.apaterno';

    /**
     * the column name for the amaterno field
     */
    const COL_AMATERNO = 'usuario.amaterno';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'usuario.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'usuario.fecha_modificacion';

    /**
     * the column name for the ultimo_acceso field
     */
    const COL_ULTIMO_ACCESO = 'usuario.ultimo_acceso';

    /**
     * the column name for the password_antiguo field
     */
    const COL_PASSWORD_ANTIGUO = 'usuario.password_antiguo';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'usuario.id_usuario_modificacion';

    /**
     * the column name for the id_entidad_operativa field
     */
    const COL_ID_ENTIDAD_OPERATIVA = 'usuario.id_entidad_operativa';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Usuario', 'Password', 'Vigencia', 'DiasVigencia', 'Correo', 'Activo', 'Nombre', 'Apaterno', 'Amaterno', 'FechaCreacion', 'FechaModificacion', 'UltimoAcceso', 'PasswordAntiguo', 'IdUsuarioModificacion', 'IdEntidadOperativa', ),
        self::TYPE_CAMELNAME     => array('clave', 'usuario', 'password', 'vigencia', 'diasVigencia', 'correo', 'activo', 'nombre', 'apaterno', 'amaterno', 'fechaCreacion', 'fechaModificacion', 'ultimoAcceso', 'passwordAntiguo', 'idUsuarioModificacion', 'idEntidadOperativa', ),
        self::TYPE_COLNAME       => array(UsuarioTableMap::COL_CLAVE, UsuarioTableMap::COL_USUARIO, UsuarioTableMap::COL_PASSWORD, UsuarioTableMap::COL_VIGENCIA, UsuarioTableMap::COL_DIAS_VIGENCIA, UsuarioTableMap::COL_CORREO, UsuarioTableMap::COL_ACTIVO, UsuarioTableMap::COL_NOMBRE, UsuarioTableMap::COL_APATERNO, UsuarioTableMap::COL_AMATERNO, UsuarioTableMap::COL_FECHA_CREACION, UsuarioTableMap::COL_FECHA_MODIFICACION, UsuarioTableMap::COL_ULTIMO_ACCESO, UsuarioTableMap::COL_PASSWORD_ANTIGUO, UsuarioTableMap::COL_ID_USUARIO_MODIFICACION, UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA, ),
        self::TYPE_FIELDNAME     => array('clave', 'usuario', 'password', 'vigencia', 'dias_vigencia', 'correo', 'activo', 'nombre', 'apaterno', 'amaterno', 'fecha_creacion', 'fecha_modificacion', 'ultimo_acceso', 'password_antiguo', 'id_usuario_modificacion', 'id_entidad_operativa', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Usuario' => 1, 'Password' => 2, 'Vigencia' => 3, 'DiasVigencia' => 4, 'Correo' => 5, 'Activo' => 6, 'Nombre' => 7, 'Apaterno' => 8, 'Amaterno' => 9, 'FechaCreacion' => 10, 'FechaModificacion' => 11, 'UltimoAcceso' => 12, 'PasswordAntiguo' => 13, 'IdUsuarioModificacion' => 14, 'IdEntidadOperativa' => 15, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'usuario' => 1, 'password' => 2, 'vigencia' => 3, 'diasVigencia' => 4, 'correo' => 5, 'activo' => 6, 'nombre' => 7, 'apaterno' => 8, 'amaterno' => 9, 'fechaCreacion' => 10, 'fechaModificacion' => 11, 'ultimoAcceso' => 12, 'passwordAntiguo' => 13, 'idUsuarioModificacion' => 14, 'idEntidadOperativa' => 15, ),
        self::TYPE_COLNAME       => array(UsuarioTableMap::COL_CLAVE => 0, UsuarioTableMap::COL_USUARIO => 1, UsuarioTableMap::COL_PASSWORD => 2, UsuarioTableMap::COL_VIGENCIA => 3, UsuarioTableMap::COL_DIAS_VIGENCIA => 4, UsuarioTableMap::COL_CORREO => 5, UsuarioTableMap::COL_ACTIVO => 6, UsuarioTableMap::COL_NOMBRE => 7, UsuarioTableMap::COL_APATERNO => 8, UsuarioTableMap::COL_AMATERNO => 9, UsuarioTableMap::COL_FECHA_CREACION => 10, UsuarioTableMap::COL_FECHA_MODIFICACION => 11, UsuarioTableMap::COL_ULTIMO_ACCESO => 12, UsuarioTableMap::COL_PASSWORD_ANTIGUO => 13, UsuarioTableMap::COL_ID_USUARIO_MODIFICACION => 14, UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA => 15, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'usuario' => 1, 'password' => 2, 'vigencia' => 3, 'dias_vigencia' => 4, 'correo' => 5, 'activo' => 6, 'nombre' => 7, 'apaterno' => 8, 'amaterno' => 9, 'fecha_creacion' => 10, 'fecha_modificacion' => 11, 'ultimo_acceso' => 12, 'password_antiguo' => 13, 'id_usuario_modificacion' => 14, 'id_entidad_operativa' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Usuario.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuario.clave' => 'CLAVE',
        'UsuarioTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuario.clave' => 'CLAVE',
        'Usuario' => 'USUARIO',
        'Usuario.Usuario' => 'USUARIO',
        'usuario' => 'USUARIO',
        'usuario.usuario' => 'USUARIO',
        'UsuarioTableMap::COL_USUARIO' => 'USUARIO',
        'COL_USUARIO' => 'USUARIO',
        'usuario' => 'USUARIO',
        'usuario.usuario' => 'USUARIO',
        'Password' => 'PASSWORD',
        'Usuario.Password' => 'PASSWORD',
        'password' => 'PASSWORD',
        'usuario.password' => 'PASSWORD',
        'UsuarioTableMap::COL_PASSWORD' => 'PASSWORD',
        'COL_PASSWORD' => 'PASSWORD',
        'password' => 'PASSWORD',
        'usuario.password' => 'PASSWORD',
        'Vigencia' => 'VIGENCIA',
        'Usuario.Vigencia' => 'VIGENCIA',
        'vigencia' => 'VIGENCIA',
        'usuario.vigencia' => 'VIGENCIA',
        'UsuarioTableMap::COL_VIGENCIA' => 'VIGENCIA',
        'COL_VIGENCIA' => 'VIGENCIA',
        'vigencia' => 'VIGENCIA',
        'usuario.vigencia' => 'VIGENCIA',
        'DiasVigencia' => 'DIAS_VIGENCIA',
        'Usuario.DiasVigencia' => 'DIAS_VIGENCIA',
        'diasVigencia' => 'DIAS_VIGENCIA',
        'usuario.diasVigencia' => 'DIAS_VIGENCIA',
        'UsuarioTableMap::COL_DIAS_VIGENCIA' => 'DIAS_VIGENCIA',
        'COL_DIAS_VIGENCIA' => 'DIAS_VIGENCIA',
        'dias_vigencia' => 'DIAS_VIGENCIA',
        'usuario.dias_vigencia' => 'DIAS_VIGENCIA',
        'Correo' => 'CORREO',
        'Usuario.Correo' => 'CORREO',
        'correo' => 'CORREO',
        'usuario.correo' => 'CORREO',
        'UsuarioTableMap::COL_CORREO' => 'CORREO',
        'COL_CORREO' => 'CORREO',
        'correo' => 'CORREO',
        'usuario.correo' => 'CORREO',
        'Activo' => 'ACTIVO',
        'Usuario.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuario.activo' => 'ACTIVO',
        'UsuarioTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuario.activo' => 'ACTIVO',
        'Nombre' => 'NOMBRE',
        'Usuario.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'usuario.nombre' => 'NOMBRE',
        'UsuarioTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'usuario.nombre' => 'NOMBRE',
        'Apaterno' => 'APATERNO',
        'Usuario.Apaterno' => 'APATERNO',
        'apaterno' => 'APATERNO',
        'usuario.apaterno' => 'APATERNO',
        'UsuarioTableMap::COL_APATERNO' => 'APATERNO',
        'COL_APATERNO' => 'APATERNO',
        'apaterno' => 'APATERNO',
        'usuario.apaterno' => 'APATERNO',
        'Amaterno' => 'AMATERNO',
        'Usuario.Amaterno' => 'AMATERNO',
        'amaterno' => 'AMATERNO',
        'usuario.amaterno' => 'AMATERNO',
        'UsuarioTableMap::COL_AMATERNO' => 'AMATERNO',
        'COL_AMATERNO' => 'AMATERNO',
        'amaterno' => 'AMATERNO',
        'usuario.amaterno' => 'AMATERNO',
        'FechaCreacion' => 'FECHA_CREACION',
        'Usuario.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'usuario.fechaCreacion' => 'FECHA_CREACION',
        'UsuarioTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'usuario.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'Usuario.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'usuario.fechaModificacion' => 'FECHA_MODIFICACION',
        'UsuarioTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'usuario.fecha_modificacion' => 'FECHA_MODIFICACION',
        'UltimoAcceso' => 'ULTIMO_ACCESO',
        'Usuario.UltimoAcceso' => 'ULTIMO_ACCESO',
        'ultimoAcceso' => 'ULTIMO_ACCESO',
        'usuario.ultimoAcceso' => 'ULTIMO_ACCESO',
        'UsuarioTableMap::COL_ULTIMO_ACCESO' => 'ULTIMO_ACCESO',
        'COL_ULTIMO_ACCESO' => 'ULTIMO_ACCESO',
        'ultimo_acceso' => 'ULTIMO_ACCESO',
        'usuario.ultimo_acceso' => 'ULTIMO_ACCESO',
        'PasswordAntiguo' => 'PASSWORD_ANTIGUO',
        'Usuario.PasswordAntiguo' => 'PASSWORD_ANTIGUO',
        'passwordAntiguo' => 'PASSWORD_ANTIGUO',
        'usuario.passwordAntiguo' => 'PASSWORD_ANTIGUO',
        'UsuarioTableMap::COL_PASSWORD_ANTIGUO' => 'PASSWORD_ANTIGUO',
        'COL_PASSWORD_ANTIGUO' => 'PASSWORD_ANTIGUO',
        'password_antiguo' => 'PASSWORD_ANTIGUO',
        'usuario.password_antiguo' => 'PASSWORD_ANTIGUO',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'Usuario.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'usuario.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'UsuarioTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'usuario.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'IdEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'Usuario.IdEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'idEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'usuario.idEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA' => 'ID_ENTIDAD_OPERATIVA',
        'COL_ID_ENTIDAD_OPERATIVA' => 'ID_ENTIDAD_OPERATIVA',
        'id_entidad_operativa' => 'ID_ENTIDAD_OPERATIVA',
        'usuario.id_entidad_operativa' => 'ID_ENTIDAD_OPERATIVA',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario');
        $this->setPhpName('Usuario');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Usuario');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('usuario', 'Usuario', 'VARCHAR', true, 10, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 1000, null);
        $this->addColumn('vigencia', 'Vigencia', 'INTEGER', true, null, 0);
        $this->addColumn('dias_vigencia', 'DiasVigencia', 'INTEGER', false, null, null);
        $this->addColumn('correo', 'Correo', 'VARCHAR', true, 1000, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('apaterno', 'Apaterno', 'VARCHAR', true, 1000, null);
        $this->addColumn('amaterno', 'Amaterno', 'VARCHAR', true, 1000, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('ultimo_acceso', 'UltimoAcceso', 'TIMESTAMP', true, null, null);
        $this->addColumn('password_antiguo', 'PasswordAntiguo', 'VARCHAR', true, 1000, '');
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', false, null, null);
        $this->addForeignKey('id_entidad_operativa', 'IdEntidadOperativa', 'INTEGER', 'entidad_operativa', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UsuarioRelatedByIdUsuarioModificacion', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('EntidadOperativa', '\\EntidadOperativa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entidad_operativa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Beneficio', '\\Beneficio', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'Beneficios', false);
        $this->addRelation('Etapa', '\\Etapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'Etapas', false);
        $this->addRelation('FlujoEtapa', '\\FlujoEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'FlujoEtapas', false);
        $this->addRelation('Login', '\\Login', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, 'Logins', false);
        $this->addRelation('Programa', '\\Programa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'Programas', false);
        $this->addRelation('ProgramaRequisito', '\\ProgramaRequisito', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'ProgramaRequisitos', false);
        $this->addRelation('RestablecerPassword', '\\RestablecerPassword', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, 'RestablecerPasswords', false);
        $this->addRelation('Rol', '\\Rol', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'Rols', false);
        $this->addRelation('RolPermiso', '\\RolPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'RolPermisos', false);
        $this->addRelation('RolUsuarioRelatedByIdUsuario', '\\RolUsuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, 'RolUsuariosRelatedByIdUsuario', false);
        $this->addRelation('RolUsuarioRelatedByIdUsuarioModificacion', '\\RolUsuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'RolUsuariosRelatedByIdUsuarioModificacion', false);
        $this->addRelation('UsuarioRelatedByClave', '\\Usuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'UsuariosRelatedByClave', false);
        $this->addRelation('UsuarioConfiguracionEtapa', '\\UsuarioConfiguracionEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionEtapas', false);
        $this->addRelation('UsuarioConfiguracionProgramaRelatedByIdUsuario', '\\UsuarioConfiguracionPrograma', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionProgramasRelatedByIdUsuario', false);
        $this->addRelation('UsuarioConfiguracionProgramaRelatedByIdUsuarioModificacion', '\\UsuarioConfiguracionPrograma', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionProgramasRelatedByIdUsuarioModificacion', false);
        $this->addRelation('UsuarioPermisoRelatedByIdUsuario', '\\UsuarioPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, 'UsuarioPermisosRelatedByIdUsuario', false);
        $this->addRelation('UsuarioPermisoRelatedByIdUsuarioModificacion', '\\UsuarioPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, 'UsuarioPermisosRelatedByIdUsuarioModificacion', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UsuarioTableMap::CLASS_DEFAULT : UsuarioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Usuario object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UsuarioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UsuarioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UsuarioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UsuarioTableMap::OM_CLASS;
            /** @var Usuario $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UsuarioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UsuarioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UsuarioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Usuario $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UsuarioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UsuarioTableMap::COL_CLAVE);
            $criteria->addSelectColumn(UsuarioTableMap::COL_USUARIO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UsuarioTableMap::COL_VIGENCIA);
            $criteria->addSelectColumn(UsuarioTableMap::COL_DIAS_VIGENCIA);
            $criteria->addSelectColumn(UsuarioTableMap::COL_CORREO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(UsuarioTableMap::COL_APATERNO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_AMATERNO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(UsuarioTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(UsuarioTableMap::COL_ULTIMO_ACCESO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_PASSWORD_ANTIGUO);
            $criteria->addSelectColumn(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->addSelectColumn(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.usuario');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.vigencia');
            $criteria->addSelectColumn($alias . '.dias_vigencia');
            $criteria->addSelectColumn($alias . '.correo');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.apaterno');
            $criteria->addSelectColumn($alias . '.amaterno');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.ultimo_acceso');
            $criteria->addSelectColumn($alias . '.password_antiguo');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->addSelectColumn($alias . '.id_entidad_operativa');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(UsuarioTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_USUARIO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_PASSWORD);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_VIGENCIA);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_DIAS_VIGENCIA);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_CORREO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_APATERNO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_AMATERNO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_ULTIMO_ACCESO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_PASSWORD_ANTIGUO);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->removeSelectColumn(UsuarioTableMap::COL_ID_ENTIDAD_OPERATIVA);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.usuario');
            $criteria->removeSelectColumn($alias . '.password');
            $criteria->removeSelectColumn($alias . '.vigencia');
            $criteria->removeSelectColumn($alias . '.dias_vigencia');
            $criteria->removeSelectColumn($alias . '.correo');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.apaterno');
            $criteria->removeSelectColumn($alias . '.amaterno');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.ultimo_acceso');
            $criteria->removeSelectColumn($alias . '.password_antiguo');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->removeSelectColumn($alias . '.id_entidad_operativa');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UsuarioTableMap::DATABASE_NAME)->getTable(UsuarioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UsuarioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UsuarioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UsuarioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Usuario or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Usuario object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Usuario) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UsuarioTableMap::DATABASE_NAME);
            $criteria->add(UsuarioTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = UsuarioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UsuarioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UsuarioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the usuario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UsuarioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Usuario or Criteria object.
     *
     * @param mixed               $criteria Criteria or Usuario object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Usuario object
        }

        if ($criteria->containsKey(UsuarioTableMap::COL_CLAVE) && $criteria->keyContainsValue(UsuarioTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UsuarioTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = UsuarioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UsuarioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UsuarioTableMap::buildTableMap();
