<?php

namespace Map;

use \EntidadOperativa;
use \EntidadOperativaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'entidad_operativa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class EntidadOperativaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.EntidadOperativaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'entidad_operativa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\EntidadOperativa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'EntidadOperativa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 4;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 4;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'entidad_operativa.clave';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'entidad_operativa.nombre';

    /**
     * the column name for the siglas field
     */
    const COL_SIGLAS = 'entidad_operativa.siglas';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'entidad_operativa.activo';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Nombre', 'Siglas', 'Activo', ),
        self::TYPE_CAMELNAME     => array('clave', 'nombre', 'siglas', 'activo', ),
        self::TYPE_COLNAME       => array(EntidadOperativaTableMap::COL_CLAVE, EntidadOperativaTableMap::COL_NOMBRE, EntidadOperativaTableMap::COL_SIGLAS, EntidadOperativaTableMap::COL_ACTIVO, ),
        self::TYPE_FIELDNAME     => array('clave', 'nombre', 'siglas', 'activo', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Nombre' => 1, 'Siglas' => 2, 'Activo' => 3, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'nombre' => 1, 'siglas' => 2, 'activo' => 3, ),
        self::TYPE_COLNAME       => array(EntidadOperativaTableMap::COL_CLAVE => 0, EntidadOperativaTableMap::COL_NOMBRE => 1, EntidadOperativaTableMap::COL_SIGLAS => 2, EntidadOperativaTableMap::COL_ACTIVO => 3, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'nombre' => 1, 'siglas' => 2, 'activo' => 3, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'EntidadOperativa.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'entidadOperativa.clave' => 'CLAVE',
        'EntidadOperativaTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'entidad_operativa.clave' => 'CLAVE',
        'Nombre' => 'NOMBRE',
        'EntidadOperativa.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'entidadOperativa.nombre' => 'NOMBRE',
        'EntidadOperativaTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'entidad_operativa.nombre' => 'NOMBRE',
        'Siglas' => 'SIGLAS',
        'EntidadOperativa.Siglas' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'entidadOperativa.siglas' => 'SIGLAS',
        'EntidadOperativaTableMap::COL_SIGLAS' => 'SIGLAS',
        'COL_SIGLAS' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'entidad_operativa.siglas' => 'SIGLAS',
        'Activo' => 'ACTIVO',
        'EntidadOperativa.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'entidadOperativa.activo' => 'ACTIVO',
        'EntidadOperativaTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'entidad_operativa.activo' => 'ACTIVO',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('entidad_operativa');
        $this->setPhpName('EntidadOperativa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\EntidadOperativa');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('siglas', 'Siglas', 'VARCHAR', false, 45, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Programa', '\\Programa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_entidad_operativa',
    1 => ':clave',
  ),
), null, null, 'Programas', false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_entidad_operativa',
    1 => ':clave',
  ),
), null, null, 'Usuarios', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EntidadOperativaTableMap::CLASS_DEFAULT : EntidadOperativaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (EntidadOperativa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EntidadOperativaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EntidadOperativaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EntidadOperativaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EntidadOperativaTableMap::OM_CLASS;
            /** @var EntidadOperativa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EntidadOperativaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EntidadOperativaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EntidadOperativaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var EntidadOperativa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EntidadOperativaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EntidadOperativaTableMap::COL_CLAVE);
            $criteria->addSelectColumn(EntidadOperativaTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(EntidadOperativaTableMap::COL_SIGLAS);
            $criteria->addSelectColumn(EntidadOperativaTableMap::COL_ACTIVO);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.siglas');
            $criteria->addSelectColumn($alias . '.activo');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(EntidadOperativaTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(EntidadOperativaTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(EntidadOperativaTableMap::COL_SIGLAS);
            $criteria->removeSelectColumn(EntidadOperativaTableMap::COL_ACTIVO);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.siglas');
            $criteria->removeSelectColumn($alias . '.activo');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EntidadOperativaTableMap::DATABASE_NAME)->getTable(EntidadOperativaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EntidadOperativaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EntidadOperativaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EntidadOperativaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a EntidadOperativa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or EntidadOperativa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntidadOperativaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \EntidadOperativa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EntidadOperativaTableMap::DATABASE_NAME);
            $criteria->add(EntidadOperativaTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = EntidadOperativaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EntidadOperativaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EntidadOperativaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the entidad_operativa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EntidadOperativaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a EntidadOperativa or Criteria object.
     *
     * @param mixed               $criteria Criteria or EntidadOperativa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EntidadOperativaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from EntidadOperativa object
        }

        if ($criteria->containsKey(EntidadOperativaTableMap::COL_CLAVE) && $criteria->keyContainsValue(EntidadOperativaTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EntidadOperativaTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = EntidadOperativaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EntidadOperativaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EntidadOperativaTableMap::buildTableMap();
