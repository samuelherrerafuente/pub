<?php

namespace Map;

use \Etapa;
use \EtapaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'etapa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class EtapaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.EtapaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'etapa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Etapa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Etapa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'etapa.clave';

    /**
     * the column name for the id_programa field
     */
    const COL_ID_PROGRAMA = 'etapa.id_programa';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'etapa.nombre';

    /**
     * the column name for the siglas field
     */
    const COL_SIGLAS = 'etapa.siglas';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'etapa.activo';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'etapa.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'etapa.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'etapa.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdPrograma', 'Nombre', 'Siglas', 'Activo', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'idPrograma', 'nombre', 'siglas', 'activo', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(EtapaTableMap::COL_CLAVE, EtapaTableMap::COL_ID_PROGRAMA, EtapaTableMap::COL_NOMBRE, EtapaTableMap::COL_SIGLAS, EtapaTableMap::COL_ACTIVO, EtapaTableMap::COL_FECHA_CREACION, EtapaTableMap::COL_FECHA_MODIFICACION, EtapaTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_programa', 'nombre', 'siglas', 'activo', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdPrograma' => 1, 'Nombre' => 2, 'Siglas' => 3, 'Activo' => 4, 'FechaCreacion' => 5, 'FechaModificacion' => 6, 'IdUsuarioModificacion' => 7, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idPrograma' => 1, 'nombre' => 2, 'siglas' => 3, 'activo' => 4, 'fechaCreacion' => 5, 'fechaModificacion' => 6, 'idUsuarioModificacion' => 7, ),
        self::TYPE_COLNAME       => array(EtapaTableMap::COL_CLAVE => 0, EtapaTableMap::COL_ID_PROGRAMA => 1, EtapaTableMap::COL_NOMBRE => 2, EtapaTableMap::COL_SIGLAS => 3, EtapaTableMap::COL_ACTIVO => 4, EtapaTableMap::COL_FECHA_CREACION => 5, EtapaTableMap::COL_FECHA_MODIFICACION => 6, EtapaTableMap::COL_ID_USUARIO_MODIFICACION => 7, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_programa' => 1, 'nombre' => 2, 'siglas' => 3, 'activo' => 4, 'fecha_creacion' => 5, 'fecha_modificacion' => 6, 'id_usuario_modificacion' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Etapa.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'etapa.clave' => 'CLAVE',
        'EtapaTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'etapa.clave' => 'CLAVE',
        'IdPrograma' => 'ID_PROGRAMA',
        'Etapa.IdPrograma' => 'ID_PROGRAMA',
        'idPrograma' => 'ID_PROGRAMA',
        'etapa.idPrograma' => 'ID_PROGRAMA',
        'EtapaTableMap::COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'id_programa' => 'ID_PROGRAMA',
        'etapa.id_programa' => 'ID_PROGRAMA',
        'Nombre' => 'NOMBRE',
        'Etapa.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'etapa.nombre' => 'NOMBRE',
        'EtapaTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'etapa.nombre' => 'NOMBRE',
        'Siglas' => 'SIGLAS',
        'Etapa.Siglas' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'etapa.siglas' => 'SIGLAS',
        'EtapaTableMap::COL_SIGLAS' => 'SIGLAS',
        'COL_SIGLAS' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'etapa.siglas' => 'SIGLAS',
        'Activo' => 'ACTIVO',
        'Etapa.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'etapa.activo' => 'ACTIVO',
        'EtapaTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'etapa.activo' => 'ACTIVO',
        'FechaCreacion' => 'FECHA_CREACION',
        'Etapa.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'etapa.fechaCreacion' => 'FECHA_CREACION',
        'EtapaTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'etapa.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'Etapa.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'etapa.fechaModificacion' => 'FECHA_MODIFICACION',
        'EtapaTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'etapa.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'Etapa.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'etapa.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'EtapaTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'etapa.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('etapa');
        $this->setPhpName('Etapa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Etapa');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_programa', 'IdPrograma', 'INTEGER', 'programa', 'clave', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('siglas', 'Siglas', 'VARCHAR', true, 1000, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Programa', '\\Programa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('FlujoEtapaRelatedByIdEtapa', '\\FlujoEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_etapa',
    1 => ':clave',
  ),
), null, null, 'FlujoEtapasRelatedByIdEtapa', false);
        $this->addRelation('FlujoEtapaRelatedByIdEtapaSiguiente', '\\FlujoEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_etapa_siguiente',
    1 => ':clave',
  ),
), null, null, 'FlujoEtapasRelatedByIdEtapaSiguiente', false);
        $this->addRelation('ProgramaRequisito', '\\ProgramaRequisito', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_etapa',
    1 => ':clave',
  ),
), null, null, 'ProgramaRequisitos', false);
        $this->addRelation('UsuarioConfiguracionEtapa', '\\UsuarioConfiguracionEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_etapa',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionEtapas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EtapaTableMap::CLASS_DEFAULT : EtapaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Etapa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EtapaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EtapaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EtapaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EtapaTableMap::OM_CLASS;
            /** @var Etapa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EtapaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EtapaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EtapaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Etapa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EtapaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EtapaTableMap::COL_CLAVE);
            $criteria->addSelectColumn(EtapaTableMap::COL_ID_PROGRAMA);
            $criteria->addSelectColumn(EtapaTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(EtapaTableMap::COL_SIGLAS);
            $criteria->addSelectColumn(EtapaTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(EtapaTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(EtapaTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(EtapaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_programa');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.siglas');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(EtapaTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(EtapaTableMap::COL_ID_PROGRAMA);
            $criteria->removeSelectColumn(EtapaTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(EtapaTableMap::COL_SIGLAS);
            $criteria->removeSelectColumn(EtapaTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(EtapaTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(EtapaTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(EtapaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_programa');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.siglas');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EtapaTableMap::DATABASE_NAME)->getTable(EtapaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EtapaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EtapaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EtapaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Etapa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Etapa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Etapa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EtapaTableMap::DATABASE_NAME);
            $criteria->add(EtapaTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = EtapaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EtapaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EtapaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the etapa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EtapaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Etapa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Etapa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtapaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Etapa object
        }

        if ($criteria->containsKey(EtapaTableMap::COL_CLAVE) && $criteria->keyContainsValue(EtapaTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EtapaTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = EtapaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EtapaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EtapaTableMap::buildTableMap();
