<?php

namespace Map;

use \Beneficio;
use \BeneficioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'beneficio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class BeneficioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.BeneficioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'beneficio';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Beneficio';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Beneficio';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'beneficio.clave';

    /**
     * the column name for the id_tipo_distribucion field
     */
    const COL_ID_TIPO_DISTRIBUCION = 'beneficio.id_tipo_distribucion';

    /**
     * the column name for the id_tipo_beneficio field
     */
    const COL_ID_TIPO_BENEFICIO = 'beneficio.id_tipo_beneficio';

    /**
     * the column name for the id_tipo_recurso field
     */
    const COL_ID_TIPO_RECURSO = 'beneficio.id_tipo_recurso';

    /**
     * the column name for the id_unidad_medida field
     */
    const COL_ID_UNIDAD_MEDIDA = 'beneficio.id_unidad_medida';

    /**
     * the column name for the valor field
     */
    const COL_VALOR = 'beneficio.valor';

    /**
     * the column name for the cantidad field
     */
    const COL_CANTIDAD = 'beneficio.cantidad';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'beneficio.nombre';

    /**
     * the column name for the id_programa field
     */
    const COL_ID_PROGRAMA = 'beneficio.id_programa';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'beneficio.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'beneficio.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'beneficio.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdTipoDistribucion', 'IdTipoBeneficio', 'IdTipoRecurso', 'IdUnidadMedida', 'Valor', 'Cantidad', 'Nombre', 'IdPrograma', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'idTipoDistribucion', 'idTipoBeneficio', 'idTipoRecurso', 'idUnidadMedida', 'valor', 'cantidad', 'nombre', 'idPrograma', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(BeneficioTableMap::COL_CLAVE, BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION, BeneficioTableMap::COL_ID_TIPO_BENEFICIO, BeneficioTableMap::COL_ID_TIPO_RECURSO, BeneficioTableMap::COL_ID_UNIDAD_MEDIDA, BeneficioTableMap::COL_VALOR, BeneficioTableMap::COL_CANTIDAD, BeneficioTableMap::COL_NOMBRE, BeneficioTableMap::COL_ID_PROGRAMA, BeneficioTableMap::COL_FECHA_CREACION, BeneficioTableMap::COL_FECHA_MODIFICACION, BeneficioTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_tipo_distribucion', 'id_tipo_beneficio', 'id_tipo_recurso', 'id_unidad_medida', 'valor', 'cantidad', 'nombre', 'id_programa', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdTipoDistribucion' => 1, 'IdTipoBeneficio' => 2, 'IdTipoRecurso' => 3, 'IdUnidadMedida' => 4, 'Valor' => 5, 'Cantidad' => 6, 'Nombre' => 7, 'IdPrograma' => 8, 'FechaCreacion' => 9, 'FechaModificacion' => 10, 'IdUsuarioModificacion' => 11, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idTipoDistribucion' => 1, 'idTipoBeneficio' => 2, 'idTipoRecurso' => 3, 'idUnidadMedida' => 4, 'valor' => 5, 'cantidad' => 6, 'nombre' => 7, 'idPrograma' => 8, 'fechaCreacion' => 9, 'fechaModificacion' => 10, 'idUsuarioModificacion' => 11, ),
        self::TYPE_COLNAME       => array(BeneficioTableMap::COL_CLAVE => 0, BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION => 1, BeneficioTableMap::COL_ID_TIPO_BENEFICIO => 2, BeneficioTableMap::COL_ID_TIPO_RECURSO => 3, BeneficioTableMap::COL_ID_UNIDAD_MEDIDA => 4, BeneficioTableMap::COL_VALOR => 5, BeneficioTableMap::COL_CANTIDAD => 6, BeneficioTableMap::COL_NOMBRE => 7, BeneficioTableMap::COL_ID_PROGRAMA => 8, BeneficioTableMap::COL_FECHA_CREACION => 9, BeneficioTableMap::COL_FECHA_MODIFICACION => 10, BeneficioTableMap::COL_ID_USUARIO_MODIFICACION => 11, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_tipo_distribucion' => 1, 'id_tipo_beneficio' => 2, 'id_tipo_recurso' => 3, 'id_unidad_medida' => 4, 'valor' => 5, 'cantidad' => 6, 'nombre' => 7, 'id_programa' => 8, 'fecha_creacion' => 9, 'fecha_modificacion' => 10, 'id_usuario_modificacion' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Beneficio.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'beneficio.clave' => 'CLAVE',
        'BeneficioTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'beneficio.clave' => 'CLAVE',
        'IdTipoDistribucion' => 'ID_TIPO_DISTRIBUCION',
        'Beneficio.IdTipoDistribucion' => 'ID_TIPO_DISTRIBUCION',
        'idTipoDistribucion' => 'ID_TIPO_DISTRIBUCION',
        'beneficio.idTipoDistribucion' => 'ID_TIPO_DISTRIBUCION',
        'BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION' => 'ID_TIPO_DISTRIBUCION',
        'COL_ID_TIPO_DISTRIBUCION' => 'ID_TIPO_DISTRIBUCION',
        'id_tipo_distribucion' => 'ID_TIPO_DISTRIBUCION',
        'beneficio.id_tipo_distribucion' => 'ID_TIPO_DISTRIBUCION',
        'IdTipoBeneficio' => 'ID_TIPO_BENEFICIO',
        'Beneficio.IdTipoBeneficio' => 'ID_TIPO_BENEFICIO',
        'idTipoBeneficio' => 'ID_TIPO_BENEFICIO',
        'beneficio.idTipoBeneficio' => 'ID_TIPO_BENEFICIO',
        'BeneficioTableMap::COL_ID_TIPO_BENEFICIO' => 'ID_TIPO_BENEFICIO',
        'COL_ID_TIPO_BENEFICIO' => 'ID_TIPO_BENEFICIO',
        'id_tipo_beneficio' => 'ID_TIPO_BENEFICIO',
        'beneficio.id_tipo_beneficio' => 'ID_TIPO_BENEFICIO',
        'IdTipoRecurso' => 'ID_TIPO_RECURSO',
        'Beneficio.IdTipoRecurso' => 'ID_TIPO_RECURSO',
        'idTipoRecurso' => 'ID_TIPO_RECURSO',
        'beneficio.idTipoRecurso' => 'ID_TIPO_RECURSO',
        'BeneficioTableMap::COL_ID_TIPO_RECURSO' => 'ID_TIPO_RECURSO',
        'COL_ID_TIPO_RECURSO' => 'ID_TIPO_RECURSO',
        'id_tipo_recurso' => 'ID_TIPO_RECURSO',
        'beneficio.id_tipo_recurso' => 'ID_TIPO_RECURSO',
        'IdUnidadMedida' => 'ID_UNIDAD_MEDIDA',
        'Beneficio.IdUnidadMedida' => 'ID_UNIDAD_MEDIDA',
        'idUnidadMedida' => 'ID_UNIDAD_MEDIDA',
        'beneficio.idUnidadMedida' => 'ID_UNIDAD_MEDIDA',
        'BeneficioTableMap::COL_ID_UNIDAD_MEDIDA' => 'ID_UNIDAD_MEDIDA',
        'COL_ID_UNIDAD_MEDIDA' => 'ID_UNIDAD_MEDIDA',
        'id_unidad_medida' => 'ID_UNIDAD_MEDIDA',
        'beneficio.id_unidad_medida' => 'ID_UNIDAD_MEDIDA',
        'Valor' => 'VALOR',
        'Beneficio.Valor' => 'VALOR',
        'valor' => 'VALOR',
        'beneficio.valor' => 'VALOR',
        'BeneficioTableMap::COL_VALOR' => 'VALOR',
        'COL_VALOR' => 'VALOR',
        'valor' => 'VALOR',
        'beneficio.valor' => 'VALOR',
        'Cantidad' => 'CANTIDAD',
        'Beneficio.Cantidad' => 'CANTIDAD',
        'cantidad' => 'CANTIDAD',
        'beneficio.cantidad' => 'CANTIDAD',
        'BeneficioTableMap::COL_CANTIDAD' => 'CANTIDAD',
        'COL_CANTIDAD' => 'CANTIDAD',
        'cantidad' => 'CANTIDAD',
        'beneficio.cantidad' => 'CANTIDAD',
        'Nombre' => 'NOMBRE',
        'Beneficio.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'beneficio.nombre' => 'NOMBRE',
        'BeneficioTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'beneficio.nombre' => 'NOMBRE',
        'IdPrograma' => 'ID_PROGRAMA',
        'Beneficio.IdPrograma' => 'ID_PROGRAMA',
        'idPrograma' => 'ID_PROGRAMA',
        'beneficio.idPrograma' => 'ID_PROGRAMA',
        'BeneficioTableMap::COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'id_programa' => 'ID_PROGRAMA',
        'beneficio.id_programa' => 'ID_PROGRAMA',
        'FechaCreacion' => 'FECHA_CREACION',
        'Beneficio.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'beneficio.fechaCreacion' => 'FECHA_CREACION',
        'BeneficioTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'beneficio.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'Beneficio.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'beneficio.fechaModificacion' => 'FECHA_MODIFICACION',
        'BeneficioTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'beneficio.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'Beneficio.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'beneficio.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'BeneficioTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'beneficio.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('beneficio');
        $this->setPhpName('Beneficio');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Beneficio');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_tipo_distribucion', 'IdTipoDistribucion', 'INTEGER', 'tipo_distribucion', 'clave', true, null, null);
        $this->addForeignKey('id_tipo_beneficio', 'IdTipoBeneficio', 'INTEGER', 'tipo_beneficio', 'clave', true, null, null);
        $this->addForeignKey('id_tipo_recurso', 'IdTipoRecurso', 'INTEGER', 'tipo_recurso', 'clave', true, null, null);
        $this->addForeignKey('id_unidad_medida', 'IdUnidadMedida', 'INTEGER', 'unidad_medida', 'clave', true, null, null);
        $this->addColumn('valor', 'Valor', 'DECIMAL', true, 65, null);
        $this->addColumn('cantidad', 'Cantidad', 'INTEGER', false, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addForeignKey('id_programa', 'IdPrograma', 'INTEGER', 'programa', 'clave', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TipoDistribucion', '\\TipoDistribucion', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tipo_distribucion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('TipoBeneficio', '\\TipoBeneficio', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tipo_beneficio',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('TipoRecurso', '\\TipoRecurso', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tipo_recurso',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UnidadMedida', '\\UnidadMedida', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_unidad_medida',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Programa', '\\Programa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? BeneficioTableMap::CLASS_DEFAULT : BeneficioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Beneficio object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = BeneficioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = BeneficioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + BeneficioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = BeneficioTableMap::OM_CLASS;
            /** @var Beneficio $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            BeneficioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = BeneficioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = BeneficioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Beneficio $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                BeneficioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(BeneficioTableMap::COL_CLAVE);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_TIPO_BENEFICIO);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_TIPO_RECURSO);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA);
            $criteria->addSelectColumn(BeneficioTableMap::COL_VALOR);
            $criteria->addSelectColumn(BeneficioTableMap::COL_CANTIDAD);
            $criteria->addSelectColumn(BeneficioTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_PROGRAMA);
            $criteria->addSelectColumn(BeneficioTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(BeneficioTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_tipo_distribucion');
            $criteria->addSelectColumn($alias . '.id_tipo_beneficio');
            $criteria->addSelectColumn($alias . '.id_tipo_recurso');
            $criteria->addSelectColumn($alias . '.id_unidad_medida');
            $criteria->addSelectColumn($alias . '.valor');
            $criteria->addSelectColumn($alias . '.cantidad');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.id_programa');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(BeneficioTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_TIPO_DISTRIBUCION);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_TIPO_BENEFICIO);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_TIPO_RECURSO);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_UNIDAD_MEDIDA);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_VALOR);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_CANTIDAD);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_PROGRAMA);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(BeneficioTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_tipo_distribucion');
            $criteria->removeSelectColumn($alias . '.id_tipo_beneficio');
            $criteria->removeSelectColumn($alias . '.id_tipo_recurso');
            $criteria->removeSelectColumn($alias . '.id_unidad_medida');
            $criteria->removeSelectColumn($alias . '.valor');
            $criteria->removeSelectColumn($alias . '.cantidad');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.id_programa');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(BeneficioTableMap::DATABASE_NAME)->getTable(BeneficioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(BeneficioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(BeneficioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new BeneficioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Beneficio or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Beneficio object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BeneficioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Beneficio) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(BeneficioTableMap::DATABASE_NAME);
            $criteria->add(BeneficioTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = BeneficioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            BeneficioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                BeneficioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the beneficio table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return BeneficioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Beneficio or Criteria object.
     *
     * @param mixed               $criteria Criteria or Beneficio object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(BeneficioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Beneficio object
        }

        if ($criteria->containsKey(BeneficioTableMap::COL_CLAVE) && $criteria->keyContainsValue(BeneficioTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.BeneficioTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = BeneficioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // BeneficioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BeneficioTableMap::buildTableMap();
