<?php

namespace Map;

use \RestablecerPassword;
use \RestablecerPasswordQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'restablecer_password' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class RestablecerPasswordTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RestablecerPasswordTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'restablecer_password';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\RestablecerPassword';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'RestablecerPassword';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'restablecer_password.clave';

    /**
     * the column name for the id_usuario field
     */
    const COL_ID_USUARIO = 'restablecer_password.id_usuario';

    /**
     * the column name for the fecha field
     */
    const COL_FECHA = 'restablecer_password.fecha';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'restablecer_password.token';

    /**
     * the column name for the usado field
     */
    const COL_USADO = 'restablecer_password.usado';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdUsuario', 'Fecha', 'Token', 'Usado', ),
        self::TYPE_CAMELNAME     => array('clave', 'idUsuario', 'fecha', 'token', 'usado', ),
        self::TYPE_COLNAME       => array(RestablecerPasswordTableMap::COL_CLAVE, RestablecerPasswordTableMap::COL_ID_USUARIO, RestablecerPasswordTableMap::COL_FECHA, RestablecerPasswordTableMap::COL_TOKEN, RestablecerPasswordTableMap::COL_USADO, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_usuario', 'fecha', 'token', 'usado', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdUsuario' => 1, 'Fecha' => 2, 'Token' => 3, 'Usado' => 4, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idUsuario' => 1, 'fecha' => 2, 'token' => 3, 'usado' => 4, ),
        self::TYPE_COLNAME       => array(RestablecerPasswordTableMap::COL_CLAVE => 0, RestablecerPasswordTableMap::COL_ID_USUARIO => 1, RestablecerPasswordTableMap::COL_FECHA => 2, RestablecerPasswordTableMap::COL_TOKEN => 3, RestablecerPasswordTableMap::COL_USADO => 4, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_usuario' => 1, 'fecha' => 2, 'token' => 3, 'usado' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'RestablecerPassword.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'restablecerPassword.clave' => 'CLAVE',
        'RestablecerPasswordTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'restablecer_password.clave' => 'CLAVE',
        'IdUsuario' => 'ID_USUARIO',
        'RestablecerPassword.IdUsuario' => 'ID_USUARIO',
        'idUsuario' => 'ID_USUARIO',
        'restablecerPassword.idUsuario' => 'ID_USUARIO',
        'RestablecerPasswordTableMap::COL_ID_USUARIO' => 'ID_USUARIO',
        'COL_ID_USUARIO' => 'ID_USUARIO',
        'id_usuario' => 'ID_USUARIO',
        'restablecer_password.id_usuario' => 'ID_USUARIO',
        'Fecha' => 'FECHA',
        'RestablecerPassword.Fecha' => 'FECHA',
        'fecha' => 'FECHA',
        'restablecerPassword.fecha' => 'FECHA',
        'RestablecerPasswordTableMap::COL_FECHA' => 'FECHA',
        'COL_FECHA' => 'FECHA',
        'fecha' => 'FECHA',
        'restablecer_password.fecha' => 'FECHA',
        'Token' => 'TOKEN',
        'RestablecerPassword.Token' => 'TOKEN',
        'token' => 'TOKEN',
        'restablecerPassword.token' => 'TOKEN',
        'RestablecerPasswordTableMap::COL_TOKEN' => 'TOKEN',
        'COL_TOKEN' => 'TOKEN',
        'token' => 'TOKEN',
        'restablecer_password.token' => 'TOKEN',
        'Usado' => 'USADO',
        'RestablecerPassword.Usado' => 'USADO',
        'usado' => 'USADO',
        'restablecerPassword.usado' => 'USADO',
        'RestablecerPasswordTableMap::COL_USADO' => 'USADO',
        'COL_USADO' => 'USADO',
        'usado' => 'USADO',
        'restablecer_password.usado' => 'USADO',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('restablecer_password');
        $this->setPhpName('RestablecerPassword');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\RestablecerPassword');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_usuario', 'IdUsuario', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addColumn('fecha', 'Fecha', 'TIMESTAMP', true, null, null);
        $this->addColumn('token', 'Token', 'VARCHAR', true, 1000, null);
        $this->addColumn('usado', 'Usado', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RestablecerPasswordTableMap::CLASS_DEFAULT : RestablecerPasswordTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RestablecerPassword object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RestablecerPasswordTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RestablecerPasswordTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RestablecerPasswordTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RestablecerPasswordTableMap::OM_CLASS;
            /** @var RestablecerPassword $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RestablecerPasswordTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RestablecerPasswordTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RestablecerPasswordTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RestablecerPassword $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RestablecerPasswordTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RestablecerPasswordTableMap::COL_CLAVE);
            $criteria->addSelectColumn(RestablecerPasswordTableMap::COL_ID_USUARIO);
            $criteria->addSelectColumn(RestablecerPasswordTableMap::COL_FECHA);
            $criteria->addSelectColumn(RestablecerPasswordTableMap::COL_TOKEN);
            $criteria->addSelectColumn(RestablecerPasswordTableMap::COL_USADO);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_usuario');
            $criteria->addSelectColumn($alias . '.fecha');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.usado');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(RestablecerPasswordTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(RestablecerPasswordTableMap::COL_ID_USUARIO);
            $criteria->removeSelectColumn(RestablecerPasswordTableMap::COL_FECHA);
            $criteria->removeSelectColumn(RestablecerPasswordTableMap::COL_TOKEN);
            $criteria->removeSelectColumn(RestablecerPasswordTableMap::COL_USADO);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_usuario');
            $criteria->removeSelectColumn($alias . '.fecha');
            $criteria->removeSelectColumn($alias . '.token');
            $criteria->removeSelectColumn($alias . '.usado');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RestablecerPasswordTableMap::DATABASE_NAME)->getTable(RestablecerPasswordTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RestablecerPasswordTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RestablecerPasswordTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RestablecerPasswordTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RestablecerPassword or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RestablecerPassword object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RestablecerPasswordTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \RestablecerPassword) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RestablecerPasswordTableMap::DATABASE_NAME);
            $criteria->add(RestablecerPasswordTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = RestablecerPasswordQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RestablecerPasswordTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RestablecerPasswordTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the restablecer_password table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RestablecerPasswordQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RestablecerPassword or Criteria object.
     *
     * @param mixed               $criteria Criteria or RestablecerPassword object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RestablecerPasswordTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RestablecerPassword object
        }

        if ($criteria->containsKey(RestablecerPasswordTableMap::COL_CLAVE) && $criteria->keyContainsValue(RestablecerPasswordTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RestablecerPasswordTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = RestablecerPasswordQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RestablecerPasswordTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RestablecerPasswordTableMap::buildTableMap();
