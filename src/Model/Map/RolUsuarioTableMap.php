<?php

namespace Map;

use \RolUsuario;
use \RolUsuarioQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'rol_usuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class RolUsuarioTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RolUsuarioTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'rol_usuario';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\RolUsuario';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'RolUsuario';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'rol_usuario.clave';

    /**
     * the column name for the id_usuario field
     */
    const COL_ID_USUARIO = 'rol_usuario.id_usuario';

    /**
     * the column name for the id_rol field
     */
    const COL_ID_ROL = 'rol_usuario.id_rol';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'rol_usuario.activo';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'rol_usuario.fecha_modificacion';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'rol_usuario.fecha_creacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'rol_usuario.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdUsuario', 'IdRol', 'Activo', 'FechaModificacion', 'FechaCreacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'idUsuario', 'idRol', 'activo', 'fechaModificacion', 'fechaCreacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(RolUsuarioTableMap::COL_CLAVE, RolUsuarioTableMap::COL_ID_USUARIO, RolUsuarioTableMap::COL_ID_ROL, RolUsuarioTableMap::COL_ACTIVO, RolUsuarioTableMap::COL_FECHA_MODIFICACION, RolUsuarioTableMap::COL_FECHA_CREACION, RolUsuarioTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_usuario', 'id_rol', 'activo', 'fecha_modificacion', 'fecha_creacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdUsuario' => 1, 'IdRol' => 2, 'Activo' => 3, 'FechaModificacion' => 4, 'FechaCreacion' => 5, 'IdUsuarioModificacion' => 6, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idUsuario' => 1, 'idRol' => 2, 'activo' => 3, 'fechaModificacion' => 4, 'fechaCreacion' => 5, 'idUsuarioModificacion' => 6, ),
        self::TYPE_COLNAME       => array(RolUsuarioTableMap::COL_CLAVE => 0, RolUsuarioTableMap::COL_ID_USUARIO => 1, RolUsuarioTableMap::COL_ID_ROL => 2, RolUsuarioTableMap::COL_ACTIVO => 3, RolUsuarioTableMap::COL_FECHA_MODIFICACION => 4, RolUsuarioTableMap::COL_FECHA_CREACION => 5, RolUsuarioTableMap::COL_ID_USUARIO_MODIFICACION => 6, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_usuario' => 1, 'id_rol' => 2, 'activo' => 3, 'fecha_modificacion' => 4, 'fecha_creacion' => 5, 'id_usuario_modificacion' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'RolUsuario.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'rolUsuario.clave' => 'CLAVE',
        'RolUsuarioTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'rol_usuario.clave' => 'CLAVE',
        'IdUsuario' => 'ID_USUARIO',
        'RolUsuario.IdUsuario' => 'ID_USUARIO',
        'idUsuario' => 'ID_USUARIO',
        'rolUsuario.idUsuario' => 'ID_USUARIO',
        'RolUsuarioTableMap::COL_ID_USUARIO' => 'ID_USUARIO',
        'COL_ID_USUARIO' => 'ID_USUARIO',
        'id_usuario' => 'ID_USUARIO',
        'rol_usuario.id_usuario' => 'ID_USUARIO',
        'IdRol' => 'ID_ROL',
        'RolUsuario.IdRol' => 'ID_ROL',
        'idRol' => 'ID_ROL',
        'rolUsuario.idRol' => 'ID_ROL',
        'RolUsuarioTableMap::COL_ID_ROL' => 'ID_ROL',
        'COL_ID_ROL' => 'ID_ROL',
        'id_rol' => 'ID_ROL',
        'rol_usuario.id_rol' => 'ID_ROL',
        'Activo' => 'ACTIVO',
        'RolUsuario.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rolUsuario.activo' => 'ACTIVO',
        'RolUsuarioTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rol_usuario.activo' => 'ACTIVO',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'RolUsuario.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'rolUsuario.fechaModificacion' => 'FECHA_MODIFICACION',
        'RolUsuarioTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'rol_usuario.fecha_modificacion' => 'FECHA_MODIFICACION',
        'FechaCreacion' => 'FECHA_CREACION',
        'RolUsuario.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'rolUsuario.fechaCreacion' => 'FECHA_CREACION',
        'RolUsuarioTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'rol_usuario.fecha_creacion' => 'FECHA_CREACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'RolUsuario.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'rolUsuario.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'RolUsuarioTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'rol_usuario.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rol_usuario');
        $this->setPhpName('RolUsuario');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\RolUsuario');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_usuario', 'IdUsuario', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addForeignKey('id_rol', 'IdRol', 'INTEGER', 'rol', 'clave', true, null, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UsuarioRelatedByIdUsuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Rol', '\\Rol', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_rol',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UsuarioRelatedByIdUsuarioModificacion', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RolUsuarioTableMap::CLASS_DEFAULT : RolUsuarioTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RolUsuario object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RolUsuarioTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RolUsuarioTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RolUsuarioTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RolUsuarioTableMap::OM_CLASS;
            /** @var RolUsuario $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RolUsuarioTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RolUsuarioTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RolUsuarioTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RolUsuario $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RolUsuarioTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_CLAVE);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_ID_USUARIO);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_ID_ROL);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(RolUsuarioTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_usuario');
            $criteria->addSelectColumn($alias . '.id_rol');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_ID_USUARIO);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_ID_ROL);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(RolUsuarioTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_usuario');
            $criteria->removeSelectColumn($alias . '.id_rol');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RolUsuarioTableMap::DATABASE_NAME)->getTable(RolUsuarioTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RolUsuarioTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RolUsuarioTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RolUsuarioTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RolUsuario or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RolUsuario object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolUsuarioTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \RolUsuario) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RolUsuarioTableMap::DATABASE_NAME);
            $criteria->add(RolUsuarioTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = RolUsuarioQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RolUsuarioTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RolUsuarioTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the rol_usuario table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RolUsuarioQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RolUsuario or Criteria object.
     *
     * @param mixed               $criteria Criteria or RolUsuario object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolUsuarioTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RolUsuario object
        }

        if ($criteria->containsKey(RolUsuarioTableMap::COL_CLAVE) && $criteria->keyContainsValue(RolUsuarioTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RolUsuarioTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = RolUsuarioQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RolUsuarioTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RolUsuarioTableMap::buildTableMap();
