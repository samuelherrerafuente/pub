<?php

namespace Map;

use \ProgramaRequisito;
use \ProgramaRequisitoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'programa_requisito' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ProgramaRequisitoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ProgramaRequisitoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'programa_requisito';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\ProgramaRequisito';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'ProgramaRequisito';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'programa_requisito.clave';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'programa_requisito.nombre';

    /**
     * the column name for the descripcion field
     */
    const COL_DESCRIPCION = 'programa_requisito.descripcion';

    /**
     * the column name for the id_programa field
     */
    const COL_ID_PROGRAMA = 'programa_requisito.id_programa';

    /**
     * the column name for the id_etapa field
     */
    const COL_ID_ETAPA = 'programa_requisito.id_etapa';

    /**
     * the column name for the id_tipo_requisito field
     */
    const COL_ID_TIPO_REQUISITO = 'programa_requisito.id_tipo_requisito';

    /**
     * the column name for the obligatorio field
     */
    const COL_OBLIGATORIO = 'programa_requisito.obligatorio';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'programa_requisito.activo';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'programa_requisito.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'programa_requisito.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'programa_requisito.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Nombre', 'Descripcion', 'IdPrograma', 'IdEtapa', 'IdTipoRequisito', 'Obligatorio', 'Activo', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'nombre', 'descripcion', 'idPrograma', 'idEtapa', 'idTipoRequisito', 'obligatorio', 'activo', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(ProgramaRequisitoTableMap::COL_CLAVE, ProgramaRequisitoTableMap::COL_NOMBRE, ProgramaRequisitoTableMap::COL_DESCRIPCION, ProgramaRequisitoTableMap::COL_ID_PROGRAMA, ProgramaRequisitoTableMap::COL_ID_ETAPA, ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO, ProgramaRequisitoTableMap::COL_OBLIGATORIO, ProgramaRequisitoTableMap::COL_ACTIVO, ProgramaRequisitoTableMap::COL_FECHA_CREACION, ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION, ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'nombre', 'descripcion', 'id_programa', 'id_etapa', 'id_tipo_requisito', 'obligatorio', 'activo', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Nombre' => 1, 'Descripcion' => 2, 'IdPrograma' => 3, 'IdEtapa' => 4, 'IdTipoRequisito' => 5, 'Obligatorio' => 6, 'Activo' => 7, 'FechaCreacion' => 8, 'FechaModificacion' => 9, 'IdUsuarioModificacion' => 10, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'idPrograma' => 3, 'idEtapa' => 4, 'idTipoRequisito' => 5, 'obligatorio' => 6, 'activo' => 7, 'fechaCreacion' => 8, 'fechaModificacion' => 9, 'idUsuarioModificacion' => 10, ),
        self::TYPE_COLNAME       => array(ProgramaRequisitoTableMap::COL_CLAVE => 0, ProgramaRequisitoTableMap::COL_NOMBRE => 1, ProgramaRequisitoTableMap::COL_DESCRIPCION => 2, ProgramaRequisitoTableMap::COL_ID_PROGRAMA => 3, ProgramaRequisitoTableMap::COL_ID_ETAPA => 4, ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO => 5, ProgramaRequisitoTableMap::COL_OBLIGATORIO => 6, ProgramaRequisitoTableMap::COL_ACTIVO => 7, ProgramaRequisitoTableMap::COL_FECHA_CREACION => 8, ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION => 9, ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION => 10, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'id_programa' => 3, 'id_etapa' => 4, 'id_tipo_requisito' => 5, 'obligatorio' => 6, 'activo' => 7, 'fecha_creacion' => 8, 'fecha_modificacion' => 9, 'id_usuario_modificacion' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'ProgramaRequisito.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'programaRequisito.clave' => 'CLAVE',
        'ProgramaRequisitoTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'programa_requisito.clave' => 'CLAVE',
        'Nombre' => 'NOMBRE',
        'ProgramaRequisito.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'programaRequisito.nombre' => 'NOMBRE',
        'ProgramaRequisitoTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'programa_requisito.nombre' => 'NOMBRE',
        'Descripcion' => 'DESCRIPCION',
        'ProgramaRequisito.Descripcion' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'programaRequisito.descripcion' => 'DESCRIPCION',
        'ProgramaRequisitoTableMap::COL_DESCRIPCION' => 'DESCRIPCION',
        'COL_DESCRIPCION' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'programa_requisito.descripcion' => 'DESCRIPCION',
        'IdPrograma' => 'ID_PROGRAMA',
        'ProgramaRequisito.IdPrograma' => 'ID_PROGRAMA',
        'idPrograma' => 'ID_PROGRAMA',
        'programaRequisito.idPrograma' => 'ID_PROGRAMA',
        'ProgramaRequisitoTableMap::COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'id_programa' => 'ID_PROGRAMA',
        'programa_requisito.id_programa' => 'ID_PROGRAMA',
        'IdEtapa' => 'ID_ETAPA',
        'ProgramaRequisito.IdEtapa' => 'ID_ETAPA',
        'idEtapa' => 'ID_ETAPA',
        'programaRequisito.idEtapa' => 'ID_ETAPA',
        'ProgramaRequisitoTableMap::COL_ID_ETAPA' => 'ID_ETAPA',
        'COL_ID_ETAPA' => 'ID_ETAPA',
        'id_etapa' => 'ID_ETAPA',
        'programa_requisito.id_etapa' => 'ID_ETAPA',
        'IdTipoRequisito' => 'ID_TIPO_REQUISITO',
        'ProgramaRequisito.IdTipoRequisito' => 'ID_TIPO_REQUISITO',
        'idTipoRequisito' => 'ID_TIPO_REQUISITO',
        'programaRequisito.idTipoRequisito' => 'ID_TIPO_REQUISITO',
        'ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO' => 'ID_TIPO_REQUISITO',
        'COL_ID_TIPO_REQUISITO' => 'ID_TIPO_REQUISITO',
        'id_tipo_requisito' => 'ID_TIPO_REQUISITO',
        'programa_requisito.id_tipo_requisito' => 'ID_TIPO_REQUISITO',
        'Obligatorio' => 'OBLIGATORIO',
        'ProgramaRequisito.Obligatorio' => 'OBLIGATORIO',
        'obligatorio' => 'OBLIGATORIO',
        'programaRequisito.obligatorio' => 'OBLIGATORIO',
        'ProgramaRequisitoTableMap::COL_OBLIGATORIO' => 'OBLIGATORIO',
        'COL_OBLIGATORIO' => 'OBLIGATORIO',
        'obligatorio' => 'OBLIGATORIO',
        'programa_requisito.obligatorio' => 'OBLIGATORIO',
        'Activo' => 'ACTIVO',
        'ProgramaRequisito.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'programaRequisito.activo' => 'ACTIVO',
        'ProgramaRequisitoTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'programa_requisito.activo' => 'ACTIVO',
        'FechaCreacion' => 'FECHA_CREACION',
        'ProgramaRequisito.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'programaRequisito.fechaCreacion' => 'FECHA_CREACION',
        'ProgramaRequisitoTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'programa_requisito.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'ProgramaRequisito.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'programaRequisito.fechaModificacion' => 'FECHA_MODIFICACION',
        'ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'programa_requisito.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'ProgramaRequisito.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'programaRequisito.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'programa_requisito.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('programa_requisito');
        $this->setPhpName('ProgramaRequisito');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\ProgramaRequisito');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('descripcion', 'Descripcion', 'VARCHAR', false, 2000, null);
        $this->addForeignKey('id_programa', 'IdPrograma', 'INTEGER', 'programa', 'clave', true, null, null);
        $this->addForeignKey('id_etapa', 'IdEtapa', 'INTEGER', 'etapa', 'clave', true, null, null);
        $this->addForeignKey('id_tipo_requisito', 'IdTipoRequisito', 'INTEGER', 'tipo_requisito', 'clave', true, null, null);
        $this->addColumn('obligatorio', 'Obligatorio', 'INTEGER', true, null, 0);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Etapa', '\\Etapa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_etapa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Programa', '\\Programa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('TipoRequisito', '\\TipoRequisito', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tipo_requisito',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProgramaRequisitoTableMap::CLASS_DEFAULT : ProgramaRequisitoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProgramaRequisito object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProgramaRequisitoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProgramaRequisitoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProgramaRequisitoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProgramaRequisitoTableMap::OM_CLASS;
            /** @var ProgramaRequisito $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProgramaRequisitoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProgramaRequisitoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProgramaRequisitoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProgramaRequisito $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProgramaRequisitoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_CLAVE);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_DESCRIPCION);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_ID_PROGRAMA);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_ID_ETAPA);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_OBLIGATORIO);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.descripcion');
            $criteria->addSelectColumn($alias . '.id_programa');
            $criteria->addSelectColumn($alias . '.id_etapa');
            $criteria->addSelectColumn($alias . '.id_tipo_requisito');
            $criteria->addSelectColumn($alias . '.obligatorio');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_DESCRIPCION);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_ID_PROGRAMA);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_ID_ETAPA);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_ID_TIPO_REQUISITO);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_OBLIGATORIO);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(ProgramaRequisitoTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.descripcion');
            $criteria->removeSelectColumn($alias . '.id_programa');
            $criteria->removeSelectColumn($alias . '.id_etapa');
            $criteria->removeSelectColumn($alias . '.id_tipo_requisito');
            $criteria->removeSelectColumn($alias . '.obligatorio');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProgramaRequisitoTableMap::DATABASE_NAME)->getTable(ProgramaRequisitoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProgramaRequisitoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProgramaRequisitoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProgramaRequisitoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProgramaRequisito or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProgramaRequisito object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaRequisitoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \ProgramaRequisito) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProgramaRequisitoTableMap::DATABASE_NAME);
            $criteria->add(ProgramaRequisitoTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = ProgramaRequisitoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProgramaRequisitoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProgramaRequisitoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the programa_requisito table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProgramaRequisitoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProgramaRequisito or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProgramaRequisito object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaRequisitoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProgramaRequisito object
        }

        if ($criteria->containsKey(ProgramaRequisitoTableMap::COL_CLAVE) && $criteria->keyContainsValue(ProgramaRequisitoTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProgramaRequisitoTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = ProgramaRequisitoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProgramaRequisitoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProgramaRequisitoTableMap::buildTableMap();
