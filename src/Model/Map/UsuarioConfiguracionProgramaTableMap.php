<?php

namespace Map;

use \UsuarioConfiguracionPrograma;
use \UsuarioConfiguracionProgramaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'usuario_configuracion_programa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UsuarioConfiguracionProgramaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.UsuarioConfiguracionProgramaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'usuario_configuracion_programa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\UsuarioConfiguracionPrograma';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'UsuarioConfiguracionPrograma';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'usuario_configuracion_programa.clave';

    /**
     * the column name for the id_programa field
     */
    const COL_ID_PROGRAMA = 'usuario_configuracion_programa.id_programa';

    /**
     * the column name for the id_usuario field
     */
    const COL_ID_USUARIO = 'usuario_configuracion_programa.id_usuario';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'usuario_configuracion_programa.activo';

    /**
     * the column name for the p_permisos field
     */
    const COL_P_PERMISOS = 'usuario_configuracion_programa.p_permisos';

    /**
     * the column name for the p_consulta field
     */
    const COL_P_CONSULTA = 'usuario_configuracion_programa.p_consulta';

    /**
     * the column name for the p_edicion field
     */
    const COL_P_EDICION = 'usuario_configuracion_programa.p_edicion';

    /**
     * the column name for the p_estatus field
     */
    const COL_P_ESTATUS = 'usuario_configuracion_programa.p_estatus';

    /**
     * the column name for the p_desactivar field
     */
    const COL_P_DESACTIVAR = 'usuario_configuracion_programa.p_desactivar';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'usuario_configuracion_programa.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'usuario_configuracion_programa.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'usuario_configuracion_programa.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdPrograma', 'IdUsuario', 'Activo', 'PPermisos', 'PConsulta', 'PEdicion', 'PEstatus', 'PDesactivar', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'idPrograma', 'idUsuario', 'activo', 'pPermisos', 'pConsulta', 'pEdicion', 'pEstatus', 'pDesactivar', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA, UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO, UsuarioConfiguracionProgramaTableMap::COL_ACTIVO, UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS, UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA, UsuarioConfiguracionProgramaTableMap::COL_P_EDICION, UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS, UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR, UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION, UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION, UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_programa', 'id_usuario', 'activo', 'p_permisos', 'p_consulta', 'p_edicion', 'p_estatus', 'p_desactivar', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdPrograma' => 1, 'IdUsuario' => 2, 'Activo' => 3, 'PPermisos' => 4, 'PConsulta' => 5, 'PEdicion' => 6, 'PEstatus' => 7, 'PDesactivar' => 8, 'FechaCreacion' => 9, 'FechaModificacion' => 10, 'IdUsuarioModificacion' => 11, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idPrograma' => 1, 'idUsuario' => 2, 'activo' => 3, 'pPermisos' => 4, 'pConsulta' => 5, 'pEdicion' => 6, 'pEstatus' => 7, 'pDesactivar' => 8, 'fechaCreacion' => 9, 'fechaModificacion' => 10, 'idUsuarioModificacion' => 11, ),
        self::TYPE_COLNAME       => array(UsuarioConfiguracionProgramaTableMap::COL_CLAVE => 0, UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA => 1, UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO => 2, UsuarioConfiguracionProgramaTableMap::COL_ACTIVO => 3, UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS => 4, UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA => 5, UsuarioConfiguracionProgramaTableMap::COL_P_EDICION => 6, UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS => 7, UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR => 8, UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION => 9, UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION => 10, UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION => 11, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_programa' => 1, 'id_usuario' => 2, 'activo' => 3, 'p_permisos' => 4, 'p_consulta' => 5, 'p_edicion' => 6, 'p_estatus' => 7, 'p_desactivar' => 8, 'fecha_creacion' => 9, 'fecha_modificacion' => 10, 'id_usuario_modificacion' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'UsuarioConfiguracionPrograma.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuarioConfiguracionPrograma.clave' => 'CLAVE',
        'UsuarioConfiguracionProgramaTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuario_configuracion_programa.clave' => 'CLAVE',
        'IdPrograma' => 'ID_PROGRAMA',
        'UsuarioConfiguracionPrograma.IdPrograma' => 'ID_PROGRAMA',
        'idPrograma' => 'ID_PROGRAMA',
        'usuarioConfiguracionPrograma.idPrograma' => 'ID_PROGRAMA',
        'UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'COL_ID_PROGRAMA' => 'ID_PROGRAMA',
        'id_programa' => 'ID_PROGRAMA',
        'usuario_configuracion_programa.id_programa' => 'ID_PROGRAMA',
        'IdUsuario' => 'ID_USUARIO',
        'UsuarioConfiguracionPrograma.IdUsuario' => 'ID_USUARIO',
        'idUsuario' => 'ID_USUARIO',
        'usuarioConfiguracionPrograma.idUsuario' => 'ID_USUARIO',
        'UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO' => 'ID_USUARIO',
        'COL_ID_USUARIO' => 'ID_USUARIO',
        'id_usuario' => 'ID_USUARIO',
        'usuario_configuracion_programa.id_usuario' => 'ID_USUARIO',
        'Activo' => 'ACTIVO',
        'UsuarioConfiguracionPrograma.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuarioConfiguracionPrograma.activo' => 'ACTIVO',
        'UsuarioConfiguracionProgramaTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuario_configuracion_programa.activo' => 'ACTIVO',
        'PPermisos' => 'P_PERMISOS',
        'UsuarioConfiguracionPrograma.PPermisos' => 'P_PERMISOS',
        'pPermisos' => 'P_PERMISOS',
        'usuarioConfiguracionPrograma.pPermisos' => 'P_PERMISOS',
        'UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS' => 'P_PERMISOS',
        'COL_P_PERMISOS' => 'P_PERMISOS',
        'p_permisos' => 'P_PERMISOS',
        'usuario_configuracion_programa.p_permisos' => 'P_PERMISOS',
        'PConsulta' => 'P_CONSULTA',
        'UsuarioConfiguracionPrograma.PConsulta' => 'P_CONSULTA',
        'pConsulta' => 'P_CONSULTA',
        'usuarioConfiguracionPrograma.pConsulta' => 'P_CONSULTA',
        'UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA' => 'P_CONSULTA',
        'COL_P_CONSULTA' => 'P_CONSULTA',
        'p_consulta' => 'P_CONSULTA',
        'usuario_configuracion_programa.p_consulta' => 'P_CONSULTA',
        'PEdicion' => 'P_EDICION',
        'UsuarioConfiguracionPrograma.PEdicion' => 'P_EDICION',
        'pEdicion' => 'P_EDICION',
        'usuarioConfiguracionPrograma.pEdicion' => 'P_EDICION',
        'UsuarioConfiguracionProgramaTableMap::COL_P_EDICION' => 'P_EDICION',
        'COL_P_EDICION' => 'P_EDICION',
        'p_edicion' => 'P_EDICION',
        'usuario_configuracion_programa.p_edicion' => 'P_EDICION',
        'PEstatus' => 'P_ESTATUS',
        'UsuarioConfiguracionPrograma.PEstatus' => 'P_ESTATUS',
        'pEstatus' => 'P_ESTATUS',
        'usuarioConfiguracionPrograma.pEstatus' => 'P_ESTATUS',
        'UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS' => 'P_ESTATUS',
        'COL_P_ESTATUS' => 'P_ESTATUS',
        'p_estatus' => 'P_ESTATUS',
        'usuario_configuracion_programa.p_estatus' => 'P_ESTATUS',
        'PDesactivar' => 'P_DESACTIVAR',
        'UsuarioConfiguracionPrograma.PDesactivar' => 'P_DESACTIVAR',
        'pDesactivar' => 'P_DESACTIVAR',
        'usuarioConfiguracionPrograma.pDesactivar' => 'P_DESACTIVAR',
        'UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR' => 'P_DESACTIVAR',
        'COL_P_DESACTIVAR' => 'P_DESACTIVAR',
        'p_desactivar' => 'P_DESACTIVAR',
        'usuario_configuracion_programa.p_desactivar' => 'P_DESACTIVAR',
        'FechaCreacion' => 'FECHA_CREACION',
        'UsuarioConfiguracionPrograma.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'usuarioConfiguracionPrograma.fechaCreacion' => 'FECHA_CREACION',
        'UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'usuario_configuracion_programa.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'UsuarioConfiguracionPrograma.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'usuarioConfiguracionPrograma.fechaModificacion' => 'FECHA_MODIFICACION',
        'UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'usuario_configuracion_programa.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'UsuarioConfiguracionPrograma.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'usuarioConfiguracionPrograma.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'usuario_configuracion_programa.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario_configuracion_programa');
        $this->setPhpName('UsuarioConfiguracionPrograma');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\UsuarioConfiguracionPrograma');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_programa', 'IdPrograma', 'INTEGER', 'programa', 'clave', true, null, null);
        $this->addForeignKey('id_usuario', 'IdUsuario', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('p_permisos', 'PPermisos', 'INTEGER', true, null, 0);
        $this->addColumn('p_consulta', 'PConsulta', 'INTEGER', true, null, 1);
        $this->addColumn('p_edicion', 'PEdicion', 'INTEGER', true, null, 0);
        $this->addColumn('p_estatus', 'PEstatus', 'INTEGER', true, null, 0);
        $this->addColumn('p_desactivar', 'PDesactivar', 'INTEGER', true, null, 0);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Programa', '\\Programa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UsuarioRelatedByIdUsuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UsuarioRelatedByIdUsuarioModificacion', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UsuarioConfiguracionEtapa', '\\UsuarioConfiguracionEtapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_usuario_configuracion_programa',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionEtapas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UsuarioConfiguracionProgramaTableMap::CLASS_DEFAULT : UsuarioConfiguracionProgramaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UsuarioConfiguracionPrograma object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UsuarioConfiguracionProgramaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UsuarioConfiguracionProgramaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UsuarioConfiguracionProgramaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UsuarioConfiguracionProgramaTableMap::OM_CLASS;
            /** @var UsuarioConfiguracionPrograma $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UsuarioConfiguracionProgramaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UsuarioConfiguracionProgramaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UsuarioConfiguracionProgramaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UsuarioConfiguracionPrograma $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UsuarioConfiguracionProgramaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_CLAVE);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_EDICION);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_programa');
            $criteria->addSelectColumn($alias . '.id_usuario');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.p_permisos');
            $criteria->addSelectColumn($alias . '.p_consulta');
            $criteria->addSelectColumn($alias . '.p_edicion');
            $criteria->addSelectColumn($alias . '.p_estatus');
            $criteria->addSelectColumn($alias . '.p_desactivar');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_PROGRAMA);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_PERMISOS);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_CONSULTA);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_EDICION);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_ESTATUS);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_P_DESACTIVAR);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(UsuarioConfiguracionProgramaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_programa');
            $criteria->removeSelectColumn($alias . '.id_usuario');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.p_permisos');
            $criteria->removeSelectColumn($alias . '.p_consulta');
            $criteria->removeSelectColumn($alias . '.p_edicion');
            $criteria->removeSelectColumn($alias . '.p_estatus');
            $criteria->removeSelectColumn($alias . '.p_desactivar');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME)->getTable(UsuarioConfiguracionProgramaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UsuarioConfiguracionProgramaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UsuarioConfiguracionProgramaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UsuarioConfiguracionPrograma or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UsuarioConfiguracionPrograma object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \UsuarioConfiguracionPrograma) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
            $criteria->add(UsuarioConfiguracionProgramaTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = UsuarioConfiguracionProgramaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UsuarioConfiguracionProgramaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UsuarioConfiguracionProgramaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the usuario_configuracion_programa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UsuarioConfiguracionProgramaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UsuarioConfiguracionPrograma or Criteria object.
     *
     * @param mixed               $criteria Criteria or UsuarioConfiguracionPrograma object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionProgramaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UsuarioConfiguracionPrograma object
        }

        if ($criteria->containsKey(UsuarioConfiguracionProgramaTableMap::COL_CLAVE) && $criteria->keyContainsValue(UsuarioConfiguracionProgramaTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UsuarioConfiguracionProgramaTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = UsuarioConfiguracionProgramaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UsuarioConfiguracionProgramaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UsuarioConfiguracionProgramaTableMap::buildTableMap();
