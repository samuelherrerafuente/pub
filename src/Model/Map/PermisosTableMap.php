<?php

namespace Map;

use \Permisos;
use \PermisosQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'permisos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PermisosTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.PermisosTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'permisos';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Permisos';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Permisos';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'permisos.clave';

    /**
     * the column name for the seccion field
     */
    const COL_SECCION = 'permisos.seccion';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'permisos.nombre';

    /**
     * the column name for the descripcion field
     */
    const COL_DESCRIPCION = 'permisos.descripcion';

    /**
     * the column name for the siglas field
     */
    const COL_SIGLAS = 'permisos.siglas';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'permisos.activo';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Seccion', 'Nombre', 'Descripcion', 'Siglas', 'Activo', ),
        self::TYPE_CAMELNAME     => array('clave', 'seccion', 'nombre', 'descripcion', 'siglas', 'activo', ),
        self::TYPE_COLNAME       => array(PermisosTableMap::COL_CLAVE, PermisosTableMap::COL_SECCION, PermisosTableMap::COL_NOMBRE, PermisosTableMap::COL_DESCRIPCION, PermisosTableMap::COL_SIGLAS, PermisosTableMap::COL_ACTIVO, ),
        self::TYPE_FIELDNAME     => array('clave', 'seccion', 'nombre', 'descripcion', 'siglas', 'activo', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Seccion' => 1, 'Nombre' => 2, 'Descripcion' => 3, 'Siglas' => 4, 'Activo' => 5, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'seccion' => 1, 'nombre' => 2, 'descripcion' => 3, 'siglas' => 4, 'activo' => 5, ),
        self::TYPE_COLNAME       => array(PermisosTableMap::COL_CLAVE => 0, PermisosTableMap::COL_SECCION => 1, PermisosTableMap::COL_NOMBRE => 2, PermisosTableMap::COL_DESCRIPCION => 3, PermisosTableMap::COL_SIGLAS => 4, PermisosTableMap::COL_ACTIVO => 5, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'seccion' => 1, 'nombre' => 2, 'descripcion' => 3, 'siglas' => 4, 'activo' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Permisos.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'permisos.clave' => 'CLAVE',
        'PermisosTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'permisos.clave' => 'CLAVE',
        'Seccion' => 'SECCION',
        'Permisos.Seccion' => 'SECCION',
        'seccion' => 'SECCION',
        'permisos.seccion' => 'SECCION',
        'PermisosTableMap::COL_SECCION' => 'SECCION',
        'COL_SECCION' => 'SECCION',
        'seccion' => 'SECCION',
        'permisos.seccion' => 'SECCION',
        'Nombre' => 'NOMBRE',
        'Permisos.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'permisos.nombre' => 'NOMBRE',
        'PermisosTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'permisos.nombre' => 'NOMBRE',
        'Descripcion' => 'DESCRIPCION',
        'Permisos.Descripcion' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'permisos.descripcion' => 'DESCRIPCION',
        'PermisosTableMap::COL_DESCRIPCION' => 'DESCRIPCION',
        'COL_DESCRIPCION' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'permisos.descripcion' => 'DESCRIPCION',
        'Siglas' => 'SIGLAS',
        'Permisos.Siglas' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'permisos.siglas' => 'SIGLAS',
        'PermisosTableMap::COL_SIGLAS' => 'SIGLAS',
        'COL_SIGLAS' => 'SIGLAS',
        'siglas' => 'SIGLAS',
        'permisos.siglas' => 'SIGLAS',
        'Activo' => 'ACTIVO',
        'Permisos.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'permisos.activo' => 'ACTIVO',
        'PermisosTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'permisos.activo' => 'ACTIVO',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('permisos');
        $this->setPhpName('Permisos');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Permisos');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('seccion', 'Seccion', 'VARCHAR', true, 1000, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('descripcion', 'Descripcion', 'VARCHAR', true, 2000, null);
        $this->addColumn('siglas', 'Siglas', 'VARCHAR', true, 1000, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RolPermiso', '\\RolPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_permiso',
    1 => ':clave',
  ),
), null, null, 'RolPermisos', false);
        $this->addRelation('UsuarioPermiso', '\\UsuarioPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_permiso',
    1 => ':clave',
  ),
), null, null, 'UsuarioPermisos', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PermisosTableMap::CLASS_DEFAULT : PermisosTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Permisos object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PermisosTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PermisosTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PermisosTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PermisosTableMap::OM_CLASS;
            /** @var Permisos $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PermisosTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PermisosTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PermisosTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Permisos $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PermisosTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PermisosTableMap::COL_CLAVE);
            $criteria->addSelectColumn(PermisosTableMap::COL_SECCION);
            $criteria->addSelectColumn(PermisosTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(PermisosTableMap::COL_DESCRIPCION);
            $criteria->addSelectColumn(PermisosTableMap::COL_SIGLAS);
            $criteria->addSelectColumn(PermisosTableMap::COL_ACTIVO);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.seccion');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.descripcion');
            $criteria->addSelectColumn($alias . '.siglas');
            $criteria->addSelectColumn($alias . '.activo');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(PermisosTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(PermisosTableMap::COL_SECCION);
            $criteria->removeSelectColumn(PermisosTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(PermisosTableMap::COL_DESCRIPCION);
            $criteria->removeSelectColumn(PermisosTableMap::COL_SIGLAS);
            $criteria->removeSelectColumn(PermisosTableMap::COL_ACTIVO);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.seccion');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.descripcion');
            $criteria->removeSelectColumn($alias . '.siglas');
            $criteria->removeSelectColumn($alias . '.activo');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PermisosTableMap::DATABASE_NAME)->getTable(PermisosTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PermisosTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PermisosTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PermisosTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Permisos or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Permisos object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PermisosTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Permisos) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PermisosTableMap::DATABASE_NAME);
            $criteria->add(PermisosTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = PermisosQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PermisosTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PermisosTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the permisos table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PermisosQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Permisos or Criteria object.
     *
     * @param mixed               $criteria Criteria or Permisos object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PermisosTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Permisos object
        }


        // Set the correct dbName
        $query = PermisosQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PermisosTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PermisosTableMap::buildTableMap();
