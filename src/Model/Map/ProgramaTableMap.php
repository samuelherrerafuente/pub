<?php

namespace Map;

use \Programa;
use \ProgramaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'programa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ProgramaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.ProgramaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'programa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Programa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Programa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 14;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 14;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'programa.clave';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'programa.nombre';

    /**
     * the column name for the descripcion field
     */
    const COL_DESCRIPCION = 'programa.descripcion';

    /**
     * the column name for the fecha_inicio field
     */
    const COL_FECHA_INICIO = 'programa.fecha_inicio';

    /**
     * the column name for the fecha_fin field
     */
    const COL_FECHA_FIN = 'programa.fecha_fin';

    /**
     * the column name for the id_entidad_operativa field
     */
    const COL_ID_ENTIDAD_OPERATIVA = 'programa.id_entidad_operativa';

    /**
     * the column name for the id_estatus_programa field
     */
    const COL_ID_ESTATUS_PROGRAMA = 'programa.id_estatus_programa';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'programa.activo';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'programa.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'programa.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'programa.id_usuario_modificacion';

    /**
     * the column name for the id_tipo_bebeficiario field
     */
    const COL_ID_TIPO_BEBEFICIARIO = 'programa.id_tipo_bebeficiario';

    /**
     * the column name for the persona_moral field
     */
    const COL_PERSONA_MORAL = 'programa.persona_moral';

    /**
     * the column name for the digrama_yucatan field
     */
    const COL_DIGRAMA_YUCATAN = 'programa.digrama_yucatan';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Nombre', 'Descripcion', 'FechaInicio', 'FechaFin', 'IdEntidadOperativa', 'IdEstatusPrograma', 'Activo', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', 'IdTipoBebeficiario', 'PersonaMoral', 'DigramaYucatan', ),
        self::TYPE_CAMELNAME     => array('clave', 'nombre', 'descripcion', 'fechaInicio', 'fechaFin', 'idEntidadOperativa', 'idEstatusPrograma', 'activo', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', 'idTipoBebeficiario', 'personaMoral', 'digramaYucatan', ),
        self::TYPE_COLNAME       => array(ProgramaTableMap::COL_CLAVE, ProgramaTableMap::COL_NOMBRE, ProgramaTableMap::COL_DESCRIPCION, ProgramaTableMap::COL_FECHA_INICIO, ProgramaTableMap::COL_FECHA_FIN, ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA, ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA, ProgramaTableMap::COL_ACTIVO, ProgramaTableMap::COL_FECHA_CREACION, ProgramaTableMap::COL_FECHA_MODIFICACION, ProgramaTableMap::COL_ID_USUARIO_MODIFICACION, ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO, ProgramaTableMap::COL_PERSONA_MORAL, ProgramaTableMap::COL_DIGRAMA_YUCATAN, ),
        self::TYPE_FIELDNAME     => array('clave', 'nombre', 'descripcion', 'fecha_inicio', 'fecha_fin', 'id_entidad_operativa', 'id_estatus_programa', 'activo', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', 'id_tipo_bebeficiario', 'persona_moral', 'digrama_yucatan', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Nombre' => 1, 'Descripcion' => 2, 'FechaInicio' => 3, 'FechaFin' => 4, 'IdEntidadOperativa' => 5, 'IdEstatusPrograma' => 6, 'Activo' => 7, 'FechaCreacion' => 8, 'FechaModificacion' => 9, 'IdUsuarioModificacion' => 10, 'IdTipoBebeficiario' => 11, 'PersonaMoral' => 12, 'DigramaYucatan' => 13, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'fechaInicio' => 3, 'fechaFin' => 4, 'idEntidadOperativa' => 5, 'idEstatusPrograma' => 6, 'activo' => 7, 'fechaCreacion' => 8, 'fechaModificacion' => 9, 'idUsuarioModificacion' => 10, 'idTipoBebeficiario' => 11, 'personaMoral' => 12, 'digramaYucatan' => 13, ),
        self::TYPE_COLNAME       => array(ProgramaTableMap::COL_CLAVE => 0, ProgramaTableMap::COL_NOMBRE => 1, ProgramaTableMap::COL_DESCRIPCION => 2, ProgramaTableMap::COL_FECHA_INICIO => 3, ProgramaTableMap::COL_FECHA_FIN => 4, ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA => 5, ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA => 6, ProgramaTableMap::COL_ACTIVO => 7, ProgramaTableMap::COL_FECHA_CREACION => 8, ProgramaTableMap::COL_FECHA_MODIFICACION => 9, ProgramaTableMap::COL_ID_USUARIO_MODIFICACION => 10, ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO => 11, ProgramaTableMap::COL_PERSONA_MORAL => 12, ProgramaTableMap::COL_DIGRAMA_YUCATAN => 13, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'fecha_inicio' => 3, 'fecha_fin' => 4, 'id_entidad_operativa' => 5, 'id_estatus_programa' => 6, 'activo' => 7, 'fecha_creacion' => 8, 'fecha_modificacion' => 9, 'id_usuario_modificacion' => 10, 'id_tipo_bebeficiario' => 11, 'persona_moral' => 12, 'digrama_yucatan' => 13, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Programa.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'programa.clave' => 'CLAVE',
        'ProgramaTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'programa.clave' => 'CLAVE',
        'Nombre' => 'NOMBRE',
        'Programa.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'programa.nombre' => 'NOMBRE',
        'ProgramaTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'programa.nombre' => 'NOMBRE',
        'Descripcion' => 'DESCRIPCION',
        'Programa.Descripcion' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'programa.descripcion' => 'DESCRIPCION',
        'ProgramaTableMap::COL_DESCRIPCION' => 'DESCRIPCION',
        'COL_DESCRIPCION' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'programa.descripcion' => 'DESCRIPCION',
        'FechaInicio' => 'FECHA_INICIO',
        'Programa.FechaInicio' => 'FECHA_INICIO',
        'fechaInicio' => 'FECHA_INICIO',
        'programa.fechaInicio' => 'FECHA_INICIO',
        'ProgramaTableMap::COL_FECHA_INICIO' => 'FECHA_INICIO',
        'COL_FECHA_INICIO' => 'FECHA_INICIO',
        'fecha_inicio' => 'FECHA_INICIO',
        'programa.fecha_inicio' => 'FECHA_INICIO',
        'FechaFin' => 'FECHA_FIN',
        'Programa.FechaFin' => 'FECHA_FIN',
        'fechaFin' => 'FECHA_FIN',
        'programa.fechaFin' => 'FECHA_FIN',
        'ProgramaTableMap::COL_FECHA_FIN' => 'FECHA_FIN',
        'COL_FECHA_FIN' => 'FECHA_FIN',
        'fecha_fin' => 'FECHA_FIN',
        'programa.fecha_fin' => 'FECHA_FIN',
        'IdEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'Programa.IdEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'idEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'programa.idEntidadOperativa' => 'ID_ENTIDAD_OPERATIVA',
        'ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA' => 'ID_ENTIDAD_OPERATIVA',
        'COL_ID_ENTIDAD_OPERATIVA' => 'ID_ENTIDAD_OPERATIVA',
        'id_entidad_operativa' => 'ID_ENTIDAD_OPERATIVA',
        'programa.id_entidad_operativa' => 'ID_ENTIDAD_OPERATIVA',
        'IdEstatusPrograma' => 'ID_ESTATUS_PROGRAMA',
        'Programa.IdEstatusPrograma' => 'ID_ESTATUS_PROGRAMA',
        'idEstatusPrograma' => 'ID_ESTATUS_PROGRAMA',
        'programa.idEstatusPrograma' => 'ID_ESTATUS_PROGRAMA',
        'ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA' => 'ID_ESTATUS_PROGRAMA',
        'COL_ID_ESTATUS_PROGRAMA' => 'ID_ESTATUS_PROGRAMA',
        'id_estatus_programa' => 'ID_ESTATUS_PROGRAMA',
        'programa.id_estatus_programa' => 'ID_ESTATUS_PROGRAMA',
        'Activo' => 'ACTIVO',
        'Programa.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'programa.activo' => 'ACTIVO',
        'ProgramaTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'programa.activo' => 'ACTIVO',
        'FechaCreacion' => 'FECHA_CREACION',
        'Programa.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'programa.fechaCreacion' => 'FECHA_CREACION',
        'ProgramaTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'programa.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'Programa.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'programa.fechaModificacion' => 'FECHA_MODIFICACION',
        'ProgramaTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'programa.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'Programa.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'programa.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'ProgramaTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'programa.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'IdTipoBebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'Programa.IdTipoBebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'idTipoBebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'programa.idTipoBebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO' => 'ID_TIPO_BEBEFICIARIO',
        'COL_ID_TIPO_BEBEFICIARIO' => 'ID_TIPO_BEBEFICIARIO',
        'id_tipo_bebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'programa.id_tipo_bebeficiario' => 'ID_TIPO_BEBEFICIARIO',
        'PersonaMoral' => 'PERSONA_MORAL',
        'Programa.PersonaMoral' => 'PERSONA_MORAL',
        'personaMoral' => 'PERSONA_MORAL',
        'programa.personaMoral' => 'PERSONA_MORAL',
        'ProgramaTableMap::COL_PERSONA_MORAL' => 'PERSONA_MORAL',
        'COL_PERSONA_MORAL' => 'PERSONA_MORAL',
        'persona_moral' => 'PERSONA_MORAL',
        'programa.persona_moral' => 'PERSONA_MORAL',
        'DigramaYucatan' => 'DIGRAMA_YUCATAN',
        'Programa.DigramaYucatan' => 'DIGRAMA_YUCATAN',
        'digramaYucatan' => 'DIGRAMA_YUCATAN',
        'programa.digramaYucatan' => 'DIGRAMA_YUCATAN',
        'ProgramaTableMap::COL_DIGRAMA_YUCATAN' => 'DIGRAMA_YUCATAN',
        'COL_DIGRAMA_YUCATAN' => 'DIGRAMA_YUCATAN',
        'digrama_yucatan' => 'DIGRAMA_YUCATAN',
        'programa.digrama_yucatan' => 'DIGRAMA_YUCATAN',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('programa');
        $this->setPhpName('Programa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Programa');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('descripcion', 'Descripcion', 'VARCHAR', false, 2000, null);
        $this->addColumn('fecha_inicio', 'FechaInicio', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_fin', 'FechaFin', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('id_entidad_operativa', 'IdEntidadOperativa', 'INTEGER', 'entidad_operativa', 'clave', true, null, null);
        $this->addForeignKey('id_estatus_programa', 'IdEstatusPrograma', 'INTEGER', 'estatus_programa', 'clave', true, null, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addForeignKey('id_tipo_bebeficiario', 'IdTipoBebeficiario', 'INTEGER', 'tipo_beneficiario', 'clave', false, null, null);
        $this->addColumn('persona_moral', 'PersonaMoral', 'INTEGER', false, null, null);
        $this->addColumn('digrama_yucatan', 'DigramaYucatan', 'INTEGER', true, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('EntidadOperativa', '\\EntidadOperativa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_entidad_operativa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('EstatusPrograma', '\\EstatusPrograma', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_estatus_programa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('TipoBeneficiario', '\\TipoBeneficiario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_tipo_bebeficiario',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Beneficio', '\\Beneficio', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, 'Beneficios', false);
        $this->addRelation('Etapa', '\\Etapa', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, 'Etapas', false);
        $this->addRelation('ProgramaRequisito', '\\ProgramaRequisito', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, 'ProgramaRequisitos', false);
        $this->addRelation('UsuarioConfiguracionPrograma', '\\UsuarioConfiguracionPrograma', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_programa',
    1 => ':clave',
  ),
), null, null, 'UsuarioConfiguracionProgramas', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProgramaTableMap::CLASS_DEFAULT : ProgramaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Programa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProgramaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProgramaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProgramaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProgramaTableMap::OM_CLASS;
            /** @var Programa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProgramaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProgramaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProgramaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Programa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProgramaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProgramaTableMap::COL_CLAVE);
            $criteria->addSelectColumn(ProgramaTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(ProgramaTableMap::COL_DESCRIPCION);
            $criteria->addSelectColumn(ProgramaTableMap::COL_FECHA_INICIO);
            $criteria->addSelectColumn(ProgramaTableMap::COL_FECHA_FIN);
            $criteria->addSelectColumn(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA);
            $criteria->addSelectColumn(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA);
            $criteria->addSelectColumn(ProgramaTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(ProgramaTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(ProgramaTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->addSelectColumn(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO);
            $criteria->addSelectColumn(ProgramaTableMap::COL_PERSONA_MORAL);
            $criteria->addSelectColumn(ProgramaTableMap::COL_DIGRAMA_YUCATAN);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.descripcion');
            $criteria->addSelectColumn($alias . '.fecha_inicio');
            $criteria->addSelectColumn($alias . '.fecha_fin');
            $criteria->addSelectColumn($alias . '.id_entidad_operativa');
            $criteria->addSelectColumn($alias . '.id_estatus_programa');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->addSelectColumn($alias . '.id_tipo_bebeficiario');
            $criteria->addSelectColumn($alias . '.persona_moral');
            $criteria->addSelectColumn($alias . '.digrama_yucatan');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(ProgramaTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_DESCRIPCION);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_FECHA_INICIO);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_FECHA_FIN);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_ID_ENTIDAD_OPERATIVA);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_ID_ESTATUS_PROGRAMA);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_ID_TIPO_BEBEFICIARIO);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_PERSONA_MORAL);
            $criteria->removeSelectColumn(ProgramaTableMap::COL_DIGRAMA_YUCATAN);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.descripcion');
            $criteria->removeSelectColumn($alias . '.fecha_inicio');
            $criteria->removeSelectColumn($alias . '.fecha_fin');
            $criteria->removeSelectColumn($alias . '.id_entidad_operativa');
            $criteria->removeSelectColumn($alias . '.id_estatus_programa');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->removeSelectColumn($alias . '.id_tipo_bebeficiario');
            $criteria->removeSelectColumn($alias . '.persona_moral');
            $criteria->removeSelectColumn($alias . '.digrama_yucatan');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProgramaTableMap::DATABASE_NAME)->getTable(ProgramaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProgramaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProgramaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProgramaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Programa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Programa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Programa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProgramaTableMap::DATABASE_NAME);
            $criteria->add(ProgramaTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = ProgramaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProgramaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProgramaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the programa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProgramaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Programa or Criteria object.
     *
     * @param mixed               $criteria Criteria or Programa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProgramaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Programa object
        }

        if ($criteria->containsKey(ProgramaTableMap::COL_CLAVE) && $criteria->keyContainsValue(ProgramaTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProgramaTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = ProgramaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProgramaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProgramaTableMap::buildTableMap();
