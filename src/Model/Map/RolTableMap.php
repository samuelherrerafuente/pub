<?php

namespace Map;

use \Rol;
use \RolQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'rol' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class RolTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RolTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'rol';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Rol';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Rol';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'rol.clave';

    /**
     * the column name for the nombre field
     */
    const COL_NOMBRE = 'rol.nombre';

    /**
     * the column name for the descripcion field
     */
    const COL_DESCRIPCION = 'rol.descripcion';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'rol.activo';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'rol.id_usuario_modificacion';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'rol.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'rol.fecha_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Nombre', 'Descripcion', 'Activo', 'IdUsuarioModificacion', 'FechaCreacion', 'FechaModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'nombre', 'descripcion', 'activo', 'idUsuarioModificacion', 'fechaCreacion', 'fechaModificacion', ),
        self::TYPE_COLNAME       => array(RolTableMap::COL_CLAVE, RolTableMap::COL_NOMBRE, RolTableMap::COL_DESCRIPCION, RolTableMap::COL_ACTIVO, RolTableMap::COL_ID_USUARIO_MODIFICACION, RolTableMap::COL_FECHA_CREACION, RolTableMap::COL_FECHA_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'nombre', 'descripcion', 'activo', 'id_usuario_modificacion', 'fecha_creacion', 'fecha_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Nombre' => 1, 'Descripcion' => 2, 'Activo' => 3, 'IdUsuarioModificacion' => 4, 'FechaCreacion' => 5, 'FechaModificacion' => 6, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'activo' => 3, 'idUsuarioModificacion' => 4, 'fechaCreacion' => 5, 'fechaModificacion' => 6, ),
        self::TYPE_COLNAME       => array(RolTableMap::COL_CLAVE => 0, RolTableMap::COL_NOMBRE => 1, RolTableMap::COL_DESCRIPCION => 2, RolTableMap::COL_ACTIVO => 3, RolTableMap::COL_ID_USUARIO_MODIFICACION => 4, RolTableMap::COL_FECHA_CREACION => 5, RolTableMap::COL_FECHA_MODIFICACION => 6, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'nombre' => 1, 'descripcion' => 2, 'activo' => 3, 'id_usuario_modificacion' => 4, 'fecha_creacion' => 5, 'fecha_modificacion' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'Rol.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'rol.clave' => 'CLAVE',
        'RolTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'rol.clave' => 'CLAVE',
        'Nombre' => 'NOMBRE',
        'Rol.Nombre' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'rol.nombre' => 'NOMBRE',
        'RolTableMap::COL_NOMBRE' => 'NOMBRE',
        'COL_NOMBRE' => 'NOMBRE',
        'nombre' => 'NOMBRE',
        'rol.nombre' => 'NOMBRE',
        'Descripcion' => 'DESCRIPCION',
        'Rol.Descripcion' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'rol.descripcion' => 'DESCRIPCION',
        'RolTableMap::COL_DESCRIPCION' => 'DESCRIPCION',
        'COL_DESCRIPCION' => 'DESCRIPCION',
        'descripcion' => 'DESCRIPCION',
        'rol.descripcion' => 'DESCRIPCION',
        'Activo' => 'ACTIVO',
        'Rol.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rol.activo' => 'ACTIVO',
        'RolTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rol.activo' => 'ACTIVO',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'Rol.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'rol.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'RolTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'rol.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'FechaCreacion' => 'FECHA_CREACION',
        'Rol.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'rol.fechaCreacion' => 'FECHA_CREACION',
        'RolTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'rol.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'Rol.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'rol.fechaModificacion' => 'FECHA_MODIFICACION',
        'RolTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'rol.fecha_modificacion' => 'FECHA_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rol');
        $this->setPhpName('Rol');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Rol');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 1000, null);
        $this->addColumn('descripcion', 'Descripcion', 'VARCHAR', false, 2000, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('RolPermiso', '\\RolPermiso', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_rol',
    1 => ':clave',
  ),
), null, null, 'RolPermisos', false);
        $this->addRelation('RolUsuario', '\\RolUsuario', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':id_rol',
    1 => ':clave',
  ),
), null, null, 'RolUsuarios', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RolTableMap::CLASS_DEFAULT : RolTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Rol object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RolTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RolTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RolTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RolTableMap::OM_CLASS;
            /** @var Rol $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RolTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RolTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RolTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Rol $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RolTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RolTableMap::COL_CLAVE);
            $criteria->addSelectColumn(RolTableMap::COL_NOMBRE);
            $criteria->addSelectColumn(RolTableMap::COL_DESCRIPCION);
            $criteria->addSelectColumn(RolTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(RolTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->addSelectColumn(RolTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(RolTableMap::COL_FECHA_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.nombre');
            $criteria->addSelectColumn($alias . '.descripcion');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(RolTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(RolTableMap::COL_NOMBRE);
            $criteria->removeSelectColumn(RolTableMap::COL_DESCRIPCION);
            $criteria->removeSelectColumn(RolTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(RolTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->removeSelectColumn(RolTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(RolTableMap::COL_FECHA_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.nombre');
            $criteria->removeSelectColumn($alias . '.descripcion');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RolTableMap::DATABASE_NAME)->getTable(RolTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RolTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RolTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RolTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Rol or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Rol object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Rol) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RolTableMap::DATABASE_NAME);
            $criteria->add(RolTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = RolQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RolTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RolTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the rol table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RolQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Rol or Criteria object.
     *
     * @param mixed               $criteria Criteria or Rol object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Rol object
        }

        if ($criteria->containsKey(RolTableMap::COL_CLAVE) && $criteria->keyContainsValue(RolTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RolTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = RolQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RolTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RolTableMap::buildTableMap();
