<?php

namespace Map;

use \UsuarioConfiguracionEtapa;
use \UsuarioConfiguracionEtapaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'usuario_configuracion_etapa' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UsuarioConfiguracionEtapaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.UsuarioConfiguracionEtapaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'usuario_configuracion_etapa';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\UsuarioConfiguracionEtapa';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'UsuarioConfiguracionEtapa';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'usuario_configuracion_etapa.clave';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'usuario_configuracion_etapa.activo';

    /**
     * the column name for the id_usuario_configuracion_programa field
     */
    const COL_ID_USUARIO_CONFIGURACION_PROGRAMA = 'usuario_configuracion_etapa.id_usuario_configuracion_programa';

    /**
     * the column name for the id_etapa field
     */
    const COL_ID_ETAPA = 'usuario_configuracion_etapa.id_etapa';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'usuario_configuracion_etapa.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'usuario_configuracion_etapa.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'usuario_configuracion_etapa.id_usuario_modificacion';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'Activo', 'IdUsuarioConfiguracionPrograma', 'IdEtapa', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', ),
        self::TYPE_CAMELNAME     => array('clave', 'activo', 'idUsuarioConfiguracionPrograma', 'idEtapa', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', ),
        self::TYPE_COLNAME       => array(UsuarioConfiguracionEtapaTableMap::COL_CLAVE, UsuarioConfiguracionEtapaTableMap::COL_ACTIVO, UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_CONFIGURACION_PROGRAMA, UsuarioConfiguracionEtapaTableMap::COL_ID_ETAPA, UsuarioConfiguracionEtapaTableMap::COL_FECHA_CREACION, UsuarioConfiguracionEtapaTableMap::COL_FECHA_MODIFICACION, UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_MODIFICACION, ),
        self::TYPE_FIELDNAME     => array('clave', 'activo', 'id_usuario_configuracion_programa', 'id_etapa', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'Activo' => 1, 'IdUsuarioConfiguracionPrograma' => 2, 'IdEtapa' => 3, 'FechaCreacion' => 4, 'FechaModificacion' => 5, 'IdUsuarioModificacion' => 6, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'activo' => 1, 'idUsuarioConfiguracionPrograma' => 2, 'idEtapa' => 3, 'fechaCreacion' => 4, 'fechaModificacion' => 5, 'idUsuarioModificacion' => 6, ),
        self::TYPE_COLNAME       => array(UsuarioConfiguracionEtapaTableMap::COL_CLAVE => 0, UsuarioConfiguracionEtapaTableMap::COL_ACTIVO => 1, UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_CONFIGURACION_PROGRAMA => 2, UsuarioConfiguracionEtapaTableMap::COL_ID_ETAPA => 3, UsuarioConfiguracionEtapaTableMap::COL_FECHA_CREACION => 4, UsuarioConfiguracionEtapaTableMap::COL_FECHA_MODIFICACION => 5, UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_MODIFICACION => 6, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'activo' => 1, 'id_usuario_configuracion_programa' => 2, 'id_etapa' => 3, 'fecha_creacion' => 4, 'fecha_modificacion' => 5, 'id_usuario_modificacion' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'UsuarioConfiguracionEtapa.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuarioConfiguracionEtapa.clave' => 'CLAVE',
        'UsuarioConfiguracionEtapaTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'usuario_configuracion_etapa.clave' => 'CLAVE',
        'Activo' => 'ACTIVO',
        'UsuarioConfiguracionEtapa.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuarioConfiguracionEtapa.activo' => 'ACTIVO',
        'UsuarioConfiguracionEtapaTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'usuario_configuracion_etapa.activo' => 'ACTIVO',
        'IdUsuarioConfiguracionPrograma' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'UsuarioConfiguracionEtapa.IdUsuarioConfiguracionPrograma' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'idUsuarioConfiguracionPrograma' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'usuarioConfiguracionEtapa.idUsuarioConfiguracionPrograma' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_CONFIGURACION_PROGRAMA' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'COL_ID_USUARIO_CONFIGURACION_PROGRAMA' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'id_usuario_configuracion_programa' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'usuario_configuracion_etapa.id_usuario_configuracion_programa' => 'ID_USUARIO_CONFIGURACION_PROGRAMA',
        'IdEtapa' => 'ID_ETAPA',
        'UsuarioConfiguracionEtapa.IdEtapa' => 'ID_ETAPA',
        'idEtapa' => 'ID_ETAPA',
        'usuarioConfiguracionEtapa.idEtapa' => 'ID_ETAPA',
        'UsuarioConfiguracionEtapaTableMap::COL_ID_ETAPA' => 'ID_ETAPA',
        'COL_ID_ETAPA' => 'ID_ETAPA',
        'id_etapa' => 'ID_ETAPA',
        'usuario_configuracion_etapa.id_etapa' => 'ID_ETAPA',
        'FechaCreacion' => 'FECHA_CREACION',
        'UsuarioConfiguracionEtapa.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'usuarioConfiguracionEtapa.fechaCreacion' => 'FECHA_CREACION',
        'UsuarioConfiguracionEtapaTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'usuario_configuracion_etapa.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'UsuarioConfiguracionEtapa.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'usuarioConfiguracionEtapa.fechaModificacion' => 'FECHA_MODIFICACION',
        'UsuarioConfiguracionEtapaTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'usuario_configuracion_etapa.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'UsuarioConfiguracionEtapa.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'usuarioConfiguracionEtapa.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'usuario_configuracion_etapa.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario_configuracion_etapa');
        $this->setPhpName('UsuarioConfiguracionEtapa');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\UsuarioConfiguracionEtapa');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
        $this->addForeignKey('id_usuario_configuracion_programa', 'IdUsuarioConfiguracionPrograma', 'INTEGER', 'usuario_configuracion_programa', 'clave', true, null, null);
        $this->addForeignKey('id_etapa', 'IdEtapa', 'INTEGER', 'etapa', 'clave', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Etapa', '\\Etapa', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_etapa',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('UsuarioConfiguracionPrograma', '\\UsuarioConfiguracionPrograma', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_configuracion_programa',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UsuarioConfiguracionEtapaTableMap::CLASS_DEFAULT : UsuarioConfiguracionEtapaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UsuarioConfiguracionEtapa object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UsuarioConfiguracionEtapaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UsuarioConfiguracionEtapaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UsuarioConfiguracionEtapaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UsuarioConfiguracionEtapaTableMap::OM_CLASS;
            /** @var UsuarioConfiguracionEtapa $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UsuarioConfiguracionEtapaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UsuarioConfiguracionEtapaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UsuarioConfiguracionEtapaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UsuarioConfiguracionEtapa $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UsuarioConfiguracionEtapaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_CLAVE);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ACTIVO);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_CONFIGURACION_PROGRAMA);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_ETAPA);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.activo');
            $criteria->addSelectColumn($alias . '.id_usuario_configuracion_programa');
            $criteria->addSelectColumn($alias . '.id_etapa');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ACTIVO);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_CONFIGURACION_PROGRAMA);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_ETAPA);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(UsuarioConfiguracionEtapaTableMap::COL_ID_USUARIO_MODIFICACION);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.activo');
            $criteria->removeSelectColumn($alias . '.id_usuario_configuracion_programa');
            $criteria->removeSelectColumn($alias . '.id_etapa');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UsuarioConfiguracionEtapaTableMap::DATABASE_NAME)->getTable(UsuarioConfiguracionEtapaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UsuarioConfiguracionEtapaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UsuarioConfiguracionEtapaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UsuarioConfiguracionEtapaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UsuarioConfiguracionEtapa or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UsuarioConfiguracionEtapa object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionEtapaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \UsuarioConfiguracionEtapa) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UsuarioConfiguracionEtapaTableMap::DATABASE_NAME);
            $criteria->add(UsuarioConfiguracionEtapaTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = UsuarioConfiguracionEtapaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UsuarioConfiguracionEtapaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UsuarioConfiguracionEtapaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the usuario_configuracion_etapa table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UsuarioConfiguracionEtapaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UsuarioConfiguracionEtapa or Criteria object.
     *
     * @param mixed               $criteria Criteria or UsuarioConfiguracionEtapa object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsuarioConfiguracionEtapaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UsuarioConfiguracionEtapa object
        }

        if ($criteria->containsKey(UsuarioConfiguracionEtapaTableMap::COL_CLAVE) && $criteria->keyContainsValue(UsuarioConfiguracionEtapaTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UsuarioConfiguracionEtapaTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = UsuarioConfiguracionEtapaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UsuarioConfiguracionEtapaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UsuarioConfiguracionEtapaTableMap::buildTableMap();
