<?php

namespace Map;

use \RolPermiso;
use \RolPermisoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'rol_permiso' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class RolPermisoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = '.Map.RolPermisoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'rol_permiso';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\RolPermiso';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'RolPermiso';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the clave field
     */
    const COL_CLAVE = 'rol_permiso.clave';

    /**
     * the column name for the id_rol field
     */
    const COL_ID_ROL = 'rol_permiso.id_rol';

    /**
     * the column name for the id_permiso field
     */
    const COL_ID_PERMISO = 'rol_permiso.id_permiso';

    /**
     * the column name for the fecha_creacion field
     */
    const COL_FECHA_CREACION = 'rol_permiso.fecha_creacion';

    /**
     * the column name for the fecha_modificacion field
     */
    const COL_FECHA_MODIFICACION = 'rol_permiso.fecha_modificacion';

    /**
     * the column name for the id_usuario_modificacion field
     */
    const COL_ID_USUARIO_MODIFICACION = 'rol_permiso.id_usuario_modificacion';

    /**
     * the column name for the activo field
     */
    const COL_ACTIVO = 'rol_permiso.activo';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Clave', 'IdRol', 'IdPermiso', 'FechaCreacion', 'FechaModificacion', 'IdUsuarioModificacion', 'Activo', ),
        self::TYPE_CAMELNAME     => array('clave', 'idRol', 'idPermiso', 'fechaCreacion', 'fechaModificacion', 'idUsuarioModificacion', 'activo', ),
        self::TYPE_COLNAME       => array(RolPermisoTableMap::COL_CLAVE, RolPermisoTableMap::COL_ID_ROL, RolPermisoTableMap::COL_ID_PERMISO, RolPermisoTableMap::COL_FECHA_CREACION, RolPermisoTableMap::COL_FECHA_MODIFICACION, RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION, RolPermisoTableMap::COL_ACTIVO, ),
        self::TYPE_FIELDNAME     => array('clave', 'id_rol', 'id_permiso', 'fecha_creacion', 'fecha_modificacion', 'id_usuario_modificacion', 'activo', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Clave' => 0, 'IdRol' => 1, 'IdPermiso' => 2, 'FechaCreacion' => 3, 'FechaModificacion' => 4, 'IdUsuarioModificacion' => 5, 'Activo' => 6, ),
        self::TYPE_CAMELNAME     => array('clave' => 0, 'idRol' => 1, 'idPermiso' => 2, 'fechaCreacion' => 3, 'fechaModificacion' => 4, 'idUsuarioModificacion' => 5, 'activo' => 6, ),
        self::TYPE_COLNAME       => array(RolPermisoTableMap::COL_CLAVE => 0, RolPermisoTableMap::COL_ID_ROL => 1, RolPermisoTableMap::COL_ID_PERMISO => 2, RolPermisoTableMap::COL_FECHA_CREACION => 3, RolPermisoTableMap::COL_FECHA_MODIFICACION => 4, RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION => 5, RolPermisoTableMap::COL_ACTIVO => 6, ),
        self::TYPE_FIELDNAME     => array('clave' => 0, 'id_rol' => 1, 'id_permiso' => 2, 'fecha_creacion' => 3, 'fecha_modificacion' => 4, 'id_usuario_modificacion' => 5, 'activo' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var string[]
     */
    protected $normalizedColumnNameMap = [

        'Clave' => 'CLAVE',
        'RolPermiso.Clave' => 'CLAVE',
        'clave' => 'CLAVE',
        'rolPermiso.clave' => 'CLAVE',
        'RolPermisoTableMap::COL_CLAVE' => 'CLAVE',
        'COL_CLAVE' => 'CLAVE',
        'clave' => 'CLAVE',
        'rol_permiso.clave' => 'CLAVE',
        'IdRol' => 'ID_ROL',
        'RolPermiso.IdRol' => 'ID_ROL',
        'idRol' => 'ID_ROL',
        'rolPermiso.idRol' => 'ID_ROL',
        'RolPermisoTableMap::COL_ID_ROL' => 'ID_ROL',
        'COL_ID_ROL' => 'ID_ROL',
        'id_rol' => 'ID_ROL',
        'rol_permiso.id_rol' => 'ID_ROL',
        'IdPermiso' => 'ID_PERMISO',
        'RolPermiso.IdPermiso' => 'ID_PERMISO',
        'idPermiso' => 'ID_PERMISO',
        'rolPermiso.idPermiso' => 'ID_PERMISO',
        'RolPermisoTableMap::COL_ID_PERMISO' => 'ID_PERMISO',
        'COL_ID_PERMISO' => 'ID_PERMISO',
        'id_permiso' => 'ID_PERMISO',
        'rol_permiso.id_permiso' => 'ID_PERMISO',
        'FechaCreacion' => 'FECHA_CREACION',
        'RolPermiso.FechaCreacion' => 'FECHA_CREACION',
        'fechaCreacion' => 'FECHA_CREACION',
        'rolPermiso.fechaCreacion' => 'FECHA_CREACION',
        'RolPermisoTableMap::COL_FECHA_CREACION' => 'FECHA_CREACION',
        'COL_FECHA_CREACION' => 'FECHA_CREACION',
        'fecha_creacion' => 'FECHA_CREACION',
        'rol_permiso.fecha_creacion' => 'FECHA_CREACION',
        'FechaModificacion' => 'FECHA_MODIFICACION',
        'RolPermiso.FechaModificacion' => 'FECHA_MODIFICACION',
        'fechaModificacion' => 'FECHA_MODIFICACION',
        'rolPermiso.fechaModificacion' => 'FECHA_MODIFICACION',
        'RolPermisoTableMap::COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'COL_FECHA_MODIFICACION' => 'FECHA_MODIFICACION',
        'fecha_modificacion' => 'FECHA_MODIFICACION',
        'rol_permiso.fecha_modificacion' => 'FECHA_MODIFICACION',
        'IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'RolPermiso.IdUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'rolPermiso.idUsuarioModificacion' => 'ID_USUARIO_MODIFICACION',
        'RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'COL_ID_USUARIO_MODIFICACION' => 'ID_USUARIO_MODIFICACION',
        'id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'rol_permiso.id_usuario_modificacion' => 'ID_USUARIO_MODIFICACION',
        'Activo' => 'ACTIVO',
        'RolPermiso.Activo' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rolPermiso.activo' => 'ACTIVO',
        'RolPermisoTableMap::COL_ACTIVO' => 'ACTIVO',
        'COL_ACTIVO' => 'ACTIVO',
        'activo' => 'ACTIVO',
        'rol_permiso.activo' => 'ACTIVO',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rol_permiso');
        $this->setPhpName('RolPermiso');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\RolPermiso');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('clave', 'Clave', 'INTEGER', true, null, null);
        $this->addForeignKey('id_rol', 'IdRol', 'INTEGER', 'rol', 'clave', true, null, null);
        $this->addForeignKey('id_permiso', 'IdPermiso', 'INTEGER', 'permisos', 'clave', true, null, null);
        $this->addColumn('fecha_creacion', 'FechaCreacion', 'TIMESTAMP', true, null, null);
        $this->addColumn('fecha_modificacion', 'FechaModificacion', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('id_usuario_modificacion', 'IdUsuarioModificacion', 'INTEGER', 'usuario', 'clave', true, null, null);
        $this->addColumn('activo', 'Activo', 'INTEGER', true, null, 1);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Rol', '\\Rol', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_rol',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Permisos', '\\Permisos', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_permiso',
    1 => ':clave',
  ),
), null, null, null, false);
        $this->addRelation('Usuario', '\\Usuario', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':id_usuario_modificacion',
    1 => ':clave',
  ),
), null, null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Clave', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? RolPermisoTableMap::CLASS_DEFAULT : RolPermisoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (RolPermiso object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = RolPermisoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = RolPermisoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + RolPermisoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = RolPermisoTableMap::OM_CLASS;
            /** @var RolPermiso $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            RolPermisoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = RolPermisoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = RolPermisoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var RolPermiso $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                RolPermisoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(RolPermisoTableMap::COL_CLAVE);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_ID_ROL);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_ID_PERMISO);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_FECHA_CREACION);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_FECHA_MODIFICACION);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->addSelectColumn(RolPermisoTableMap::COL_ACTIVO);
        } else {
            $criteria->addSelectColumn($alias . '.clave');
            $criteria->addSelectColumn($alias . '.id_rol');
            $criteria->addSelectColumn($alias . '.id_permiso');
            $criteria->addSelectColumn($alias . '.fecha_creacion');
            $criteria->addSelectColumn($alias . '.fecha_modificacion');
            $criteria->addSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->addSelectColumn($alias . '.activo');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria object containing the columns to remove.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function removeSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_CLAVE);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_ID_ROL);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_ID_PERMISO);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_FECHA_CREACION);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_FECHA_MODIFICACION);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_ID_USUARIO_MODIFICACION);
            $criteria->removeSelectColumn(RolPermisoTableMap::COL_ACTIVO);
        } else {
            $criteria->removeSelectColumn($alias . '.clave');
            $criteria->removeSelectColumn($alias . '.id_rol');
            $criteria->removeSelectColumn($alias . '.id_permiso');
            $criteria->removeSelectColumn($alias . '.fecha_creacion');
            $criteria->removeSelectColumn($alias . '.fecha_modificacion');
            $criteria->removeSelectColumn($alias . '.id_usuario_modificacion');
            $criteria->removeSelectColumn($alias . '.activo');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(RolPermisoTableMap::DATABASE_NAME)->getTable(RolPermisoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(RolPermisoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(RolPermisoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new RolPermisoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a RolPermiso or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or RolPermiso object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolPermisoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \RolPermiso) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(RolPermisoTableMap::DATABASE_NAME);
            $criteria->add(RolPermisoTableMap::COL_CLAVE, (array) $values, Criteria::IN);
        }

        $query = RolPermisoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            RolPermisoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                RolPermisoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the rol_permiso table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return RolPermisoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a RolPermiso or Criteria object.
     *
     * @param mixed               $criteria Criteria or RolPermiso object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(RolPermisoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from RolPermiso object
        }

        if ($criteria->containsKey(RolPermisoTableMap::COL_CLAVE) && $criteria->keyContainsValue(RolPermisoTableMap::COL_CLAVE) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.RolPermisoTableMap::COL_CLAVE.')');
        }


        // Set the correct dbName
        $query = RolPermisoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // RolPermisoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
RolPermisoTableMap::buildTableMap();
