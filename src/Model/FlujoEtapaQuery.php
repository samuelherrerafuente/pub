<?php

use Base\FlujoEtapaQuery as BaseFlujoEtapaQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'flujo_etapa' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class FlujoEtapaQuery extends BaseFlujoEtapaQuery
{

}
