<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Routing\RouteCollectorProxy;

class HealthCheck
{
    public function routes(RouteCollectorProxy $group)
    {
        $group->get('', self::class . ':index');
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('ok');
        return $response;
    }

}
