<?php

use Map\UsuarioTableMap;
use Dflydev\FigCookies\SetCookie;
use Slim\Routing\RouteCollectorProxy;
use Psr\Http\Message\ResponseInterface;
use Dflydev\FigCookies\Modifier\SameSite;
use Dflydev\FigCookies\FigResponseCookies;
use Psr\Http\Message\ServerRequestInterface;
use GuzzleHttp\Client;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// todo: autorizacion y prvilegios, obtener clave de usuario que modifica para setear en los fields de auditoria, validaciones de campos
class UserHandler
{
  private $re = '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*(\d)\1{2})(?=.*[@_\-!¡?¿+*$%&])(?=.*[a-zA-Z]).{8,}$/';

  public function routes(RouteCollectorProxy $group)
  {
    $group->get('/', self::class . ':getAll')->addMiddleware(new AuthUtils("administracion.usuarios.ver"));
    $group->post('/', self::class . ':newUser')->addMiddleware(new AuthUtils("administracion.usuarios.nuevo"));
    $group->get('/whoami', self::class . ':getByClaveWhoami')->addMiddleware(new AuthUtils(null, true));
    $group->post('/search', self::class . ':search')->addMiddleware(new AuthUtils("administracion.usuarios.ver"));
    $group->post('/{user}/login', self::class . ':logIn');
    $group->post('/reestablecer', self::class . ':generateResetLink');
    $group->post('/resetPassword/{token}', self::class . ':resetPassword');
    $group->put('/{clave}', self::class . ':updUser')->addMiddleware(new AuthUtils("administracion.usuarios.editar", true));
    $group->put('/{clave}/pass', self::class . ':updUserPass')->addMiddleware(new AuthUtils("administracion.usuarios.editar", true));
    $group->put('/{clave}/visited', self::class . ':updUserVisit')->addMiddleware(new AuthUtils(null, true));
    $group->delete('/{clave}', self::class . ':delUser')->addMiddleware(new AuthUtils("administracion.usuarios.desactivar"));
    $group->get('/{clave}', self::class . ':getByClave')->addMiddleware(new AuthUtils("administracion.usuarios.ver", true));
    $group->get('', self::class . ':index')->addMiddleware(new AuthUtils("administracion.usuarios.ver", true));
  }

  public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $response->getBody()->write('ok');
    return $response;
  }

  public function getByClaveWhoami(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $fields = UsuarioTableMap::getFieldNames();
    $fields = array_values(array_diff($fields, ["Password", "PasswordAntiguo"]));
    $usuario = UsuarioQuery::create()->findOneByClave($request->getAttribute('claveUsuario'));

    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }

    $usuario->initUsuarioPermisosRelatedByIdUsuario(true);
    $usuario->initRolPermisos(true);
    $usuario->initUsuarioConfiguracionProgramasRelatedByIdUsuario(true);
    $response->getBody()->write($usuario->toJSON());
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function getByClave(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $fields = UsuarioTableMap::getFieldNames();
    $fields = array_values(array_diff($fields, ["Password", "PasswordAntiguo"]));
    $usuario = UsuarioQuery::create()->findOneByClave($args['clave']);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $usuario->initUsuarioPermisosRelatedByIdUsuario(true);
    $usuario->initRolPermisos(true);
    $response->getBody()->write($usuario->toJSON());
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function search(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $query = UsuarioQuery::create()->join('EntidadOperativa')->withColumn('EntidadOperativa.Nombre', 'EntidadOperativaNombre');
    $conditions = array();
    $filters = array();
    if (!empty($body['Clave'])) {
      $query->condition('c1', 'Usuario.Clave = ?', $body['Clave']);
      array_push($conditions, 'c1');
    }
    if (!empty($body['Nombre'])) {
      $query->condition('c2', 'Usuario.Nombre like ?', $this->wrapLike($body['Nombre']));
      array_push($conditions, 'c2');
    }
    if (!empty($body['Apaterno'])) {
      $query->condition('c3', 'Usuario.Apaterno LIKE ?', $this->wrapLike($body['Apaterno']));
      array_push($conditions, 'c3');
    }
    if (!empty($body['Amaterno'])) {
      $query->condition('c4', 'Usuario.Amaterno LIKE ?', $this->wrapLike($body['Amaterno']));
      array_push($conditions, 'c4');
    }
    if (!empty($body['Correo'])) {
      $query->condition('c5', 'Usuario.Correo LIKE ?', $this->wrapLike($body['Correo']));
      array_push($conditions, 'c5');
    }
    if (!empty($body['Usuario'])) {
      $query->condition('c6', 'Usuario.Usuario LIKE ?', $this->wrapLike($body['Usuario']));
      array_push($conditions, 'c6');
    }
    if (count($conditions) > 0) {
      $query->combine($conditions, 'or', 'mainFields');
      array_push($filters, 'mainFields');
    }
    if (!empty($body['Activo']) || (isset($body['Activo']) && $body['Activo'] == '0')) {
      $query->condition('actv', 'Usuario.Activo = ?', $body['Activo']);
    } else {
      $query->condition('actv', 'Usuario.Activo = ?', '1');
    }
    array_push($filters, 'actv');

    if (!empty($body['IdRol'])) {
      $query->join('RolUsuarioRelatedByIdUsuario');
      $query->condition('cr1', 'RolUsuarioRelatedByIdUsuario.id_rol = ?', $body['IdRol']);
      $query->condition('cr2', 'RolUsuarioRelatedByIdUsuario.Activo = ?', '1');
      $query->combine(['cr1', 'cr2'], 'and', 'crr');
      array_push($filters, 'crr');
    }

    if (!empty($body['FechaInicio'])) {
      $query->condition('fi', 'Usuario.FechaCreacion > ?', $body['FechaInicio']);
      array_push($filters, 'fi');
    }

    if (!empty($body['FechaFin'])) {
      $query->condition('ff', 'Usuario.FechaCreacion < ?', $body['FechaFin']);
      array_push($filters, 'ff');
    }

    if (count($filters) > 1) {
      $query->where($filters, 'and');
    } else {
      $query->where($filters);
    }

    if (!empty($request->getAttribute('sort'))) {
      $query = $query->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
    }
    $query = $query->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));
    $query->populateRelation('UsuarioPermisoRelatedByIdUsuario');
    $query->populateRelation('RolUsuarioRelatedByIdUsuario');
    // $query->populateRelation('EntidadOperativa');

    $response->getBody()->write($query->toJSON());
    return $response->withHeader('Content-Type', 'application/json')
      ->withHeader('pagination-total', $query->getNbResults());
  }

  private function wrapLike(String $word): String
  {
    return '%' . $word . '%';
  }

  public function getAll(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $fields = UsuarioTableMap::getFieldNames();
    $fields = array_values(array_diff($fields, ["Password", "PasswordAntiguo"]));
    $query = UsuarioQuery::create()->join('EntidadOperativa')->withColumn('EntidadOperativa.Nombre', 'EntidadOperativaNombre');

    if (!empty($request->getAttribute('sort'))) {
      $query = $query->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
    }
    $query = $query->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));

    $query->populateRelation('UsaurioPermisoRelatedByIdUsuario');
    $query->populateRelation('RolUsuarioRelatedByIdUsuario');
    // $query->populateRelation('EntidadOperativa');

    $response->getBody()->write($query->toJSON());
    return $response->withHeader('Content-Type', 'application/json')
      ->withHeader('pagination-total', $query->getNbResults());
  }

  public function updUser(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $usuario = UsuarioQuery::create()->findOneByClave($args['clave']);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $usuario->setUsuario($body['Usuario'])
      ->setVigencia($body['Vigencia'])
      ->setDiasVigencia($body['DiasVigencia'])
      ->setCorreo($body['Correo'])
      ->setActivo($body['Activo'])
      ->setNombre($body['Nombre'])
      ->setApaterno($body['Apaterno'])
      ->setAmaterno($body['Amaterno'])
      ->setIdEntidadOperativa($body['IdEntidadOperativa'])
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaModificacion(new DateTime('NOW'));

    if (!empty($body['Password'])) {
      $usuario->setPassword(password_hash($body['Password'], PASSWORD_DEFAULT));
    }

    $roles = RolUsuarioQuery::create()->findByIdUsuario($args['clave']);
    if (isset($body['RolUsuarios'])) {
      $pFromBody = $body['RolUsuarios'];
      $existent = array();
      foreach ($roles as $p) {
        if (in_array($p->getIdRol(), $pFromBody)) {
          array_push($existent, $p->getIdRol());
          if ($p->getActivo() != 1) {
            $p->setActivo(1)
              ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
              ->setFechaModificacion(new DateTime('NOW'));
          }
        } else {
          $p->setActivo(0)
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
        }
        $usuario->addRolUsuarioRelatedByIdUsuario($p);
      }
      foreach ($pFromBody as $value) {
        if (isset($value) && !in_array($value, $existent)) {
          $ru = new RolUsuario();
          $ru->setUsuarioRelatedByIdUsuario($usuario)
            ->setIdRol($value)
            ->setActivo(1)
            ->setFechaCreacion(new DateTime('NOW'))
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
          $usuario->addRolUsuarioRelatedByIdUsuario($ru);
        }
      }
    }

    $permisosUsuario = UsuarioPermisoQuery::create()->findByIdUsuario($args['clave']);
    if (isset($body['UsuarioPermisos'])) {
      $pFromBody = $body['UsuarioPermisos'];
      $existent = array();
      foreach ($permisosUsuario as $p) {
        if (in_array($p->getIdPermiso(), $pFromBody)) {
          array_push($existent, $p->getIdPermiso());
          if ($p->getActivo() != 1) {
            $p->setActivo(1)
              ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
              ->setFechaModificacion(new DateTime('NOW'));
          }
        } else {
          $p->setActivo(0)
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
        }
        $usuario->addUsuarioPermisoRelatedByIdUsuario($p);
      }
      foreach ($pFromBody as $value) {
        if (isset($value) && !in_array($value, $existent)) {
          $up = new UsuarioPermiso();
          $up->setUsuarioRelatedByIdUsuario($usuario)
            ->setIdPermiso($value)
            ->setActivo(1)
            ->setIdUsuarioModificacion(0)
            ->setFechaCreacion(new DateTime('NOW'))
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
          $usuario->addUsuarioPermisoRelatedByIdUsuario($up);
        }
      }
    }

    $response->getBody()->write(json_encode(["updated" => $usuario->save()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function updUserPass(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $pass = $body['Password'];
    if (!preg_match($this->re, $pass, $matches, PREG_OFFSET_CAPTURE, 0)) {
      $response->getBody()->write(json_encode(["message" => 'Constraseña no cuenta con los requisitos minimos.
            Mínimo 8 caracteres, debe Incluir al menos un número, una mayúscula,
             una minúscula y un carácterespecial (@_-!¡?¿+*$%&), 
             no debe incluir secuencias de 3 números consecutivos']));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(400);
    }
    $usuario = UsuarioQuery::create()->findOneByClave($args['clave']);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $usuario->setPasswordAntiguo($usuario->getPassword())
      ->setPassword(password_hash($body['Password'], PASSWORD_DEFAULT))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaModificacion(new DateTime('NOW'));
    $response->getBody()->write(json_encode(["updated" => $usuario->save()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function resetPassword(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $tkn = $args['token'];
    $pass = $body['Password'];
    if (!preg_match($this->re, $pass, $matches, PREG_OFFSET_CAPTURE, 0)) {
      $response->getBody()->write(json_encode(["message" => 'Constraseña no cuenta con los requisitos minimos.
            Mínimo 8 caracteres, debe Incluir al menos un número, una mayúscula,
             una minúscula y un carácterespecial (@_-!¡?¿+*$%&), 
             no debe incluir secuencias de 3 números consecutivos']));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(400);
    }
    $token = RestablecerPasswordQuery::create()
      ->filterByToken($tkn)
      ->filterByUsado(0)
      ->findOne();
    if (!isset($token)) {
      $response->getBody()->write(json_encode(["message" => 'Token invalido.']));
      return $response->withHeader('Content-Type', 'application/json');
    }
    $start_date = strtotime($token->getFecha());
    $end_date = time();
    if (($end_date - $start_date) < 2) {
      $response->getBody()->write(json_encode(["message" => 'Token expirado.']));
      return $response->withHeader('Content-Type', 'application/json');
    }
    $usuario = UsuarioQuery::create()
      ->filterByActivo(1)->findOneByClave($token->getIdUsuario());
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $token->setUsado(1);
    $usuario->setPasswordAntiguo($usuario->getPassword())
      ->setPassword(password_hash($body['Password'], PASSWORD_DEFAULT))
      ->setIdUsuarioModificacion($token->getIdUsuario())
      ->addRestablecerPassword($token)
      ->setFechaModificacion(new DateTime('NOW'));
    
    $response->getBody()->write(json_encode(["updated" => $usuario->save()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function generateResetLink(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $httpClient = new Client(['verify' => false]);
    $captchaRes = $httpClient->request('POST',  $_ENV["CFG"]["captcha"]["api-base"] . $body['captchaRes'], []);
    $captchaData = $captchaRes->getBody()->getContents();
    $captchaData = json_decode($captchaData);
    if (!($captchaData->success && $captchaData->score > 0.4)) {
      $response->getBody()->write(json_encode(["message" => 'ReCaptcha piensa que eres un robot, porfavor contacta a soporte.']));
      return $response->withHeader('Content-Type', 'application/json');
    }

    $usuario = UsuarioQuery::create()->filterByCorreo($body['Email'])->filterByActivo(1)->findOne();
    if (!$usuario) {
      $response->getBody()->write(json_encode(["message" => 'No se encontro un usuario activo asociado al email.']));
      return $response->withHeader('Content-Type', 'application/json');
    }
    $uuid = $this->guidv4();
    $rs = new RestablecerPassword();
    $rs->setToken($uuid)
      ->setUsado(0)
      ->setFecha(new DateTime('NOW'));
    $usuario->addRestablecerPassword($rs);
    $saved = $usuario->save();
    if ($saved) {
      $url = $_ENV["CFG"]["mail"]["reset-link-base"] . $uuid;
      $message = 'Usa el siguiente link para reestablecer tu contraseña: <a href="' . $url . '" >Reestablecer contrase&ntilde;a</a>.';
      $messageAlt = 'Usa el siguiente link para reestablecer tu contraseña: ' . $url . '.';
      $mail = new PHPMailer($_ENV["CFG"]["mail"]["debug"] == "true" ? true : false);
      try {
        $mail->SMTPDebug = $_ENV["CFG"]["mail"]["debug"] == "true" ? SMTP::DEBUG_SERVER : SMTP::DEBUG_OFF;
        $mail->isSMTP();
        $mail->Host = $_ENV["CFG"]["mail"]["smtp-host"];
        $mail->SMTPAuth = $_ENV["CFG"]["mail"]["smtp-auth"] == "true" ? true : false;
        $mail->Username = $_ENV["CFG"]["mail"]["username"];
        $mail->Password = $_ENV["CFG"]["mail"]["password"];
        $mail->SMTPOptions = array(
          'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
          )
        );
        $mail->SMTPSecure = $_ENV["CFG"]["mail"]["smtp-secure"];
        $mail->Port = $_ENV["CFG"]["mail"]["port"];

        $mail->setFrom($_ENV["CFG"]["mail"]["reset-from"], $_ENV["CFG"]["mail"]["reset-from-name"]);
        $mail->addAddress($body['Email']);
        $mail->isHTML(true);
        $mail->Subject = $_ENV["CFG"]["mail"]["reset-subject"];
        $mail->Body    = $message;
        $mail->AltBody = $messageAlt;

        if ($mail->send()) {
          $response->getBody()->write(json_encode(["updated" => $saved]));
          return $response->withHeader('Content-Type', 'application/json');
        } else {
          $response->getBody()->write(json_encode(["message" => 'El servido no logo enviar el mail, contratar a soporte. ' . $mail->ErrorInfo]));
          return $response->withHeader('Content-Type', 'application/json');
        }
      } catch (\Exception $e) {
        $response->getBody()->write(json_encode(["message" => 'El servido no logo enviar el mail, contratar a soporte.']));
        return $response->withHeader('Content-Type', 'application/json');
      }
    } else {
      $response->getBody()->write(json_encode(["message" => 'Se produjo un error en el servidor al enviar el email, contactar a soporte.']));
      return $response->withHeader('Content-Type', 'application/json');
    }
  }

  public function updUserVisit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $claveUsuario = $request->getAttribute('claveUsuario');
    $usuario = UsuarioQuery::create()->findOneByClave($claveUsuario);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $usuario->setUltimoAcceso(new DateTime('NOW'));
    $response->getBody()->write(json_encode(["updated" => $usuario->save()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function logIn(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();

    $httpClient = new Client(['verify' => false]);
    $captchaRes = $httpClient->request('POST',  $_ENV["CFG"]["captcha"]["api-base"] . $body['captchaRes'], []);
    $captchaData = $captchaRes->getBody()->getContents();
    $captchaData = json_decode($captchaData);
    if (!($captchaData->success && $captchaData->score > 0.4)) {
      $response->getBody()->write(json_encode(["message" => 'ReCaptcha piensa que eres un robot, porfavor contacta a soporte.']));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(400);
    }

    $usuario = UsuarioQuery::create()->filterByActivo(1)->setIgnoreCase(true)->findOneByUsuario($args['user']);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }

    $res = password_verify($body['Password'], $usuario->getPassword());
    if (!$res) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(403);
    }
    $uuid = $this->guidv4();
    $l = LoginQuery::create()->findOneByIdUsuario($args['user']);
    if(!$l && $usuario->getPasswordAntiguo() == '-1'){
      $r = RestablecerPasswordQuery::create()->filterByUsado(0)->findOneByIdUsuario($args['user']);
      if(!$r){
        $rs = new RestablecerPassword();
        $rs->setToken($uuid)
          ->setUsado(0)
          ->setFecha(new DateTime('NOW'));
        $usuario->addRestablecerPassword($rs);
      } else {
        $uuid = $r->getToken();
      }
    } else {
      $l = true;
      $login = new Login();
      $dateNow = new DateTime('NOW');
      $login->setIdUsuario($usuario->getClave())->setToken($uuid)->setFechaLogin($dateNow);
      $usuario->setUltimoAcceso($dateNow)->addLogin($login);
      $response = FigResponseCookies::set(
        $response,
        SetCookie::create('AUTH-TOKEN')
          ->withValue($uuid)
          ->withPath('/')
          // ->withSecure(true)  //habilitar cuando el servidor trabaje sobre https
          ->withHttpOnly(true)
          ->withSameSite(SameSite::lax())
      );
    }

    $response->getBody()
      ->write(json_encode(
        [
          "tokenExpire" => Date('y:m:d', strtotime('+2 days')),
          "updated" => $usuario->save(),
          "firstLogin" => !$l,
          "token" => !$l ? $uuid : ''
        ]
      ));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function delUser(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $usuario = UsuarioQuery::create()->findOneByClave($args['clave']);
    if (!$usuario) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    // $usuario->delete()
    $usuario->setActivo(0)->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
    $usuario->save();

    $response->getBody()->write(json_encode(["deleted" => true]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function newUser(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $usuario = new Usuario();
    $pass = $body['Password'];
    if (!preg_match($this->re, $pass, $matches, PREG_OFFSET_CAPTURE, 0)) {
      $response->getBody()->write(json_encode(["message" => 'Constraseña no cuenta con los requisitos minimos.
            Mínimo 8 caracteres, debe Incluir al menos un número, una mayúscula,
             una minúscula y un carácterespecial (@_-!¡?¿+*$%&), 
             no debe incluir secuencias de 3 números consecutivos']));
      return $response->withHeader('Content-Type', 'application/json')->withStatus(400);
    }
    $usuario->setUsuario($body['Usuario'])
      ->setPassword(password_hash($pass, PASSWORD_DEFAULT))
      ->setVigencia($body['Vigencia'])
      ->setDiasVigencia(isset($body['DiasVigencia']) ? $body['DiasVigencia'] : 0)
      ->setCorreo($body['Correo'])
      ->setActivo($body['Activo'])
      ->setNombre($body['Nombre'])
      ->setApaterno($body['Apaterno'])
      ->setAmaterno($body['Amaterno'])
      ->setIdEntidadOperativa($body['IdEntidadOperativa'])
      ->setFechaCreacion(new DateTime('NOW'))
      ->setFechaModificacion(new DateTime('NOW'))
      ->setUltimoAcceso(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setPasswordAntiguo("-1");
    if (isset($body['RolUsuarios'])) {
      foreach ($body['RolUsuarios'] as $value) {
        $ru = new RolUsuario();
        $ru->setIdRol($value)
          ->setActivo(1)
          ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
          ->setFechaCreacion(new DateTime('NOW'))
          ->setFechaModificacion(new DateTime('NOW'));
        $usuario->addRolUsuarioRelatedByIdUsuario($ru);
      }
    }
    if (isset($body['UsuarioPermisos'])) {
      foreach ($body['UsuarioPermisos'] as $value) {
        $up = new UsuarioPermiso();
        $up->setUsuarioRelatedByIdUsuario($usuario)
          ->setIdPermiso($value)
          ->setActivo(1)
          ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
          ->setFechaCreacion(new DateTime('NOW'))
          ->setFechaModificacion(new DateTime('NOW'));
        $usuario->addUsuarioPermisoRelatedByIdUsuario($up);
      }
    }
    $response->getBody()->write(json_encode(["created" => $usuario->save(), "clave" => $usuario->getClave()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  private function guidv4($data = null)
  {
    $data = $data ?? random_bytes(16);
    assert(strlen($data) == 16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }
}
