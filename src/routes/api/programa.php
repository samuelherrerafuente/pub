<?php

use Map\EtapaTableMap;
use Map\FlujoEtapaTableMap;
use Map\UsuarioConfiguracionEtapaTableMap;
use Map\UsuarioConfiguracionProgramaTableMap;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Propel;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Routing\RouteCollectorProxy;

class ProgramaHandler
{
  public function routes(RouteCollectorProxy $group)
  {
    $group->get('/', self::class . ':getAll')->addMiddleware(new AuthUtils(["administracion.programas.ver", "PConsulta"]));
    $group->post('/search', self::class . ':search')->addMiddleware(new AuthUtils(["administracion.programas.ver", "PConsulta"]));
    $group->post('/', self::class . ':new')->addMiddleware(new AuthUtils("administracion.programas.nuevo", "PConsulta"));
    $group->put('/{clavePrograma}', self::class . ':edit')->addMiddleware(new AuthUtils(["administracion.programas.editar", "PEdicion"]));
    $group->put('/{clavePrograma}/configure', self::class . ':configure')->addMiddleware(new AuthUtils(["administracion.programas.configurar", "PEdicion"]));
    $group->put('/{clavePrograma}/status', self::class . ':status')->addMiddleware(new AuthUtils(["administracion.programas.editar", "PEstatus"]));
    $group->delete('/{clavePrograma}', self::class . ':del')->addMiddleware(new AuthUtils(["administracion.programas.desactivar", "PDesactivar"]));
    $group->get('', self::class . ':new_configure');
  }

  public function search(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $query = ProgramaQuery::create();
    $conditions = array();
    $filters = array();
    if (!empty($body['Clave'])) {
      $query->condition('c1', 'Programa.Clave = ?', $body['Clave']);
      array_push($conditions, 'c1');
    }
    if (!empty($body['Nombre'])) {
      $query->condition('c2', 'Programa.Nombre like ?', $this->wrapLike($body['Nombre']));
      array_push($conditions, 'c2');
    }
    if (!empty($body['Descripcion'])) {
      $query->condition('c3', 'Programa.Descripcion LIKE ?', $this->wrapLike($body['Descripcion']));
      array_push($conditions, 'c3');
    }
    if (count($conditions) > 0) {
      $query->combine($conditions, 'or', 'mainFields');
      array_push($filters, 'mainFields');
    }
    if (!empty($body['IdEntidadOperativa'])) {
      $query->condition('eo', 'Programa.IdEntidadOperativa = ?', $body['IdEntidadOperativa']);
      array_push($filters, 'eo');
    }
    if (!empty($body['IdEstatusPrograma'])) {
      $query->condition('ep', 'Programa.IdEstatusPrograma = ?', $body['IdEstatusPrograma']);
      array_push($filters, 'ep');
    }
    if (!empty($body['IdTipoBebeficiario'])) {
      $query->condition('tb', 'Programa.IdTipoBebeficiario = ?', $body['IdTipoBebeficiario']);
      array_push($filters, 'tb');
    }
    //Aqui se repetia la clave unica, se ajusto
    if (!empty($body['PersonaMoral'])) {
      $query->condition('pm', 'Programa.PersonaMoral = ?', $body['PersonaMoral']);
      array_push($filters, 'pm');
    }
    //nombre incorrecto - corregido
    if (!empty($body['DiagramaYucatan'])) {
      $query->condition('dy', 'Programa.DigramaYucatan = ?', $body['DiagramaYucatan']);
      array_push($filters, 'dy');
    }

    if (!empty($body['Activo']) || (isset($body['Activo']) && $body['Activo'] == '0')) {
      $query->condition('actv', 'Programa.Activo = ?', $body['Activo']);
    } else {
      $query->condition('actv', 'Programa.Activo = ?', '1');
    }
    array_push($filters, 'actv');

    if (!empty($body['FechaInicio'])) {
      $query->condition('fi', 'Programa.FechaCreacion > ?', $body['FechaInicio']);
      array_push($filters, 'fi');
    }

    if (!empty($body['FechaFin'])) {
      $query->condition('ff', 'Programa.FechaCreacion < ?', $body['FechaFin']);
      array_push($filters, 'ff');
    }

    if (count($filters) > 1) {
      $query->where($filters, 'and');
    } else {
      $query->where($filters);
    }

    if (!empty($request->getAttribute('sort'))) {
      $query = $query->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
    }
    $query = $query->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));

    $query->populateRelation('Beneficio');
    $query->populateRelation('Etapa');
    $query->populateRelation('ProgramaRequisito');
    $query->populateRelation('UsuarioConfiguracionPrograma');

    foreach ($query as &$prog) {
      $prog->getTipoBeneficiario();
      $prog->getEstatusPrograma();
      $prog->getEntidadOperativa();
      foreach ($prog->getEtapas() as &$etapa) {
        $etapa->initFlujoEtapasRelatedByIdEtapa(true);
      }
      foreach ($prog->getProgramaRequisitos() as &$req) {
        $req->getTipoRequisito();
      }
      foreach ($prog->getUsuarioConfiguracionProgramas() as &$userConfig) {
        $userConfig->initUsuarioConfiguracionEtapas(true);
        foreach ($userConfig->getUsuarioConfiguracionEtapas() as &$userEtapa) {
          $userEtapa->getEtapa();
        }
      }
    }

    $response->getBody()->write($query->toJSON());
    return $response->withHeader('Content-Type', 'application/json')
      ->withHeader('pagination-total', $query->getNbResults());
  }

  public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $response->getBody()->write('ok');
    return $response;
  }

  public function getAll(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $query = ProgramaQuery::create();

    if (!empty($request->getAttribute('sort'))) {
      $query = $query->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
    }
    $query = $query->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));

    $query->populateRelation('Beneficio');
    $query->populateRelation('UsuarioConfiguracionPrograma');
    $query->populateRelation('ProgramaRequisito');
    $query->populateRelation('Etapa');

    foreach ($query as $prog) {
      $prog->getTipoBeneficiario();
      $prog->getEstatusPrograma();
      $prog->getEntidadOperativa();
      foreach ($prog->getEtapas() as $etapa) {
        $etapa->initFlujoEtapasRelatedByIdEtapa();
      }
      foreach ($prog->getUsuarioConfiguracionProgramas() as &$userConfig) {
        $userConfig->initUsuarioConfiguracionEtapas(true);
        foreach ($userConfig->getUsuarioConfiguracionEtapas() as &$userEtapa) {
          $userEtapa->getEtapa();
        }
      }
    }

    $response->getBody()->write($query->toJSON());
    return $response->withHeader('Content-Type', 'application/json')
      ->withHeader('pagination-total', $query->getNbResults());
  }

  public function new(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();

    $prog = new Programa();
    $prog->setNombre($body['Nombre'])
      ->setDescripcion($body['Descripcion'])
      ->setFechaInicio($body['FechaInicio'])
      ->setFechaFin($body['FechaFin'])
      ->setIdEntidadOperativa($body['IdEntidadOperativa'])
      ->setIdEstatusPrograma('1')
      ->setActivo($body['Activo'])
      ->setFechaCreacion(new DateTime('NOW'))
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setIdTipoBebeficiario($body['IdTipoBebeficiario'])
      ->setPersonaMoral($body['PersonaMoral'])
      ->setDigramaYucatan($body['DigramaYucatan']);

    if (isset($body['Beneficios'])) {
      foreach ($body['Beneficios'] as $value) {
        $b = new Beneficio();
        $b->setIdTipoDistribucion($value['IdTipoDistribucion'])
          ->setIdTipoBeneficio($value['IdTipoBeneficio'])
          ->setIdTipoRecurso($value['IdTipoRecurso'])
          ->setIdUnidadMedida($value['IdUnidadMedida'])
          ->setValor($value['Valor'])
          ->setCantidad($value['Cantidad'])
          ->setNombre($value['Nombre'])
          ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
          ->setFechaCreacion(new DateTime('NOW'))
          ->setFechaModificacion(new DateTime('NOW'));
        $prog->addBeneficio($b);
      }
    }

    if (isset($body['UsuarioConfiguracionProgramas'])) {
      foreach ($body['UsuarioConfiguracionProgramas'] as $value) {
        $uc = new UsuarioConfiguracionPrograma();
        $uc->setIdUsuario($value['IdUsuario'])
          ->setActivo(1)
          ->setPPermisos($value['PPermisos'])
          ->setPConsulta($value['PConsulta'])
          ->setPDesactivar($value['PDesactivar'])
          ->setPEdicion($value['PEdicion'])
          ->setPEstatus($value['PEstatus'])
          ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
          ->setFechaCreacion(new DateTime('NOW'))
          ->setFechaModificacion(new DateTime('NOW'));
        $prog->addUsuarioConfiguracionPrograma($uc);
      }
    }

    $response->getBody()->write(json_encode(["created" => $prog->save(), "clave" => $prog->getClave()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function status(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $prog = ProgramaQuery::create()->findOneByClave($args['clavePrograma']);

    if (!$prog) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $prog->setIdEstatusPrograma($body['IdEstatusPrograma'])
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));

    $response->getBody()->write(json_encode(["updated" => $prog->save(), "clave" => $prog->getClave()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function edit(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $prog = ProgramaQuery::create()->findOneByClave($args['clavePrograma']);

    if (!$prog) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }

    $prog->setNombre($body['Nombre'])
      ->setDescripcion($body['Descripcion'])
      ->setFechaInicio($body['FechaInicio'])
      ->setFechaFin($body['FechaFin'])
      ->setIdEntidadOperativa($body['IdEntidadOperativa'])
      ->setActivo($body['Activo'])
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setIdTipoBebeficiario($body['IdTipoBebeficiario'])
      ->setPersonaMoral($body['PersonaMoral'])
      ->setDigramaYucatan($body['DigramaYucatan']);

    $beneficios = BeneficioQuery::create()->findByIdPrograma($args['clavePrograma']);
    if (isset($body['Beneficios'])) {
      $pFromBody = $body['Beneficios'];
      $existent = array();
      foreach ($beneficios as $p) {
        foreach ($pFromBody as $pp) {
          if ($p->getClave() == $pp['Clave']) {
            array_push($existent, $p->getClave());
            $p->setIdTipoDistribucion($pp['IdTipoDistribucion'])
              ->setIdTipoBeneficio($pp['IdTipoBeneficio'])
              ->setIdTipoRecurso($pp['IdTipoRecurso'])
              ->setIdUnidadMedida($pp['IdUnidadMedida'])
              ->setValor($pp['Valor'])
              ->setCantidad($pp['Cantidad'])
              ->setNombre($pp['Nombre'])
              ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
              ->setFechaModificacion(new DateTime('NOW'));
            $prog->addBeneficio($p);
            break;
          }
        }
        if (!in_array($p->getClave(), $existent)) {
          $p->delete();
        }
      }
      foreach ($pFromBody as $value) {
        if (isset($value) && !in_array($value['Clave'], $existent)) {
          $b = new Beneficio();
          $b->setIdTipoDistribucion($value['IdTipoDistribucion'])
            ->setIdTipoBeneficio($value['IdTipoBeneficio'])
            ->setIdTipoRecurso($value['IdTipoRecurso'])
            ->setIdUnidadMedida($value['IdUnidadMedida'])
            ->setValor($value['Valor'])
            ->setCantidad($value['Cantidad'])
            ->setNombre($value['Nombre'])
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaCreacion(new DateTime('NOW'))
            ->setFechaModificacion(new DateTime('NOW'));
          $prog->addBeneficio($b);
        }
      }
    }

    $ucp = UsuarioConfiguracionProgramaQuery::create()->findByIdPrograma($args['clavePrograma']);
    if (isset($body['UsuarioConfiguracionProgramas'])) {
      $aux_ = array();
      $pFromBody = $body['UsuarioConfiguracionProgramas'];
      foreach ($pFromBody as $value) {
        $aux_[$value['IdUsuario']] = $value;
      }
      $existent = array();
      foreach ($ucp as $p) {
        if (isset($aux_[$p->getIdUsuario()])) {
          array_push($existent, $p->getIdUsuario());
          $p->setActivo(1)
            ->setPPermisos($aux_[$p->getIdUsuario()]['PPermisos'])
            ->setPConsulta($aux_[$p->getIdUsuario()]['PConsulta'])
            ->setPDesactivar($aux_[$p->getIdUsuario()]['PDesactivar'])
            ->setPEdicion($aux_[$p->getIdUsuario()]['PEdicion'])
            ->setPEstatus($aux_[$p->getIdUsuario()]['PEstatus'])
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
        } else {
          $p->setActivo(0)
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
        }
        $prog->addUsuarioConfiguracionPrograma($p);
      }
      foreach ($pFromBody as $value) {
        if (isset($value) && !in_array($value['IdUsuario'], $existent)) {
          $uc = new UsuarioConfiguracionPrograma();
          $uc->setIdUsuario($value['IdUsuario'])
            ->setActivo(1)
            ->setPPermisos($value['PPermisos'])
            ->setPConsulta($value['PConsulta'])
            ->setPDesactivar($value['PDesactivar'])
            ->setPEdicion($value['PEdicion'])
            ->setPEstatus($value['PEstatus'])
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaCreacion(new DateTime('NOW'))
            ->setFechaModificacion(new DateTime('NOW'));
          $prog->addUsuarioConfiguracionPrograma($uc);
        }
      }
    }

    $response->getBody()->write(json_encode(["created" => $prog->save(), "clave" => $prog->getClave()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  public function configure(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $body = $request->getParsedBody();
    $programa = ProgramaQuery::create()->findOneByClave($args['clavePrograma']);
    if (!$programa) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    $programa->initEtapas(true);
    $programa->initUsuarioConfiguracionProgramas(true);
    $programa->initProgramaRequisitos(true);
    $con = Propel::getConnection();
    $con->beginTransaction();
    try {
      $programa->setFechaModificacion(new DateTime('NOW'))
        ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));

      $etapas = isset($body['Etapas']) ? $body['Etapas'] : array();
      $users = isset($body['UsuarioConfiguracionProgramas']) ? $body['UsuarioConfiguracionProgramas'] : array();
      $requisitos = isset($body['ProgramaRequisitos']) ? $body['ProgramaRequisitos'] : array();

      $etapasMap = $this->dictonaryByNameFn($etapas);
      $etapasEntityByNameMap = $this->dictonaryEntityByNameFn($programa->getEtapas());
      $flujosMap = $this->dictonaryOfEtapasSiguientesByNameFn($etapas);
      $flujosEntityMap = $this->dictonaryFlujosEntityByNameAndNameFn($programa->getEtapas());
      $requisitosByIOrNameMap = $this->dictonaryByNameFn($requisitos);
      $requisitosEntityByIdMap = $this->dictonaryRequisitosEntityByNameFn($programa->getProgramaRequisitos());
      $usersMap = $this->dictonaryByIdUsuarioFn($users);
      $usersEntityMap = $this->dictonaryEntityByIdUsuarioFn($programa->getUsuarioConfiguracionProgramas());
      $asignacionMap = $this->dictonaryAsignacionByIdUsuarioFn($users);
      $asignacionEntityMap = $this->dictonaryAsignacionEntityByIdUsuarioFn($programa->getUsuarioConfiguracionProgramas());

      foreach ($etapasMap as $k => $v) {
        if (isset($etapasEntityByNameMap[$k])) {
          $eEntity = $etapasEntityByNameMap[$k];
          $this->handleEtapaExistence($eEntity, $v, $request);
        } else {
          $this->handleEtapaCreation($v, $request, $etapasEntityByNameMap);
        }
      }

      foreach ($flujosMap as $k => $v) {
        foreach ($v as $_k => $_v) {
          if (isset($flujosEntityMap[$k][$_k])) {
            $eEntity = $flujosEntityMap[$k][$_k];
            $this->handleFlujoExistence($eEntity, $request);
          } else {
            $this->handleFlujoCreation($k, $_k, $request, $etapasEntityByNameMap, $flujosEntityMap);
          }
        }
      }

      foreach ($requisitosByIOrNameMap as $k => $v) {
        if (isset($requisitosEntityByIdMap[$k])) {
          $eEntity = $requisitosEntityByIdMap[$k];
          $this->handleRequisitoExistence($eEntity, $v, $etapasEntityByNameMap, $request);
        } else {
          $this->handleRequisitoCreation($v, $programa, $request, $etapasEntityByNameMap, $requisitosEntityByIdMap);
        }
      }

      foreach ($asignacionMap as $k => $v) {
        foreach ($v as $_k) {
          if (isset($asignacionEntityMap[$k][$_k])) {
            $eEntity = $asignacionEntityMap[$k][$_k];
            $this->handleAsignacionExistence($eEntity, $k, $_k, $request, $asignacionEntityMap);
          } else {
            $this->handleAsignacionCreation($k, $_k, $request, $etapasEntityByNameMap, $asignacionEntityMap);
          }
        }
      }

      foreach ($usersMap as $k => $v) {
        if (isset($usersEntityMap[$k])) {
          $eEntity = $usersEntityMap[$k];
          $asignacionEntityList = array();
          if (isset($asignacionEntityMap[$k])) {
            $asignacionEntityList = $asignacionEntityMap[$k];
          }
          $this->handleUserconfigExistence($eEntity, $asignacionEntityList, $programa, $usersEntityMap, $request);
        } else {
          $asignacionEntityList = array();
          if (isset($asignacionEntityMap[$k])) {
            $asignacionEntityList = $asignacionEntityMap[$k];
          }
          $this->handleUserconfigCreation($v, $programa, $asignacionEntityList, $usersEntityMap, $request);
        }
      }

      $collectionClassName = EtapaTableMap::getTableMap()->getCollectionClassName();
      $collection = new $collectionClassName;
      foreach ($etapasEntityByNameMap as $k => $v) $collection->append($v);
      $programa->setEtapas($collection);

      $collection2ClassName = UsuarioConfiguracionProgramaTableMap::getTableMap()->getCollectionClassName();
      $collection2 = new $collection2ClassName;
      foreach ($usersEntityMap as $k => $v){
        // foreach($v->getUsuarioConfiguracionEtapas() as $xx){
        //   echo $k.' '.$xx->getActivo().' '.$v->getActivo();
        // }
        $collection2->append($v);
      }
      $programa->setUsuarioConfiguracionProgramas($collection2);

      $programa->save();
      $con->commit();
    } catch (Exception $e) {
      $con->rollback();
      throw $e;
    }
    $response->getBody()->write(json_encode(["updated" => 1, "clave" => $programa->getClave()]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  private function handleEtapaExistence(Etapa &$e, array &$etapa_json, ServerRequestInterface $request)
  {
    $e->setActivo(1)
      ->setSiglas($etapa_json['Siglas'])
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
  }

  private function handleEtapaCreation(array &$etapa_json, ServerRequestInterface $request, array &$etapasEntityByNameMap)
  {
    $e = new Etapa();
    $e->setActivo(1)
      ->setNombre($etapa_json['Nombre'])
      ->setSiglas($etapa_json['Siglas'])
      ->setFechaModificacion(new DateTime('NOW'))
      ->setFechaCreacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
    $etapasEntityByNameMap[$etapa_json['Nombre']] = $e;
  }

  private function handleFlujoExistence(FlujoEtapa &$e, ServerRequestInterface $request)
  {
    $e->setActivo(1)
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
  }

  private function handleFlujoCreation($k, $_k, ServerRequestInterface $request, array &$etapasEntityByNameMap, array &$flujosEntityMap)
  {
    $e = new FlujoEtapa();
    $e->setActivo(1)
      ->setFechaModificacion(new DateTime('NOW'))
      ->setFechaCreacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
    $flujosEntityMap[$k][$_k] = $e;
    $etapasEntityByNameMap[$k]->addFlujoEtapaRelatedByIdEtapa($e);
    $etapasEntityByNameMap[$_k]->addFlujoEtapaRelatedByIdEtapaSiguiente($e);
  }

  private function handleRequisitoExistence(ProgramaRequisito &$r, array &$requisito_json, array &$etapasEntityByNameMap, ServerRequestInterface $request)
  {
    $r->setActivo(1)
      ->setEtapa($etapasEntityByNameMap[$requisito_json['Etapa']['Nombre']])
      ->setDescripcion($requisito_json['Descripcion'])
      ->setIdTipoRequisito($requisito_json['IdTipoRequisito'])
      ->setObligatorio($requisito_json['Obligatorio'])
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
    $etapasEntityByNameMap[$requisito_json['Etapa']['Nombre']]->addProgramaRequisito($r);
  }

  private function handleRequisitoCreation(array $reqJson, Programa &$programa, ServerRequestInterface $request, array &$etapasEntityByNameMap, array &$requisitosEntityByIdMap)
  {
    $newReq = new ProgramaRequisito();
    $newReq->setActivo(1)
      ->setNombre($reqJson['Nombre'])
      ->setIdPrograma($programa->getClave())
      ->setEtapa($etapasEntityByNameMap[$reqJson['Etapa']['Nombre']])
      ->setDescripcion($reqJson['Descripcion'])
      ->setObligatorio($reqJson['Obligatorio'])
      ->setIdTipoRequisito($reqJson['IdTipoRequisito'])
      ->setFechaCreacion(new DateTime('NOW'))
      ->setFechaModificacion(new DateTime('NOW'))
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));

    $requisitosEntityByIdMap[$reqJson['Nombre']] =  $newReq;
    $etapasEntityByNameMap[$reqJson['Etapa']['Nombre']]->addProgramaRequisito($newReq);
  }

  private function handleUserconfigExistence(UsuarioConfiguracionPrograma &$r, array &$asignacionEntityList,  Programa &$programa, array &$usersEntityByIdMap, ServerRequestInterface $request)
  {
    $r->setActivo(1)
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaModificacion(new DateTime('NOW'));

    $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();
    $collection = new $collectionClassName;
    foreach ($asignacionEntityList as $k => &$v){
      $v->setUsuarioConfiguracionPrograma($r);
      $collection->append($v);
    }
    $r->setUsuarioConfiguracionEtapas($collection);
    $usersEntityByIdMap[$r->getIdUsuario()] = $r;
  }

  private function handleUserconfigCreation(array &$uceJson, Programa &$programa, array &$asignacionEntityList, array &$usersEntityByIdMap, ServerRequestInterface $request)
  {
    $r = new UsuarioConfiguracionPrograma();
    $r->setActivo(1)
      ->setIdUsuario($uceJson['IdUsuario'])
      ->setIdPrograma($programa->getClave())
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaCreacion(new DateTime('NOW'))
      ->setFechaModificacion(new DateTime('NOW'));

    $collectionClassName = UsuarioConfiguracionEtapaTableMap::getTableMap()->getCollectionClassName();
    $collection = new $collectionClassName;
    foreach ($asignacionEntityList as $k => $v){
      $v->setUsuarioConfiguracionPrograma($r);
      $collection->append($v);
    }
    $r->setUsuarioConfiguracionEtapas($collection);

    $usersEntityByIdMap[$uceJson['IdUsuario']] = $r;
  }

  private function handleAsignacionExistence(UsuarioConfiguracionEtapa &$r, string $k, string $_k, ServerRequestInterface $request, array &$asignacionEntityMap)
  {
    $r->setActivo(1)
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaModificacion(new DateTime('NOW'));
    $asignacionEntityMap[$k][$_k] = $r;
  }

  private function handleAsignacionCreation(string $k, string $_k, ServerRequestInterface $request, array &$etapasEntityByNameMap, array &$asignacionEntityMap)
  {
    $r = new UsuarioConfiguracionEtapa();
    $r->setActivo(1)
      ->setEtapa($etapasEntityByNameMap[$_k])
      // ->setUsuarioConfiguracionPrograma() this one is added in next iteration
      ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
      ->setFechaCreacion(new DateTime('NOW'))
      ->setFechaModificacion(new DateTime('NOW'));
    $asignacionEntityMap[$k][$_k] = $r;
  }

  public function del(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
  {
    $p = ProgramaQuery::create()->findOneByClave($args['clavePrograma']);
    if (!$p) {
      return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
    }
    // $usuario->delete()
    $p->setActivo(0)->setIdUsuarioModificacion($request->getAttribute('claveUsuario'));
    $p->save();

    $response->getBody()->write(json_encode(["deleted" => true]));
    return $response->withHeader('Content-Type', 'application/json');
  }

  private function wrapLike(String $word): String
  {
    return '%' . $word . '%';
  }

  private function dictonaryByIdUsuarioFn($array)
  {
    $result = array();
    foreach ($array as $e) {
      $id = $e['IdUsuario'];
      $result[$id] = $e;
    }
    return $result;
  }

  private function dictonaryEntityByIdUsuarioFn($userEntityCollection)
  {
    $result = array();
    foreach ($userEntityCollection as &$e) {
      $id = $e->getIdUsuario();
      $e->setActivo(0);
      $result[$id] = $e;
    }
    return $result;
  }

  private function dictonaryAsignacionByIdUsuarioFn($array)
  {
    $result = array();
    foreach ($array as $e) {
      $id = $e['IdUsuario'];
      $asignacion = $e['UsuarioConfiguracionEtapas'];
      $aux = array();
      foreach ($asignacion as $k => $val) {
        if ($val) {
          array_push($aux, $k);
        }
      }
      $result[$id] = $aux;
    }
    return $result;
  }

  private function dictonaryAsignacionEntityByIdUsuarioFn($userEntityCollection)
  {
    $result = array();
    foreach ($userEntityCollection as &$e) {
      $id = $e->getIdUsuario();
      $e->setActivo(0);
      $innerMap = array();
      $collection = $e->getUsuarioConfiguracionEtapas();
      foreach ($collection as &$c) {
        $_e = $c->getEtapa();
        $c->setActivo(0);
        $innerMap[$_e->getNombre()] = $c;
      }
      $result[$id] = $innerMap;
    }
    return $result;
  }

  private function dictonaryEntityByNameFn($etapasEntityCollection)
  {
    $result = array();
    foreach ($etapasEntityCollection as &$e) {
      $id = $e->getNombre();
      $e->initFlujoEtapasRelatedByIdEtapa();
      $e->initProgramaRequisitos();
      $e->setActivo(0);
      $result[$id] = $e;
    }
    return $result;
  }

  private function dictonaryFlujosEntityByNameAndNameFn($etapasEntityCollection)
  {
    $result = array();
    foreach ($etapasEntityCollection as &$e) {
      $id = $e->getNombre();
      $innerMap = array();
      $collection = $e->getFlujoEtapasRelatedByIdEtapa();
      foreach ($collection as &$c) {
        $_e = $c->getEtapaRelatedByIdEtapaSiguiente();
        $c->setActivo(0);
        $innerMap[$_e->getNombre()] = $c;
      }
      $result[$id] = $innerMap;
    }
    return $result;
  }

  private function dictonaryRequisitosEntityByNameFn($requisitosEntityCollection)
  {
    $result = array();
    foreach ($requisitosEntityCollection as &$e) {
      $id = $e->getNombre();
      $e->setActivo(0);
      $result[$id] = $e;
    }
    return $result;
  }

  private function dictonaryByNameFn($array)
  {
    $result = array();
    foreach ($array as $row) {
      $id = $row['Nombre'];
      $result[$id] = $row;
    }
    return $result;
  }

  private function dictonaryOfEtapasSiguientesByNameFn($array)
  {
    $result = array();
    foreach ($array as $row) {
      $id = $row['Nombre'];
      $aux = array();
      if ($row['EtapasSiguientes']) {
        foreach ($row['EtapasSiguientes'] as $es) {
          $aux[$es] = $es;
        }
      }
      $result[$id] = $aux;
    }
    return $result;
  }

}
