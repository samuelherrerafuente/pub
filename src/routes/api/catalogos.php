<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Routing\RouteCollectorProxy;

class CatalogosHandler
{
    public function routes(RouteCollectorProxy $group)
    {
        $group->get('', self::class . ':index')->addMiddleware(new AuthUtils(null, true));
        $group->get('/entidades-operativas', self::class . ':getEntidades')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipos-beneficiario', self::class . ':getTipoBeneficiario')->addMiddleware(new AuthUtils(null, true));
        $group->get('/permisos', self::class . ':getPermisos')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipos-archivo', self::class . ':getTipoArchivo')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipo-beneficio', self::class . ':getTipoBeneficio')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipo-recurso', self::class . ':getTipoRecurso')->addMiddleware(new AuthUtils(null, true));
        $group->get('/estatus-programa', self::class . ':getEstatusPrograma')->addMiddleware(new AuthUtils(null, true));
        $group->get('/unidad-medida', self::class . ':getUnidadMedida')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipo-distribucion', self::class . ':getTipoDistribucion')->addMiddleware(new AuthUtils(null, true));
        $group->get('/tipo-requisito', self::class . ':getTipoRequisito')->addMiddleware(new AuthUtils(null, true));
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('ok');
        return $response;
    }

    public function getEntidades(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $ent = EntidadOperativaQuery::create()
            ->filterByActivo(array(1))
            ->find();
        $response->getBody()->write($ent->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoBeneficiario(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $tb = TipoBeneficiarioQuery::create()
            ->find();
        $response->getBody()->write($tb->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getPermisos(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $permisos = PermisosQuery::create()->find();
        $response->getBody()->write($permisos->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoArchivo(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = TipoArchivoQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoBeneficio(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = TipoBeneficioQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoRecurso(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = TipoRecursoQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getEstatusPrograma(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = EstatusProgramaQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getUnidadMedida(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = UnidadMedidaQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoDistribucion(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = TipoDistribucionQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function getTipoRequisito(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $x = TipoRequisitoQuery::create()->find();
        $response->getBody()->write($x->toJSON());
        return $response->withHeader('Content-Type', 'application/json');
    }
}
