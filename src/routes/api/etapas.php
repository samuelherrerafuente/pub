<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Routing\RouteCollectorProxy;

class EtapasHandler
{
    public function routes(RouteCollectorProxy $group)
    {
        $group->get('/', self::class . ':getAll');
        $group->get('', self::class . ':index');
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('ok');
        return $response;
    }
    public function getAll(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('ok');
        return $response;
    }
}
