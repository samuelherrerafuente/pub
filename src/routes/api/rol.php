<?php

use Map\RolTableMap;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Routing\RouteCollectorProxy;

class RolHandler
{
    public function routes(RouteCollectorProxy $group)
    {
        $group->get('', self::class . ':index')->addMiddleware(new AuthUtils("administracion.roles.ver"));
        $group->get('/', self::class . ':getAll')->addMiddleware(new AuthUtils("administracion.roles.ver"));
        $group->post('/search', self::class . ':search')->addMiddleware(new AuthUtils("administracion.roles.ver"));
        $group->delete('/{clave}', self::class . ':delRol')->addMiddleware(new AuthUtils("administracion.roles.desactivar"));
        $group->post('/', self::class . ':newRol')->addMiddleware(new AuthUtils("administracion.roles.nuevo"));
        $group->put('/{clave}', self::class . ':editRol')->addMiddleware(new AuthUtils("administracion.roles.editar"));
    }

    public function search(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $query = RolQuery::create();
        $conditions = array();
        $filters = array();
        if (!empty($body['Clave'])) {
            $query->condition('c1', 'Rol.Clave = ?', $body['Clave']);
            array_push($conditions, 'c1');
        }
        if (!empty($body['Nombre'])) {
            $query->condition('c2', 'Rol.Nombre like ?', $this->wrapLike($body['Nombre']));
            array_push($conditions, 'c2');
        }
        if (!empty($body['Descripcion'])) {
            $query->condition('c3', 'Rol.Descripcion LIKE ?', $this->wrapLike($body['Descripcion']));
            array_push($conditions, 'c3');
        }
        if (count($conditions) > 0) {
            $query->combine($conditions, 'or', 'mainFields');
            array_push($filters, 'mainFields');
        }
        if (!empty($body['Activo']) || (isset($body['Activo']) && $body['Activo'] == '0')) {
            $query->condition('actv', 'Rol.Activo = ?', $body['Activo']);
        } else {
            $query->condition('actv', 'Rol.Activo = ?', '1');
        }
        array_push($filters, 'actv');

        if (!empty($body['FechaInicio'])) {
            $query->condition('fi', 'Rol.FechaCreacion > ?', $body['FechaInicio']);
            array_push($filters, 'fi');
        }

        if (!empty($body['FechaFin'])) {
            $query->condition('ff', 'Rol.FechaCreacion < ?', $body['FechaFin']);
            array_push($filters, 'ff');
        }

        if (count($filters) > 1) {
            $query->where($filters, 'and');
        } else {
            $query->where($filters);
        }

        if (!empty($request->getAttribute('sort'))) {
            $query = $query->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
        }
        $query = $query->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));

        $query->populateRelation('RolPermiso');
        $query->populateRelation('RolUsuario');

        $response->getBody()->write($query->toJSON());
        return $response->withHeader('Content-Type', 'application/json')
            ->withHeader('pagination-total', $query->getNbResults());
    }


    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('ok');
        return $response;
    }

    public function getAll(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $roles = RolQuery::create();
        if (!empty($request->getAttribute('sort'))) {
            $roles = $roles->orderBy($request->getAttribute('sort'), $request->getAttribute('order'));
        }
        $roles = $roles->paginate($request->getAttribute('offset'), $request->getAttribute('limit'));

        $roles->populateRelation('RolPermiso');
        $roles->populateRelation('RolUsuario');

        $response->getBody()->write($roles->toJSON());
        return $response->withHeader('Content-Type', 'application/json')
            ->withHeader('pagination-total', $roles->getNbResults());
    }

    public function delRol(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $rol = RolQuery::create()->findOneByClave($args['clave']);
        if (!$rol) {
            return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
        }
        $rol->setActivo(0)
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));
        $rol->save();

        $response->getBody()->write(json_encode(["deleted" => true]));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function newRol(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $rol = new Rol();
        $rol->setNombre($body['Nombre'])
            ->setDescripcion(isset($body['Descripcion']) ? $body['Descripcion'] : "")
            ->setActivo(1)
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaCreacion(new DateTime('NOW'))
            ->setFechaModificacion(new DateTime('NOW'));
        if ($body['RolPermisos']) {
            foreach ($body['RolPermisos'] as $value) {
                $rp = new RolPermiso();
                $rp->setActivo(1)
                    ->setIdPermiso($value)
                    ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                    ->setFechaCreacion(new DateTime('NOW'))
                    ->setFechaModificacion(new DateTime('NOW'));
                $rol->addRolPermiso($rp);
            }
        }
        if ($body['RolUsuarios']) {
            foreach ($body['RolUsuarios'] as $value) {
                $ru = new RolUsuario();
                $ru->setActivo(1)
                    ->setIdUsuario($value)
                    ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                    ->setFechaCreacion(new DateTime('NOW'))
                    ->setFechaModificacion(new DateTime('NOW'));
                $rol->addRolUsuario($ru);
            }
        }
        $response->getBody()->write(json_encode(["created" => $rol->save(), "clave" => $rol->getClave()]));
        return $response->withHeader('Content-Type', 'application/json');
    }

    public function editRol(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $body = $request->getParsedBody();
        $rol = RolQuery::create()->findOneByClave($args['clave']);
        if (!$rol) {
            return $response->withHeader('Content-Type', 'application/json')->withStatus(404);
        }
        $rol->setNombre($body['Nombre'])
            ->setDescripcion(isset($body['Descripcion']) ? $body['Descripcion'] : "")
            ->setActivo($body['Activo'])
            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
            ->setFechaModificacion(new DateTime('NOW'));

        $permisos = $rol->getRolPermisos();
        if (isset($body['RolPermisos'])) {
            $pFromBody = $body['RolPermisos'];
            $existent = array();
            foreach ($permisos as $p) {
                if (in_array($p->getIdPermiso(), $pFromBody)) {
                    array_push($existent, $p->getIdPermiso());
                    if ($p->getActivo() != 1) {
                        $p->setActivo(1)
                            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                            ->setFechaModificacion(new DateTime('NOW'));
                    }
                } else {
                    $p->setActivo(0)
                        ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                        ->setFechaModificacion(new DateTime('NOW'));
                }
            }
            foreach ($pFromBody as $value) {
                if (isset($value) && !in_array($value, $existent)) {
                    $rp = new RolPermiso();
                    $rp->setActivo(1)
                        ->setIdPermiso($value)
                        ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                        ->setFechaCreacion(new DateTime('NOW'))
                        ->setFechaModificacion(new DateTime('NOW'));
                    $rol->addRolPermiso($rp);
                }
            }
        }

        $usuarios = $rol->getRolUsuarios();
        if (isset($body['RolUsuarios'])) {
            $pFromBody = $body['RolUsuarios'];
            $existent = array();
            foreach ($usuarios as $p) {
                if (in_array($p->getIdUsuario(), $pFromBody)) {
                    array_push($existent, $p->getIdUsuario());
                    if ($p->getActivo() != 1) {
                        $p->setActivo(1)
                            ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                            ->setFechaModificacion(new DateTime('NOW'));
                    }
                } else {
                    $p->setActivo(0)
                        ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                        ->setFechaModificacion(new DateTime('NOW'));
                }
            }
            foreach ($pFromBody as $value) {
                if (isset($value) && !in_array($value, $existent)) {
                    $ru = new RolUsuario();
                    $ru->setActivo(1)
                        ->setIdUsuario($value)
                        ->setIdUsuarioModificacion($request->getAttribute('claveUsuario'))
                        ->setFechaCreacion(new DateTime('NOW'))
                        ->setFechaModificacion(new DateTime('NOW'));
                    $rol->addRolUsuario($ru);
                }
            }
        }

        $response->getBody()->write(json_encode(["updated" => $rol->save()]));
        return $response->withHeader('Content-Type', 'application/json');
    }

    private function wrapLike(String $word): String
    {
        return '%' . $word . '%';
    }
}
