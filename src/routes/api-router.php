<?php

use Slim\Routing\RouteCollectorProxy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Dflydev\FigCookies\FigResponseCookies;
use Dflydev\FigCookies\SetCookie;
use Dflydev\FigCookies\Modifier\SameSite;

class ApiRouteHandler
{
    public function routes(RouteCollectorProxy $group)
    {
        $group->group('/healthcheck', \HealthCheck::class . ':routes');
        $group->group('/user', \UserHandler::class . ':routes');
        $group->group('/rol', \RolHandler::class . ':routes');
        $group->group('/programa', \ProgramaHandler::class . ':routes');
        $group->group('/catalogo', \CatalogosHandler::class . ':routes');
        $group->get('/logout', self::class . ':logout');
        $group->get('', self::class . ':index');
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response->getBody()->write('api ok');
        return $response;
    }

    public function logout(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $response = FigResponseCookies::expire($response, 'AUTH-TOKEN');
        $response = FigResponseCookies::set(
            $response,
            SetCookie::create('AUTH-TOKEN')
                ->withValue('-1')
                ->withPath('/')
                // ->withSecure(true)  //habiltar cuando el servidor trabaje sobre https
                ->withHttpOnly(true)
                ->withSameSite(SameSite::lax())
                ->withExpires('Tue, 15-Jan-2000 21:47:38 GMT')
        );
        return $response->withStatus(200);
    }
}
