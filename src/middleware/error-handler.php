<?php

use Slim\Interfaces\ErrorRendererInterface;

class MyCustomErrorRenderer implements ErrorRendererInterface
{
    public function __invoke(\Throwable $exception, bool $displayErrorDetails): string
    {
        error_log($exception);
        if($displayErrorDetails){
            return '{"mensaje":"Ocurrio un error mientras se procesaba la petición: <br>' . $exception . '"}';
        }
        return '{"mensaje":"Ocurrio un error mientras se procesaba la petición. Contactar a soporte.' . '"}';
    }
}