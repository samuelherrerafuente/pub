<?php

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Psr7\Response;
use Dflydev\FigCookies\FigRequestCookies;
use Slim\Routing\RouteContext;

class AuthUtils implements MiddlewareInterface
{
  /**
   * Clave de permiso a ser usado
   * @var String
   */
  protected $clavePermiso = null;

  /**
   * Clave de permiso a ser usado
   * @var String
   */
  protected $bypassSelf = false;

  /**
   * Initializes internal state of Base\Rol object.
   * @see applyDefaults()
   */
  public function __construct($cve, $bs = false)
  {
    $this->clavePermiso = $cve;
    $this->bypassSelf = $bs;
  }

  public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
  {
    $imposible = $this->isThisImposible($request);
    if (!isset($imposible)) {
      $response = new Response();
      return $response->withStatus(403);
    }
    $start_date = strtotime($imposible->getFechaLogin());
    $end_date = time();
    if (($end_date - $start_date) > (1000 * 60 * 60 * 24)) {
      $response = new Response();
      return $response->withStatus(401);
    }
    $qParams = $request->getQueryParams();
    $request = $request->withAttribute('claveUsuario', $imposible->getIdUsuario());
    $request = $request->withAttribute('limit', !empty($qParams['limit']) ? $qParams['limit'] : 999);
    $request = $request->withAttribute('offset', !empty($qParams['offset']) ? ($qParams['offset'] + 1) : 1);
    $request = $request->withAttribute('order', !empty($qParams['order']) ? $qParams['order'] : null);
    $request = $request->withAttribute('sort', !empty($qParams['sort']) ? $qParams['sort'] : null);
    return $handler->handle($request);
  }

  private function isThisImposible(ServerRequestInterface $request)
  {
    $routeContext = RouteContext::fromRequest($request);
    $route = $routeContext->getRoute();
    $clave = $route->getArgument('clave');
    $authToken = FigRequestCookies::get($request, 'AUTH-TOKEN');
    if (isset($authToken)) {
      $login = LoginQuery::create()->useUsuarioQuery()->filterByActivo(1)->endUse()->findByToken($authToken->getValue());
      $login = $login->getLast();
      if (isset($login)) {
        $request = $request->withAttribute('claveUsuario', $login->getIdUsuario());
        // $usr = UsuarioQuery::create()->findOneByClave($login->getIdUsuario());
        // if (isset($usr) && $usr->getVigencia() == "1") {
        //     $s_date = strtotime($usr->);
        //     $e_date = time();
        //     if (($e_date - $s_date) < 2) {
        //         $response = new Response();
        //         return $response->withStatus(401);
        //     }
        // }
        if ($this->bypassSelf && (!isset($clave) || $login->getIdUsuario() == $clave)) {
          return $login;
        }
        if (isset($this->clavePermiso)) {
          $programaRules = false;
          $progCond = array();
          $programaRules = UsuarioConfiguracionProgramaQuery::create();
          $programaRules->condition('c0', 'UsuarioConfiguracionPrograma.IdPrograma = ?', $route->getArgument('clavePrograma'));
          if ((is_array($this->clavePermiso) ? in_array('PPermisos', $this->clavePermiso) : ($this->clavePermiso == 'PPermisos'))) {
            $programaRules->condition('c1', 'UsuarioConfiguracionPrograma.PPermisos = ?', '1');
            array_push($progCond, 'c1');
          }
          if ((is_array($this->clavePermiso) ? in_array('PConsulta', $this->clavePermiso) : ($this->clavePermiso == 'PConsulta'))) {
            $programaRules->condition('c2', 'UsuarioConfiguracionPrograma.PConsulta = ?', '1');
            array_push($progCond, 'c2');
          }
          if ((is_array($this->clavePermiso) ? in_array('PEdicion', $this->clavePermiso) : ($this->clavePermiso == 'PEdicion'))) {
            $programaRules->condition('c3', 'UsuarioConfiguracionPrograma.PEdicion = ?', '1');
            array_push($progCond, 'c3');
          }
          if ((is_array($this->clavePermiso) ? in_array('PEstatus', $this->clavePermiso) : ($this->clavePermiso == 'PEstatus'))) {
            $programaRules->condition('c4', 'UsuarioConfiguracionPrograma.PEstatus = ?', '1');
            array_push($progCond, 'c4');
          }
          if ((is_array($this->clavePermiso) ? in_array('PDesactivar', $this->clavePermiso) : ($this->clavePermiso == 'PDesactivar'))) {
            $programaRules->condition('c5', 'UsuarioConfiguracionPrograma.PDesactivar = ?', '1');
            array_push($progCond, 'c5');
          }
          if (count($progCond) > 0) {
            $programaRules->combine($progCond, 'or', 'grants');
            $programaRules->where(['c0', 'grants'], 'and');
          }
          $qryAsigbyRol = RolUsuarioQuery::create()
            ->useRolQuery()
            ->useRolPermisoQuery()
            ->usePermisosQuery()
            ->filterByActivo(1)
            ->filterBySiglas($this->clavePermiso)
            ->endUse()
            ->filterByActivo(1)
            ->endUse()
            ->filterByActivo(1)
            ->endUse()
            ->filterByActivo(1)
            ->filterByIdUsuario($login->getIdUsuario())
            ->findOne();
          $qryAsigDirect =
            UsuarioPermisoQuery::create()
            ->usePermisosQuery()
            ->filterByActivo(1)
            ->filterBySiglas($this->clavePermiso)
            ->endUse()
            ->filterByActivo(1)
            ->filterByIdUsuario($login->getIdUsuario())
            ->findOne();
          if (isset($qryAsigDirect) || isset($qryAsigbyRol) || !!$programaRules) {
            return $login;
          }
        }
      }
    }
    return null;
  }
}
