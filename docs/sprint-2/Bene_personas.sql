-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: bene2
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beneficio`
--

DROP TABLE IF EXISTS `beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beneficio` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_tipo_distribucion` int NOT NULL,
  `id_tipo_beneficio` int NOT NULL,
  `id_tipo_recurso` int NOT NULL,
  `id_unidad_medida` int NOT NULL,
  `valor` decimal(65,2) NOT NULL,
  `cantidad` int DEFAULT NULL,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `id_programa` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_beneficio_1_idx` (`id_tipo_distribucion`),
  KEY `fk_beneficio_2_idx` (`id_tipo_beneficio`),
  KEY `fk_beneficio_3_idx` (`id_tipo_recurso`),
  KEY `fk_beneficio_4_idx` (`id_unidad_medida`),
  KEY `fk_beneficio_5_idx` (`id_programa`),
  KEY `fk_beneficio_6_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_beneficio_1` FOREIGN KEY (`id_tipo_distribucion`) REFERENCES `tipo_distribucion` (`clave`),
  CONSTRAINT `fk_beneficio_2` FOREIGN KEY (`id_tipo_beneficio`) REFERENCES `tipo_beneficio` (`clave`),
  CONSTRAINT `fk_beneficio_3` FOREIGN KEY (`id_tipo_recurso`) REFERENCES `tipo_recurso` (`clave`),
  CONSTRAINT `fk_beneficio_4` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_medida` (`clave`),
  CONSTRAINT `fk_beneficio_5` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`clave`),
  CONSTRAINT `fk_beneficio_6` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `entidad_operativa`
--

DROP TABLE IF EXISTS `entidad_operativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entidad_operativa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `siglas` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `activo` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estado_civil`
--

DROP TABLE IF EXISTS `estado_civil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estado_civil` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `estatus_programa`
--

DROP TABLE IF EXISTS `estatus_programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estatus_programa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `etapa`
--

DROP TABLE IF EXISTS `etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `etapa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_programa` int NOT NULL,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `siglas` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  `comprometido` int NOT NULL DEFAULT '0',
  `entregado` int NOT NULL DEFAULT '0',
  `rechazado` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`clave`),
  KEY `fk_etapa_1_idx` (`id_programa`),
  KEY `fk_etapa_2_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_etapa_1` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`clave`),
  CONSTRAINT `fk_etapa_2` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `flujo_etapa`
--

DROP TABLE IF EXISTS `flujo_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `flujo_etapa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_etapa` int NOT NULL,
  `id_etapa_siguiente` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  UNIQUE KEY `index5` (`id_etapa`,`id_etapa_siguiente`),
  KEY `fk_flujo_etapa_1_idx` (`id_etapa`),
  KEY `fk_flujo_etapa_2_idx` (`id_etapa_siguiente`),
  KEY `fk_flujo_etapa_3_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_flujo_etapa_1` FOREIGN KEY (`id_etapa`) REFERENCES `etapa` (`clave`),
  CONSTRAINT `fk_flujo_etapa_2` FOREIGN KEY (`id_etapa_siguiente`) REFERENCES `etapa` (`clave`),
  CONSTRAINT `fk_flujo_etapa_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `login` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_usuario` int NOT NULL,
  `token` varchar(256) COLLATE utf8_bin NOT NULL,
  `fecha_login` datetime NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_login_1_idx` (`id_usuario`),
  CONSTRAINT `fk_login_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permisos`
--

DROP TABLE IF EXISTS `permisos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permisos` (
  `clave` int NOT NULL,
  `seccion` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `siglas` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`clave`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `clave` int NOT NULL,
  `id_tipo_persona` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_persona_1_idx` (`id_tipo_persona`),
  CONSTRAINT `fk_persona_1` FOREIGN KEY (`id_tipo_persona`) REFERENCES `tipo_persona` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_extranjera`
--

DROP TABLE IF EXISTS `persona_extranjera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona_extranjera` (
  `clave` int NOT NULL,
  `nombre` varchar(1000) NOT NULL,
  `apellido_paterno` varchar(1000) NOT NULL,
  `apellido_materno` varchar(45) DEFAULT NULL,
  `genero` varchar(1) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `correo_electronico` varchar(1000) DEFAULT NULL,
  `clave_identidad` varchar(20) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  `id_estado_civil` int DEFAULT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_persona_extranjera_2_idx` (`id_usuario_modificacion`),
  KEY `fk_persona_extranjera_3_idx` (`id_estado_civil`),
  CONSTRAINT `fk_persona_extranjera_1` FOREIGN KEY (`clave`) REFERENCES `persona` (`clave`),
  CONSTRAINT `fk_persona_extranjera_2` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_persona_extranjera_3` FOREIGN KEY (`id_estado_civil`) REFERENCES `estado_civil` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_fisica`
--

DROP TABLE IF EXISTS `persona_fisica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona_fisica` (
  `clave` int NOT NULL,
  `nombre` varchar(1000) NOT NULL,
  `apellido_paterno` varchar(1000) NOT NULL,
  `apellido_materno` varchar(1000) NOT NULL,
  `id_estado_civil` int DEFAULT NULL,
  `genero` varchar(1) NOT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `correo_electronico` varchar(1000) DEFAULT NULL,
  `curp` varchar(45) DEFAULT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_persona_fisica_1_idx` (`id_estado_civil`),
  KEY `fk_persona_fisica_2_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_persona_fisica_1` FOREIGN KEY (`id_estado_civil`) REFERENCES `estado_civil` (`clave`),
  CONSTRAINT `fk_persona_fisica_2` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_persona_fisica_3` FOREIGN KEY (`clave`) REFERENCES `persona` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `persona_moral`
--

DROP TABLE IF EXISTS `persona_moral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona_moral` (
  `clave` int NOT NULL,
  `nombre` varchar(1000) DEFAULT NULL,
  `telefono` varchar(100) DEFAULT NULL,
  `correo_electronico` varchar(1000) DEFAULT NULL,
  `rfc` varchar(45) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_persona_moral_2_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_persona_moral_1` FOREIGN KEY (`clave`) REFERENCES `persona` (`clave`),
  CONSTRAINT `fk_persona_moral_2` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programa`
--

DROP TABLE IF EXISTS `programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `programa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_fin` datetime DEFAULT NULL,
  `id_entidad_operativa` int NOT NULL,
  `id_estatus_programa` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  `id_tipo_bebeficiario` int DEFAULT NULL,
  `persona_moral` int DEFAULT NULL,
  `digrama_yucatan` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`clave`),
  KEY `fk_programa_1_idx` (`id_entidad_operativa`),
  KEY `fk_programa_2_idx` (`id_estatus_programa`),
  KEY `fk_programa_3_idx` (`id_usuario_modificacion`),
  KEY `fk_programa_4_idx` (`id_tipo_bebeficiario`),
  CONSTRAINT `fk_programa_1` FOREIGN KEY (`id_entidad_operativa`) REFERENCES `entidad_operativa` (`clave`),
  CONSTRAINT `fk_programa_2` FOREIGN KEY (`id_estatus_programa`) REFERENCES `estatus_programa` (`clave`),
  CONSTRAINT `fk_programa_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_programa_4` FOREIGN KEY (`id_tipo_bebeficiario`) REFERENCES `tipo_beneficiario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `programa_requisito`
--

DROP TABLE IF EXISTS `programa_requisito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `programa_requisito` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  `id_programa` int NOT NULL,
  `id_etapa` int NOT NULL,
  `id_tipo_requisito` int NOT NULL,
  `obligatorio` int NOT NULL DEFAULT '0',
  `activo` int NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_programa_requisito_programa_idx` (`id_programa`),
  KEY `fk_programa_requisito_etapa1_idx` (`id_etapa`),
  KEY `fk_programa_requisito_tipo_requisito1_idx` (`id_tipo_requisito`),
  KEY `fk_programa_requisito_usuario1_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_programa_requisito_etapa1` FOREIGN KEY (`id_etapa`) REFERENCES `etapa` (`clave`),
  CONSTRAINT `fk_programa_requisito_programa` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`clave`),
  CONSTRAINT `fk_programa_requisito_tipo_requisito1` FOREIGN KEY (`id_tipo_requisito`) REFERENCES `tipo_requisito` (`clave`),
  CONSTRAINT `fk_programa_requisito_usuario1` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `restablecer_password`
--

DROP TABLE IF EXISTS `restablecer_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `restablecer_password` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_usuario` int NOT NULL,
  `fecha` datetime NOT NULL,
  `token` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `usado` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`clave`),
  KEY `fk_restablecer_password_1_idx` (`id_usuario`),
  CONSTRAINT `fk_restablecer_password_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`clave`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `id_usuario_modificacion` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  PRIMARY KEY (`clave`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`),
  KEY `fk_rol_1_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_rol_1` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol_permiso`
--

DROP TABLE IF EXISTS `rol_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol_permiso` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_rol` int NOT NULL,
  `id_permiso` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`clave`),
  KEY `fk_rol_permiso_1_idx` (`id_rol`),
  KEY `fk_rol_permiso_2_idx` (`id_permiso`),
  KEY `fk_rol_permiso_3_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_rol_permiso_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`clave`),
  CONSTRAINT `fk_rol_permiso_2` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`clave`),
  CONSTRAINT `fk_rol_permiso_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rol_usuario`
--

DROP TABLE IF EXISTS `rol_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol_usuario` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_usuario` int NOT NULL,
  `id_rol` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `fecha_modificacion` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_rol_usuario_1_idx` (`id_usuario`),
  KEY `fk_rol_usuario_3_idx` (`id_usuario_modificacion`),
  KEY `fk_rol_usuario_2` (`id_rol`),
  CONSTRAINT `fk_rol_usuario_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_rol_usuario_2` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`clave`),
  CONSTRAINT `fk_rol_usuario_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_archivo`
--

DROP TABLE IF EXISTS `tipo_archivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_archivo` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `tipo_archivocol` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_beneficiario`
--

DROP TABLE IF EXISTS `tipo_beneficiario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_beneficiario` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_beneficio`
--

DROP TABLE IF EXISTS `tipo_beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_beneficio` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_distribucion`
--

DROP TABLE IF EXISTS `tipo_distribucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_distribucion` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_persona`
--

DROP TABLE IF EXISTS `tipo_persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_persona` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_recurso`
--

DROP TABLE IF EXISTS `tipo_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_recurso` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tipo_requisito`
--

DROP TABLE IF EXISTS `tipo_requisito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_requisito` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) COLLATE utf8_bin NOT NULL,
  `mime_type` varchar(500) COLLATE utf8_bin NOT NULL,
  `siglas` varchar(45) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `unidad_medida`
--

DROP TABLE IF EXISTS `unidad_medida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `unidad_medida` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `descripcion` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `usuario` varchar(10) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `vigencia` int NOT NULL DEFAULT '0',
  `dias_vigencia` int DEFAULT NULL,
  `correo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `nombre` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `apaterno` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `amaterno` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `ultimo_acceso` datetime NOT NULL,
  `password_antiguo` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_usuario_modificacion` int DEFAULT NULL,
  `id_entidad_operativa` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_usuario_1_idx` (`id_usuario_modificacion`),
  KEY `fk_usuario_2_idx` (`id_entidad_operativa`),
  CONSTRAINT `fk_usuario_1` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_usuario_2` FOREIGN KEY (`id_entidad_operativa`) REFERENCES `entidad_operativa` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario_configuracion_etapa`
--

DROP TABLE IF EXISTS `usuario_configuracion_etapa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario_configuracion_etapa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `activo` int NOT NULL DEFAULT '1',
  `id_usuario_configuracion_programa` int NOT NULL,
  `id_etapa` int NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_usuario_configuracion_etapa_usuario_configuracion_progra_idx` (`id_usuario_configuracion_programa`),
  KEY `fk_usuario_configuracion_etapa_etapa1_idx` (`id_etapa`),
  KEY `fk_usuario_configuracion_etapa_usuario1_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_usuario_configuracion_etapa_etapa1` FOREIGN KEY (`id_etapa`) REFERENCES `etapa` (`clave`),
  CONSTRAINT `fk_usuario_configuracion_etapa_usuario1` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_usuario_configuracion_etapa_usuario_configuracion_programa1` FOREIGN KEY (`id_usuario_configuracion_programa`) REFERENCES `usuario_configuracion_programa` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario_configuracion_programa`
--

DROP TABLE IF EXISTS `usuario_configuracion_programa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario_configuracion_programa` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_programa` int NOT NULL,
  `id_usuario` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `p_permisos` int NOT NULL DEFAULT '0',
  `p_consulta` int NOT NULL DEFAULT '1',
  `p_edicion` int NOT NULL DEFAULT '0',
  `p_estatus` int NOT NULL DEFAULT '0',
  `p_desactivar` int NOT NULL DEFAULT '0',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_usuario_configuracion_programa_1_idx` (`id_programa`),
  KEY `fk_usuario_configuracion_programa_2_idx` (`id_usuario`),
  KEY `fk_usuario_configuracion_programa_3_idx` (`id_usuario_modificacion`),
  CONSTRAINT `fk_usuario_configuracion_programa_1` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`clave`),
  CONSTRAINT `fk_usuario_configuracion_programa_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_usuario_configuracion_programa_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `usuario_permiso`
--

DROP TABLE IF EXISTS `usuario_permiso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuario_permiso` (
  `clave` int NOT NULL AUTO_INCREMENT,
  `id_usuario` int NOT NULL,
  `id_permiso` int NOT NULL,
  `activo` int NOT NULL DEFAULT '1',
  `fecha_creacion` datetime NOT NULL,
  `fecha_modificacion` datetime NOT NULL,
  `id_usuario_modificacion` int NOT NULL,
  PRIMARY KEY (`clave`),
  KEY `fk_usuario_permiso_1_idx` (`id_usuario`),
  KEY `fk_usuario_permiso_3_idx` (`id_usuario_modificacion`),
  KEY `fk_usuario_permiso_2` (`id_permiso`),
  CONSTRAINT `fk_usuario_permiso_1` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`clave`),
  CONSTRAINT `fk_usuario_permiso_2` FOREIGN KEY (`id_permiso`) REFERENCES `permisos` (`clave`),
  CONSTRAINT `fk_usuario_permiso_3` FOREIGN KEY (`id_usuario_modificacion`) REFERENCES `usuario` (`clave`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-21 11:36:37
