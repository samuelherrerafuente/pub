# Notas

## Peparar entorno de desarrollo

1) Generar un nuevo esquema mysql con usuario y contraseña
2) Validar que las configuraciones en `propel.json` y `config.ini` (schema, user, pass) son correctas
3) Ejecutar `composer install` para instalar las dependencias
4) Ejecutar instrucciones en `dd.sql` para generar la bd.
5) Ejecutar instrucciones en `seed.sql` para inicializar administrador y catalogos

* Cada vez que la estructura de la BD sea actualizada se necesita correr el comando `composer run propel:update`

## Ejecutar en modo desarrollo

1) Ejecutar `composer run dev`
2) Navegar a 127.0.0.1:8888

## Configuraciones

* Las configuraciones de mail, base de datos y otros se encuentran en config.ini


## Frameworks

### Backend

* <http://propelorm.org>
* <https://www.slimframework.com>

### Frontend

* <https://getbootstrap.com>
* <https://bootstrap-table.com>
* <https://github.com/krasimir/navigo>

##  Beneficiarios-bugs detectados

###	Programas

1) Entidad operativa no se ve correctamente✅
2)	Se permite poner fecha de inicio posterior a la fecha fin  ✅
3)	El tamaño del cuadro de la descripción se mantiene el mismo al entrar de nuevo al modal ✅
4)	Faltas de ortografía en el alta y filtro de búsqueda.✅
5)	Valor y cantidad permite alfanumérico y caracteres especiales (pestaña beneficios)✅
6)	4 de las opciones de la pestaña beneficios no muestran datos. 
7)	En alta se queda guardado los datos anteriores cuando se entra de nuevo al modal✅
8)	Filtro no muestra datos una de las opciones✅
9)	Marca error la búsqueda por filtro.✅
10)	Faltas ortográficas en el DT (Las faltas ortograficas en las opciones son traidas de la BD) ⚠️
11)	Beneficios y exportar no funcionan✅
12) Al crear un nuevo programa y dar guardar, queda el formulario abierto y se puede guardar mas con los atrivutos. ✅
13) Al crear un programa con una descripcion larga, el programa no se guarda y se muestra el mensaje: El servidor no pudo completar la solicitud debido a un error ⚠️ ¡DUDAS! ⚠️ ¿Que es muy larga?
14) Al abrir un programa para editarlo, este pierde sus atrivutos de Fecha inicial, Fecha final, Entidad Operativa, tipo de distribucion, unidad de medida, tipo de beneficio, tipo de recurso, tipo de beneficiario. ✅

###	Administración

1)	El excel solo muestra una oración ✅
2)	Faltas de ortografía en búsqueda y alta, rol y usuario 
3)	Formulario de alta al activar vigencia de contraseña muestra un número pero no dice que es..✅
4)	Si se escribe mal la contraseña no dice nada hasta que le des guardar dice invalido ✅
5)	El formulario de alta de roles dice filtrar pero no esta muy claro ¿filtrar que? 
6)	Cuando no se completa el formulario de alta al no poner la dependencia el mensaje de error sale vacio ✅
7)	Los mensajes no son especificos.✅
8)	Cuando se ingresa con un usuario sin permisos puede ver opciones a las que no tiene permiso, es correcto que no las pueda usar pero tampoco las debe ver…
9)	No llega el correo de solicitud para recuperar contraseña
10)	Al ingresar por primera vez no se solicita el cambio de contraseña (historia E10108)
11) Despues de crear un rol o editarlo despues de presionar el boton guadar el formularo continua.✅
12) No hay validacion al activar o desactivar un rol. ✅
13) Al activar un rol  no muestra mensaje con el numero de usuarios asignados. ⚠️ ¡DUDAS! ⚠️ ¿A que se refiere?


### Generales
1) Al seleccionar editar y darle guarda, no se cierran correctamente los modales en las pantallas ✅
2) Al buscar algo sin respuesta no aparece el mesaje y solo se queda la animacion de carga. ⚠️ En proceso ⚠️ 
3) Los archivos solo se exporta en Excel  ⚠️
4) El archivo excel cuenta con mas atributos que los mencionados en la documentacion (Clave, Nombre, Apellidos, Usuario, Estatus, Fecha de creación, Fecha de edición y Ultimo acceso. ⚠️ ¡DUDAS! ⚠️ ¿A que se refiere?
5) Se descargará el listado de roles con los atributos: Clave, Nombre, Descripción) ✅


### Usuarios
1) Al abrir el formulario de nuevo usuario el fondo no se despliega detras de todos los componentes en firefox.  ✅
2) El formulario no muestra el boton para añadir o guardar nuevo usuario en firefox y Chrome hasta reducir la vista a un 80%.  ✅
3) Despues de llenar el formulario, seleccionar perfiles y permisos para nuevo usuario se presionar el boton guardar pero el formulario no se cierra y permite seguir guardando usuarios con los mismos valores de formunlario (los permisos y roles se tienen que sleccionar manualmente).  ✅
4) Cuando se edita un usuario la contraseña no se muestra con el numero de caracteres que deben ser y al guardar el cambio, indica que hay que colocar una caontraseña valida.✅ (aumentado a 8, agregado mensaje de requisitos minimos)
5) Al editar usuario no hay el mensaje de confirmacion para aceptar los cambios. ✅